
def includeme(config):
    config.include('.invoice')
    config.include('.cancelinvoice')
    config.include('.lists')
    config.include('.rest_api')
