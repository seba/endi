# -*- coding: utf-8 -*-
import os

from endi.views.admin import (
    AdminIndexView,
    BASE_URL,
)
from endi.views.admin.tools import BaseAdminIndexView


ACCOUNTING_URL = os.path.join(BASE_URL, 'accounting')


class AccountingIndexView(BaseAdminIndexView):
    route_name = ACCOUNTING_URL
    title = "Module Comptabilité"
    description = "Configurer les tableaux de bord (trésorerie, \
comptes de résultat) et les paramètres liés au logiciel de comptabilité."


def includeme(config):
    config.add_route(ACCOUNTING_URL, ACCOUNTING_URL)
    config.add_admin_view(AccountingIndexView, parent=AdminIndexView)
    config.include('.accounting_software')
    config.include('.treasury_measures')
    config.include('.income_statement_measures')
