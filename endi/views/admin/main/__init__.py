# -*- coding: utf-8 -*-
import logging
import os

from endi.views.admin import (
    AdminIndexView,
    BASE_URL as BASE_ROUTE,
)
from endi.views.admin.tools import BaseAdminIndexView

MAIN_ROUTE = os.path.join(BASE_ROUTE, 'main')


class MainIndexView(BaseAdminIndexView):
    route_name = MAIN_ROUTE
    title = "Configuration générale"
    description = "Configurer les informations générales (message d'accueil, \
types de fichier, e-mail de contact)"


def includeme(config):
    config.add_route(MAIN_ROUTE, MAIN_ROUTE)
    config.add_admin_view(MainIndexView, parent=AdminIndexView)
    config.include('.cae')
    config.include('.internal_companies')
    config.include(".site")
    config.include('.contact')
    config.include('.file_types')
