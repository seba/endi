# -*- coding: utf-8 -*-
"""
Configuration générale du module vente:

    Mise en forme des PDFs
    Unité de prestation
"""
import logging
import os

from endi.models.task import (
    WorkUnit,
    PaymentConditions,
)
from endi.models.payments import (
    PaymentMode,
)
from endi.forms.admin import get_config_schema

from endi.views.admin.tools import (
    get_model_admin_view,
)
from endi.views.admin.sale import (
    SaleIndexView,
    SALE_URL,
)
from endi.views.admin.tools import BaseConfigView

logger = logging.getLogger(__name__)


FORM_CONFIG_URL = os.path.join(SALE_URL, 'form')
BaseWorkUnitAdminView = get_model_admin_view(WorkUnit, r_path=SALE_URL)


class SaleFormAdminView(BaseConfigView):
    title = u"Formulaire de saisie des devis/factures"
    description = (u"Configurer les options proposés dans les formulaires de "
    u"saisie des devis/factures")
    route_name = FORM_CONFIG_URL
    validation_msg = u"Les informations ont bien été enregistrées"

    keys = (
        "sale_use_ttc_mode",
        "estimation_validity_duration_default",
    )
    schema = get_config_schema(keys)


class WorkUnitAdminView(BaseWorkUnitAdminView):
    disable = False


BasePaymentModeAdminView = get_model_admin_view(PaymentMode, r_path=SALE_URL)


class PaymentModeAdminView(BasePaymentModeAdminView):
    disable = False


PaymentConditionsAdminView = get_model_admin_view(
    PaymentConditions,
    r_path=SALE_URL,
)


def includeme(config):
    for view in (
        SaleFormAdminView,
        WorkUnitAdminView,
        PaymentModeAdminView,
        PaymentConditionsAdminView
    ):
        config.add_route(view.route_name, view.route_name)
        config.add_admin_view(view, parent=SaleIndexView)
