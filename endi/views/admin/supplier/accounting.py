# -*- coding: utf-8 -*-
import os
import logging

from endi.forms.admin import get_config_schema
from endi.views.admin.tools import BaseConfigView
from endi.views.admin.supplier import (
    SUPPLIER_URL,
    SupplierIndexView,
)

logger = logging.getLogger(__name__)
SUPPLIER_ACCOUNTING_URL = os.path.join(SUPPLIER_URL, 'accounting')

SUPPLIER_INFO_MESSAGE="""Configurez les exports comptables des factures \
et paiements fournisseur.<br/>
<h4>Variables utilisables dans les gabarits de libellés</h4>\
<p>Il est possible de personaliser les libellés comptables à l'aide d'un \
gabarit. Plusieurs variables sont disponibles :</p>\
<ul>\
    <li><code>{company.name}</code> : le nom de l'enseigne concernée</li>\
    <li><code>{supplier.label}</code> : le nom du fournisseur concerné</li>\
</ul>"""


class SupplierAccountingConfigView(BaseConfigView):
    title = "Configuration comptable du module Fournisseur"
    description = "Configurer la génération des écritures fournisseur"
    route_name = SUPPLIER_ACCOUNTING_URL

    validation_msg = "Les informations ont bien été enregistrées"
    keys = [
        'code_journal_frns', 
        'cae_general_supplier_account', 
        'cae_third_party_supplier_account',
        'bookentry_supplier_invoice_label_template',
        'bookentry_supplier_payment_label_template'
    ]
    schema = get_config_schema(keys)
    info_message = SUPPLIER_INFO_MESSAGE


def add_routes(config):
    config.add_route(SUPPLIER_ACCOUNTING_URL, SUPPLIER_ACCOUNTING_URL)


def includeme(config):
    add_routes(config)
    config.add_admin_view(
        SupplierAccountingConfigView, 
        parent=SupplierIndexView
    )
