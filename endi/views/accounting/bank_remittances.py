# -*- coding: utf-8 -*-
"""
Bank remittances views
"""
import logging
import colander

from pyramid.httpexceptions import HTTPFound
from sqla_inspect.csv import CsvExporter

from endi.utils.pdf import (
    render_html,
    write_html_as_pdf_response,
)
from endi.utils.widgets import (
    Link,
    ViewLink,
    POSTButton,
)
from endi.utils.strings import format_amount
from endi_base.utils.date import format_date
from endi.models.task.payment import (
    Payment,
    BankRemittance,
)
from endi.views import (
    BaseView,
    BaseListView,
)
from endi.views.accounting.routes import (
    BANK_REMITTANCE_ROUTE,
    BANK_REMITTANCE_ITEM_ROUTE,
)
from endi.forms.bank_remittance import (
    get_bank_remittances_list_schema,
    RemittanceDateSchema,
)
from endi.export.utils import write_file_to_request

logger = log = logging.getLogger(__name__)


class BankRemittanceListView(BaseListView):
    """
    Bank Remittances listing view
    """
    title = "Liste des remises en banque"
    schema = get_bank_remittances_list_schema()
    sort_columns = {
        "id": BankRemittance.id,
        "created_at": BankRemittance.created_at,
        "remittance_date": BankRemittance.remittance_date,
    }
    default_sort = "created_at"
    default_direction = "desc"

    def query(self):
        return BankRemittance.query()

    def filter_remittance_id(self, query, appstruct):
        search = appstruct.get('search', None)
        if search:
            query = query.filter(
                BankRemittance.id.like("%" + search + "%")
            )
        return query

    def filter_payment_mode(self, query, appstruct):
        payment_mode = appstruct.get('payment_mode')
        if payment_mode:
            query = query.filter(BankRemittance.payment_mode == payment_mode)
        return query

    def filter_bank(self, query, appstruct):
        bank = appstruct.get('bank_id')
        if bank:
            query = query.filter(BankRemittance.bank_id == bank)
        return query

    def filter_closed(self, query, appstruct):
        closed = appstruct.get('closed', True)
        if closed in (False, colander.null):
            query = query.filter_by(closed=False)
        return query


class BankRemittanceView(BaseListView):
    """
    Bank Remittance detail view
    """
    add_template_vars = ('stream_main_actions',)
    default_sort = "created_at"
    default_direction = "desc"

    @property
    def title(self):
        return "Détail de la remise en banque {0}".format(
            self.context.id
        )

    def populate_actionmenu(self, appstruct):
        self.request.actionmenu.add(
            ViewLink(
                "Liste des remises en banque",
                path="/accounting/bank_remittances",
            )
        )

    def query(self):
        query = Payment.query()
        return query.filter_by(bank_remittance_id=self.context.id)

    def stream_main_actions(self):
        if self.context.closed:
            confirm_msg = None
            if self.context.is_exported():
                confirm_msg = "Cette remise en banque a déjà été exportée en \
comptabilité.\n\nEtes vous sûr de vouloir la rouvrir ?"
            yield POSTButton(
                self.request.route_path(
                    BANK_REMITTANCE_ITEM_ROUTE,
                    id=self.context.id,
                    _query=dict(action="open")
                ),
                "Rouvrir",
                title="Rouvrir cette remise en banque",
                icon="lock-open",
                css="icon btn-primary",
                confirm=confirm_msg
            )
            yield Link(
                self.request.route_path(
                    'bank_remittance.pdf',
                    id=self.context.id
                ),
                "PDF",
                title="Editer le borderau de remise",
                icon="file-pdf",
                css="icon"
            )
            yield Link(
                self.request.route_path(
                    'bank_remittance.csv',
                    id=self.context.id
                ),
                u"CSV",
                title=u"Export au format CSV",
                icon=u"file-csv",
                css="icon"
            )
        else:
            yield Link(
                self.request.route_path(
                    BANK_REMITTANCE_ITEM_ROUTE,
                    id=self.context.id,
                    _query=dict(action="close")
                ),
                "Clôturer",
                title="Clôturer cette remise en banque",
                icon="lock",
                css="icon btn-primary",
                js="toggleModal('remittance_close_form'); return false;"
            )


class BankRemittanceCloseView(BaseView):
    """
    View to close bank remittance
    """
    def __call__(self):
        schema = RemittanceDateSchema()
        schema = schema.deserialize(self.request.POST)
        self.context.closed = True
        self.context.remittance_date = schema["remittance_date"]
        self.dbsession.merge(self.context)
        self.session.flash(
            "La remise en banque {} est maintenant fermée".format(
                self.context.id
            )
        )
        return HTTPFound(self.request.referrer)


class BankRemittanceOpenView(BaseView):
    """
    View to reopen bank remittance
    """
    def __call__(self):
        self.context.closed = False
        self.context.remittance_date = None
        self.dbsession.merge(self.context)
        self.request.session.flash(
            "La remise en banque {} est maintenant ouverte".format(
                self.context.id
            )
        )
        return HTTPFound(self.request.referrer)


def BankRemittancePdfView(context, request):
    """
    Return a pdf output of the bank remittance
    """
    from endi.resources import pdf_css, main_group
    main_group.need()
    pdf_css.need()
    filename = "remise_{}.pdf".format(context.id)
    template = "endi:templates/accounting/bank_remittance_pdf.mako"
    datas = dict(bank_remittance=context)
    html_str = render_html(request, template, datas)
    write_html_as_pdf_response(request, filename, html_str)
    return request.response


def BankRemittanceCsvView(context, request):
    """
    Return a csv output of the bank remittance
    """
    writer = CsvExporter()
    writer.encoding = 'cp1252'
    writer.headers = (
        {'name': 'date', 'label': u"Date", },
        {'name': 'bank_label', 'label': u"Banque"},
        {'name': 'issuer', 'label': u"Emetteur"},
        {'name': 'check_number', 'label': u"Num. chèque"},
        {'name': 'invoice_ref', 'label': u"Réf. facture"},
        {'name': 'code_compta', 'label': u"Code interne"},
        {'name': 'amount', 'label': u"Montant"},
    )
    br_datas = []
    for payment in context.get_grouped_payments():
        row = {
            'date': format_date(payment["date"]),
            'bank_label': payment["bank_label"],
            'issuer': payment["issuer"],
            'check_number': payment["check_number"],
            'invoice_ref': payment["invoice_ref"],
            'code_compta': payment["code_compta"],
            'amount': format_amount(
                payment["amount"],
                grouping=False,
                precision=5
            )
        }
        br_datas.append(row)
    writer.set_datas(br_datas)
    write_file_to_request(
        request,
        u"remise_{}.csv".format(context.id),
        writer.render(),
        headers="application/csv"
    )
    return request.response


def includeme(config):
    """
        Add module's views
    """
    config.add_view(
        BankRemittanceListView,
        route_name=BANK_REMITTANCE_ROUTE,
        renderer="/accounting/bank_remittances.mako",
        permission='admin_accounting',
    )
    config.add_view(
        BankRemittanceView,
        route_name=BANK_REMITTANCE_ITEM_ROUTE,
        renderer="/accounting/bank_remittance.mako",
        permission='admin_accounting',
    )
    config.add_view(
        BankRemittanceCloseView,
        route_name=BANK_REMITTANCE_ITEM_ROUTE,
        request_param='action=close',
        permission="admin_accounting",
    )
    config.add_view(
        BankRemittanceOpenView,
        route_name=BANK_REMITTANCE_ITEM_ROUTE,
        request_param='action=open',
        permission="admin_accounting",
        require_csrf=True,
        request_method='POST',
    )
    config.add_view(
        BankRemittancePdfView,
        route_name='bank_remittance.pdf',
        permission='admin_accounting',
    )
    config.add_view(
        BankRemittanceCsvView,
        route_name='bank_remittance.csv',
        permission='admin_accounting',
    )
    config.add_admin_menu(
        parent='accounting',
        order=8,
        label="Remises en banque",
        permission="admin_treasury",
        href=BANK_REMITTANCE_ROUTE,
        routes_prefixes=[BANK_REMITTANCE_ITEM_ROUTE, ]
    )
