# -*- coding: utf-8 -*-
import logging
import datetime
import colander
from collections import OrderedDict
from sqlalchemy.orm import (
    joinedload,
)

from endi.export.utils import write_file_to_request
from endi.export.excel import XlsExporter
from endi.export.ods import OdsExporter
from endi.compute import math_utils
from endi.utils.widgets import Link
from endi.utils.strings import (
    short_month_name,
    format_float,
)
from endi.models.company import Company
from endi.models.accounting.income_statement_measures import (
    IncomeStatementMeasureGrid,
    IncomeStatementMeasure,
    IncomeStatementMeasureTypeCategory,
)
from endi.forms.accounting import (
    get_income_statement_measures_list_schema,
    get_upload_treasury_list_schema,
)
from endi.views import BaseListView
from endi.views.accounting.routes import (
    UPLOAD_ITEM_ROUTE,
    UPLOAD_ITEM_INCOME_STATEMENT_ROUTE,
    INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
    INCOME_STATEMENT_GRIDS_ROUTE,
)

logger = logging.getLogger(__name__)


class IncomeStatementMeasureGridListView(BaseListView):
    """
    List companies having IncomeStatementMeasureGrid generated with the current
    context (AccountingOperationUpload)
    """
    sort_columns = {
        "company": "name",
    }
    add_template_vars = (
        'stream_actions',
    )
    default_sort = "company"
    default_direction = "asc"
    schema = get_upload_treasury_list_schema()
    title = "Liste des comptes de résultat"

    def populate_actionmenu(self, appstruct):
        self.request.navigation.breadcrumb.append(
            Link(
                self.request.route_path(
                    UPLOAD_ITEM_ROUTE,
                    id=self.context.id
                ),
                "Revenir à la liste des écritures",
            )
        )
        self.request.navigation.breadcrumb.append(Link("", self.title))

    def query(self):
        query = Company.query().filter(
            Company.id.in_(
                self.dbsession.query(
                    IncomeStatementMeasureGrid.company_id
                ).filter_by(upload_id=self.context.id)
            )
        )
        return query

    def filter_company_id(self, query, appstruct):
        company_id = appstruct.get('company_id')
        if company_id not in (colander.null, None):
            query = query.filter_by(id=company_id)
        return query

    def stream_actions(self, company):
        url = self.request.route_path(
            INCOME_STATEMENT_GRIDS_ROUTE,
            id=company.id
        )
        return (
            Link(
                url,
                "Voir ce compte de résultat",
                title="Voir le détail de ce compte de résultats",
                icon="euro-circle",
                css="icon"
            ),
        )


class YearGlobalGrid(object):
    """
    Abstract class used to modelize the income statement and group all stuff
    """
    def __init__(self, grids, turnover):
        if not grids:
            self.is_void = True
        else:
            self.is_void = False

        if not self.is_void:
            self.categories = IncomeStatementMeasureTypeCategory.get_categories(
            )
            # Month grids stored by month number
            self.grids = self._grid_by_month(grids)

            # Types by category id
            self.types = self._type_by_category()
            self.turnover = turnover

            self.rows = list(self.compile_rows())

    @staticmethod
    def _grid_by_month(month_grids):
        """
        Store month grids by month
        """
        result = OrderedDict((month, None) for month in range(1, 13))
        for grid in month_grids:
            result[grid.month] = grid
        return result

    def _get_last_filled_grid(self):
        """
        Return the last grid (month) where datas were filled
        In fact if we pass here, there should be almost one

        :returns: An IncomeStatementMeasureGrid
        """
        result = None
        grids = list(self.grids.values())
        grids.reverse()
        for grid in grids:
            if grid is not None:
                result = grid
                break
        return result

    def _type_by_category(self):
        """
        Stores IncomeStatementMeasureType by category (to keep the display
        order)

        :returns: A dict {'category.id': [IncomeStatementMeasureType]}
        :rtype: dict
        """
        result = dict((category.id, []) for category in self.categories)
        # On  est sûr d'avoir au moins une grille
        last_grid = self._get_last_filled_grid()
        if not last_grid:
            logger.error("All grids are void")
            return result
        types = IncomeStatementMeasure.get_measure_types(last_grid.id)

        for type_ in types:
            # Les types donc la catégorie a été désactivé entre temps doit
            # toujours apparaitre
            if type_.category not in self.categories:
                self.categories.append(type_.category)
                result[type_.category.id] = []
            result[type_.category.id].append(type_)
        return result

    def _get_month_cell(self, grid, type_id):
        """
        Return the value to display in month related cells
        """
        result = 0

        if grid is not None:
            measure = grid.get_measure_by_type(type_id)
            if measure is not None:
                result = measure.get_value()

        return result

    def stream_headers(self):
        yield ""
        for i in range(1, 13):
            yield short_month_name(i)
        yield "TOTAL"
        yield "% CA"

    def compile_rows(self):
        """
        Collect the output grid corresponding to the current grid list

        Stores all datas in rows (each row matches a measure_type)
        Compute totals and ratio per line

        :returns: generator yielding 2-uple (type, row) where type is a
        IncomeStatementMeasureType instance and row contains the datas of the
        grid row for the given type (15 columns).
        :rtype: tuple
        """
        for category in self.categories:
            for type_ in self.types[category.id]:
                row = []
                sum = 0
                for month, grid in list(self.grids.items()):
                    value = self._get_month_cell(grid, type_.id)
                    sum += value
                    row.append(value)

                row.append(sum)
                percent = math_utils.percent(sum, self.turnover, 0)
                row.append(percent)

                yield type_, row

    def format_datas(self):
        """
        Format all numeric datas to strings in localized formats
        """
        for row in self.rows:
            for index, data in enumerate(row[1]):
                row[1][index] = format_float(data, precision=2)


class CompanyIncomeStatementMeasuresListView(BaseListView):
    schema = get_income_statement_measures_list_schema()
    use_paginate = False
    default_sort = 'month'
    sort_columns = {'month': 'month'}
    filter_button_label = "Changer"

    title = "Compte de résultat"

    def query(self):
        """
        Collect the grids we present in the output
        """
        query = self.request.dbsession.query(IncomeStatementMeasureGrid)
        query = query.options(
            joinedload(IncomeStatementMeasureGrid.measures, innerjoin=True)
        )
        query = query.filter(
            IncomeStatementMeasureGrid.company_id == self.context.id
        )
        return query

    def filter_year(self, query, appstruct):
        """
        Filter the current query by a given year
        """
        year = appstruct.get('year')
        logger.debug("Filtering by year : %s" % year)

        if year not in (None, colander.null):
            query = query.filter(
                IncomeStatementMeasureGrid.year == year
            )
            self.year = year
        else:
            self.year = datetime.date.today().year
        return query

    def more_template_vars(self, response_dict):
        """
        Add template datas in the response dictionnary
        """
        month_grids = response_dict['records']
        logger.debug("MONTH : {}".format(month_grids.count()))
        year_turnover = self.context.get_turnover(self.year)

        grid = YearGlobalGrid(month_grids, year_turnover)
        grid.format_datas()
        response_dict['grid'] = grid
        response_dict['current_year'] = datetime.date.today().year
        response_dict['selected_year'] = int(self.year)
        response_dict['export_xls_url'] = self.request.route_path(
            INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
            id=self.context.id,
            extension="xls",
            _query=self.request.GET,
        )
        response_dict['export_ods_url'] = self.request.route_path(
            INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
            id=self.context.id,
            extension="ods",
            _query=self.request.GET,
        )
        return response_dict


class IncomeStatementMeasureGridXlsView(CompanyIncomeStatementMeasuresListView):
    """
    Xls output
    """
    _factory = XlsExporter

    @property
    def filename(self):
        return "compte_de_resultat_{}.{}".format(
            self.year,
            self.request.matchdict['extension'],
        )

    def _stream_rows(self, query):
        year_turnover = self.context.get_turnover(self.year)
        grid = YearGlobalGrid(query, year_turnover)
        for item in grid.rows:
            yield item

    def _init_writer(self):
        writer = self._factory()
        writer.add_title(
            "Compte de résultat de {} pour l’année {}".format(
                self.context.name,
                self.year,
            ),
            width=15
        )
        writer.add_breakline()
        headers = [short_month_name(i).capitalize() for i in range(1, 13)]
        headers.insert(0, "")
        headers.append("TOTAL")
        headers.append("% CA")
        writer.add_headers(headers)
        return writer

    def _build_return_value(self, schema, appstruct, query):
        writer = self._init_writer()
        writer._datas = []
        for type_, row in self._stream_rows(query):
            row_datas = [type_.label]
            row_datas.extend(row)
            if type_.is_total:
                writer.add_highlighted_row(row_datas)
            else:
                writer.add_row(row_datas)
        write_file_to_request(self.request, self.filename, writer.render())
        return self.request.response


class IncomeStatementMeasureGridOdsView(IncomeStatementMeasureGridXlsView):
    _factory = OdsExporter


def includeme(config):
    config.add_view(
        IncomeStatementMeasureGridListView,
        route_name=UPLOAD_ITEM_INCOME_STATEMENT_ROUTE,
        permission="admin_accounting",
        renderer="/accounting/income_statement_grids.mako",
    )
    config.add_view(
        CompanyIncomeStatementMeasuresListView,
        route_name=INCOME_STATEMENT_GRIDS_ROUTE,
        permission="view.accounting",
        renderer="/accounting/income_statement_measures.mako",
    )
    config.add_view(
        IncomeStatementMeasureGridXlsView,
        route_name=INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
        permission="view.accounting",
        match_param="extension=xls",
    )
    config.add_view(
        IncomeStatementMeasureGridOdsView,
        route_name=INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
        permission="view.accounting",
        match_param="extension=ods",
    )

    config.add_company_menu(
        parent='accounting',
        order=1,
        label="Comptes de résultat",
        route_name=INCOME_STATEMENT_GRIDS_ROUTE,
        route_id_key="company_id",
    )
