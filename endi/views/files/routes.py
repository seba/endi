import os


FILES = "/files"
FILE_ITEM = os.path.join(FILES, "{id}")
FILE_PNG_ITEM = os.path.join(FILES, "{id}.png")
PUBLIC_ITEM = "/public/{name}"


def includeme(config):
    """
    Add module's related routes
    """
    config.add_route(
        FILE_PNG_ITEM, FILE_PNG_ITEM,
        traverse="/files/{id}"
    )
    config.add_route(
        FILE_ITEM, FILE_ITEM,
        traverse="/files/{id}"
    )
    config.add_route(
        PUBLIC_ITEM, PUBLIC_ITEM,
        traverse="/configfiles/{name}"
    )
