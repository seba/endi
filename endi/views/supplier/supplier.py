# -*- coding: utf-8 -*-
"""
    Supplier views
"""

import logging
import colander

from sqlalchemy import or_
from sqlalchemy.orm import undefer_group

from deform import Form

from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound

from endi.models.third_party.third_party import (
    COMPANY_FORM_GRID,
)
from endi.models.third_party.supplier import Supplier
from endi.utils.widgets import (
    Link,
    ViewLink,
    POSTButton,
)
from endi.utils.rest import make_redirect_view
from endi.forms.third_party.supplier import (
    get_list_schema,
    get_supplier_schema,
    get_add_edit_supplier_schema,
)
from deform_extensions import GridFormWidget
from endi.views import (
    BaseListView,
    BaseCsvView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    BaseRestView,
)
from endi.views.csv_import import (
    CsvFileUploadView,
    ConfigFieldAssociationView,
)

logger = log = logging.getLogger(__name__)


def get_supplier_form(request, counter=None):
    """
    Returns the supplier add/edit form
    :param obj request: Pyramid's request object
    :param obj counter: An iterator for field number generation
    :returns: a deform.Form instance
    """
    schema = get_supplier_schema()
    schema = schema.bind(request=request)
    form = Form(
        schema,
        buttons=(submit_btn,),
        counter=counter,
        formid='supplier',
    )
    form.widget = GridFormWidget(named_grid=COMPANY_FORM_GRID)
    return form


class SuppliersListTools(object):
    """
    Supplier list tools
    """
    title = "Liste des fournisseurs"
    schema = get_list_schema()
    sort_columns = {
        'label': Supplier.label,
        "code": Supplier.code,
        "company_name": Supplier.company_name,
        "created_at": Supplier.created_at,
    }
    default_sort = "created_at"
    default_direction = "desc"

    def query(self):
        company = self.request.context
        return Supplier.query().filter_by(company_id=company.id)

    def filter_archived(self, query, appstruct):
        archived = appstruct.get('archived', False)
        if archived in (False, colander.null):
            query = query.filter_by(archived=False)
        return query

    def filter_name_or_contact(self, records, appstruct):
        """
        Filter the records by supplier name or contact lastname
        """
        search = appstruct.get('search')
        if search:
            records = records.filter(
                or_(Supplier.label.like("%" + search + "%"),
                    Supplier.lastname.like("%" + search + "%")))
        return records


class SuppliersListView(SuppliersListTools, BaseListView):
    """
    Supplier listing view
    """
    add_template_vars = (
        'stream_actions',
        'title',
        'forms',
    )

    @property
    def forms(self):
        res = []
        form_title = "Fournisseur"
        form = get_supplier_form(self.request)
        res.append((form_title, form))
        return res

    def stream_actions(self, supplier):
        """
            Return action buttons with permission handling
        """

        if self.request.has_permission('delete_supplier', supplier):
            yield POSTButton(
                self.request.route_path(
                    "supplier",
                    id=supplier.id,
                    _query=dict(action="delete"),
                ),
                "Supprimer",
                title="Supprimer définitivement ce fournisseur",
                icon="trash-alt",
                css="negative",
                confirm="Êtes-vous sûr de vouloir supprimer ce fournisseur ?"
            )

        yield Link(
            self.request.route_path("supplier", id=supplier.id),
            "Voir",
            title="Voir/Modifier ce fournisseur",
            icon="pen",
        )

        if supplier.archived:
            label = "Désarchiver"
        else:
            label = "Archiver"
        yield POSTButton(
            self.request.route_path(
                "supplier",
                id=supplier.id,
                _query=dict(action="archive"),
            ),
            label,
            icon="archive",
        )


class SuppliersCsv(SuppliersListTools, BaseCsvView):
    """
        Supplier csv view
    """
    model = Supplier

    @property
    def filename(self):
        return "fournisseurs.csv"

    def query(self):
        company = self.request.context
        query = Supplier.query().options(undefer_group('edit'))
        return query.filter(Supplier.company_id == company.id)


def supplier_archive(request):
    """
    Archive the current supplier
    """
    supplier = request.context
    if not supplier.archived:
        supplier.archived = True
    else:
        supplier.archived = False
    request.dbsession.merge(supplier)
    return HTTPFound(request.referer)


def supplier_delete(request):
    """
        Delete the current supplier
    """
    supplier = request.context
    request.dbsession.delete(supplier)
    request.session.flash(
        "Le fournisseur '{0}' a bien été supprimé".format(supplier.label)
    )
    return HTTPFound(request.referer)


class BaseSupplierView(BaseFormView):
    """
        Return the view of a supplier
    """
    def __call__(self):
        populate_actionmenu(self.request)
        title = "Fournisseur : {0}".format(self.context.label)
        if self.request.context.code:
            title += " {0}".format(self.context.code)

        pending_orders = self.context.get_orders(pending_invoice_only=True)
        pending_orders_ids = [i.id for i in pending_orders]
        action_groups = [
            [
                Link(
                    self.request.route_path(
                        "supplier",
                        id=self.context.id,
                        _query=dict(action="edit")
                    ),
                    "Modifier",
                    icon='pen',
                    css='btn btn-primary icon',
                ),
            ],
            [
                POSTButton(
                    self.request.route_path(
                        "/company/{id}/suppliers_invoices",
                        id=self.context.company_id,
                        _query=dict(action="new"),
                    ),
                    'Facturer<span class="no_mobile"> les encours validés</span>',
                    icon='file-invoice-euro',
                    css='btn icon',
                    extra_fields=[
                        ('suppliers_orders_ids', pending_orders_ids),
                        ('submit', ''),
                    ],
                    title="Facturer les encours validés",
                ),
                POSTButton(
                    self.request.route_path(
                        "/company/{id}/suppliers_orders",
                        id=self.context.company_id,
                        _query=dict(action="new"),
                    ),
                    '<span class="screen-reader-text">Nouvelle </span>Commande',
                    icon='plus',
                    css='btn icon',
                    extra_fields=[
                        ('supplier_id', self.context.id),
                        ('submit', ''),
                    ],
                    title="Nouvelle commande",
                )
            ]
        ]

        return dict(
            action_groups=action_groups,
            records=self.get_subview_records(),
            title=title,
            supplier=self.request.context,
        )

    def get_subview_records(self):
        """
        Returns a list of items to show additionaly to the main view.
        """
        raise NotImplementedError()


class SupplierViewRunningOrders(BaseSupplierView):
    """ Supplier detail with running orders tab opened
    """
    def get_subview_records(self):
        from endi.models.supply import SupplierOrder
        query = self.context.get_orders(waiting_only=True)
        query = query.order_by(-SupplierOrder.created_at)
        return query


class SupplierViewInvoicedOrders(BaseSupplierView):
    """ Supplier detail with invoiced orders tab opened
    """
    def get_subview_records(self):
        from endi.models.supply import SupplierOrder
        query = self.context.get_orders(invoiced_only=True)
        query = query.order_by(-SupplierOrder.created_at)
        return query


class SupplierViewInvoices(BaseSupplierView):
    """ Supplier detail with invoices tab opened
    """
    def get_subview_records(self):
        from endi.models.supply import SupplierInvoice
        query = self.context.get_invoices()
        query = query.order_by(-SupplierInvoice.date)
        return query


class SupplierAdd(BaseFormView):
    """
    Supplier add form
    """
    add_template_vars = ('title', 'suppliers', )
    title = "Ajouter un fournisseur"
    _schema = None
    buttons = (submit_btn, cancel_btn)
    validation_msg = "Le fournisseur a bien été ajouté"

    @property
    def form_options(self):
        return (('formid', 'supplier'),)

    @property
    def suppliers(self):
        codes = self.context.get_supplier_codes_and_names()
        return codes

    # Schema is here a property since we need to build it dynamically regarding
    # the current request (the same should have been built using the after_bind
    # method ?)
    @property
    def schema(self):
        """
        The getter for our schema property
        """
        if self._schema is None:
            self._schema = get_supplier_schema()
        return self._schema

    @schema.setter
    def schema(self, value):
        """
        A setter for the schema property
        The BaseClass in pyramid_deform gets and sets the schema attribute that
        is here transformed as a property
        """
        self._schema = value

    def before(self, form):
        populate_actionmenu(self.request, self.context)
        form.widget = GridFormWidget(named_grid=COMPANY_FORM_GRID)

    def submit_success(self, appstruct):
        model = self.schema.objectify(appstruct)
        model.company = self.context
        model.type = "company"
        self.dbsession.add(model)
        self.dbsession.flush()
        self.session.flash(self.validation_msg)
        return HTTPFound(
            self.request.route_path(
                'supplier',
                id=model.id
            )
        )

    def cancel_success(self, appstruct):
        return HTTPFound(
            self.request.route_path('company_suppliers', id=self.context.id)
        )
    cancel_failure = cancel_success


class SupplierEdit(SupplierAdd):
    """
    Supplier edition form
    """
    add_template_vars = ('title', 'suppliers',)
    validation_msg = "Le fournisseur a été modifié avec succès"

    def appstruct(self):
        """
        Populate the form with the current edited context (supplier)
        """
        return self.schema.dictify(self.request.context)

    @reify
    def title(self):
        return "Modifier le fournisseur '{0}'".format(
            self.request.context.company_name
        )

    @property
    def suppliers(self):
        company = self.context.company
        codes = company.get_supplier_codes_and_names()
        codes.filter(Supplier.id != self.context.id)
        return codes

    def submit_success(self, appstruct):
        model = self.schema.objectify(appstruct, self.context)
        model = self.dbsession.merge(model)
        self.dbsession.flush()
        self.session.flash(self.validation_msg)
        return HTTPFound(
            self.request.route_path(
                'supplier',
                id=model.id
            )
        )

    def cancel_success(self, appstruct):
        return HTTPFound(
            self.request.route_path(
                'supplier',
                id=self.context.id
            )
        )


def populate_actionmenu(request, context=None):
    """
        populate the actionmenu for the different views (list/add/edit ...)
    """
    company_id = request.context.get_company_id()
    request.actionmenu.add(get_list_view_btn(company_id))
    if context is not None and context.__name__ == 'supplier':
        request.actionmenu.add(get_view_btn(context.id))


def get_list_view_btn(id_):
    return ViewLink(
        "Liste des fournisseurs",
        "list_suppliers",
        path="company_suppliers",
        id=id_
    )


def get_view_btn(supplier_id):
    return ViewLink(
        "Revenir au fournisseur",
        "view_supplier",
        path="supplier",
        id=supplier_id
    )


def get_edit_btn(supplier_id):
    return ViewLink(
        "Modifier",
        "edit_supplier",
        path="supplier",
        id=supplier_id,
        _query=dict(action="edit")
    )


class SupplierImportStep1(CsvFileUploadView):
    title = "Import des fournisseurs, étape 1 : chargement d'un fichier au \
format csv"
    model_types = ("suppliers",)
    default_model_type = 'suppliers'

    def get_next_step_route(self, args):
        return self.request.route_path(
            "company_suppliers_import_step2",
            id=self.context.id,
            _query=args
        )


class SupplierImportStep2(ConfigFieldAssociationView):

    title = "Import de fournisseurs, étape 2 : associer les champs"
    model_types = SupplierImportStep1.model_types

    def get_previous_step_route(self):
        return self.request.route_path(
            "company_suppliers_import_step1",
            id=self.context.id,
        )

    def get_default_values(self):
        log.info("Asking for default values : %s" % self.context.id)
        return dict(company_id=self.context.id)


class SupplierRestView(BaseRestView):
    """
    Supplier rest view

    collection : context Root

        GET : return list of suppliers (company_id should be provided)
    """
    def get_schema(self, submitted):
        if 'formid' in submitted:
            schema = get_supplier_schema()
        else:
            excludes = ('company_id',)
            schema = get_add_edit_supplier_schema(excludes=excludes)
        return schema

    def collection_get(self):
        return self.context.suppliers

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent company
        """
        if not edit:
            entry.company = self.context
        return entry


def add_routes(config):
    config.add_route(
        'supplier',
        '/suppliers/{id}',
        traverse='/suppliers/{id}',
    )
    config.add_route(
        'supplier_running_orders',
        '/suppliers/{id}/running_orders',
        traverse='/suppliers/{id}',
    )

    config.add_route(
        'supplier_invoiced_orders',
        '/suppliers/{id}/invoiced_orders',
        traverse='/suppliers/{id}',
    )
    config.add_route(
        'supplier_invoices',
        '/suppliers/{id}/invoices',
        traverse='/suppliers/{id}',
    )

    config.add_route(
        "/api/v1/companies/{id}/suppliers",
        "/api/v1/companies/{id}/suppliers",
        traverse="/companies/{id}",
    )
    config.add_route(
        "/api/v1/suppliers/{id}",
        "/api/v1/suppliers/{id}",
        traverse='/suppliers/{id}',
    )

    config.add_route(
        'company_suppliers',
        '/company/{id:\d+}/suppliers',
        traverse='/companies/{id}',
    )

    config.add_route(
        'suppliers.csv',
        '/company/{id:\d+}/suppliers.csv',
        traverse='/companies/{id}'
    )


def includeme(config):
    """
        Add module's views
    """
    add_routes(config)

    for i in range(2):
        index = i + 1
        route_name = 'company_suppliers_import_step%d' % index
        path = '/company/{id:\d+}/suppliers/import/%d' % index
        config.add_route(route_name, path, traverse='/companies/{id}')

    config.add_view(
        SupplierAdd,
        route_name='company_suppliers',
        renderer='supplier_edit.mako',
        request_method='POST',
        permission='add_supplier',
    )

    config.add_view(
        SupplierAdd,
        route_name='company_suppliers',
        renderer='supplier_edit.mako',
        request_param='action=add',
        permission='add_supplier',
    )

    config.add_view(
        SuppliersListView,
        route_name='company_suppliers',
        renderer='suppliers.mako',
        request_method='GET',
        permission='list_suppliers',
    )

    config.add_view(
        SuppliersCsv,
        route_name='suppliers.csv',
        request_method='GET',
        permission='list_suppliers',
    )

    config.add_view(
        SupplierEdit,
        route_name='supplier',
        renderer='supplier_edit.mako',
        request_param='action=edit',
        permission='edit_supplier',
    )

    config.add_view(
        make_redirect_view("supplier_running_orders", True),
        route_name='supplier',
        request_method='GET',
        permission='view_supplier',
    )
    config.add_view(
        SupplierViewRunningOrders,
        route_name='supplier_running_orders',
        renderer='/supplier/supplier_list_of_orders.mako',
        request_method='GET',
        permission='view_supplier',
        layout='supplier',
    )
    config.add_view(
        SupplierViewInvoicedOrders,
        route_name='supplier_invoiced_orders',
        renderer='/supplier/supplier_list_of_orders.mako',
        request_method='GET',
        permission='view_supplier',
        layout='supplier',
    )
    config.add_view(
        SupplierViewInvoices,
        route_name='supplier_invoices',
        renderer='/supplier/supplier_list_of_invoices.mako',
        request_method='GET',
        permission='view_supplier',
        layout='supplier',
    )

    config.add_view(
        supplier_delete,
        route_name="supplier",
        request_param="action=delete",
        permission='delete_supplier',
        request_method='POST',
        require_csrf=True,
    )
    config.add_view(
        supplier_archive,
        route_name="supplier",
        request_param="action=archive",
        permission='edit_supplier',
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        SupplierImportStep1,
        route_name="company_suppliers_import_step1",
        permission="add_supplier",
        renderer="base/formpage.mako",
    )

    config.add_view(
        SupplierImportStep2,
        route_name="company_suppliers_import_step2",
        permission="add_supplier",
        renderer="base/formpage.mako",
    )

    config.add_rest_service(
        SupplierRestView,
        "/api/v1/suppliers/{id}",
        collection_route_name="/api/v1/companies/{id}/suppliers",
        view_rights="view_supplier",
        edit_rights="edit_supplier",
        add_rights="add_supplier",
        delete_rights="delete_supplier",
        collection_view_rights="list_suppliers",
    )
    config.add_company_menu(
        parent='third_party',
        order=1,
        label="Fournisseurs",
        route_name="company_suppliers",
        route_id_key="company_id",
        routes_prefixes=['supplier'],
    )
