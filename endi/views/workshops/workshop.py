# -*- coding: utf-8 -*-
"""
Workshop related views
"""
import logging
import peppercorn
import colander
import datetime

from pyramid.httpexceptions import (
    HTTPFound,
    HTTPForbidden,
)
from sqlalchemy import (
    or_,
    func,
    distinct,
    not_,
)

from js.deform import auto_need
from js.jquery_timepicker_addon import timepicker_fr
from sqla_inspect.csv import CsvExporter
from sqla_inspect.excel import XlsExporter
from sqla_inspect.ods import OdsExporter

from endi_base.models.base import DBSESSION
from endi.models.company import Company
from endi.models.workshop import WorkshopAction
from endi.models.activity import Attendance
from endi.models.user.user import User
from endi.utils.navigation import NavigationHandler
from endi.export.workshop_pdf import workshop_pdf
from endi.export.utils import write_file_to_request
from endi.resources import (
    workshop_css,
)
from endi.utils.widgets import Link
from endi.forms.workshop import (
    WORKSHOPSCHEMA,
    get_list_schema,
    ATTENDANCE_STATUS,
    Attendances as AttendanceSchema,
)
from endi.views import (
    BaseListView,
    BaseCsvView,
    BaseFormView,
    DuplicateView,
)
from endi.forms import(
    merge_session_with_post,
)
from endi.views.render_api import (
    format_datetime,
    format_date,
    format_account,
)
from endi.views.files.views import FileUploadView
from endi.models.workshop import (
    WorkshopTagOption,
    Workshop,
    Timeslot,
)
from endi.models.services.user import UserPrefsService

logger = log = logging.getLogger(__name__)


NAVIGATION_KEY = "/workshops"

WORKSHOP_SUCCESS_MSG = "L'atelier a bien été programmée : \
<a href='{0}'>Voir</a>"


COMPLETE_SEARCH_FORM_GRID = (
    (('year', 3,), ('search', 3), ('date', 3), ('notfilled', 3)),
    (('company_manager', 6), ('trainer_id', 6)),
    (('participant_id', 6), ('info_1_id', 6)),
    (('items_per_page', 3),),
)
COMPANY_SEARCH_FORM_GRID = (
    (('year', 3,), ('search', 3), ('date', 3), ('notfilled', 3)),
    (('trainer_id', 6), ('info_1_id', 6)),
    (('items_per_page', 3),),
)
USER_SEARCH_FORM_GRID = (
    (('year', 3,), ('search', 3), ('date', 3), ('notfilled', 3)),
    (('trainer_id', 6), ('info_1_id', 6)),
    (('participant_id', 6), ('items_per_page', 3),),
)


def get_new_datetime(now, hour, minute=0):
    """
    Return a new datetime object based on the 'now' element

        hour

            The hour we'd like to set

        minute

            The minute value we want to set (default 0)
    """
    return now.replace(hour=hour, minute=minute, second=0, microsecond=0)


def get_default_timeslots():
    """
    Return default timeslots for workshop creation
    """
    now = datetime.datetime.now()
    morning = {
        'name': 'Matinée',
        'start_time': get_new_datetime(now, 9),
        'end_time': get_new_datetime(now, 12, 30),
    }
    afternoon = {
        'name': 'Après-midi',
        'start_time': get_new_datetime(now, 14),
        'end_time': get_new_datetime(now, 18),
    }
    return [morning, afternoon]


class WorkshopAddView(BaseFormView):
    """
    View for adding workshop
    """
    title = "Créer un nouvel atelier"
    schema = WORKSHOPSCHEMA

    def before(self, form):
        auto_need(form)
        timepicker_fr.need()
        default_timeslots = get_default_timeslots()
        if self.request.has_permission('manage'):
            if self.context.__name__ == 'company':
                form.set_appstruct({
                    'timeslots': default_timeslots,
                    'company_manager_id': self.context.id
                })
            else:
                form.set_appstruct({
                    'timeslots': default_timeslots,
                })
        else:
            form.set_appstruct({
                'timeslots': default_timeslots,
                'company_manager_id': self.context.id,
                'trainers': [self.request.user.id],
            })

    def submit_success(self, appstruct):
        """
        Create a new workshop
        """
        come_from = appstruct.pop('come_from')

        timeslots_datas = appstruct.pop('timeslots')
        for i in timeslots_datas:
            i.pop('id', None)

        timeslots_datas.sort(key=lambda val: val['start_time'])

        appstruct['datetime'] = timeslots_datas[0]['start_time']
        appstruct['timeslots'] = [
            Timeslot(**data) for data in timeslots_datas
        ]

        participants_ids = set(appstruct.pop('participants', []))
        appstruct['participants'] = [
            User.get(id_) for id_ in participants_ids
        ]

        for timeslot in appstruct['timeslots']:
            timeslot.participants = appstruct['participants']

        trainers_ids = set(appstruct.pop('trainers', []))
        appstruct['trainers'] = [
            User.get(id_) for id_ in trainers_ids
        ]

        workshop_tags_ids = set(appstruct.pop('tags', []))
        appstruct['tags'] = [
            WorkshopTagOption.get(id_) for id_ in workshop_tags_ids
        ]

        if self.context is not None:
            if self.context.__name__ == 'company':
                appstruct['company_manager_id'] = self.context.id
            else:
                appstruct['company_manager_id'] = None

        if self.request.user is not None:
            if self.request.user.id is not None:
                appstruct['owner'] = User.get(self.request.user.id)

        workshop_obj = Workshop(**appstruct)

        workshop_obj = merge_session_with_post(
            workshop_obj, appstruct, remove_empty_values=False,
        )
        self.dbsession.add(workshop_obj)
        self.dbsession.flush()

        workshop_url = self.request.route_path(
            "workshop",
            id=workshop_obj.id,
            _query=dict(action="edit")
            )

        if not come_from:
            redirect = workshop_url
        else:
            msg = WORKSHOP_SUCCESS_MSG.format(workshop_url)
            self.session.flash(msg)
            redirect = come_from
        return HTTPFound(redirect)


class WorkshopEditView(BaseFormView):
    """
    Workshop edition view

    Provide edition functionnality and display a form for attendance recording
    """
    schema = WORKSHOPSCHEMA
    add_template_vars = ('title', 'available_status', )

    @property
    def title(self):
        return self.context.title

    @property
    def available_status(self):
        return ATTENDANCE_STATUS

    def before(self, form):
        """
        Populate the form before rendering

            form

                The deform form object used in this form view (see parent class
                in pyramid_deform)
        """
        add_tree_to_navigation(self.request)
        self.request.navigation.breadcrumb.append(Link('', self.title))

        auto_need(form)
        timepicker_fr.need()

        appstruct = self.context.appstruct()
        participants = self.context.participants
        appstruct['participants'] = [p.id for p in participants]

        trainers = self.context.trainers
        appstruct['trainers'] = [p.id for p in trainers]

        appstruct['tags'] = [
            a.id for a in self.request.context.tags
        ]

        timeslots = self.context.timeslots
        appstruct['timeslots'] = [t.appstruct() for t in timeslots]

        form.set_appstruct(appstruct)

        return form

    def _retrieve_workshop_timeslot(self, id_):
        """
        Retrieve an existing workshop model from the current context
        """
        for timeslot in self.context.timeslots:
            if timeslot.id == id_:
                return timeslot
        log.warn("Possible break in attempt : On essaye d'éditer un timeslot \
qui n'appartient pas au contexte courant !!!!")
        raise HTTPForbidden()

    def _get_timeslots(self, appstruct):
        datas = appstruct.pop('timeslots')
        objects = []
        datas.sort(key=lambda val: val['start_time'])

        for data in datas:
            id_ = data.pop('id', None)
            if id_ is None:
                # New timeslots
                objects.append(Timeslot(**data))
            else:
                # existing timeslots
                obj = self._retrieve_workshop_timeslot(id_)
                merge_session_with_post(obj, data)
                objects.append(obj)

        return objects

    def submit_success(self, appstruct):
        """
        Handle successfull submission of our edition form
        """
        logger.info("Submitting workshop edit")
        logger.info(appstruct)
        come_from = appstruct.pop('come_from')
        appstruct['timeslots'] = self._get_timeslots(appstruct)
        appstruct['datetime'] = appstruct['timeslots'][0].start_time

        participants_ids = set(appstruct.pop('participants', []))
        appstruct['participants'] = [
            User.get(id_) for id_ in participants_ids
        ]

        for timeslot in appstruct['timeslots']:
            timeslot.participants = appstruct['participants']

        trainers_ids = set(appstruct.pop('trainers', []))
        appstruct['trainers'] = [
            User.get(id_) for id_ in trainers_ids
        ]

        workshop_tags_ids = set(appstruct.pop('tags', []))
        appstruct['tags'] = [
            WorkshopTagOption.get(id_) for id_ in workshop_tags_ids
        ]

        if 'company_manager_id' in appstruct:
            if appstruct['company_manager_id'] == -1:
                appstruct['company_manager_id'] = None

        if self.request.user is not None:
            if self.request.user.id is not None:
                appstruct['owner'] = User.get(self.request.user.id)

        merge_session_with_post(
            self.context, appstruct, remove_empty_values=False,
        )
        self.dbsession.merge(self.context)

        workshop_url = self.request.route_path(
            "workshop",
            id=self.context.id,
            _query=dict(action="edit")
            )

        if not come_from:
            redirect = workshop_url
        else:
            msg = WORKSHOP_SUCCESS_MSG.format(workshop_url)
            self.session.flash(msg)
            redirect = come_from

        return HTTPFound(redirect)


def record_attendances_view(context, request):
    """
    Record attendances for the given context (workshop)

    Special Note : Since we need a special layout in the form (with tabs and
        lines with the username as header, we can't render it with deform.  We
        use peppercorn's parser and we build an appropriate form in the template
    """
    schema = AttendanceSchema().bind(request=request)
    if 'submit' in request.params:
        controls = list(request.params.items())
        values = peppercorn.parse(controls)
        try:
            appstruct = schema.deserialize(values)
        except colander.Invalid as e:
            log.error("Error while validating workshop attendance")
            log.error(e)
        else:
            for datas in appstruct['attendances']:
                account_id = datas['account_id']
                timeslot_id = datas['timeslot_id']
                obj = Attendance.get((account_id, timeslot_id))
                obj.status = datas['status']
                request.dbsession.merge(obj)
            request.session.flash("L'émargement a bien été enregistré")

    url = request.route_path(
        'workshop',
        id=context.id,
        _query=dict(action="edit"),
        )

    return HTTPFound(url)


class WorkshopListTools(object):
    """
    Tools for listing workshops
    """
    title = "Liste des ateliers"
    schema = get_list_schema()
    sort_columns = dict(datetime=Workshop.datetime)
    default_sort = 'datetime'
    default_direction = 'desc'

    def remember_navigation_history(self):
        handler = NavigationHandler(self.request, NAVIGATION_KEY)
        extensions_to_ignore = ('.csv', '.ods', '.xls')
        current_path = self.request.current_route_path()
        if not any(ext in current_path for ext in extensions_to_ignore):
            handler.remember()

    def query(self):
        query = Workshop.query()
        return query

    def filter_participant(self, query, appstruct):
        participant_id = appstruct.get('participant_id')
        if participant_id not in (None, colander.null):
            logger.debug("Filtering by participant")
            query = query.filter(
                Workshop.attendances.any(
                    Attendance.account_id == participant_id
                )
            )
        return query

    def filter_info_1_id(self, query, appstruct):
        info_1_id = appstruct.get('info_1_id')
        if info_1_id not in (None, colander.null):
            logger.debug("Filtering by info_1_id")
            query = query.filter(
                Workshop.info1.has(
                    WorkshopAction.id == info_1_id
                )
            )
        return query

    def filter_trainer(self, query, appstruct):
        trainer_id = appstruct.get('trainer_id')
        if trainer_id:
            logger.debug("Filtering by trainer")
            query = query.join(Workshop.trainers).filter(
                User.id == trainer_id,
            )
        return query

    def filter_search(self, query, appstruct):
        search = appstruct['search']
        if search not in (None, colander.null, ''):
            logger.debug("Filtering by search word")
            query = query.filter(
                Workshop.name.like('%{}%'.format(search))
            )
        return query

    def filter_date(self, query, appstruct):
        date = appstruct.get('date')
        year = appstruct.get('year')
        if date not in (None, colander.null):
            logger.debug("Filtering by date")
            query = query.filter(
                Workshop.timeslots.any(
                    func.date(Timeslot.start_time) == date
                )
            )
        # Only filter by year if no date filter is set
        elif year not in (None, colander.null, -1):
            logger.debug("Filtering by year")
            query = query.filter(
                Workshop.timeslots.any(
                    func.extract('YEAR', Timeslot.start_time) == year
                )
            )

        return query

    def filter_notfilled(self, query, appstruct):
        """
        Filter the workshops for which timeslots have not been filled
        """
        notfilled = appstruct.get('notfilled')
        if notfilled not in (None, colander.null, False):
            logger.debug("Filtering the workshop that where not filled")
            attendance_query = DBSESSION().query(distinct(Attendance.event_id))
            attendance_query = attendance_query.filter(
                Attendance.status != 'registered'
            )

            timeslot_ids = [item[0] for item in attendance_query]

            query = query.filter(
                not_(
                    Workshop.timeslots.any(
                        Timeslot.id.in_(timeslot_ids)
                    )
                )
            )
        return query

    def filter_company_manager_or_cae(self, query, appstruct):
        """
        Show all workshops or only CAE workshops (workshops wihtout company
        name)
        """
        company_manager = appstruct.get('company_manager')

        if company_manager not in (colander.null, None):
            if company_manager in (-1, '-1'):
                logger.debug("Company manager is -1")
                query = query.outerjoin(Workshop.company_manager).filter(
                    or_(
                        Workshop.company_manager_id == None,  # noqa: E711
                        Company.internal == True,
                    )
                )
            else:
                logger.debug("Company manager is {}".format(company_manager))
                query = query.filter(
                    Workshop.company_manager_id == int(company_manager)
                )
        logger.debug("Company manager is -1")
        return query

    def __call__(self):
        self.logger.debug("# Calling the list view #")
        self.logger.debug(" + Collecting the appstruct from submitted datas")
        schema, appstruct = self._collect_appstruct()
        self.appstruct = appstruct
        self.logger.debug(appstruct)
        self.logger.debug(" + Launching query")
        query = self.query()
        if query is not None:
            self.logger.debug(" + Filtering query")
            query = self._filter(query, appstruct)
            self.logger.debug(" + Sorting query")
            query = self._sort(query, appstruct)

        self.logger.debug(" + Getting the current route_name")
        self.remember_navigation_history()
        self.logger.debug(" + Building the return values")
        return self._build_return_value(schema, appstruct, query)


class WorkshopCsvWriter(CsvExporter):
    headers = (
        {'name': 'date', 'label': 'Date'},
        {'name': 'label', 'label': "Intitulé"},
        {'name': 'name', 'label': "Nom"},
        {'name': 'role', 'label': "Rôle"},
        {'name': 'duration', 'label': "Durée"},
    )


class WorkshopXlsWriter(XlsExporter):
    headers = (
        {'name': 'date', 'label': 'Date'},
        {'name': 'label', 'label': "Intitulé"},
        {'name': 'name', 'label': "Nom"},
        {'name': 'role', 'label': "Rôle"},
        {'name': 'duration', 'label': "Durée"},
    )


class WorkshopOdsWriter(OdsExporter):
    headers = (
        {'name': 'date', 'label': 'Date'},
        {'name': 'label', 'label': "Intitulé"},
        {'name': 'name', 'label': "Nom"},
        {'name': 'role', 'label': "Rôle"},
        {'name': 'duration', 'label': "Durée"},
    )


def stream_workshop_entries_for_export(query):
    """
    Stream workshop datas for csv export
    """
    for workshop in query.all():

        hours = sum(t.duration[0] for t in workshop.timeslots)
        minutes = sum(t.duration[1] for t in workshop.timeslots)

        duration = hours * 60 + minutes

        start_date = workshop.timeslots[0].start_time.date()

        for participant in workshop.participants:

            attended = False
            for timeslot in workshop.timeslots:
                # On exporte une ligne que si le user était là au moins une
                # fois
                if timeslot.user_status(participant.id) == 'Présent':
                    attended = True
                    break

            if attended:
                yield {
                    "date": start_date,
                    "label": workshop.name,
                    "name": format_account(participant),
                    "role": "apprenant",
                    "duration": duration,
                }

        for trainer in workshop.trainers:
                yield {
                    "date": start_date,
                    "label": workshop.name,
                    "name": format_account(trainer),
                    "role": "formateur",
                    "duration": duration,
                }


class WorkshopCsvView(WorkshopListTools, BaseCsvView):
    """
    Workshop csv export view
    """
    writer = WorkshopCsvWriter

    @property
    def filename(self):
        return "ateliers.csv"

    def _init_writer(self):
        return self.writer()

    def _stream_rows(self, query):
        return stream_workshop_entries_for_export(query)


class CaeWorkshopCsvView(WorkshopCsvView):
    """
    cae Workshop csv export view
    """
    def filter_company_manager_or_cae(self, query, appstruct):
        company_manager = appstruct.get('company_manager')
        if company_manager == colander.null:
                query = query.filter(
                    Workshop.company_manager_id == None  # noqa: E711
                )
        elif company_manager is not None:
            if company_manager in (-1, '-1'):
                query = query.outerjoin(Workshop.company_manager).filter(
                    or_(
                        Workshop.company_manager_id == None,  # noqa: E711
                        Company.internal == True,
                    )
                )
            else:
                query = query.filter(
                    Workshop.company_manager_id == int(company_manager)
                )
        return query


class CompanyWorkshopCsvView(WorkshopCsvView):
    """
    company Workshop csv export view
    """
    def filter_company_manager_or_cae(self, query, appstruct):
        company = self.context
        employee_ids = company.get_employee_ids()
        query = query.filter(
            or_(
                Workshop.company_manager_id == company.id,
                Workshop.trainers.any(User.id.in_(employee_ids))
            )
        )
        return query


class WorkshopXlsView(WorkshopCsvView):
    """
    Workshop excel export view
    """
    writer = WorkshopXlsWriter

    @property
    def filename(self):
        return "ateliers.xls"


class CaeWorkshopXlsView(WorkshopXlsView):
    """
    cae Workshop xls export view
    """
    def filter_company_manager_or_cae(self, query, appstruct):
        company_manager = appstruct.get('company_manager')
        if company_manager == colander.null:
                query = query.filter(
                    Workshop.company_manager_id == None  # noqa: E711
                )
        elif company_manager is not None:
            if company_manager in (-1, '-1'):
                query = query.outerjoin(Workshop.company_manager).filter(
                    or_(
                        Workshop.company_manager_id == None,  # noqa: E711
                        Company.internal == True,
                    )
                )
            else:
                query = query.filter(
                    Workshop.company_manager_id == int(company_manager)
                )
        return query


class CompanyWorkshopXlsView(WorkshopXlsView):
    """
    company Workshop xls export view
    """
    def filter_company_manager_or_cae(self, query, appstruct):
        company = self.context
        employee_ids = company.get_employee_ids()
        query = query.filter(
            or_(
                Workshop.company_manager_id == company.id,
                Workshop.trainers.any(User.id.in_(employee_ids))
            )
        )
        return query


class WorkshopOdsView(WorkshopCsvView):
    """
    Workshop ods export view
    """
    writer = WorkshopOdsWriter

    @property
    def filename(self):
        return "ateliers.ods"


class CaeWorkshopOdsView(WorkshopOdsView):
    """
    cae Workshop ods export view
    """
    def filter_company_manager_or_cae(self, query, appstruct):
        company_manager = appstruct.get('company_manager')
        if company_manager == colander.null:
                query = query.filter(
                    Workshop.company_manager_id == None  # noqa: E711
                )
        elif company_manager is not None:
            if company_manager in (-1, '-1'):
                query = query.outerjoin(Workshop.company_manager).filter(
                    or_(
                        Workshop.company_manager_id == None,  # noqa: E711
                        Company.internal == True,
                    )
                )
            else:
                query = query.filter(
                    Workshop.company_manager_id == int(company_manager)
                )
        return query


class CompanyWorkshopOdsView(WorkshopOdsView):
    """
    company Workshop ods export view
    """
    def filter_company_manager_or_cae(self, query, appstruct):
        company = self.context
        employee_ids = company.get_employee_ids()
        query = query.filter(
            or_(
                Workshop.company_manager_id == company.id,
                Workshop.trainers.any(User.id.in_(employee_ids))
            )
        )
        return query


class BaseWorkshopListView(WorkshopListTools, BaseListView):
    add_template_vars = ('is_admin_view', 'is_edit_view', 'is_company')
    is_admin_view = True
    is_edit_view = True
    is_company = False

    def __init__(self, *args, **kwargs):
        super(BaseWorkshopListView, self).__init__(*args, **kwargs)
        workshop_css.need()


class WorkshopListView(BaseWorkshopListView):
    """
    All Workshop listing view for EA
    """
    add_template_vars = ('is_admin_view', 'is_edit_view', 'is_company', 'route_name_root')
    title = "Tous les ateliers"
    # grid = COMPLETE_SEARCH_FORM_GRID
    route_name_root = 'workshops{file_format}'


class CaeWorkshopListView(BaseWorkshopListView):
    """
    CAE Workshop listing view for EA
    """
    add_template_vars = ('is_admin_view', 'is_edit_view', 'is_company', 'route_name_root')
    title = "Tous les ateliers de la CAE"
    schema = get_list_schema(company=False, default_company_value=-1)
    # grid = COMPLETE_SEARCH_FORM_GRID
    route_name_root = 'cae_workshops{file_format}'


class CompanyWorkshopListView(BaseWorkshopListView):
    """
    View for listing company's workshops dedicated to EA and ES formateur roles

    Outils métiers -> Organisation d'ateliers
    """
    add_template_vars = ('is_admin_view', 'is_edit_view', 'current_users', 'is_company', 'company_id')
    title = "Organisation d'ateliers"
    is_company = True
    schema = get_list_schema(company=True)
    # grid = COMPANY_SEARCH_FORM_GRID

    @property
    def current_users(self):
        return self.context.employees

    @property
    def company_id(self):
        return self.context.id

    def filter_company_manager_or_cae(self, query, appstruct):
        company = self.context
        employee_ids = company.get_employee_ids()
        query = query.filter(
            or_(
                Workshop.company_manager_id == company.id,
                Workshop.trainers.any(User.id.in_(employee_ids))
            )
        )
        return query


class CompanyWorkshopSubscribedListView(BaseWorkshopListView):
    """
    View for listing company's user participant to workshops dedicated to EA
    role

    Gestion -> Mes inscriptions
    """
    add_template_vars = ('is_admin_view', 'is_edit_view', 'current_users')
    title = "Ateliers auxquels un des membres de l'enseigne est inscrit"
    # grid = COMPANY_SEARCH_FORM_GRID
    is_admin_view = False
    is_edit_view = False
    schema = get_list_schema(company=True)

    @property
    def current_users(self):
        return self.context.employees

    def filter_participant(self, query, appstruct):
        company = self.context
        employees_id = company.get_employee_ids()
        query = query.filter(
            Workshop.participants.any(
                User.id.in_(employees_id)
            )
        )
        return query


class UserWorkshopSubscribedListView(CompanyWorkshopSubscribedListView):
    """
    View for listing user's workshops as participant dedicated to EA role

    Gestion sociale -> Mes inscriptions
    """
    # grid = USER_SEARCH_FORM_GRID

    @property
    def current_users(self):
        return [self.context]

    @property
    def title(self):
        return "Ateliers auxquels {} assiste".format(self.context.label)

    def filter_participant(self, query, appstruct):
        user_id = self.context.id
        query = query.filter(
            Workshop.participants.any(
                User.id == user_id
            )
        )
        return query


class UserWorkshopSubscriptionsListView(BaseWorkshopListView):
    """
    User workshops subscriptions listing view

    Ateliers
    """
    add_template_vars = ('is_admin_view', 'is_edit_view', 'current_users')
    schema = get_list_schema(company=False)
    is_admin_view = False
    is_edit_view = False
    # grid = USER_SEARCH_FORM_GRID
    title = "Mes inscriptions"

    @property
    def current_users(self):
        return [self.context]

    def filter_participant(self, query, appstruct):
        user_id = self.context.id
        query = query.filter(
            or_(
                Workshop.attendances.any(
                    Attendance.account_id == user_id
                ),
                Workshop.signup_mode == 'open',
            )
        )
        return query

    def filter_workshop_trainer(self, query, appstruct):
        return query


def timeslots_pdf_output(timeslots, workshop, request):
    """
    write the pdf output of an attendance sheet to the current request response

        timeslots

            The timeslots to render in the attendance sheet (one timeslot = one
            column)

        workshop

            The workshop object

        request

            The current request object
    """
    if not hasattr(timeslots, '__iter__'):
        timeslots = [timeslots]

    date = workshop.datetime.strftime("%e_%m_%Y")
    filename = "atelier_{0}_{1}.pdf".format(date, workshop.id)

    pdf_buffer = workshop_pdf(workshop, timeslots, request)

    write_file_to_request(
        request,
        filename,
        pdf_buffer,
        'application/pdf'
    )
    return request.response


def timeslot_pdf_view(timeslot, request):
    """
    Return a pdf attendance sheet for the given timeslot

        timeslot

            A timeslot object returned as a current context by traversal
    """
    return timeslots_pdf_output(timeslot, timeslot.workshop, request)


def workshop_pdf_view(workshop, request):
    """
    Return a pdf attendance sheet for all the timeslots of the given workshop

        workshop

            A workshop object returned as a current context by traversal
    """
    return timeslots_pdf_output(workshop.timeslots, workshop, request)


def workshop_view(workshop, request):
    """
    Workshop view_only view

        workshop

            the context returned by the traversal tree
    """
    if request.has_permission('edit.workshop'):
        url = request.route_path(
            'workshop',
            id=workshop.id,
            _query=dict(action='edit'),
        )
        return HTTPFound(url)
    add_tree_to_navigation(request)
    request.navigation.breadcrumb.append(Link('', workshop.title))

    timeslots_datas = []

    for timeslot in workshop.timeslots:
        if timeslot.start_time.day == timeslot.end_time.day:
            time_str = "le {0} de {1} à {2}".format(
                format_date(timeslot.start_time),
                format_datetime(timeslot.start_time, timeonly=True),
                format_datetime(timeslot.end_time, timeonly=True)
                )
        else:
            time_str = "du {0} au {1}".format(
                format_datetime(timeslot.start_time),
                format_datetime(timeslot.end_time)
                )

        status = timeslot.user_status(request.user.id)
        timeslots_datas.append((timeslot.name, time_str, status))

    return dict(title=workshop.title, timeslots_datas=timeslots_datas)


def workshop_delete_view(workshop, request):
    """
    Workshop deletion view
    """
    url = request.referer
    request.dbsession.delete(workshop)
    request.session.flash("L'atelier a bien été supprimé")
    if not url:
        url = request.route_path('workshops')
    return HTTPFound(url)


def workshop_signup_view(workshop, request):
    """
    Self-service user signup to a workshop.
    """
    url = request.referer

    if request.user not in workshop.participants:
        workshop.participants.append(request.user)

        for timeslot in workshop.timeslots:
            timeslot.participants.append(request.user)

        request.dbsession.merge(workshop)
    request.session.flash(
        "Vous êtes inscrit à « {} ».".format(workshop.title)
    )
    if not url:
        url = request.route_path('workshops')
    return HTTPFound(url)


def workshop_signout_view(workshop, request):
    """
    Self-service user signout from a workshop.
    """
    url = request.referer

    if request.user in workshop.participants:
        workshop.participants.remove(request.user)

        for timeslot in workshop.timeslots:
            timeslot.participants.remove(request.user)

        request.dbsession.merge(workshop)
    request.session.flash(
        "Vous êtes désinscrit de « {} ».".format(workshop.title)
    )
    if not url:
        url = request.route_path('workshops')
    return HTTPFound(url)


class WorkShopDuplicateView(DuplicateView):
    """
    Workshop duplication view
    """
    message = "Vous avez été redirigé vers le nouvel atelier"
    route_name = "workshop"


def add_tree_to_navigation(request):
    """
    Add elements in the actionmenu regarding the current context
    """
    handler = NavigationHandler(request, NAVIGATION_KEY)
    last = handler.last()

    if last is not None:
        link = Link(last, "Liste des ateliers")
        request.navigation.breadcrumb.append(link)


def add_routes(config):
    """
    Add module's related routes
    """
    config.add_route(
        'workshop',
        "/workshops/{id:\d+}",
        traverse='/workshops/{id}',
    )

    config.add_route(
        "user_workshops_subscribed",
        "/users/{id}/workshops/subscribed",
        traverse="/users/{id}",
    )

    config.add_route(
        "user_workshops_mysubscriptions",
        "/users/{id}/workshops/mysubscriptions",
        traverse="/users/{id}",
    )

    config.add_route(
        'company_workshops_subscribed',
        "/company/{id}/workshops/subscribed",
        traverse="/companies/{id}",
    )

    config.add_route(
        'company_workshops',
        "/company/{id}/workshops",
        traverse="/companies/{id}",
    )

    config.add_route(
        "user_workshop_subscriptions",
        "/users/{id}/workshops/my_subscriptions",
        traverse="/users/{id}",
    )

    config.add_route(
        'workshop.pdf',
        "/workshops/{id}.pdf",
        traverse='/workshops/{id}',
    )

    config.add_route(
        'timeslot.pdf',
        "/timeslots/{id}.pdf",
        traverse='/timeslots/{id}',
    )

    config.add_route('workshops', "/workshops")
    config.add_route('cae_workshops', "/cae/workshops")
    config.add_route('workshops{file_format}', "/workshops{file_format}")
    config.add_route('cae_workshops{file_format}', "/cae/workshops{file_format}")
    config.add_route(
        'company_workshops{file_format}',
        "/company/{id}/workshops/workshops{file_format}",
        traverse="/companies/{id}",
    )


def add_views(config):
    config.add_view(
        WorkshopAddView,
        route_name='workshops',
        permission='add.workshop',
        request_param='action=new',
        renderer="/base/formpage.mako",
    )

    config.add_view(
        CaeWorkshopListView,
        route_name='cae_workshops',
        permission='admin.workshop',
        renderer="/workshops/workshops.mako",
    )

    config.add_view(
        WorkshopListView,
        route_name='workshops',
        permission='admin.workshop',
        renderer="/workshops/workshops.mako",
    )

    config.add_view(
        CompanyWorkshopSubscribedListView,
        route_name='company_workshops_subscribed',
        permission='list.workshop',
        renderer="/workshops/workshops.mako",
    )

    config.add_view(
        UserWorkshopSubscribedListView,
        route_name='user_workshops_subscribed',
        permission='view.user',
        renderer="/workshops/user_workshops.mako",
        layout="user",
    )
    config.add_view(
        UserWorkshopSubscribedListView,
        route_name='user_workshops_mysubscriptions',
        permission='view.user',
        renderer="/workshops/workshops.mako",
        layout="default",
    )

    config.add_view(
        CompanyWorkshopListView,
        route_name='company_workshops',
        permission='list.training',
        renderer="/workshops/workshops.mako",
    )

    config.add_view(
        UserWorkshopSubscriptionsListView,
        route_name='user_workshop_subscriptions',
        permission='view.user',
        renderer="/workshops/workshops.mako",
    )

    config.add_view(
        WorkshopAddView,
        route_name='company_workshops',
        permission='add.training',
        request_param='action=new',
        renderer="/base/formpage.mako",
    )

    config.add_view(
        WorkshopEditView,
        route_name='workshop',
        permission='edit.workshop',
        request_param='action=edit',
        renderer="/workshops/workshop_edit.mako",
    )

    config.add_view(
        record_attendances_view,
        route_name='workshop',
        permission='edit.workshop',
        request_param='action=record',
    )

    config.add_view(
        workshop_signup_view,
        route_name='workshop',
        permission='signup.event',
        request_param='action=signup',
        request_method="POST",
        require_csrf=True,
    )
    config.add_view(
        workshop_signout_view,
        route_name='workshop',
        permission='signout.event',
        request_param='action=signout',
        request_method="POST",
        require_csrf=True,
    )

    config.add_view(
        workshop_delete_view,
        route_name='workshop',
        permission='edit.workshop',
        request_param='action=delete',
        require_csrf=True,
        request_method='POST',
    )

    config.add_view(
        WorkShopDuplicateView,
        route_name='workshop',
        permission='add.workshop',
        request_param='action=duplicate',
        require_csrf=True,
        request_method='POST',
    )

    config.add_view(
        workshop_view,
        route_name='workshop',
        permission='view.workshop',
        renderer='/workshops/workshop_view.mako',
    )

    config.add_view(
        WorkshopCsvView,
        route_name='workshops{file_format}',
        match_param='file_format=.csv',
        permission='list.workshop',
    )

    config.add_view(
        CompanyWorkshopCsvView,
        route_name='company_workshops{file_format}',
        match_param='file_format=.csv',
        permission='list.workshop',
    )

    config.add_view(
        CaeWorkshopCsvView,
        route_name='cae_workshops{file_format}',
        match_param='file_format=.csv',
        permission='list.workshop',
    )

    config.add_view(
        WorkshopXlsView,
        route_name='workshops{file_format}',
        match_param='file_format=.xls',
        permission='list.workshop',
    )

    config.add_view(
        CaeWorkshopXlsView,
        route_name='cae_workshops{file_format}',
        match_param='file_format=.xls',
        permission='list.workshop',
    )

    config.add_view(
        CompanyWorkshopXlsView,
        route_name='company_workshops{file_format}',
        match_param='file_format=.xls',
        permission='list.workshop',
    )

    config.add_view(
        WorkshopOdsView,
        route_name='workshops{file_format}',
        match_param='file_format=.ods',
        permission='list.workshop',
    )

    config.add_view(
        CaeWorkshopOdsView,
        route_name='cae_workshops{file_format}',
        match_param='file_format=.ods',
        permission='list.workshop',
    )

    config.add_view(
        CompanyWorkshopOdsView,
        route_name='company_workshops{file_format}',
        match_param='file_format=.ods',
        permission='list.workshop',
    )

    config.add_view(
        timeslot_pdf_view,
        route_name='timeslot.pdf',
        permission="view.timeslot",
    )

    config.add_view(
        workshop_pdf_view,
        route_name='workshop.pdf',
        permission="view.workshop",
    )

    config.add_view(
        FileUploadView,
        route_name="workshop",
        renderer='/base/formpage.mako',
        permission='edit.workshop',
        request_param='action=attach_file',
    )


def includeme(config):
    """
    Add view to the pyramid registry
    """
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='accompagnement',
        order=1,
        label="Ateliers",
        route_name="cae_workshops",
    )
    config.add_admin_menu(
        parent='training',
        order=1,
        label="Ateliers",
        route_name="workshops",
    )

    config.add_admin_menu(
        parent='accompagnement',
        order=2,
        label="Mes inscriptions",
        route_name="user_workshop_subscriptions",
        route_id_key="user_id",
    )

    config.add_company_menu(
        parent='accompagnement',
        order=1,
        label="Ateliers",
        route_name='company_workshops_subscribed',
        route_id_key='company_id',
        permission="manage"
    )

    def deferred_is_user_company(self, kw):
        return kw['is_user_company']

    config.add_company_menu(
        parent='accompagnement',
        order=2,
        label="Inscription aux ateliers",
        route_name='user_workshop_subscriptions',
        route_id_key='user_id',
        permission=deferred_is_user_company
    )

    def deferred_permission(self, kw):
        return kw['request'].has_permission('list.training', kw['company'])

    config.add_company_menu(
        parent='worktools',
        order=1,
        label="Organisation d'ateliers",
        route_name='company_workshops',
        route_id_key='company_id',
        permission=deferred_permission
    )
