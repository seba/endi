def includeme(config):
    config.include('.routes')
    config.include('.price_study')
    config.include('.rest_api')
