# -*- coding: utf-8 -*-
"""
Provide the following views

price_study add

price_study one page app entry point
"""
import logging
from pyramid.httpexceptions import HTTPFound

from endi.models.third_party.customer import Customer
from endi.models.price_study.price_study import PriceStudy
from endi.forms.price_study.price_study import (
    get_price_study_add_edit_schema,
    get_duplicate_schema,
)
from endi.forms.price_study.task import GenerateTaskSchema
from endi.models.task import (
    Invoice,
    Estimation,
)
from endi.resources import price_study_resources
from endi.views import (
    BaseView,
    BaseAddView,
    TreeMixin,
    DeleteView,
    BaseFormView
)
from endi.views.project.project import ProjectEntryPointView
from endi.views.project.routes import PROJECT_ITEM_PRICE_STUDY_ROUTE
from endi.views.sale_product.routes import CATALOG_API_ROUTE
from .routes import (
    PRICE_STUDY_ROUTE,
    PRICE_STUDY_DELETE_ROUTE,
    PRICE_STUDY_DUPLICATE_ROUTE,
    PRICE_STUDY_GENINV_ROUTE,
    PRICE_STUDY_GENEST_ROUTE,
    PRICE_STUDY_ITEM_API_ROUTE,
    PRODUCT_API_ROUTE,
    DISCOUNT_API_ROUTE,
)
logger = logging.getLogger(__name__)


class PriceStudyAddView(BaseAddView, TreeMixin):
    title = "Ajout d’une étude de prix"
    schema = get_price_study_add_edit_schema()
    msg = "L’étude de prix a été ajoutée avec succès"
    factory = PriceStudy
    route_name = PROJECT_ITEM_PRICE_STUDY_ROUTE

    def before(self, form):
        BaseAddView.before(self, form)
        self.populate_navigation()

    def redirect(self, new_model):
        return HTTPFound(
            self.request.route_path(
                PRICE_STUDY_ROUTE,
                id=new_model.id,
            )
        )

    def on_add(self, new_model, appstruct):
        """
        On add, set the project's company
        """
        new_model.project_id = self.context.id
        new_model.company_id = self.context.company_id
        new_model.owner_id = self.request.user.id
        return new_model


class PriceStudyView(BaseView, TreeMixin):
    route_name = PRICE_STUDY_ROUTE

    @property
    def title(self):
        return "Étude de prix : {}".format(self.context.name)

    def context_url(self, _query={}):
        return self.request.route_path(
            PRICE_STUDY_ITEM_API_ROUTE,
            id=self.context.id,
            _query=_query
        )

    def form_config_url(self):
        return self.context_url({'form_config': 1})

    def product_collection_url(self):
        return self.request.route_path(
            PRODUCT_API_ROUTE,
            id=self.context.id,
        )

    def discount_collection_url(self):
        return self.request.route_path(
            DISCOUNT_API_ROUTE,
            id=self.context.id,
        )

    def catalog_tree_url(self):
        return self.request.route_path(
            CATALOG_API_ROUTE,
            id=self.context.company_id,
        )

    def __call__(self):
        self.populate_navigation()
        price_study_resources.need()
        return dict(
            title=self.title,
            urls=dict(
                context_url=self.context_url(),
                form_config_url=self.form_config_url(),
                product_collection_url=self.product_collection_url(),
                discount_collection_url=self.discount_collection_url(),
                catalog_tree_url=self.catalog_tree_url(),
            )
        )


class PriceStudyDeleteView(DeleteView):
    delete_msg = "L’étude de prix a été supprimée"

    def redirect(self):
        url = self.request.route_path(
            PROJECT_ITEM_PRICE_STUDY_ROUTE,
            id=self.context.project_id,
        )
        return HTTPFound(url)


class PriceStudyDuplicateView(BaseFormView):
    """
    duplication view
    """
    schema = get_duplicate_schema()

    @property
    def title(self):
        return "Dupliquer {}".format(self.context.name)

    def before(self, form):
        BaseFormView.before(self, form)

    def submit_success(self, appstruct):
        logger.debug("# Duplicating a price study #")
        logger.debug(appstruct)

        project_id = appstruct.pop('project_id')

        price_study = self.context.duplicate(project_id, self.request.user.id)

        self.dbsession.add(price_study)
        self.dbsession.flush()
        logger.debug(
            "Price study {} has been duplicated to {}".format(
                self.context.id, price_study.id
            )
        )
        return HTTPFound(
            self.request.route_path(
                PRICE_STUDY_ROUTE,
                id=price_study.id
            )
        )


class PriceStudyGenInvoiceView(BaseFormView):
    schema = GenerateTaskSchema()

    def submit_success(self, appstruct):
        logger.debug(
            "Generating a new invoice from price_study {}".format(
                self.context.id
            )
        )
        customer = Customer.get(appstruct.pop('customer_id'))
        invoice = Invoice.from_price_study(
            self.context,
            self.request.user,
            customer,
            **appstruct
        )
        self.dbsession.add(invoice)
        self.dbsession.flush()
        invoice.prefix = self.request.config.get('invoiceprefix', '')
        business = invoice.gen_business()
        invoice.initialize_business_datas(business)
        logger.debug(
            "Invoice {} has been created".format(invoice.id)
        )
        return HTTPFound(
            self.request.route_path(
                "/invoices/{id}",
                id=invoice.id,
            )
        )


class PriceStudyGenEstimationView(BaseFormView):
    schema = GenerateTaskSchema()

    def submit_success(self, appstruct):
        logger.debug(
            "Generating a new estimation from price_study {}".format(
                self.context.id
            )
        )
        customer = Customer.get(appstruct.pop('customer_id'))
        estimation = Estimation.from_price_study(
            self.context,
            self.request.user,
            customer,
            **appstruct
        )
        estimation.initialize_business_datas()
        self.dbsession.add(estimation)
        self.dbsession.flush()
        logger.debug(
            "Estimation {} has been created".format(estimation.id)
        )
        return HTTPFound(
            self.request.route_path(
                "/estimations/{id}",
                id=estimation.id,
            )
        )


def includeme(config):
    config.add_tree_view(
        PriceStudyAddView,
        parent=ProjectEntryPointView,
        request_param="action=add",
        permission='add.price_study',
        renderer='endi:templates/base/formpage.mako',
        layout='default',
    )
    config.add_view(
        PriceStudyDeleteView,
        route_name=PRICE_STUDY_DELETE_ROUTE,
        permission='delete.price_study',
        request_method='POST',
        require_csrf=True,
    )
    config.add_view(
        PriceStudyDuplicateView,
        route_name=PRICE_STUDY_DUPLICATE_ROUTE,
        permission='view.price_study',
        renderer="base/formpage.mako",
    )
    config.add_tree_view(
        PriceStudyView,
        parent=ProjectEntryPointView,
        permission='view.price_study',
        renderer="endi:templates/price_study/price_study.mako",
        layout="opa",
    )
    config.add_view(
        PriceStudyGenInvoiceView,
        route_name=PRICE_STUDY_GENINV_ROUTE,
        permission="add.invoice",
        renderer="base/formpage.mako",
        layout="default"
    )
    config.add_view(
        PriceStudyGenEstimationView,
        route_name=PRICE_STUDY_GENEST_ROUTE,
        permission="add.estimation",
        renderer="base/formpage.mako",
        layout="default"
    )
