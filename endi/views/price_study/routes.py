# -*- coding: utf-8 -*-
import os
from endi.views import API_ROUTE

PRICE_STUDY_ROUTE = "/price_studies/{id}"
PRICE_STUDY_DELETE_ROUTE = "/price_studies/{id}/delete"
PRICE_STUDY_DUPLICATE_ROUTE = "/price_studies/{id}/duplicate"
PRICE_STUDY_GENINV_ROUTE = os.path.join(PRICE_STUDY_ROUTE, "geninvoice")
PRICE_STUDY_GENEST_ROUTE = os.path.join(PRICE_STUDY_ROUTE, "genestimation")

PRICE_STUDY_API_ROUTE = os.path.join(API_ROUTE, 'price_studies')
PRICE_STUDY_ITEM_API_ROUTE = os.path.join(PRICE_STUDY_API_ROUTE, '{id}')
PRODUCT_API_ROUTE = os.path.join(PRICE_STUDY_ITEM_API_ROUTE, 'products')
PRODUCT_ITEM_API_ROUTE = os.path.join(PRODUCT_API_ROUTE, "{pid}")

DISCOUNT_API_ROUTE = os.path.join(PRICE_STUDY_ITEM_API_ROUTE, 'discounts')
DISCOUNT_ITEM_API_ROUTE = os.path.join(DISCOUNT_API_ROUTE, "{pid}")
WORK_ITEMS_API_ROUTE = os.path.join(
    PRODUCT_ITEM_API_ROUTE,
    "work_items"
)
WORK_ITEMS_ITEM_API_ROUTE = os.path.join(
    WORK_ITEMS_API_ROUTE,
    "{wid}"
)


def includeme(config):
    for route in (
        PRICE_STUDY_ROUTE,
        PRICE_STUDY_DELETE_ROUTE,
        PRICE_STUDY_DUPLICATE_ROUTE,
        PRICE_STUDY_GENINV_ROUTE,
        PRICE_STUDY_GENEST_ROUTE,
        PRICE_STUDY_ITEM_API_ROUTE,
        PRODUCT_API_ROUTE,
        DISCOUNT_API_ROUTE,
    ):
        config.add_route(
            route,
            route,
            traverse="/price_studies/{id}",
        )

    config.add_route(
        PRODUCT_ITEM_API_ROUTE,
        PRODUCT_ITEM_API_ROUTE,
        traverse="/base_price_study_products/{pid}"
    )

    config.add_route(
        DISCOUNT_ITEM_API_ROUTE,
        DISCOUNT_ITEM_API_ROUTE,
        traverse="/price_study_discounts/{pid}"
    )

    config.add_route(
        WORK_ITEMS_API_ROUTE,
        WORK_ITEMS_API_ROUTE,
        traverse="/base_price_study_products/{pid}",
    )
    config.add_route(
        WORK_ITEMS_ITEM_API_ROUTE,
        WORK_ITEMS_ITEM_API_ROUTE,
        traverse="/price_study_work_items/{wid}"
    )
