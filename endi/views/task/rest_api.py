# -*- coding: utf-8 -*-
import logging

from pyramid.csrf import get_csrf_token

from endi.compute.math_utils import (
    percentage,
    integer_to_amount,
    convert_to_float,
)
from endi.models.task import (
    TaskLineGroup,
    DiscountLine,
)
from endi.models.tva import Tva, Product
from endi.models.sale_product.work import SaleProductWork
from endi.forms.tasks.task import (
    get_add_edit_tasklinegroup_schema,
    get_add_edit_taskline_schema,
    get_add_edit_discountline_schema,
)
from endi.utils.rest import RestError
from endi.views import BaseRestView
from endi.views.task.utils import (
    json_business_types,
    json_tvas,
    json_workunits,
    json_products,
    json_mentions,
)
from endi.views.price_study.routes import PRICE_STUDY_ROUTE


logger = logging.getLogger(__name__)


class TaskRestView(BaseRestView):
    """
    Base class for task rest api

    The views contexts are instances of self.factory

    Collection Views

        POST

            Create a new task

    Item views

        GET

            Returns the context in json format

        GET?form_config

            returns the form configuration

        PUT / PATCH

            Edit the current element

        DELETE

            Delete the current element
    """
    factory = None

    def get_schema(self, submitted):
        """
        Return the schema for Task add/edition

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        raise NotImplementedError("Should be implemented in subclass")

    def form_config(self):
        """
        Form display options

        :returns: The sections that the end user can edit, the options
        available
        for the different select boxes
        """
        result = {
            "actions": {
                'status': self._get_status_actions(),
                'others': self._get_other_actions(),
            }
        }
        result = self._add_form_options(result)
        result = self._add_form_sections(result)
        return result

    def _add_form_options(self, form_config):
        """
        Add the main options provided to the end user UI

        :param dict form_config: The current form configuration
        :returns: The dict with a new 'options' key
        """
        default_tva = Tva.get_default()
        if default_tva:
            tva = integer_to_amount(default_tva.value, 2)
        else:
            tva = ""
        options = {
            # Utilisé globalement dans l'interface
            'compute_mode': self.context.mode,
            'tvas': json_tvas(self.request),
            "workunits": json_workunits(self.request),
            "products": json_products(self.request),
            "mentions": json_mentions(self.request),
            "business_types": json_business_types(self.request),
            "csrf_token": get_csrf_token(self.request),
            "defaults": {
                "tva": tva,
                "quantity": 1,
                'mode': self.context.mode,  # Pour les nouveaux modèles
            },
        }

        if hasattr(self, '_more_form_options'):
            options = self._more_form_options(options)

        form_config['options'] = options
        return form_config

    def _add_form_sections(self, form_config):
        """
        Return the sections that should be displayed to the end user

        :param dict form_config: The current form_config
        """
        sections = {
            'general': {
                'business_type_edit': False,
            },
            'common': {'edit': True},
            'composition': {
                'lines': {'mode': 'classic'},
                'expenses_ht': {},
                'edit': True,
            },
            'notes': {'edit': True},
        }
        if hasattr(self, '_more_form_sections'):
            sections = self._more_form_sections(sections)

        if self.context.file_requirements:
            sections['file_requirements'] = {
                'edit': True,
                "can_validate": False,
            }
            if self.request.has_permission("valid.%s" % self.context.type_):
                sections['file_requirements']['can_validate'] = True

        if self.context.has_price_study():
            sections['composition']['edit'] = False
            sections['composition']['link'] = self._get_price_study_link()
            sections['composition'].pop('expenses_ht', None)

        if self.context.mode == 'ttc':
            sections['composition'].pop('expenses_ht', None)
            sections['composition']['mode'] = 'ttc'

        # we do not want this field anymore
        # but we have to ensure its retrocompatibility
        if self.context.expenses_ht == 0:
            sections['composition'].pop('expenses_ht', None)

        form_config['sections'] = sections
        return form_config

    def _get_status_actions(self):

        """
        Returned datas describing available actions on the current item
        :returns: List of actions
        :rtype: list of dict
        """
        actions = []
        url = self.request.current_route_path(_query={'action': 'status'})
        manager = self.context.validation_state_manager
        for action in manager.get_allowed_actions(self.request):
            json_resp = action.__json__(self.request)
            json_resp['url'] = url
            actions.append(json_resp)
        return actions

    def _get_other_actions(self):
        """
        Return the description of other available actions :
            signed_status
            duplicate
            ...
        """
        result = []

        url = self.request.route_path(
            "/%ss/{id}.preview" % self.context.type_,
            id=self.context.id,
        )
        result.append({
            'widget': 'anchor',
            'option': {
                "url": url,
                "title": "Prévisualiser votre document",
                "css": "btn icon only",
                "icon": "eye",
                "popup": True,
            }
        })

        if self.request.has_permission('add.file'):
            url = self.request.route_path(
                "/%ss/{id}/addfile" % self.context.type_,
                id=self.context.id
            )
            result.append({
                'widget': 'anchor',
                'option': {
                    "url": url,
                    "title": "Attacher un fichier à ce document",
                    "css": "btn icon only",
                    "icon": "paperclip",
                    "popup": True,
                }
            })

        if self.request.has_permission('delete.%s' % self.context.type_):
            url = self.request.route_path(
                "/%ss/{id}/delete" % self.context.type_,
                id=self.context.id
            )
            result.append({
                'widget': 'POSTButton',
                'option': {
                    "url": url,
                    "title": "Supprimer définitivement ce document",
                    "css": "btn icon only negative",
                    "icon": "trash-alt",
                    "confirm_msg": "Êtes-vous sûr de vouloir \
                        supprimer cet élément ?"
                }
            })

        return result

    def _get_price_study_link(self):
        """
        Build a link to the associated price study

        :returns: An url or None
        """
        price_study = self.context.get_price_study()
        result = None
        if price_study is not None:
            url = self.request.route_path(PRICE_STUDY_ROUTE, id=price_study.id)

            if price_study.is_editable():
                label = "Modifier l’étude de prix"
                title = "Modifier l’étude de prix à l’origine de ce document"
            else:
                label = "Voir l’étude de prix"
                title = "Voir l’étude de prix à l’origine de ce document"

            result = {
                "url": url,
                "title": title,
                "label": label,
                "css": "btn icon",
                "icon": "pen",
            }
        return result


class TaskLineGroupRestView(BaseRestView):
    """
    Rest views handling the task line groups

    Collection views : Context Task

        GET

            Return all the items belonging to the parent task

        POST

            Add a new item

    Item views

        GET

            Return the Item

        PUT/PATCH

            Edit the item

        DELETE

            Delete the item
    """
    def get_schema(self, submitted):
        """
        Return the schema for TaskLineGroup add/edition

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        excludes = ('task_id',)
        return get_add_edit_tasklinegroup_schema(excludes=excludes)

    def collection_get(self):
        """
        View returning the task line groups attached to this estimation
        """
        return self.context.line_groups

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent task
        """
        if not edit:
            entry.task = self.context
        return entry

    def post_load_groups_from_catalog_view(self):
        """
        View handling product group loading

        expects sale_product_group_ids: [id1, id2] as json POST params
        """
        logger.debug("post_load_from_catalog_view")
        sale_product_group_ids = self.request.json_body.get(
            'sale_product_group_ids', []
        )
        logger.debug("sale_product_ids : %s", sale_product_group_ids)

        groups = []
        for id_ in sale_product_group_ids:
            sale_product_work = SaleProductWork.get(id_)
            group = TaskLineGroup.from_sale_product_work(sale_product_work)
            self.context.line_groups.append(group)
            groups.append(group)
        self.request.dbsession.merge(self.context)
        return groups


class TaskLineRestView(BaseRestView):
    """
    Rest views used to handle the task lines

    Collection views : Context Task

        GET

            Return all the items belonging to the parent task

        POST

            Add a new item

    Item views

        GET

            Return the Item

        PUT/PATCH

            Edit the item

        DELETE

            Delete the item
    """

    def get_schema(self, submitted):
        """
        Return the schema for TaskLine add/edition

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        excludes = ('group_id',)
        return get_add_edit_taskline_schema(excludes=excludes)

    def collection_get(self):
        return self.context.lines

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent group
        """
        if not edit:
            entry.group = self.context

        if 'tva' in attributes and 'product_id' not in attributes and \
                entry.tva is not None:
            entry.product_id = Product.first_by_tva_value(entry.tva)
        return entry


class DiscountLineRestView(BaseRestView):
    """
    Rest views used to handle the task lines


    Collection views : Context Task

        GET

            Return all the items belonging to the parent task

        POST

            Add a new item

    Item views

        GET

            Return the Item

        PUT/PATCH

            Edit the item

        DELETE

            Delete the item
    """
    def get_schema(self, submitted):
        """
        Return the schema for DiscountLine add/edition

        :param dict submitted: The submitted datas
        :returns: A colander.Schema
        """
        excludes = ('task_id',)
        schema = get_add_edit_discountline_schema(excludes=excludes)
        return schema

    def collection_get(self):
        """
        View returning the task line groups attached to this estimation
        """
        return self.context.discounts

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent task
        """
        if not edit:
            entry.task = self.context
        return entry

    def post_percent_discount_view(self):
        """
        View handling percent discount configuration

        Generates discounts for each tva used in this document

        current context : Invoice/Estimation/CancelInvoice
        """
        logger.debug("In DiscountLineRestView.post_percent_discount_view")
        percent = self.request.json_body.get('percentage')
        description = self.request.json_body.get('description')
        lines = []
        percent = convert_to_float(percent, None)
        if percent is not None and description is not None:
            tva_parts = self.context.tva_ht_parts()
            for tva, ht in list(tva_parts.items()):
                amount = percentage(ht, percent)
                line = DiscountLine(
                    description=description,
                    amount=amount,
                    tva=tva,
                    task_id=self.context.id
                )
                self.request.dbsession.add(line)
                self.request.dbsession.flush()
                lines.append(line)
        return lines


class TaskFileRequirementRestView(BaseRestView):
    def collection_get(self):
        return self.context.file_requirements

    def get(self):
        return self.context

    def validation_status(self):
        validation_status = self.request.json_body.get('validation_status')
        if validation_status in self.context.VALIDATION_STATUS:
            return self.context.set_validation_status(validation_status)
        else:
            return RestError(['Statut inconnu'])
