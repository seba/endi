# -*- coding: utf-8 -*-
"""
Common utilities used for task edition
"""
from endi.models.task import (
    PaymentConditions,
    WorkUnit,
)

from endi.forms.tasks.base import get_business_types_from_request

from endi.models.tva import (
    Tva,
    Product,
)


def json_business_types(request):
    return [
        dict(label=i.label, value=i.id, tva_on_margin=i.tva_on_margin)
        for i in get_business_types_from_request(request)
    ]


def json_mentions(request):
    """
    Return the taskmentions available for the task related forms

    :param obj request: The current request object
    :returns: List of TaskMention in their json repr
    """
    context = request.context
    doctype = context.type_
    business_type = context.business_type
    mentions = business_type.optionnal_mentions(doctype)
    return [item.__json__(request) for item in mentions]


def json_tvas(request):
    """
    Return the tva objects available for this form

    :param obj request: The current request object
    :returns: List of Tva objects in their json repr
    """
    query = Tva.query()
    return [item.__json__(request) for item in query]


def json_products(request):
    """
    Return the product objects available for this form

    :param obj request: The current request object
    :returns: List of Product objects in their json repr
    """
    query = Product.query()
    return [item.__json__(request) for item in query]


def json_workunits(request):
    """
    Return the workunit objects available for the given form

    :param obj request: The current request object
    :returns: List of Workunits in their json repr
    """
    query = WorkUnit.query()
    return [item.__json__(request) for item in query]


def json_payment_conditions(request):
    """
    Return The PaymentConditions objects available for the given form

    :param obj request: The current request object
    :returns: List of PaymentConditions in their json repr
    """
    query = PaymentConditions.query()
    return [item.__json__(request) for item in query]
