# -*- coding: utf-8 -*-
"""
Task rendering service

Base service used to render Task in PDF format
"""
import logging
from pyramid.renderers import render
from endi_base.utils.ascii import force_filename
from facturx import generate_facturx_from_file
from endi.models.task import (Invoice, CancelInvoice)
from endi.compute import math_utils

logger = logging.getLogger(__name__)


class TaskPdfFromHtmlService(object):
    """
    This class implements the
    :class:`endi.interfaces.ITaskPdfRenderingService`
    """
    html_template = "endi:templates/tasks/task.mako"

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def _get_config_option(self, option):
        value = self.request.config.get(option, " ")
        if not value:
            value = " "
        return value

    def _get_facturx_xml(self):
        """
        Generates an XML output in Factur-X format with all invoice information

        :rtype: str
        """
        import bleach
        from lxml.etree import Element, SubElement, QName, tostring, ElementTree
        logger.debug("Generates Factur-X XML datas for {}".format(self.context))

        invoice = self.context
        invoice_type = "380"
        invoice_currency = "EUR"
        invoice_total_lines = 0
        invoice_total_discounts = 0

        class ns(object):
            qdt = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100"
            ram = "urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100"
            rsm = "urn:un:unece:uncefact:data:standard:CrossIndustryInvoice:100"
            udt = "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100"
            xsi = "http://www.w3.org/2001/XMLSchema-instance"

        fx_root = Element(
            QName(ns.rsm, "CrossIndustryInvoice"),
            nsmap={"qdt":ns.qdt, "ram":ns.ram, "rsm":ns.rsm, "udt":ns.udt, "xsi":ns.xsi}
        )

        # BLOC IDENTIFICATION DU MESSAGE
        fx_child1 = SubElement(fx_root, QName(ns.rsm, "ExchangedDocumentContext"))
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "GuidelineSpecifiedDocumentContextParameter"))
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "ID"))
        fx_child3.text = "urn:cen.eu:en16931:2017"

        # BLOC ENTETE DE MESSAGE
        fx_child1 = SubElement(fx_root, QName(ns.rsm, "ExchangedDocument"))
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "ID"))
        fx_child2.text = invoice.official_number
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "TypeCode"))
        fx_child2.text = invoice_type
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "IssueDateTime"))
        fx_child3 = SubElement(fx_child2, QName(ns.udt, "DateTimeString"), format="102")
        fx_child3.text = invoice.date.strftime("%Y%m%d")
        if invoice.notes:
            fx_child2 = SubElement(fx_child1, QName(ns.ram, "IncludedNote"))
            fx_child3 = SubElement(fx_child2, QName(ns.ram, "Content"))
            fx_child3.text = invoice.notes

        # BLOC TRANSACTION COMMERCIALE
        fx_child1 = SubElement(fx_root, QName(ns.rsm, "SupplyChainTradeTransaction"))

        # LIGNES FACTURE
        line_number = 1
        for group in invoice.line_groups:
            for line in group.lines:
                line_pdt_name = bleach.clean(line.description, tags=[], attributes={}, styles=[], strip=True)
                line_unit_code = "C62" # TODO : Récupérer le code de l'unité de la ligne
                line_tva_code = "E" if line.tva == 0 else "S" # TODO : Gérer les différents cas d'exo TVA
                line_cost = math_utils.integer_to_amount(line.cost,5)
                line_tva_rate = math_utils.integer_to_amount(line.tva,2)
                line_total_amount = math_utils.integer_to_amount(line.total_ht(),5)
                invoice_total_lines += line.total_ht()
                fx_child2 = SubElement(fx_child1, QName(ns.ram, "IncludedSupplyChainTradeLineItem"))
                fx_child3 = SubElement(fx_child2, QName(ns.ram, "AssociatedDocumentLineDocument"))
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "LineID"))
                fx_child4.text = str(line_number)
                fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedTradeProduct"))
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "Name"))
                fx_child4.text = line_pdt_name
                fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedLineTradeAgreement"))
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "GrossPriceProductTradePrice"))
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "ChargeAmount"))
                fx_child5.text = str(line_cost)
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "NetPriceProductTradePrice"))
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "ChargeAmount"))
                fx_child5.text = str(line_cost)
                fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedLineTradeDelivery"))
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "BilledQuantity"), unitCode=line_unit_code)
                fx_child4.text = str(line.quantity)
                fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedLineTradeSettlement"))
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "ApplicableTradeTax"))
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "TypeCode"))
                fx_child5.text = "VAT"
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "CategoryCode"))
                fx_child5.text = line_tva_code
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "RateApplicablePercent"))
                fx_child5.text = str(line_tva_rate)
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "SpecifiedTradeSettlementLineMonetarySummation"))
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineTotalAmount"))
                fx_child5.text = str(line_total_amount)
                line_number += 1

        # TIERS
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "ApplicableHeaderTradeAgreement"))
        # Vendeur
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "SellerTradeParty"))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "Name"))
        fx_child4.text = self._get_config_option("cae_business_name")
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "Description"))
        fx_child4.text = self._get_config_option("cae_legal_status")
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "SpecifiedLegalOrganization"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "ID"), schemeID="0002")
        fx_child5.text = self._get_config_option("cae_business_identification")
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "TradingBusinessName"))
        fx_child5.text = invoice.company.name
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "DefinedTradeContact"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "PersonName"))
        fx_child5.text = invoice.company.name
        if invoice.company.phone or invoice.company.mobile:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "TelephoneUniversalCommunication"))
            fx_child6 = SubElement(fx_child5, QName(ns.ram, "CompleteNumber"))
            if invoice.company.phone:
                fx_child6.text = invoice.company.phone
            else:
                fx_child6.text = invoice.company.mobile
        if invoice.company.email:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "EmailURIUniversalCommunication"))
            fx_child6 = SubElement(fx_child5, QName(ns.ram, "URIID"), schemeID="SMTP")
            fx_child6.text = invoice.company.email
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "PostalTradeAddress"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "PostcodeCode"))
        fx_child5.text = self._get_config_option("cae_zipcode")
        cae_address = self._get_config_option("cae_address").splitlines()
        if len(cae_address) > 0:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineOne"))
            fx_child5.text = cae_address[0]
        if len(cae_address) > 1:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineTwo"))
            fx_child5.text = cae_address[1]
        if len(cae_address) > 2:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineThree"))
            fx_child5.text = cae_address[2]
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "CityName"))
        fx_child5.text = self._get_config_option("cae_city")
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "CountryID"))
        fx_child5.text = "FR"
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "URIUniversalCommunication"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "URIID"), schemeID="SMTP")
        fx_child5.text = self._get_config_option("cae_contact_email")
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "SpecifiedTaxRegistration"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "ID"), schemeID="VA")
        fx_child5.text = self._get_config_option("cae_intercommunity_vat")
        # Acheteur
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "BuyerTradeParty"))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "Name"))
        fx_child4.text = invoice.customer.label
        if invoice.customer.type == "company":
            if invoice.customer.registration:
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "SpecifiedLegalOrganization"))
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "ID"), schemeID="0002")
                fx_child5.text = invoice.customer.registration
            if invoice.customer.lastname:
                fx_child4 = SubElement(fx_child3, QName(ns.ram, "DefinedTradeContact"))
                fx_child5 = SubElement(fx_child4, QName(ns.ram, "PersonName"))
                fx_child5.text = "{} {}".format(invoice.customer.lastname, invoice.customer.firstname)
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "PostalTradeAddress"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "PostcodeCode"))
        fx_child5.text = invoice.customer.zip_code
        buyer_address = invoice.customer.address.splitlines()
        if len(buyer_address) > 0:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineOne"))
            fx_child5.text = buyer_address[0]
        if len(buyer_address) > 1:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineTwo"))
            fx_child5.text = buyer_address[1]
        if len(buyer_address) > 2:
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "LineThree"))
            fx_child5.text = buyer_address[2]
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "CityName"))
        fx_child5.text = invoice.customer.city
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "CountryID"))
        fx_child5.text = "FR" # TODO : Récupérer le code du pays du client
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "URIUniversalCommunication"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "URIID"), schemeID="SMTP")
        fx_child5.text = invoice.customer.email
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "SpecifiedTaxRegistration"))
        fx_child5 = SubElement(fx_child4, QName(ns.ram, "ID"), schemeID="VA")
        fx_child5.text = invoice.customer.tva_intracomm

        # LIVRAISON (Obligatoire mais pas utilisé)
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "ApplicableHeaderTradeDelivery"))

        # PAIEMENT
        fx_child2 = SubElement(fx_child1, QName(ns.ram, "ApplicableHeaderTradeSettlement"))
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "PaymentReference"))
        fx_child3.text = invoice.official_number
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "InvoiceCurrencyCode"))
        fx_child3.text = invoice_currency
        # Ventilation TVA
        invoice_ht_parts = invoice.tva_ht_parts()
        for tva, tva_amount in list(invoice.get_tvas().items()):
            tva_code = "E" if tva == 0 else "S" # TODO : Gérer les différents cas d'exo TVA
            tva_base_amount = math_utils.integer_to_amount(invoice_ht_parts.get(tva,0),5)
            tva_due_time_code = "72" if self._get_config_option("cae_vat_collect_mode") == "encaissement" else "5"
            tva_rate = math_utils.integer_to_amount(tva,2)
            tva_amount = math_utils.integer_to_amount(tva_amount,5)
            fx_child3 = SubElement(fx_child2, QName(ns.ram, "ApplicableTradeTax"))
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "CalculatedAmount"))
            fx_child4.text = str(tva_amount)
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "TypeCode"))
            fx_child4.text = "VAT"
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "BasisAmount"))
            fx_child4.text = str(tva_base_amount)
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "CategoryCode"))
            fx_child4.text = tva_code
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "DueDateTypeCode"))
            fx_child4.text = tva_due_time_code
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "RateApplicablePercent"))
            fx_child4.text = str(tva_rate)
        # Remises de la facture
        for discount in invoice.discounts:
            discount_amount = math_utils.integer_to_amount(discount.amount,5)
            discount_tva_code = "E" if discount.tva == 0 else "S" # TODO : Gérer les différents cas d'exo TVA
            discount_tva_rate = math_utils.integer_to_amount(discount.tva,2)
            discount_description = bleach.clean(discount.description, tags=[], attributes={}, styles=[], strip=True)
            fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedTradeAllowanceCharge"))
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "ChargeIndicator"))
            fx_child5 = SubElement(fx_child4, QName(ns.udt, "Indicator"))
            fx_child5.text = "false"
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "ActualAmount"))
            fx_child4.text = str(discount_amount)
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "Reason"))
            fx_child4.text = discount_description
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "CategoryTradeTax"))
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "TypeCode"))
            fx_child5.text = "VAT"
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "CategoryCode"))
            fx_child5.text = discount_tva_code
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "RateApplicablePercent"))
            fx_child5.text = str(discount_tva_rate)
        # Frais et charges de la facture
        if invoice.expenses_ht > 0:
            expenses_amount = math_utils.integer_to_amount(invoice.expenses_ht,5)
            fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedTradeAllowanceCharge"))
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "ChargeIndicator"))
            fx_child5 = SubElement(fx_child4, QName(ns.udt, "Indicator"))
            fx_child5.text = "true"
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "ActualAmount"))
            fx_child4.text = str(expenses_amount)
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "CategoryTradeTax"))
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "TypeCode"))
            fx_child5.text = "VAT"
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "CategoryCode"))
            fx_child5.text = "S"
            fx_child5 = SubElement(fx_child4, QName(ns.ram, "RateApplicablePercent"))
            fx_child5.text = "20.0"
        # Conditions de paiement
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedTradePaymentTerms"))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "Description"))
        fx_child4.text = invoice.payment_conditions
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "DueDateDateTime"))
        fx_child5 = SubElement(fx_child4, QName(ns.udt, "DateTimeString"), format="102")
        fx_child5.text = invoice.date.strftime("%Y%m%d") # TODO : Calculer la date d'échéance
        # Totaux
        fx_child3 = SubElement(fx_child2, QName(ns.ram, "SpecifiedTradeSettlementHeaderMonetarySummation"))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "LineTotalAmount"))
        fx_child4.text = str(math_utils.integer_to_amount(invoice_total_lines,5))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "ChargeTotalAmount"))
        fx_child4.text = str(math_utils.integer_to_amount(invoice.expenses_ht,5))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "AllowanceTotalAmount"))
        fx_child4.text = str(invoice_total_discounts)
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "TaxBasisTotalAmount"))
        fx_child4.text = str(math_utils.integer_to_amount(invoice.ht,5))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "TaxTotalAmount"), currencyID=invoice_currency)
        fx_child4.text = str(math_utils.integer_to_amount(invoice.tva,5))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "GrandTotalAmount"))
        fx_child4.text = str(math_utils.integer_to_amount(invoice.ttc,5))
        fx_child4 = SubElement(fx_child3, QName(ns.ram, "DuePayableAmount"))
        fx_child4.text = str(math_utils.integer_to_amount(invoice.ttc,5))
        # Référence à une facture antérieure
        if hasattr(invoice, "invoice"):
            fx_child3 = SubElement(fx_child2, QName(ns.ram, "InvoiceReferencedDocument"))
            fx_child4 = SubElement(fx_child3, QName(ns.ram, "IssuerAssignedID"))
            fx_child4.text = invoice.invoice.official_number
        return fx_root

    def render(self):
        """
        Render the current context in pdf format

        :rtype: instance of :class:`io.BytesIO`
        """
        logger.debug("Rendering PDF datas for {}".format(self.context))
        from endi.export.task_pdf import task_pdf
        pdf_file = task_pdf(self.context, self.request)
        if isinstance(self.context, (Invoice, CancelInvoice)):
            generate_facturx_from_file(pdf_file, self._get_facturx_xml())
        return pdf_file

    def filename(self):
        return force_filename("{}.pdf".format(self.context.internal_number))
