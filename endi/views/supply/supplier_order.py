# -*- coding: utf-8 -*-

import datetime

from pyramid.httpexceptions import HTTPFound

from endi.resources import supplier_order_resources
from endi.views import (
    BaseListView,
    BaseView,
    DeleteView,
    DuplicateView,
    submit_btn,
)
from endi.views.files.views import (
    FileUploadView,
)
from endi.forms.supply.supplier_order import get_supplier_orders_list_schema
from endi.models.supply import (
    SupplierInvoice,
    SupplierOrder,
)
from endi.models.third_party.supplier import Supplier
from endi.utils.widgets import (
    ViewLink,
    Link,
    POSTButton,
)
from endi.views import (
    BaseListView,
    BaseCsvView,
    BaseFormView,
)
from endi.views.supply import SupplierDocListTools
from endi.forms.supply.supplier_order import SupplierOrderAddSchema


def populate_actionmenu(request):
    return request.actionmenu.add(
        ViewLink(
            "Revenir à la liste des commandes fournisseur",
            path="/company/{id}/suppliers_orders",
            id=request.context.get_company_id(),
        )
    )


def _default_order_name(supplier):
    return "Commande {}, {}".format(
            supplier.label,
            datetime.date.today(),
        )


class SupplierOrderAddView(BaseFormView):
    add_template_vars = ('title',)
    title = "Saisir une commande fournisseur"

    schema = SupplierOrderAddSchema()
    buttons = (submit_btn,)

    def before(self, form):
        assert self.context.__name__ == 'company'
        form.set_appstruct({'company_id': self.context.id})

    def submit_success(self, appstruct):
        assert self.context.__name__ == 'company'
        appstruct['company_id'] = self.context.id

        supplier = Supplier.get(appstruct['supplier_id'])

        appstruct['name'] = _default_order_name(supplier)

        obj = SupplierOrder(**appstruct)

        self.dbsession.add(obj)
        self.dbsession.flush()
        edit_url = self.request.route_path(
            '/suppliers_orders/{id}',
            id=obj.id,
        )
        return HTTPFound(edit_url)


class SupplierOrderEditView(BaseView):
    """
    Can act as edit view or readonly view (eg: waiting for validation).
    """
    def context_url(self):
        return self.request.route_path(
            '/api/v1/suppliers_orders/{id}',
            id=self.request.context.id
        )

    def form_config_url(self):
        return self.request.route_path(
            '/api/v1/suppliers_orders/{id}',
            id=self.request.context.id,
            _query={'form_config': '1'}
        )

    def __call__(self):
        populate_actionmenu(self.request)
        supplier_order_resources.need()
        return dict(
            context=self.context,
            title=self.context.name,
            context_url=self.context_url(),
            form_config_url=self.form_config_url(),
        )


class SupplierOrderDuplicateView(DuplicateView):
    route_name = "/suppliers_orders/{id}"
    message = "vous avez été redirigé vers la nouvelle commande fournisseur"

    def on_duplicate(self, item):
        src_order = self.context
        target_order = item

        target_order.name = 'Copie de {}'.format(src_order.name)
        target_order.import_lines_from_order(src_order)
        self.dbsession.merge(target_order)
        self.dbsession.flush()


class CompanySupplierOrderListTools(SupplierDocListTools):
    model_class = SupplierOrder

    def filter_invoice_status(self, query, appstruct):
        invoice_status = appstruct['invoice_status']
        if invoice_status in ('present', 'draft', 'valid', 'resulted'):
            query = query.filter(
                SupplierOrder.supplier_invoice_id != None  # noqa
            )
            query = query.join(SupplierOrder.supplier_invoice)
            if invoice_status == 'draft':
                query = query.filter(SupplierInvoice.status == 'draft')
            elif invoice_status == 'valid':
                query = query.filter(SupplierInvoice.status == 'valid')
            elif invoice_status == 'resulted':
                query = query.filter(SupplierInvoice.paid_status == 'resulted')
        elif invoice_status == 'absent':
            query = query.filter(
                SupplierOrder.supplier_invoice_id == None # noqa
            )
        return query


def stream_supplier_order_actions(request, supplier_order):
        yield Link(
            request.route_path(
                "/suppliers_orders/{id}",
                id=supplier_order.id,
            ),
            "Voir/Éditer",
            icon="pen",
            css="icon"
        )
        delete_allowed = request.has_permission(
            'delete.supplier_order',
            supplier_order,
        )
        if delete_allowed:
            yield POSTButton(
                request.route_path(
                    "/suppliers_orders/{id}",
                    id=supplier_order.id,
                    _query=dict(action="delete"),
                ),
                "Supprimer",
                title="Supprimer définitivement cette commande ?",
                icon="trash-alt",
                css="negative",
                confirm="Êtes-vous sûr de vouloir supprimer cette commande ?"
            )


class BaseSupplierOrderListView(
        CompanySupplierOrderListTools,
        BaseListView,

):
    title = 'Liste des commandes fournisseurs'
    add_template_vars = ['title', 'stream_actions']

    def stream_actions(self, supplier_order):
        return stream_supplier_order_actions(self.request, supplier_order)


class AdminSupplierOrderListView(BaseSupplierOrderListView):
    """
    Admin-level view, listing all orders from all companies.
    """
    is_admin_view = True
    add_template_vars = BaseSupplierOrderListView.add_template_vars + [
        'is_admin_view',
    ]

    schema = get_supplier_orders_list_schema(is_global=True)

    def query(self):
        return SupplierOrder.query()


class CompanySupplierOrderListView(BaseSupplierOrderListView):
    """
    Company-scoped SupplierOrder list view.
    """

    schema = get_supplier_orders_list_schema(is_global=False)

    def query(self):
        company = self.request.context
        return SupplierOrder.query().filter_by(company_id=company.id)


class SupplierOrderDeleteView(DeleteView):
    delete_msg = "La commande fournisseur a bien été supprimée"

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                '/company/{id}/suppliers_orders',
                id=self.context.company.id
            )
        )


def add_routes(config):
    config.add_route(
        '/suppliers_orders',
        '/suppliers_orders',
    )

    config.add_route(
        '/company/{id}/suppliers_orders',
        '/company/{id}/suppliers_orders',
        traverse='/companies/{id}',
    )

    config.add_route(
        '/suppliers_orders/{id}',
        '/suppliers_orders/{id}',
        traverse='/suppliers_orders/{id}',
    )
    for action in (
        'delete',
        'duplicate',
        'addfile',
    ):
        config.add_route(
            "/suppliers_orders/{id}/%s" % action,
            "/suppliers_orders/{id:\d+}/%s" % action,
            traverse="/suppliers_orders/{id}",
        )


def add_views(config):
    # Admin views
    config.add_view(
        AdminSupplierOrderListView,
        request_method='GET',
        route_name='/suppliers_orders',
        permission='admin.supplier_order',
        renderer="/supply/suppliers_orders.mako"
    )

    # Company views
    config.add_view(
        SupplierOrderAddView,
        route_name='/company/{id}/suppliers_orders',
        request_param='action=new',
        permission='add.supplier_order',
        renderer="base/formpage.mako",
    )

    config.add_view(
        CompanySupplierOrderListView,
        route_name='/company/{id}/suppliers_orders',
        request_method='GET',
        renderer='/supply/suppliers_orders.mako',
        permission='list.supplier_order',
    )
    config.add_view(
        SupplierOrderEditView,
        route_name="/suppliers_orders/{id}",
        renderer="/supply/supplier_order.mako",
        permission="view.supplier_order",
        layout="opa",
    )

    config.add_view(
        SupplierOrderDeleteView,
        route_name="/suppliers_orders/{id}",
        request_param='action=delete',
        permission="delete.supplier_order",
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        SupplierOrderDuplicateView,
        route_name="/suppliers_orders/{id}",
        request_param='action=duplicate',
        permission="view.supplier_order",
        request_method='POST',
        require_csrf=True,
    )

    # File attachment
    config.add_view(
        FileUploadView,
        route_name="/suppliers_orders/{id}/addfile",
        renderer='base/formpage.mako',
        permission='add.file',
    )



def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='sale',
        order=3,
        label='Commandes fournisseurs',
        href='/suppliers_orders',
        routes_prefixes=['/suppliers_orders/{id}']
    )
    config.add_company_menu(
        parent='supply',
        order=1,
        label='Commandes fournisseurs',
        route_name='/company/{id}/suppliers_orders',
        route_id_key='company_id',
        routes_prefixes=['/suppliers_orders/{id}']
    )
