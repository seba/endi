# -*- coding: utf-8 -*-
"""
    Estimation views


Estimation datas edition :
    date
    address
    customer
    object
    note
    mentions
    ....

Estimation line edition :
    description
    quantity
    cost
    unity
    tva
    ...

Estimation line group edition :
    title
    description

Estimation discount edition

Estimation payment edition

"""
import logging

from pyramid.httpexceptions import HTTPFound
from endi.forms.tasks.estimation import InvoiceAttachSchema
from endi.models.task import (
    Estimation,
    PaymentLine,
    Invoice,
)
from endi.utils.widgets import ViewLink
from endi.resources import (
    estimation_signed_status_js,
    task_html_pdf_css,
)
from endi.forms.tasks.estimation import get_add_edit_estimation_schema
from endi.views import (
    BaseEditView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    add_panel_page_view,
)
from endi.views.files.views import FileUploadView
from endi.views.project.routes import PROJECT_ITEM_ESTIMATION_ROUTE
from endi.views.business.business import BusinessOverviewView
from endi.views.task.views import (
    TaskAddView,
    TaskEditView,
    TaskDeleteView,
    TaskHtmlView,
    TaskPdfView,
    TaskDuplicateView,
    TaskSetMetadatasView,
    TaskSetDraftView,
    TaskMoveToPhaseView,
    TaskSyncWithPriceStudyView,
)

log = logger = logging.getLogger(__name__)


class EstimationAddView(TaskAddView):
    """
    Estimation add view
    context is a project
    """
    title = "Nouveau devis"
    factory = Estimation

    def _more_init_attributes(self, estimation, appstruct):
        """
        Add Estimation's specific attribute while adding this task
        """
        estimation.payment_lines = [PaymentLine(description='Solde', amount=0)]
        estimation.initialize_business_datas()
        estimation.set_default_validity_duration()
        return estimation

    def _after_flush(self, estimation):
        """
        Launch after the new estimation has been flushed
        """
        logger.debug(
            "  + Estimation successfully added : {0}".format(estimation.id)
        )


class EstimationEditView(TaskEditView):
    route_name = '/estimations/{id}'

    @property
    def title(self):
        customer = self.context.customer
        customer_label = customer.label
        if customer.code is not None:
            customer_label += " ({0})".format(customer.code)
        return (
            "Modification du devis {task.name} avec le client "
            "{customer}".format(
                task=self.context,
                customer=customer_label,
            )
        )

    def _before(self):
        """
        Ensure some stuff on the current context
        """
        if not self.context.payment_lines:
            self.context.payment_lines = [
                PaymentLine(description='Solde', amount=self.context.ttc)
            ]
            self.request.dbsession.merge(self.context)
            self.request.dbsession.flush()

    def discount_api_url(self):
        return self.context_url() + "/discount_lines"

    def more_js_app_options(self):
        return {
            'discount_api_url': self.discount_api_url(),
        }


class EstimationDeleteView(TaskDeleteView):
    msg = "Le devis {context.name} a bien été supprimé."

    def post_delete(self):
        if self.context.business and self.context.business.is_void():
            self.request.dbsession.delete(self.context.business)


class EstimationAdminView(BaseEditView):
    factory = Estimation
    schema = get_add_edit_estimation_schema(isadmin=True)


class EstimationHtmlView(TaskHtmlView):
    label = "Devis"
    route_name = "/estimations/{id}.html"

    def actions(self):
        estimation_signed_status_js.need()
        actions = []
        for action in self.context.signed_state_manager.get_allowed_actions(
            self.request
        ):
            actions.append(action)
        return actions


class EstimationPdfView(TaskPdfView):
    pass


class EstimationDuplicateView(TaskDuplicateView):
    label = "le devis"

    def _after_task_duplicate(self, task, appstruct):
        task.initialize_business_datas()
        return task


class EstimationSetMetadatasView(TaskSetMetadatasView):
    @property
    def title(self):
        return "Modification du devis {task.name}".format(
            task=self.context
        )


class EstimationAttachInvoiceView(BaseFormView):
    schema = InvoiceAttachSchema()
    buttons = (submit_btn, cancel_btn,)

    def before(self, form):
        self.request.actionmenu.add(
            ViewLink(
                label="Revenir au devis",
                path="/estimations/{id}.html",
                id=self.context.id,
            )
        )
        form.set_appstruct(
            {
                'invoice_ids': [
                    str(invoice.id) for invoice in self.context.invoices
                ]
            }
        )

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                '/estimations/{id}.html',
                id=self.context.id,
            )
        )

    def submit_success(self, appstruct):
        invoice_ids = appstruct.get('invoice_ids')
        for invoice_id in invoice_ids:
            invoice = Invoice.get(invoice_id)
            invoice.estimation_id = self.context.id
            self.request.dbsession.merge(invoice)

        if invoice_ids:
            self.context.geninv = True
            self.request.dbsession.merge(self.context)
        return self.redirect()

    def cancel_success(self, appstruct):
        return self.redirect()

    cancel_failure = cancel_success


def estimation_geninv_view(context, request):
    """
    Invoice generation view : used in shorthanded workflow

    :param obj context: The current context (estimation)
    """
    business = context.gen_business()
    invoices = business.gen_invoices(request.user)

    context.geninv = True
    request.dbsession.merge(context)

    if len(invoices) > 1:
        msg = "{0} factures ont été générées".format(len(invoices))
    else:
        msg = "Une facture a été générée"
    request.session.flash(msg)
    request.dbsession.flush()
    return HTTPFound(
        request.route_path('/invoices/{id}', id=invoices[0].id)
    )


def estimation_genbusiness_view(context, request):
    """
    Business generation view : used in long handed workflows

    :param obj context: The current estimation
    """
    logger.info("Generating a business for estimation {}".format(context.id))
    business = context.gen_business()
    return HTTPFound(request.route_path("/businesses/{id}", id=business.id))


def add_routes(config):
    """
    Add module's specific routes
    """
    config.add_route(
        '/estimations/{id}',
        '/estimations/{id:\d+}',
        traverse='/estimations/{id}'
    )
    for extension in ('html', 'pdf', 'preview'):
        config.add_route(
            '/estimations/{id}.%s' % extension,
            '/estimations/{id:\d+}.%s' % extension,
            traverse='/estimations/{id}'
        )
    for action in (
        'addfile',
        'delete',
        'duplicate',
        'admin',
        'geninv',
        'genbusiness',
        'set_metadatas',
        'attach_invoices',
        'set_draft',
        'move',
        "sync_price_study",
    ):
        config.add_route(
            '/estimations/{id}/%s' % action,
            '/estimations/{id:\d+}/%s' % action,
            traverse='/estimations/{id}'
        )


def includeme(config):
    add_routes(config)

    config.add_view(
        EstimationAddView,
        route_name=PROJECT_ITEM_ESTIMATION_ROUTE,
        renderer='tasks/add.mako',
        permission='add_estimation',
        request_param="action=add",
        layout="default"
    )
    config.add_tree_view(
        EstimationEditView,
        parent=BusinessOverviewView,
        renderer='tasks/form.mako',
        permission='view.estimation',
        layout='opa',
    )

    config.add_view(
        EstimationDeleteView,
        route_name='/estimations/{id}/delete',
        permission='delete.estimation',
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        EstimationAdminView,
        route_name='/estimations/{id}/admin',
        renderer="base/formpage.mako",
        permission="admin",
    )

    config.add_view(
        EstimationDuplicateView,
        route_name="/estimations/{id}/duplicate",
        permission="duplicate.estimation",
        renderer='tasks/add.mako',
    )
    config.add_tree_view(
        EstimationHtmlView,
        parent=BusinessOverviewView,
        renderer='tasks/estimation_view_only.mako',
        permission='view.estimation',
    )
    add_panel_page_view(
        config,
        'task_pdf_estimation_content',
        js_resources=(task_html_pdf_css,),
        route_name="/estimations/{id}.preview",
        permission='view.estimation',
    )

    config.add_view(
        EstimationPdfView,
        route_name='/estimations/{id}.pdf',
        permission='view.estimation',
    )

    config.add_view(
        FileUploadView,
        route_name="/estimations/{id}/addfile",
        renderer='base/formpage.mako',
        permission='add.file',
    )

    config.add_view(
        estimation_geninv_view,
        route_name="/estimations/{id}/geninv",
        permission='geninv.estimation',
        request_method='POST',
        require_csrf=True,
    )

    config.add_view(
        estimation_genbusiness_view,
        route_name="/estimations/{id}/genbusiness",
        permission='genbusiness.estimation',
        require_csrf=True,
        request_method="POST",
    )

    config.add_view(
        EstimationSetMetadatasView,
        route_name="/estimations/{id}/set_metadatas",
        permission='view.estimation',
        renderer='tasks/add.mako',
    )
    config.add_view(
        TaskMoveToPhaseView,
        route_name="/estimations/{id}/move",
        permission='view.estimation',
        require_csrf=True,
        request_method="POST",
    )
    config.add_view(
        TaskSetDraftView,
        route_name="/estimations/{id}/set_draft",
        permission="draft.estimation",
        require_csrf=True,
        request_method="POST",
    )

    config.add_view(
        EstimationAttachInvoiceView,
        route_name="/estimations/{id}/attach_invoices",
        permission='view.estimation',
        renderer="/base/formpage.mako",
    )
    config.add_view(
        TaskSyncWithPriceStudyView,
        route_name="/estimations/{id}/sync_price_study",
        permission="edit.estimation",
    )
