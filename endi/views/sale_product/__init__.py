# -*- coding: utf-8 -*-


def includeme(config):
    config.include('.routes')
    config.include('.sale_product')
    config.include('.rest_api')
