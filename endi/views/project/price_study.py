# -*- coding: utf-8 -*-
import colander

from sqlalchemy import (
    distinct,
)
from endi_base.models.base import DBSESSION
from endi.utils.widgets import Link

from endi.models.price_study.price_study import PriceStudy
from endi.forms.price_study.price_study import get_list_schema
from endi.views import (
    BaseListView,
    TreeMixin,
)
from endi.views.project.routes import PROJECT_ITEM_PRICE_STUDY_ROUTE

from endi.views.price_study.routes import PRICE_STUDY_ROUTE
from endi.views.project.lists import ProjectListView
from endi.views.project.project import remember_navigation_history


class ProjectPriceStudyListView(BaseListView, TreeMixin):
    add_template_vars = (
        'stream_actions',
        'add_url',
    )
    schema = get_list_schema()
    sort_columns = {
        "created_at": PriceStudy.created_at,
        'name': PriceStudy.name,
    }
    default_sort = "created_at"
    default_direction = "desc"
    route_name = PROJECT_ITEM_PRICE_STUDY_ROUTE
    item_route_name = PRICE_STUDY_ROUTE

    @property
    def add_url(self):
        return self.request.route_path(
            self.route_name,
            id=self.request.context.id,
            _query={'action': 'add'}
        )

    @property
    def title(self):
        return "Études de prix du dossier {0}".format(self.current().name)

    def current_id(self):
        if hasattr(self.context, 'project_id'):
            return self.context.project_id
        else:
            return self.context.id

    @property
    def tree_url(self):
        return self.request.route_path(self.route_name, id=self.current_id())

    def current(self):
        if hasattr(self.context, 'project'):
            return self.context.project
        else:
            return self.context

    def query(self):
        remember_navigation_history(self.request, self.context.id)
        self.populate_navigation()
        query = DBSESSION().query(distinct(PriceStudy.id), PriceStudy)
        query = query.filter_by(project_id=self.current_id())
        return query

    def filter_search(self, query, appstruct):
        search = appstruct.get('search')
        if search not in ('', None, colander.null):
            query = query.filter(PriceStudy.name.like('%{}%'.format(search)))
        return query

    def filter_with_estimation(self, query, appstruct):
        with_estimation = appstruct.get('with_estimation', False)
        if with_estimation:
            query = PriceStudy.add_filter_with_estimation(query)
        return query

    def stream_actions(self, item):
        yield Link(
            self._get_item_url(item),
            "Voir/Modifier",
            icon="pen",
            css="icon"
        )


def includeme(config):
    config.add_tree_view(
        ProjectPriceStudyListView,
        parent=ProjectListView,
        renderer="endi:templates/project/price_study_list.mako",
        permission="list.price_studies",
        layout='project',
    )
