# -*- coding: utf-8 -*-
"""
Attached files related views
"""
from pyramid.httpexceptions import HTTPFound

from endi.models import files
from endi.models.task import Task
from endi.models.project.business import Business

from endi.views import (
    BaseView,
    TreeMixin,
)

from endi.views.files.views import FileUploadView
from endi.views.project.routes import (
    PROJECT_ITEM_FILE_ROUTE,
    PROJECT_ITEM_ADD_FILE_ROUTE,
)
from endi.views.project.project import ProjectListView
from endi.views.project.project import remember_navigation_history


class ProjectFileAddView(FileUploadView, TreeMixin):
    route_name = PROJECT_ITEM_ADD_FILE_ROUTE

    def __init__(self, *args, **kw):
        FileUploadView.__init__(self, *args, **kw)
        self.populate_navigation()

    def redirect(self, come_from):
        return HTTPFound(
            self.request.route_path(
                self.route_name,
                id=self.context.id,
            )
        )


class ProjectFilesView(BaseView, TreeMixin):
    route_name = PROJECT_ITEM_FILE_ROUTE
    add_route = PROJECT_ITEM_ADD_FILE_ROUTE
    help_message = """
    Liste des fichiers attachés au dossier courant ou à un des documents
    qui le composent."""

    @property
    def title(self):
        return "Fichiers attachés au dossier {0}".format(self.context.name)

    @property
    def tree_url(self):
        return self.request.route_path(self.route_name, id=self.context.id)

    def collect_parent_ids(self):
        """
        Collect the element ids for which we want the associated files
        """
        ids = [
            i[0] for i in self.request.dbsession.query(Task.id).filter_by(
                project_id=self.context.id
            )
        ]
        ids.extend(
            [
                i[0]
                for i in self.request.dbsession.query(Business.id).filter_by(
                    project_id=self.context.id
                )
            ]
        )
        ids.append(self.context.id)
        return ids

    def __call__(self):
        remember_navigation_history(self.request, self.context.id)
        self.populate_navigation()
        ids = self.collect_parent_ids()
        query = files.File.query_for_filetable()
        query = query.filter(files.File.parent_id.in_(ids))

        return dict(
            title=self.title,
            files=query,
            add_url=self.request.route_path(
                self.add_route,
                id=self.context.id,
                _query=dict(action='attach_file')
            ),
            help_message=self.help_message,
        )


def includeme(config):
    config.add_tree_view(
        ProjectFileAddView,
        parent=ProjectFilesView,
        permission="add.file",
        request_param="action=attach_file",
        layout='default',
        renderer="endi:templates/base/formpage.mako",
    )
    config.add_tree_view(
        ProjectFilesView,
        parent=ProjectListView,
        permission="list.files",
        renderer="endi:templates/project/files.mako",
        layout="project",
    )
