# -*- coding: utf-8 -*-
import logging
from collections import OrderedDict
from sqlalchemy import or_

from endi.models.supply.supplier_invoice import SupplierInvoice
from endi.compute.sage import SupplierInvoiceExport
from endi.export.sage import SageSupplierInvoiceCsvWriter
from endi.export.utils import write_file_to_request
from endi.utils.files import get_timestamped_filename
from endi.utils import strings
from endi.utils.widgets import ViewLink
from endi.views.export import BaseExportView
from endi.views.export.utils import (
    get_supplier_invoice_all_form,
    get_supplier_invoice_id_form,
    get_supplier_invoice_form,
)
from endi.views.admin.supplier.accounting import (
    SUPPLIER_ACCOUNTING_URL,
)


logger = logging.getLogger(__name__)

CONFIG_ERROR_MSG = """Une facture fournisseur n'est pas exportable car
certains éléments de configuration n'ont pas été renseignés.<br/>
<a onclick="window.openPopup('{0}');" href='#'>
    Configuration comptable du module Fournisseur
</a>"""
COMPANY_ERROR_MSG = """Le code analytique de l'enseigne {0} n'a pas été
configuré
<a onclick="window.openPopup('{1}');" href='#'>Voir l'enseigne</a>"""


class SageSupplierInvoiceExportPage(BaseExportView):
    """
    Sage SupplierInvoice export views
    """
    title = "Export des factures fournisseurs au format CSV pour Sage"
    config_keys = ('cae_general_supplier_account', 'code_journal_frns',)

    def _populate_action_menu(self):
        self.request.actionmenu.add(
            ViewLink(
                label="Liste des factures fournisseur",
                path='/suppliers_invoices',
            )
        )

    def before(self):
        self._populate_action_menu()

    def _get_forms(
        self,
        prefix='0',
        label="factures fournisseurs",
        genre='e',
        counter=None
    ):
        """
        Generate forms for the given parameters

        :param str prefix: The prefix to give to the form ids
        :param str label: The label of our supplier invoice type
        :param str genre: 'e' or ''
        :returns: A dict with forms in it
            {formid: {'form': form, title:formtitle}}
        :rtype: OrderedDict
        """
        result = OrderedDict()

        main_form = get_supplier_invoice_form(
            self.request,
            title="Exporter les %s d'une enseigne" % label,
            prefix=prefix,
            counter=counter,
        )
        id_form = get_supplier_invoice_id_form(
            self.request,
            main_form.counter,
            title="Exporter les %s depuis un identifiant" % label,
            prefix=prefix,
        )
        all_form = get_supplier_invoice_all_form(
            self.request,
            main_form.counter,
            title="Exporter les %s non exporté%ss" % (label, genre),
            prefix=prefix,
        )

        for form in all_form, main_form, id_form:
            result[form.formid] = {'form': form, 'title': form.schema.title}

        return result

    def get_forms(self):
        """
        Implement parent get_forms method
        """
        result = self._get_forms()
        counter = list(result.values())[0]['form'].counter
        return result

    def _filter_by_supplier_invoice_id(self, query, appstruct):
        """
        Add an id filter on the query
        :param obj query: A sqlalchemy query
        :param dict appstruct: The form datas
        """
        supplier_invoice_id = appstruct['supplier_invoice_id']
        return query.filter(SupplierInvoice.id == supplier_invoice_id)

    def _filter_by_company(self, query, appstruct):
        """
        Add a filter on the company_id
        :param obj query: A sqlalchemy query
        :param dict appstruct: The form datas
        """
        if appstruct.get('company_id', 0) != 0:
            company_id = appstruct['company_id']
            query = query.filter(SupplierInvoice.company_id == company_id)
        return query

    def _filter_by_exported(self, query, appstruct):
        """
        Add a filter on the exported status
        :param obj query: A sqlalchemy query
        :param dict appstruct: The form datas
        """
        if not appstruct.get('exported'):
            query = query.filter_by(exported=False)
        return query

    def query(self, appstruct, form_name):
        """
        Base Query for supplier invoices
        :param appstruct: params passed in the query for the export
        :param str form_name: The submitted form's name
        """
        query = SupplierInvoice.query()
        query = query.filter(SupplierInvoice.status == 'valid')

        if form_name.endswith('_id_form'):
            query = self._filter_by_supplier_invoice_id(query, appstruct)

        elif form_name.endswith('_main_form'):
            query = self._filter_by_company(query, appstruct)

        query = self._filter_by_exported(query, appstruct)
        return query

    def _check_config(self, config):
        """
        Check all configuration values are set for export

        :param config: The application configuration dict
        """
        for key in self.config_keys:
            if not config.get(key):
                return False
        return True

    def _check_company(self, company):
        """
        Check if the company is fully configured

        :param company: The company we are exporting invoices from
        """
        if not company.code_compta:
            company_url = self.request.route_path(
                'company',
                id=company.id,
                _query={'action': 'edit'},
            )
            message = COMPANY_ERROR_MSG.format(company.name, company_url)
            return message
        return None

    def check(self, supplier_invoices):
        """
        Check if we can export the supplier_invoices

        :param supplier_invoices: the supplier_invoices to export
        :returns: a 2-uple (is_ok, messages)
        """
        count = supplier_invoices.count()
        if count == 0:
            title = "Il n'y a aucune facture fournisseur à exporter"
            res = {'title': title, 'errors': []}
            return False, res

        title = "Vous vous apprêtez à exporter {0} \
            factures fournisseurs".format(count)
        errors = []

        if not self._check_config(self.request.config):
            errors.append(CONFIG_ERROR_MSG.format(SUPPLIER_ACCOUNTING_URL))

        for invoice in supplier_invoices:
            company = invoice.company
            error = self._check_company(company)
            if error is not None:
                errors.append(
                    "La facture fournisseur de {0} n'est pas exportable "
                    "<br />{1}".format(
                        strings.format_company(invoice.company),
                        error
                    )
                )

        res = {'title': title, 'errors': errors}
        return len(errors) == 0, res

    def record_exported(self, supplier_invoices, form_name, appstruct):
        """
        Tag the exported supplier_invoices

        :param supplier_invoices: The supplier_invoices we are exporting
        """
        for supplier_invoice in supplier_invoices:
            logger.info("The supplier invoice with id {0} has been exported".format(
                supplier_invoice.id))
            supplier_invoice.exported = True

            self.request.dbsession.merge(supplier_invoice)

    def write_file(self, supplier_invoices, form_name, appstruct):
        exporter = SupplierInvoiceExport(self.context, self.request)
        writer = SageSupplierInvoiceCsvWriter(self.context, self.request)
        writer.set_datas(exporter.get_book_entries(supplier_invoices))
        write_file_to_request(
            self.request,
            get_timestamped_filename("export_factures_frn", writer.extension),
            writer.render(),
            headers="application/csv",
        )
        return self.request.response


def add_routes(config):
    config.add_route(
        '/export/treasury/supplier_invoices',
        '/export/treasury/supplier_invoices'
    )
    config.add_route(
        '/export/treasury/supplier_invoices/{id}',
        '/export/treasury/supplier_invoices/{id}'
    )


def add_views(config):
    config.add_view(
        SageSupplierInvoiceExportPage,
        route_name='/export/treasury/supplier_invoices',
        renderer='/export/main.mako',
        permission='admin_treasury',
    )


def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='accounting',
        order=4,
        label="Export des factures fournisseurs",
        href="/export/treasury/supplier_invoices",
        permission="admin_treasury",
    )
