# -*- coding: utf-8 -*-
import logging
from collections import OrderedDict

from endi.export.utils import write_file_to_request
from endi.utils.files import get_timestamped_filename
from endi.models.supply.payment import SupplierPayment
from endi.compute.sage import SupplierPaymentExport
from endi.export.sage import SageSupplierPaymentCsvWriter
from endi.utils import strings
from endi.utils.widgets import ViewLink
from endi.views.export.utils import (
    get_period_form,
    get_all_form,
    get_supplier_invoice_id_form,
)
from endi.views.export import BaseExportView

logger = logging.getLogger(__name__)


ERR_COMPANY_CONFIG = """Un paiement de la facture fournisseur {0}
n'est pas exportable : Le code analytique de l'enseigne {1} n'a pas été
configuré
<a onclick="window.openPopup('{2}');" href='#'>Voir l'enseigne</a>"""
ERR_BANK_CONFIG = """Un paiement de la facture fournisseur {0}
n'est pas exportable : Le paiement n'est associé à aucune banque
<a onclick="window.openPopup('{1}');" href='#'>Voir le paiement</a>"""


class SageSupplierPaymentExportPage(BaseExportView):
    """
    Provide an supplier payment export page
    """
    title = "Export des règlements des factures fournisseurs \
        au format CSV pour Sage"

    def _populate_action_menu(self):
        self.request.actionmenu.add(
            ViewLink(
                label="Liste des factures fournisseurs",
                path='/suppliers_invoices',
            )
        )

    def before(self):
        self._populate_action_menu()

    def get_forms(self):
        """
        Implement parent get_forms method
        """
        result = OrderedDict()
        period_form = get_period_form(
            self.request,
            title="Exporter les paiements fournisseurs d'une période donnée"
        )
        supplier_invoice_id_form = get_supplier_invoice_id_form(
            self.request,
            period_form.counter,
            title="Exporter les paiements d'une facture fournisseur",
        )
        all_form = get_all_form(
            self.request,
            period_form.counter,
            title="Exporter les paiements fournisseurs non exportés",
        )
        for form in all_form, period_form, supplier_invoice_id_form:
            result[form.formid] = {'form': form, 'title': form.schema.title}
        return result

    def _filter_date(self, query, start_date, end_date):
        return query.filter(
            SupplierPayment.created_at.between(start_date, end_date)
        )

    def _filter_number(self, query, supplier_invoice_id):
        return query.filter(
            SupplierPayment.supplier_invoice_id == supplier_invoice_id
        )

    def query(self, query_params_dict, form_name):
        """
            Retrieve the exports we want to export
        """
        query = SupplierPayment.query()

        if form_name == 'period_form':
            start_date = query_params_dict['start_date']
            end_date = query_params_dict['end_date']
            query = self._filter_date(query, start_date, end_date)

        elif form_name == 'supplier_invoice_id_form':
            supplier_invoice_id = query_params_dict['supplier_invoice_id']
            query = self._filter_number(query, supplier_invoice_id)

        if 'exported' not in query_params_dict or \
                not query_params_dict.get('exported'):
            query = query.filter_by(exported=False)

        return query

    def _check_bank(self, supplier_payment):
        if not supplier_payment.bank:
            return False
        return True

    def _check_company(self, company):
        if not company.code_compta:
            return False
        return True

    def check(self, supplier_payments):
        """
        Check that the given supplier_payments can be exported

        :param obj supplier_payments: A SQLA query of SupplierPayment objects
        """
        count = supplier_payments.count()
        if count == 0:
            title = "Il n'y a aucun paiement fournisseur à exporter"
            res = {
                'title': title,
                'errors': [],
            }
            return False, res

        title = "Vous vous apprêtez à exporter {0} \
            paiements fournisseurs".format(count)
        res = {'title': title, 'errors': []}

        for payment in supplier_payments:
            supplier_invoice = payment.supplier_invoice
            company = supplier_invoice.company
            if not self._check_company(company):
                company_url = self.request.route_path(
                    "company",
                    id=company.id,
                    _query={'action': 'edit'},
                )
                message = ERR_COMPANY_CONFIG.format(
                    supplier_invoice.id,
                    company.name,
                    company_url,
                )
                res['errors'].append(message)
                continue
            if not self._check_bank(payment):
                payment_url = self.request.route_path(
                    'supplier_payment',
                    id=payment.id,
                    _query={'action': 'edit'}
                )
                message = ERR_BANK_CONFIG.format(
                    supplier_invoice.id,
                    payment_url)
                res['errors'].append(message)
                continue

        return len(res['errors']) == 0, res

    def record_exported(self, supplier_payments, form_name, appstruct):
        """
        Record that those supplier_payments have already been exported
        """
        for payment in supplier_payments:
            logger.info(
                "The supplier payment id : {0} (supplier invoice {1}) \
                    has been exported"
                .format(
                    payment.id,
                    payment.supplier_invoice.id,
                )
            )
            payment.exported = True
            self.request.dbsession.merge(payment)

    def write_file(self, supplier_payments, form_name, appstruct):
        """
        Write the exported csv file to the request
        """
        exporter = SupplierPaymentExport(self.context, self.request)
        writer = SageSupplierPaymentCsvWriter(self.context, self.request)
        writer.set_datas(exporter.get_book_entries(supplier_payments))
        write_file_to_request(
            self.request,
            get_timestamped_filename("export_paiements_frn", writer.extension),
            writer.render(),
            headers="application/csv")
        return self.request.response


def add_routes(config):
    config.add_route(
        '/export/treasury/supplier_payments',
        '/export/treasury/supplier_payments'
    )
    config.add_route(
        '/export/treasury/supplier_payments/{id}',
        '/export/treasury/supplier_payments/{id}'
    )


def add_views(config):
    config.add_view(
        SageSupplierPaymentExportPage,
        route_name='/export/treasury/supplier_payments',
        renderer='/export/main.mako',
        permission='admin_treasury',
    )


def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='accounting',
        order=5,
        label="Export des paiements de facture fournisseur",
        href="/export/treasury/supplier_payments",
        permission="admin_treasury",
    )
