BPF_EXPORT_ODS_URL = '/export/training/bpf.ods'


def includeme(config):
    for route in [
            BPF_EXPORT_ODS_URL,
    ]:
        config.add_route(route, route)
