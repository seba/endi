# -*- coding: utf-8 -*-
import logging
from collections import OrderedDict

from endi.export.utils import write_file_to_request
from endi.utils.files import get_timestamped_filename

from endi.models.expense.payment import ExpensePayment
from endi.compute.sage import ExpensePaymentExport
from endi.export.sage import SageExpensePaymentCsvWriter
from endi.utils import strings
from endi.utils.widgets import ViewLink

from endi.views.user.routes import USER_ACCOUNTING_URL
from endi.views.admin.expense.accounting import (
    EXPENSE_PAYMENT_ACCOUNTING_URL,
)
from endi.views.export.utils import (
    get_period_form,
    get_all_form,
    get_expense_id_form,
)

from endi.views.export import BaseExportView

logger = logging.getLogger(__name__)


ERR_COMPANY_CONFIG = """Un paiement de la note de dépenses {0}
n'est pas exportable : Le code analytique de l'enseigne {1} n'a pas été
configuré
<a onclick="window.openPopup('{2}');" href='#'>Voir l'enseigne</a>"""
ERR_USER_CONFIG = """Un paiement de la note de dépense {0}
n'est pas exportable : Le compte tiers de l'entrepreneur {1} n'a pas été
configuré
<a onclick="window.openPopup('{2}');" href='#'>Voir l'entrepreneur</a>"""

ERR_BANK_CONFIG = """Un paiement de la note de dépense {0}
n'est pas exportable : Le paiement n'est associé à aucune banque
<a onclick="window.openPopup('{1}');" href='#'>Voir le paiement</a>"""
ERR_WAIVER_CONFIG = """Le compte pour les abandons de créances n'a pas
été configuré, vous pouvez le configurer
<a onclick="window.openPopup('{}');" href='#'>Ici</a>
"""


class SageExpensePaymentExportPage(BaseExportView):
    """
    Provide an expense payment export page
    """
    title = "Export des règlements de notes de frais au format CSV pour Sage"
    admin_route_name = EXPENSE_PAYMENT_ACCOUNTING_URL

    def _populate_action_menu(self):
        self.request.actionmenu.add(
            ViewLink(
                label="Liste des notes de dépenses",
                path='expenses',
            )
        )

    def before(self):
        self._populate_action_menu()

    def get_forms(self):
        """
        Implement parent get_forms method
        """
        result = OrderedDict()
        period_form = get_period_form(
            self.request,
            title="Exporter les paiements saisis sur la période donnée"
        )
        expense_id_form = get_expense_id_form(
            self.request,
            period_form.counter,
            title="Exporter les paiements correspondant à une note de dépense",
        )
        all_form = get_all_form(
            self.request,
            period_form.counter,
            title="Exporter les paiements non exportés",
        )
        for form in period_form, expense_id_form, all_form:
            result[form.formid] = {'form': form, 'title': form.schema.title}
        return result

    def _filter_date(self, query, start_date, end_date):
        return query.filter(
            ExpensePayment.created_at.between(start_date, end_date)
        )

    def _filter_number(self, query, sheet_id):
        return query.filter(ExpensePayment.expense_sheet_id == sheet_id)

    def query(self, query_params_dict, form_name):
        """
            Retrieve the exports we want to export
        """
        query = ExpensePayment.query()

        if form_name == 'period_form':
            start_date = query_params_dict['start_date']
            end_date = query_params_dict['end_date']
            query = self._filter_date(query, start_date, end_date)

        elif form_name == 'expense_id_form':
            sheet_id = query_params_dict['sheet_id']
            query = self._filter_number(query, sheet_id)

        if 'exported' not in query_params_dict or \
                not query_params_dict.get('exported'):
            query = query.filter_by(exported=False)

        return query

    def _check_bank(self, payment):
        if not payment.bank and not payment.waiver:
            return False
        return True

    def _check_company(self, company):
        if not company.code_compta:
            return False
        return True

    def _check_user(self, user):
        if not user.compte_tiers:
            return False
        return True

    def _check_waiver(self, payment):
        """
        Check that the wayver cg account has been configured
        """
        if not self.request.config.get('compte_cg_waiver_ndf'):
            return False
        return True

    def check(self, payments):
        """
        Check that the given expense_payments can be exported

        :param obj payments: A SQLA query of ExpensePayment objects
        """
        count = payments.count()
        if count == 0:
            title = "Il n'y a aucun paiement à exporter"
            res = {
                'title': title,
                'errors': [],
            }
            return False, res

        title = "Vous vous apprêtez à exporter {0} paiements".format(
                count)
        res = {'title': title, 'errors': []}

        for payment in payments:
            expense = payment.expense
            company = expense.company
            if not self._check_company(company):
                company_url = self.request.route_path(
                    "company",
                    id=company.id,
                    _query={'action': 'edit'},
                )
                message = ERR_COMPANY_CONFIG.format(
                    expense.id,
                    company.name,
                    company_url,
                )
                res['errors'].append(message)
                continue

            user = expense.user
            if not self._check_user(user):
                user_url = self.request.route_path(
                    USER_ACCOUNTING_URL,
                    id=user.id,
                    _query={'action': 'edit'},
                )
                message = ERR_USER_CONFIG.format(
                    expense.id,
                    strings.format_account(user),
                    user_url,
                )
                res['errors'].append(message)
                continue

            if not self._check_bank(payment):
                payment_url = self.request.route_path(
                    'expense_payment',
                    id=payment.id,
                    _query={'action': 'edit'}
                )
                message = ERR_BANK_CONFIG.format(
                    expense.id,
                    payment_url)
                res['errors'].append(message)
                continue

            if payment.waiver and not self._check_waiver(payment):
                admin_url = self.request.route_path(
                    self.admin_route_name
                )
                message = ERR_WAIVER_CONFIG.format(admin_url)
                res['errors'].append(message)
                continue

        return len(res['errors']) == 0, res

    def record_exported(self, payments, form_name, appstruct):
        """
        Record that those payments have already been exported
        """
        for payment in payments:
            logger.info(
                "The payment id : {0} (expense {1}) has been exported"
                .format(
                    payment.id,
                    payment.expense.id,
                )
            )
            payment.exported = True
            self.request.dbsession.merge(payment)

    def write_file(self, payments, form_name, appstruct):
        """
        Write the exported csv file to the request
        """
        exporter = ExpensePaymentExport(self.context, self.request)
        writer = SageExpensePaymentCsvWriter(self.context, self.request)
        writer.set_datas(exporter.get_book_entries(payments))
        write_file_to_request(
            self.request,
            get_timestamped_filename("export_paiement_ndf", writer.extension),
            writer.render(),
            headers="application/csv")
        return self.request.response


def add_routes(config):
    config.add_route(
        '/export/treasury/expense_payments',
        '/export/treasury/expense_payments'
    )
    config.add_route(
        '/export/treasury/expense_payments/{id}',
        '/export/treasury/expense_payments/{id}'
    )


def add_views(config):
    config.add_view(
        SageExpensePaymentExportPage,
        route_name='/export/treasury/expense_payments',
        renderer='/export/main.mako',
        permission='admin_treasury',
    )


def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent='accounting',
        order=3,
        label="Export des paiements de dépense",
        href="/export/treasury/expense_payments",
        permission="admin_treasury",
    )

