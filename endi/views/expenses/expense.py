# -*- coding: utf-8 -*-
"""
    Expense handling view
"""
import logging
from pyramid.httpexceptions import (
    HTTPFound,
)

from endi.forms.expense import (
    ExpensePaymentSchema,
    get_add_edit_sheet_schema,
)
from endi.utils import strings
from endi.models.company import Company
from endi.models.expense.sheet import (
    ExpenseLine,
    ExpenseSheet,
    get_expense_sheet_name,
)
from endi.models.expense.payment import ExpensePayment
from endi.models.expense.types import (
    ExpenseTelType,
)
from endi.models.user.user import User
from endi.events.status_changed import StatusChanged
from endi.utils.widgets import (
    ViewLink,
)
from endi.export.expense_excel import (
    XlsExpense,
)
from endi.export.excel import (
    make_excel_view,
)
from endi.resources import (
    expense_resources,
)
from endi.views import (
    BaseFormView,
    BaseView,
    DeleteView,
)
from endi.views.render_api import (
    month_name,
    format_account,
)
from endi.views.files.views import (
    FileUploadView,
)


logger = logging.getLogger(__name__)


def get_expense_sheet(year, month, cid, uid):
    """
        Return the expense sheet for the given 4-uple
    """
    return ExpenseSheet.query()\
        .filter(ExpenseSheet.year == year)\
        .filter(ExpenseSheet.month == month)\
        .filter(ExpenseSheet.company_id == cid)\
        .filter(ExpenseSheet.user_id == uid).first()


def get_new_expense_sheet(year, month, cid, uid):
    """
        Return a new expense sheet for the given 4-uple
    """
    expense = ExpenseSheet()
    expense.name = get_expense_sheet_name(month, year)
    expense.year = year
    expense.month = month
    expense.company_id = cid
    expense.user_id = uid
    query = ExpenseTelType.query()
    query = query.filter(ExpenseTelType.active == True)
    teltypes = query.filter(ExpenseTelType.initialize == True)
    for type_ in teltypes:
        line = ExpenseLine(
            type_id=type_.id,
            ht=0,
            tva=0,
            description=type_.label
        )
        expense.lines.append(line)
    return expense


def notify_status_changed(request, status):
    """
    Fire An ExpenseStatusChanged event

    :param obj request: The Pyramid request object
    :param str status: The new status
    """
    request.registry.notify(
        StatusChanged(request, request.context, status)
    )


def get_redirect_btn(request, id_):
    """
        Button for "go back to project" link
    """


def populate_actionmenu(request, tolist=False):
    """
        Add buttons in the request actionmenu attribute
    """
    link = None
    if isinstance(request.context, Company):
        link = ViewLink(
            "Revenir à la liste des dépenses",
            path="company_expenses",
            id=request.context.id
        )
    elif isinstance(request.context, ExpenseSheet):
        if tolist:
            link = ViewLink(
                "Revenir à la liste des dépenses",
                path="company_expenses",
                id=request.context.company_id
            )
        else:
            link = ViewLink(
                "Revenir à la note de dépenses",
                path="/expenses/{id}",
                id=request.context.id
            )
    if link is not None:
        request.actionmenu.add(link)


class ExpenseSheetAddView(BaseFormView):
    """
    A simple expense sheet add view
    """
    schema = get_add_edit_sheet_schema()

    @property
    def title(self):
        user = User.get(self.request.matchdict['uid'])
        return 'Ajouter une note de dépenses ({})'.format(
            user.label,
        )

    def before(self, form):
        populate_actionmenu(self.request)

    def redirect(self, sheet):
        return HTTPFound(
            self.request.route_path('/expenses/{id}', id=sheet.id)
        )

    def create_instance(self, appstruct):
        """
        Create a new expense sheet instance
        """
        result = get_new_expense_sheet(
            appstruct['year'],
            appstruct['month'],
            self.context.id,
            self.request.matchdict['uid']
        )
        return result

    def submit_success(self, appstruct):
        sheet = self.create_instance(appstruct)
        self.dbsession.add(sheet)
        self.dbsession.flush()
        return self.redirect(sheet)

    def submit_failure(self, e):
        BaseFormView.submit_failure(self, e)


class ExpenseSheetEditView(BaseView):
    def title(self):
        return "Notes de dépense de {0} pour la période de {1} {2}"\
            .format(
                format_account(self.request.context.user),
                month_name(self.context.month),
                self.context.year,
            )

    def context_url(self):
        return self.request.route_path(
            '/api/v1/expenses/{id}',
            id=self.request.context.id
        )

    def form_config_url(self):
        return self.request.route_path(
            '/api/v1/expenses/{id}',
            id=self.request.context.id,
            _query={'form_config': '1'}
        )

    def __call__(self):
        # if not self.request.has_permission('edit.expense'):
        #    return HTTPFound(self.request.current_route_path() + '.html')
        populate_actionmenu(self.request, tolist=True)
        expense_resources.need()
        return dict(
            context=self.context,
            title=self.title(),
            context_url=self.context_url(),
            form_config_url=self.form_config_url(),
            communication_history=self.context.communications,
        )


class ExpenseSheetDeleteView(DeleteView):
    """
    Expense deletion view

    Current context is an expensesheet
    """
    delete_msg = "La note de frais a bien été supprimée"

    def redirect(self):
        url = self.request.route_path(
            'company_expenses',
            id=self.context.company.id
        )
        return HTTPFound(url)


class ExpenseSheetDuplicateView(BaseFormView):
    form_options = (('formid', 'duplicate_form'),)
    schema = get_add_edit_sheet_schema()

    @property
    def title(self):
        return "Dupliquer la note de dépenses de {0} {1}".format(
            strings.month_name(self.context.month),
            self.context.year,
        )

    def before(self, form):
        populate_actionmenu(self.request)

    def redirect(self, sheet):
        return HTTPFound(
            self.request.route_path('/expenses/{id}', id=sheet.id)
        )

    def submit_success(self, appstruct):
        logger.debug("# Duplicating an expensesheet #")
        sheet = self.context.duplicate(appstruct['year'], appstruct['month'])
        self.dbsession.add(sheet)
        self.dbsession.flush()
        logger.debug(
            "ExpenseSheet {0} was duplicated to {1}".format(
                self.context.id, sheet.id
            )
        )
        return self.redirect(sheet)

    def submit_failure(self, e):
        BaseFormView.submit_failure(self, e)


class ExpenseSheetPaymentView(BaseFormView):
    """
    Called for setting a payment on an expensesheet
    """
    schema = ExpensePaymentSchema()
    title = "Saisie d'un paiement"

    def before(self, form):
        populate_actionmenu(self.request)

    def redirect(self, come_from):
        if come_from:
            return HTTPFound(come_from)
        else:
            return HTTPFound(
                self.request.route_path(
                    "/expenses/{id}", id=self.request.context.id
                )
            )

    def submit_success(self, appstruct):
        """
        Create the payment
        """
        logger.debug("+ Submitting an expense payment")
        logger.debug(appstruct)
        come_from = appstruct.pop('come_from', None)
        force_resulted = appstruct.pop('resulted', None)
        payment = ExpensePayment(
            user_id=self.request.user.id,
            **appstruct
        )
        self.context.record_payment(
            payment,
            force_resulted=force_resulted,
        )
        self.dbsession.merge(self.context)
        self.request.session.flash("Le paiement a bien été enregistré")
        notify_status_changed(self.request, self.context.paid_status)
        return self.redirect(come_from)


def excel_filename(request):
    """
        return an excel filename based on the request context
    """
    exp = request.context
    return "ndf_{0}_{1}_{2}_{3}.xlsx".format(
        exp.year,
        exp.month,
        exp.user.lastname,
        exp.user.firstname,
    )


def add_routes(config):
    """
    Add module's related routes
    """
    config.add_route("expenses", "/expenses")

    config.add_route(
        "user_expenses",
        "/company/{id}/{uid}/expenses",
        traverse='/companies/{id}'
    )

    config.add_route(
        "/expenses/{id}",
        "/expenses/{id:\d+}",
        traverse="/expenses/{id}",
    )

    for extension in ('xls',):
        config.add_route(
            "/expenses/{id}.xlsx",
            "/expenses/{id:\d+}.xlsx",
            traverse="/expenses/{id}",
        )

    for action in (
        'delete',
        'duplicate',
        'addpayment',
        'addfile',
    ):
        config.add_route(
            "/expenses/{id}/%s" % action,
            "/expenses/{id:\d+}/%s" % action,
            traverse="/expenses/{id}",
        )


def includeme(config):
    """
        Declare all the routes and views related to this module
    """
    add_routes(config)

    config.add_view(
        ExpenseSheetAddView,
        route_name="user_expenses",
        permission="add.expense",
        renderer="base/formpage.mako",
    )

    config.add_view(
        ExpenseSheetEditView,
        route_name="/expenses/{id}",
        renderer="expenses/expense.mako",
        permission="view.expensesheet",
        layout="opa",
    )

    config.add_view(
        ExpenseSheetDeleteView,
        route_name="/expenses/{id}/delete",
        permission="delete.expensesheet",
        request_method="POST",
        require_csrf=True,
    )

    config.add_view(
        ExpenseSheetDuplicateView,
        route_name="/expenses/{id}/duplicate",
        renderer="base/formpage.mako",
        permission="view.expensesheet",
    )

    config.add_view(
        ExpenseSheetPaymentView,
        route_name="/expenses/{id}/addpayment",
        permission="add_payment.expensesheet",
        renderer="base/formpage.mako",
    )

    # Xls export
    config.add_view(
        make_excel_view(excel_filename, XlsExpense),
        route_name="/expenses/{id}.xlsx",
        permission="view.expensesheet",
    )
    # File attachment
    config.add_view(
        FileUploadView,
        route_name="/expenses/{id}/addfile",
        renderer='base/formpage.mako',
        permission='add.file',
    )
