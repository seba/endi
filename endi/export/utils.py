# -*- coding: utf-8 -*-
"""
Export utilities:

    * Tools to build file responses (pdf, xls ...)
"""
from endi_base.utils.ascii import (
        force_ascii,
        )
import mimetypes


def detect_file_headers(filename):
    """
        Return the headers adapted to the given filename
    """
    mimetype = mimetypes.guess_type(filename)[0] or "text/plain"
    return mimetype


def write_headers(request, filename, header):
    """
        Write the given headers to the current request
    """
    # Here enforce ascii chars and string object as content type
    header = force_ascii(header)
    request.response.content_type = str(header)
    request.response.headerlist.append(
            ('Content-Disposition',
             'attachment; filename="{0}"'.format(force_ascii(filename))))
    return request


def get_buffer_value(filebuffer):
    """
    Return the content of the given filebuffer, handles the different
    interfaces between opened files and BytesIO containers
    """
    if hasattr(filebuffer, 'getvalue'):
        return filebuffer.getvalue()
    elif hasattr(filebuffer, 'read'):
        return filebuffer.read()
    else:
        raise Exception("Unknown file buffer type")


def write_file_to_request(request, filename, buf, headers=None):
    """
        Write a buffer as request content
        :param request: Pyramid's request object
        :param filename: The destination filename
        :param buf: The file buffer mostly BytesIO object, should provide a
            getvalue method
        :param headers: Headers to pass to the request
            (automatic detection is provided for many types)
    """
    # Ref #384 : 'text/plain' is the default stored in the db
    if headers is None or headers == 'text/plain':
        headers = detect_file_headers(filename)
    request = write_headers(request, filename, headers)
    request.response.write(get_buffer_value(buf))
    return request
