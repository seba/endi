import io
from PyPDF4 import PdfFileReader, PdfFileWriter
from endi.utils.pdf import (
    fetch_resource,
    HTMLWithHeadersAndFooters,
    Overlay,
    weazyprint_pdf_css,
)


def _pdf_renderer(task, request):
    footer = Overlay(
        panel_name='task_pdf_footer',
        context_dict={"context": task},
    )
    content = request.layout_manager.render_panel(
        'task_pdf_%s_content' % task.type_, context=task
    )
    html_object = HTMLWithHeadersAndFooters(
        request,
        content,
        footer_overlay=footer,
        url_fetcher=fetch_resource,
        base_url="test"
    )
    return html_object


def task_weasyprint_document(task, request):
    """
    Build a Weasyprint Document instance from the task object

    :param obj task: A Task instance
    :rtype: weasyprint.Document
    """
    html_object = _pdf_renderer(task, request)
    return html_object.render(stylesheets=weazyprint_pdf_css())


def task_pdf(task, request):
    """
    Generates the pdf output for a given task

    :rtype: io.BytesIO instance
    """
    result = io.BytesIO()
    html_object = _pdf_renderer(task, request)
    html_object.write_pdf(result, stylesheets=weazyprint_pdf_css())
    result.seek(0)
    return result


# Fonctions relatives à l'Export massif
def _get_pages_without_cgv(pdf_reader):
    """
    return A Pdf buffer with the task pdf datas without cgv

    :param obj pdf_buffer: Bytes format pdf buffer
    """
    pdf_reader = PdfFileReader(pdf_buffer)

    cgv_outline = None
    for outline in pdf_reader:
        if isinstance(outline, Destination):
            if outline.title == 'CGV':
                cgv_outline = outline
                break

    if cgv_outline is not None:
        cgv_page_number = pdf_reader.getDestinationPageNumber(outline) + 1
        return pdf_reader.pages[0:cgv_page_number]

    else:
        return pdf_reader.pages


def task_bulk_pdf(tasks):
    """
    Produce a pdf containing pdf infos for the given tasks

    :param list tasks: list of Task objects
    """
    pdf_writer = PdfFileWriter()
    for task in tasks:
        reader = PdfFileReader(task.file_object.getvalue())
        for page in _get_pages_without_cgv(reader):
            pdf_writer.addPage(page)
    result = io.BytesIO()
    pdf_writer.write(result)
    return result
