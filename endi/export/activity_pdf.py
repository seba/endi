"""
Utilities used to export an activity in pdf format
"""
import io
from endi.utils.pdf import (
    fetch_resource,
    HTMLWithHeadersAndFooters,
    Overlay,
    weazyprint_pdf_css,
)


def _pdf_renderer(activity, request):
    """
    Build a Weazyprint html to pdf renderer specific to activities. Here we
    need a custom footer
    """
    footer = Overlay(
        panel_name='activity_pdf_footer',
        context_dict={"context": activity},
    )
    content = request.layout_manager.render_panel(
        'activity_pdf_content', context=activity
    )
    html_object = HTMLWithHeadersAndFooters(
        request,
        content,
        footer_overlay=footer,
        url_fetcher=fetch_resource,
        base_url="test"
    )
    return html_object


def activity_weasyprint_document(activity, request):
    """
    Build a Weasyprint Document instance from the activity object

    :param obj activity: A activity instance
    :rtype: weasyprint.Document
    """
    html_object = _pdf_renderer(activity, request)
    return html_object.render(stylesheets=weazyprint_pdf_css())


def activity_pdf(activity, request):
    """
    Generates the pdf output for a given activity

    :rtype: io.BytesIO instance
    """
    result = io.BytesIO()
    html_object = _pdf_renderer(activity, request)
    html_object.write_pdf(result, stylesheets=weazyprint_pdf_css())
    result.seek(0)
    return result
