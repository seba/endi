# -*- coding: utf-8 -*-
import logging

import pkg_resources
from endi.resources import (
    main_group,
    opa_group,
)

logger = logging.getLogger(__name__)


class DefaultLayout(object):
    endi_version = pkg_resources.get_distribution('endi').version

    def __init__(self, context, request):
        main_group.need()
        self.context = context
        self.request = request


class OpaLayout(object):
    endi_version = pkg_resources.get_distribution('endi').version

    def __init__(self, context, request):
        opa_group.need()
        self.context = context
        self.request = request


def includeme(config):
    config.add_layout(
        DefaultLayout,
        template='endi:templates/layouts/default.mako'
    )
    config.add_layout(
        DefaultLayout,
        template='endi:templates/layouts/default.mako',
        name='default',
    )
    config.add_layout(
        OpaLayout,
        template='endi:templates/layouts/default.mako',
        name='opa'
    )
    config.add_layout(
        DefaultLayout,
        template='endi:templates/layouts/login.mako',
        name='login',
    )
