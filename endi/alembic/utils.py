# -*- coding: utf-8 -*-
from alembic import op
import sqlalchemy as sa


def force_rename_table(old, new):
    from alembic.context import get_bind
    conn = get_bind()
    if table_exists(old):
        if table_exists(new):
            op.drop_table(new)
        op.rename_table(old, new)


def table_exists(tbl):
    from alembic.context import get_bind
    conn = get_bind()
    ret = False
    try:
        conn.execute("select * from `%s`" % tbl)
        ret = True
    except:
        pass
    return ret


def rename_column(tbl, column_name, name, type_=sa.Integer, nullable=False,
                  autoincrement=False, **kw):
    if column_exists(tbl, column_name):
        if autoincrement:
            op.execute("Alter table `%s` change `%s` `%s` int(11) NOT NULL "
                       "AUTO_INCREMENT;" % (tbl, column_name, name))
        else:
            op.alter_column(tbl, column_name, new_column_name=name, type_=type_,
                            nullable=nullable, **kw)


def column_exists(tbl, column_name):
    from alembic.context import get_bind
    conn = get_bind()
    ret = False
    try:
        conn.execute("select %s from %s" % (column_name, tbl))
        ret = True
    except:
        pass
    return ret


def add_column(tbl, column):
    if not column_exists(tbl, column.name):
        op.add_column(tbl, column)


def disable_constraints():
    op.execute("SET FOREIGN_KEY_CHECKS=0;")


def enable_constraints():
    op.execute("SET FOREIGN_KEY_CHECKS=1;")

def drop_foreign_key_if_exists(table, fkey_name):
    from alembic.context import get_bind
    conn = get_bind()

    schema = conn.engine.url.database

    result = conn.execute(
        "select * from information_schema.REFERENTIAL_CONSTRAINTS where "
        "CONSTRAINT_SCHEMA='%s' and REFERENCED_TABLE_NAME = '%s' "
        "and CONSTRAINT_NAME = '%s';" % (schema, table, fkey_name)
    )
    if result.fetchone():
        op.drop_constraint(table, fkey_name, type_='foreignkey')
        result = True
    else:
        result = False
    return result


def drop_index_if_exists(table, index_name):
    from alembic.context import get_bind
    conn = get_bind()

    schema = conn.engine.url.database

    result = conn.execute(
        "select * from information_schema.statistics where "
        "TABLE_SCHEMA='%s' and TABLE_NAME = '%s' "
        "and INDEX_NAME = '%s';" % (schema, table, index_name)
    )
    if result.fetchone():
        op.drop_index(index_name, table_name=table)
        result = True
    else:
        result = False
    return result
