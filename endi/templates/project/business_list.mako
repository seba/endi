<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
% if request.has_permission('add.estimation') or request.has_permission('add.invoice'):
	<div class='layout flex main_actions'>
		<div role='group'>
            % if request.has_permission('add.price_study'):
				<a class='btn btn-primary' href='${add_price_study_url}'>
					<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#calculator"></use></svg> Créer une étude de prix
				</a>
            % endif
			% if request.has_permission('add.estimation'):
				<a class='btn btn-primary' href='${add_estimation_url}'>
					<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-list"></use></svg> Créer un devis
				</a>
			% endif
			% if request.has_permission('add.invoice'):
				<a class='btn btn-primary' href='${add_invoice_url}'>
					<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-invoice-euro"></use></svg> Créer une facture
				</a>
			% endif

		</div>
	</div>
% endif
</%block>

<%block name='mainblock'>

${searchform()}

<div>
    <div>
    	${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
		<table class="top_align_table hover_table">
			<thead>
				<tr>
					<th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
					<th scope="col" class="col_date">${sortable(u"Créé le", "created_at")}</th>
					<th scope="col" class="col_text">${sortable(u"Nom", "name")}</th>
					<th scope="col" class="col_text">Documents</th>
					<th scope="col" class="col_number">CA</th>
					<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
				</tr>
			</thead>
			<tbody>
				% if records:
					% for id, business in records:
						<tr class='tableelement business-closed-${business.closed} business-check-${business.file_requirement_service.check(business)}'>
							% if business.closed:
								<td class="col_status" title="Affaire clôturée">
									<span class="icon status draft"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder"></use></svg></span>
							% else:
								<td class="col_status" title="Affaire en cours">
									<span class="icon status valid"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder-open"></use></svg></span>
							% endif
							</td>
							<td class="col_date">${api.format_date(business.created_at)}</td>
							<td class="col_text">${business.name}</td>
							<td class="col_text">
								<ul>
									% for estimation in business.estimations:
										<li>Devis : ${estimation.name}</li>
									% endfor
									% for invoice in business.invoices:
										<li>
											${api.format_task_type(invoice)}
											% if invoice.official_number:
												n<span class="screen-reader-text">umér</span><sup>o</sup> ${invoice.official_number}
											% endif
											: ${invoice.name}
										</li>
									% endfor
								</ul>
							</td>
							<td class="col_number">
								${api.format_amount(sum([i.ttc for i in business.invoices]), precision=5)}&nbsp;€
							</td>
							<td class='col_actions width_one'>
								${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(business))}
							</td>
						</tr>
					% endfor
				% else:
					<tr>
						<td colspan='7' class="col_text"><em>Aucune affaire n’a été initiée pour l’instant</em></td>
					</tr>
				% endif
			</tbody>
		</table>
    </div>
    ${pager(records)}
</div>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>
