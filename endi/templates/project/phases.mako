<%inherit file="${context['main_template'].uri}" />

<%block name='mainblock'>
<div class='project-view'>

    <div class='panel-group' id='phase_accordion'>
        % for phase in phases:
            % if phase.id == latest_phase_id:
                <% section_css = 'in collapse' %>
            % else:
                <% section_css = 'collapse' %>
            % endif
            <div class='collapsible separate_bottom'>
                <h3 class='collapse_title'>
                    <a href="#phase_${phase.id}" data-toggle='collapse' data-parent='#phase_accordion' class='accordion-toggle'>
                        % if phase.id == latest_phase_id:
                            <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder-open"></use></svg></span>
                        % else:
                            <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder"></use></svg></span>
                        % endif                    
                        ${phase.label()}
                        <svg class="arrow"><use href="${request.static_url('endi:static/icons/endi.svg')}#chevron-down"></use></svg>
                    </a>
                </h3>
                <div class="collapse_title_buttons">
                    % if api.has_permission('edit.phase', phase):
                        <a class="btn icon unstyled" href="${request.route_path('/phases/{id}', id=phase.id)}" title="Éditer le libellé du sous-dossier ${phase.label()}">
                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
                        </a>
                    % endif
                    % if api.has_permission('delete.phase', phase):
                        <a class="btn icon negative unstyled" href="${request.route_path('/phases/{id}', id=phase.id, _query=dict(action='delete'))}"
                            onclick="return confirm('Êtes-vous sûr de vouloir supprimer le sous-dossier ${phase.label()} ?');"
                            title="Supprimer le sous-dossier ${phase.label()}">
                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#trash-alt"></use></svg>
                        </a>
                    % endif
                </div>
                <div class="collapse_content">
                    <div id='phase_${phase.id}' class='panel-body ${section_css}'>
                        ${request.layout_manager.render_panel('phase_estimations', phase=phase, estimations=tasks_by_phase[phase.id]['estimations'])}
                        ${request.layout_manager.render_panel('phase_invoices', phase=phase, invoices=tasks_by_phase[phase.id]['invoices'])}
                    </div>
                </div>
            </div>
        % endfor
    </div>
    
    % if not project.phases or tasks_without_phases['estimations'] or tasks_without_phases['invoices']:
        <div class='panel panel-default no-border'>
            <div class='panel-body'>
                ${request.layout_manager.render_panel('phase_estimations', phase=None, estimations=tasks_without_phases['estimations'])}
                ${request.layout_manager.render_panel('phase_invoices', phase=None, invoices=tasks_without_phases['invoices'])}
            </div>
        </div>
    % endif

</div>

<section id="new_phase_form" class="modal_view size_middle" style="display: none;">
    <div role="dialog" id="phase-forms" aria-modal="true" aria-labelledby="phase-forms_title">
        <div class="modal_layout">
            <header>
                <button class="icon only unstyled close" title="Fermer cette fenêtre" aria-label="Fermer cette fenêtre" onclick="toggleModal('new_phase_form'); return false;">
                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg>
                </button>
                <h2 id="phase-forms_title">Ajouter un sous-dossier</h2>
            </header>
            <main>
                ${phase_form.render()|n}
            </main>
        </div>
    </div>
</section>
</%block>

<%block name="footerjs">
$( function() {
    if (window.location.hash == "#showphase"){
        $("#project-addphase").addClass('in');
    }
});
</%block>
