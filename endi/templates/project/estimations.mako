<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <a class='btn btn-primary' href='${add_url}'>
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-export"></use></svg> Créer un devis
    </a>
</div>
</%block>

<%block name='mainblock'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        <% columns = 8 %>
        <table class="top_align_table hover_table">
            <thead>
                <th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
                % if is_admin:
                    <% columns += 1 %>
                    <th scope="col" class="col_text">${sortable(u"Entrepreneur", 'company')}</th>
                % endif
                <th scope="col" class="col_date">${sortable(u"Émis le", 'date')}</th>
                <th scope="col" class="col_text">Description</th>
                <th scope="col" class="col_text">${sortable(u"Client", 'customer')}</th>
                <th scope="col" class="col_number"><span class="screen-reader-text">Montant </span>HT</th>
                <th scope="col" class="col_number">TVA</th>
                <th scope="col" class="col_number">TTC</th>
                <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
            </thead>
            <tbody>
                % if records:
                    <tr class="row_recap">
                        <th scope='row' colspan='${columns - 4}' class='col_text'>Total</td>
                        <td class='col_number'>${api.format_amount(totalht, precision=5)}&nbsp;€</td>
                        <td class='col_number'>${api.format_amount(totaltva, precision=5)}&nbsp;€</td>
                        <td class='col_number'>${api.format_amount(totalttc, precision=5)}&nbsp;€</td>
                        <td colspan='1'></td>
                    </tr>
                    % for id_, document in records:
                        <% name = document.name %>
                        <% internal_number = document.internal_number %>
                        <% status = document.global_status %>
                        <% signed_status = document.signed_status %>
                        <% date = document.date %>
                        <% geninv = document.geninv %>
                        <% description = document.description %>
                        <% ht = document.ht %>
                        <% tva = document.tva %>
                        <% ttc = document.ttc %>
                        <% customer_id = document.customer_id %>
                        <% customer_label = document.customer.name %>
                        <% company_id = document.company_id %>
                        <% company_name = document.company.name %>
                        <% business_type = document.business_type %>
                        <tr class="status status-${status} signed-status-${signed_status} geninv-${geninv}">
                            <td class="col_status" title="${api.format_estimation_status(document)}">
                                <span class="icon status ${status}">
                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(document)}"></use></svg>
                                </span>
                            </td>
                            % if is_admin:
                                <td class='col_text invoice_company_name'><a href="${request.route_path("company", id=company_id)}">${company_name}</a></td>
                            % endif
                            <td class="col_date" title="${api.format_status(document)}">${api.format_date(date)}</td>
                            <td class="col_text">
                                <a href="${request.route_path('/estimations/{id}.html', id=id_)}" title="Voir le document">${name} (<small>${internal_number}</small>)</a>
                                ${request.layout_manager.render_panel('business_type_label', business_type)}
                                <small>${format_text(description)}</small>
                            </td>
                            <td class="col_text"><a href="${request.route_path("customer", id=customer_id)}">${customer_label}</a></td>
                            <td class="col_number"><strong>${api.format_amount(ht, precision=5) | n}&nbsp;€</strong></td>
                            <td class="col_number">${api.format_amount(tva, precision=5) | n}&nbsp;€</td>
                            <td class="col_number">${api.format_amount(ttc, precision=5) | n}&nbsp;€</td>
                            <td class="col_actions width_one">
                                <a class='btn icon only' href="${request.route_path('/estimations/{id}.pdf', id=id_)}" title="Télécharger la version PDF" aria-label="Télécharger la version PDF">
                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg>
                                </a>
                            </td>
                        </tr>
                    % endfor
                    <tr class="row_recap">
                        <th scope='row' colspan='${columns - 4}' class='col_text'>Total</td>
                        <td class='col_number'>${api.format_amount(totalht, precision=5)}&nbsp;€</td>
                        <td class='col_number'>${api.format_amount(totaltva, precision=5)}&nbsp;€</td>
                        <td class='col_number'>${api.format_amount(totalttc, precision=5)}&nbsp;€</td>
                        <td colspan='1'></td>
                    </tr>
                % else:
                    <tr><td class='col_text' colspan='7'><em>Aucun devis n’a pu être retrouvé</em></td></tr>
                % endif
            </tbody>
        </table>
    </div>
    ${pager(records)}
</div>
</%block>
