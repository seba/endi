<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/utils.mako" import="show_project_type_and_business_types_labels"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
% if api.has_permission('add_project'):
	<div class='layout flex main_actions'>
		<a class='btn btn-primary' href="${add_url}">
			<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg> Ajouter un dossier
		</a>
	</div>
% endif
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
    	${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
		<table class="hover_table">
			<thead>
				<tr>
					<th scope="col" class="col_date">${sortable(u"Créé le", "created_at")}</th>
					<th scope="col">${sortable(u"Code", "code")}</th>
					<th scope="col" class="col_text">${sortable(u"Nom", "name")}</th>
					<th scope="col" class="col_text">Clients</th>
					<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
				</tr>
			</thead>
			<tbody>
				% if records:
					% for id, project in records:
						<tr class='tableelement' id="${project.id}">
							<% url = request.route_path("/projects/{id}", id=project.id) %>
							<% onclick = "document.location='{url}'".format(url=url) %>
							<td onclick="${onclick}" class="col_date" >${api.format_date(project.created_at)}</td>
							<td onclick="${onclick}">${project.code}</td>
							<td onclick="${onclick}" class='col_text'>
								% if project.archived:
                                    <small title="Ce dossier a été archivé"><span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#archive"></use></svg> Dossier archivé</span></small><br />
								% endif
								${project.name}
								${show_project_type_and_business_types_labels(project)}
							</td>
							<td onclick="${onclick}" class='col_text'>
								% if len(project.customers) < 6:
									${', '.join((customer.label for customer in project.customers))}
								% else:
									${project.customers[0].label}, 
									${project.customers[1].label}, 
									${project.customers[2].label} 
									et ${len(project.customers)-3} autres clients
								% endif
							</td>
							<td class='col_actions width_one'>
								${request.layout_manager.render_panel('menu_dropdown', label="Actions", links=stream_actions(project))}
							</td>
						</tr>
					% endfor
				% else:
					<tr>
						<td colspan='6' class='col_text'><em>Aucun dossier n’a été créé pour l’instant</em></td>
					</tr>
				% endif
			</tbody>
		</table>
	</div>
	${pager(records)}
</div>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>
