<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="definition_list" />
<%namespace file="/base/utils.mako" import="format_mail" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_filelist" />
<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <a class='btn' href='${request.route_path("activity.pdf", id=request.context.id)}' title='Télécharger la fiche de rendez-vous au format PDF'>
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-pdf"></use></svg>PDF
    </a>
</div>
</%block>

<%block name="content">
	<div class='data_display separate_bottom'>
        <h2>Informations</h2>
		<% items = (\
		(u'Conseiller(s)', ', '.join([api.format_account(conseiller) for conseiller in activity.conseillers])), \
			(u'Horaire', api.format_datetime(activity.datetime)), \
			(u"Action financée", u"%s %s" % (activity.action_label, activity.subaction_label)), \
			(u"Nature du rendez-vous", activity.type_object.label), \
			(u"Mode d'entretien", activity.mode), \
			)
		%>
        ${definition_list(items)}
    </div>
	<div class='data_display separate_bottom'>
        <h2>Fichiers attachés</h2>
        ${format_filelist(activity)}
    </div>
	<div class='data_display separate_bottom'>
        <h2>Participants</h2>
        % for participant in activity.participants:
		<dl>
			<dt>${api.format_account(participant)}</dt>
			<dd class='hidden-print'>${ format_mail(participant.email) }</dd>
		</dl>
        %endfor
    </div>
	<div class='data_display separate_bottom'>
        <% options = (\
                (u"Point de suivi", "point"),\
                (u"Définition des objectifs", "objectifs"), \
                (u"Plan d’action et préconisations", "action" ),\
                (u"Documents produits", "documents" ),\
                )
        %>
        % for label, attr in options:
            <h3>${label}</h3>
            ${format_text(getattr(activity, attr))}
        % endfor
    </div>
</%block>
