<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_mail" />
<%namespace file="/base/utils.mako" import="format_phone" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="table_btn" />

<%block name='actionmenucontent'>
<div class="layout flex main_actions">
    <a class='btn btn-primary icon' href='${request.route_path("customer", id=customer.id, _query=dict(action="edit"))}'>
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg> Modifier
    </a>
    <div role='group'>
        <a class='btn btn-primary icon' href='${add_project_url}'>
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#plus"></use></svg> Nouveau dossier
        </a>
    </div>
</div>
</%block>

<%block name='content'>
<div class="layout flex two_cols">
    <div>
        <div class='data_display'>
            <h2>Informations générales</h2>
            <div class='panel-body'>
                % if customer.is_company():
                    <h3 class="highlight_title">Entreprise ${customer.label.upper()}</h3>
                % else:
                    <h3 class="highlight_title">${customer.label}</h3>
                % endif
                % if customer.is_company():
                    % if customer.registration:
                        <div class='layout flex two_cols'>
                            <div><strong>Numéro d'immatriculation</strong></div>
                            <div>${customer.registration}</div>
                        </div>
                    % endif
                    <div class='layout flex two_cols'>
                        <div><strong>Contact principal</strong></div>
                        <div>${customer.get_name()}</div>
                    </div>
                    % if customer.function:
                        <div class='layout flex two_cols'>
                            <div><strong>Fonction</strong></div>
                            <div>${format_text(customer.function)}</div>
                        </div>
                    % endif
                % endif
               <div class="layout flex two_cols">
                    <div><strong>Adresse Postale</strong></div>
                    <div><address>${format_text(customer.full_address)}</address></div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Addresse électronique</strong></div>
                    <div>
                        %if customer.email:
                            ${format_mail(customer.email)}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Téléphone portable</strong></div>
                    <div>
                        %if customer.mobile:
                            ${format_phone(customer.mobile)}
                        %else:
                            <em>Non renseigné</em>
                        %endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Téléphone</strong></div>
                    <div>
                        %if customer.phone:
                            ${format_phone(customer.phone)}
                        %else:
                            <em>Non renseigné</em>
                        %endif
                    </div>
                </div>
                <div class="layout flex two_cols">
                    <div><strong>Fax</strong></div>
                    <div>
                        %if customer.fax:
                            ${format_phone(customer.fax)}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
            </div>
        </div>
        <div class='data_display separate_top'>
            <h2>Informations comptables</h2>
            <div class='panel-body'>
                % if customer.is_company():
                    <% datas = (
                    (u"TVA intracommunautaire", customer.tva_intracomm),
                    (u"Compte CG", customer.compte_cg),
                    (u"Compte Tiers", customer.compte_tiers),) %>
                %else:
                    <% datas = (
                    (u"Compte CG", customer.compte_cg),
                    (u"Compte Tiers", customer.compte_tiers),) %>
                % endif
                % for label, value in datas :
                <div class='layout flex two_cols'>
                    <div><strong>${label}</strong></div>
                    <div>
                        % if value:
                            ${value}
                        % else:
                            <em>Non renseigné</em>
                        % endif
                    </div>
                </div>
                % endfor
                <div class='layout flex two_cols separate_top'>
                    <div><strong>Total des dépenses HT</strong></div>
                    <div>${api.format_amount(customer.get_total_expenses())}&nbsp;€</div>
                </div>
                <div class='layout flex two_cols separate_bottom'>
                    <div><strong>Total facturé HT</strong></div>
                    <div>${api.format_amount(customer.get_total_income(), precision=5)}&nbsp;€</div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class='data_display'>
            <h2>Dossiers</h2>
            <div class='panel-body'>
                %if customer.projects:
                <table class="hover_table">
                    <thead>
                        <tr>
                            <th scope="col">Code</th>
                            <th scope="col" class="col_text">Nom</th>
                            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        % for project in customer.projects:
                            %if project.archived:
                                <tr class='row_archive' id="${project.id}">
                            %else:
                                <tr id="${project.id}">
                            %endif
                                <td>${project.code}</td>
                                <td class="col_text">
                                    ${project.name}
                                    %if project.archived:
                                        (ce dossier a été archivé)
                                    %endif
                                </td>
                                <td class="col_actions width_one">
                                    <div class='btn-group'>
                                        <button type="button" class="btn icon only dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Actions" aria-label="Actions">
                                            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#dots"></use></svg>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="${request.route_path('/projects/{id}', id=project.id)}" title="Voir ce dossier" class="icon">
                                                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>&nbsp;Voir ce dossier
                                            </a></li>
                                            %if not project.archived:
                                                <% add_estimation_url = request.route_path('/projects/{id}/estimations', id=project.id, _query={'action': 'add'}) %>
                                                <li><a href="${add_estimation_url}" title="Ajouter un devis" class="icon">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-list"></use></svg>&nbsp;Ajouter un devis
                                                </a></li>
                                                <% add_invoice_url = request.route_path('/projects/{id}/invoices', id=project.id, _query={'action': 'add'}) %>
                                                <li><a href="${add_invoice_url}" title="Ajouter une facture" class="icon">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#file-invoice-euro"></use></svg>&nbsp;Ajouter une facture
                                                </a></li>
                                                <% archive_url = request.route_path('/projects/{id}', id=project.id, _query=dict(action='archive')) %>
                                                <li><a href="${archive_url}" title="Archiver ce dossier" class="icon" onclick="return confirm('Êtes-vous sûr de vouloir archiver ce dossier ?');">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#archive"></use></svg>&nbsp;Archiver ce dossier
                                                </a></li>
                                            %elif api.has_permission('delete_project', project):
                                                <% delete_url = request.route_path('/projects/{id}', id=project.id, _query=dict(action="delete")) %>
                                                <li><a href="${delete_url}" title="Supprimer ce dossier" class="icon negative" onclick="return confirm('Êtes-vous sûr de vouloir supprimer définitivement ce dossier ?');">
                                                    <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#trash-alt"></use></svg>&nbsp;Supprimer ce dossier
                                                </a></li>
                                            %endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        %endfor
                    </tbody>
                </table>
                %else:
                    <p><em>Aucun dossier n’a été initié avec ce client</em></p>
                %endif
                <div class="collapsible">
                    <h3 class='collapse_title'>
                        <a data-target="#add-project-form" aria-expanded="false" aria-controls="add-project-form" data-toggle='collapse'>
                            <svg class="arrow"><use href="${request.static_url('endi:static/icons/endi.svg')}#chevron-down"></use></svg> Rattacher à un dossier
                        </a>
                    </h3>
                    <div class='collapse_content'>
                        <div id='add-project-form' class='collapse' aria-expanded='false'>
                            ${project_form.render()|n}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='data_display separate_top'>
    <h3>Commentaires</h3>
    % if customer.comments:
        ${format_text(customer.comments)}
    %else :
        <em>Aucun commentaire</em>
    % endif
</div>
</%block>
