
<%def name="esc(datas)">
    <%text>${</%text>${datas}<%text>}</%text>\
</%def>


<%def name="format_text(data, breaklines=True)">
    <%doc>
        Replace \n with br for html output
    </%doc>
    % if data is not UNDEFINED and data is not None:
        <% text = data %>
        %if breaklines:
            <% text = text.replace(u'\n', u'<br />') %>
        % else:
            <% text = text.replace(u'\n', u'') %>
        %endif
        ${api.clean_html(text)|n}
    %endif
</%def>


<%def name="format_customer(customer, link=True)">
    <%doc>
        Render a customer
    </%doc>
    %if customer is not UNDEFINED and customer is not None:
        % if link:
            <a href="${request.route_path('customer', id=customer.id)}"
                title="Voir le client ${customer.label}">
        % endif
        ${customer.label}
        % if link:
            </a>
        %endif
    %endif
</%def>


<%def name="format_supplier(supplier, link=True)">
    <%doc>
        Render a supplier
    </%doc>
    %if supplier is not UNDEFINED and supplier is not None:
        % if link:
            <a href="${request.route_path('supplier', id=supplier.id)}"
                title="Voir le fournisseur ${supplier.label}">
        % endif
        ${supplier.label}
        % if link:
            </a>
        %endif
    %endif
</%def>


<%def name="format_project(project, link=True)">
    <%doc>
        Render a project
    </%doc>
    %if project is not UNDEFINED and project is not None:
        % if link:
            <a href="${request.route_path('/projects/{id}', id=project.id)}"
                title="Voir le dossier ${project.name}">
        % endif
        ${project.name}
        % if link:
            </a>
        % endif
    %endif
</%def>


<%def name="format_mail(mail)">
    <%doc>
        Render an email address
    </%doc>
    % if mail is not UNDEFINED and mail is not None:
        <a href="mailto:${mail}">
            <span class="icon">
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#envelope"></use></svg>
            </span>&nbsp;
            ${mail}
        </a>
    % endif
</%def>


<%def name="format_phone(phone)">
    <%doc>
        Render a phone with a phone link
    </%doc>
    % if phone is not UNDEFINED and phone is not None:
        <a class="phone_link" href="tel://${phone}">${phone}</a>
    % endif
</%def>


<%def name="format_address(obj, multiline=False)">
    <% separator = '<br />' if multiline else ', ' %>
    % if obj.address:
        ${obj.address}${separator if obj.city else ''  | n}
        ${obj.zip_code} ${obj.city.upper()}
    % endif
    % if obj.country and obj.country != 'France':
        % if multiline:
            ${separator | n}${obj.country}
        % else:
            (${obj.country})
        % endif
    % endif
</%def>


<%def name="format_company(company)">
    <h3><a href="${request.route_path('company', id=company.id)}">Enseigne ${company.name}</a></h3>
    <p>${company.goal}</p>
    %if company.logo_id:
        <img src="${api.img_url(company.logo_file)}" alt=""  width="250px" />
    %endif
    <dl>
        % if company.email:
            <dt>E-mail</dt>
            <dd>${format_mail(company.email)}</dd>
        % endif
        % for label, attr in ((u'Téléphone', 'phone'), (u"Téléphone portable", "mobile"),):
            %if getattr(company, attr):
                <dt>${label}</dt>
                <dd>${format_phone(getattr(company, attr))}</dd>
            % endif
        % endfor
        % if company.address or company.country or company.city:
            <dt>Adresse</dt>
            <dd>${format_address(company, multiline=False)}</dd>
        % endif
        % if company.activities:
            <dt>Domaine(s) d'activité</dt>
            <dd>
                <ul>
                    % for activity in company.activities:
                        <li>${activity.label}</li>
                    % endfor
                </ul>
            </dd>
        % endif
        % if request.has_permission('admin_treasury'):
            <dt>Code comptable</dt>
            <dd>${company.code_compta or u"Non renseigné"}</dd>
            <dt>Compte client général</dt>
            <dd>${company.general_customer_account or u"Non renseigné"}</dd>
            <dt>Compte client tiers</dt>
            <dd>${company.third_party_customer_account or u"Non renseigné"}</dd>
            <dt>Compte fournisseur général</dt>
            <dd>${company.general_supplier_account or u"Non renseigné"}</dd>
            <dt>Compte fournisseur tiers</dt>
            <dd>${company.third_party_supplier_account or u"Non renseigné"}</dd>
            <dt>Compte de banque</dt>
            <dd>${company.bank_account or u"Non renseigné"}</dd>
            <dt>Taux d'assurance professionnelle</dt>
            <dd>${company.custom_insurance_rate or u"Non renseigné"}</dd>
            <dt>Contribution à la CAE (en %)</dt>
            <dd>
                % if company.contribution:
                    ${company.contribution}
                % elif request.config.get('contribution_cae'):
                    ${request.config.get('contribution_cae')} (par défaut)
                % else:
                    Non renseigné
                % endif
            </dd>
            <dt>RIB / IBAN</dt>
            <dd>${company.RIB or u'Non renseigné'} / ${company.IBAN or u'Non renseigné'}</dd>
        % endif
        <dt>Coefficient de frais généraux</dt>
        <dd>${company.general_overhead or 0}</dd>
        <dt>Coefficient de marge</dt>
        <dd>${company.margin_rate or 0}</dd>
        <dt>CGV complémentaires</dt>
        <dd>${company.cgv and u'Renseignées' or u"Non renseignées"}</dd>
        <dt>En-tête des documents</dt>
        <dd>${company.header_id and u'Personalisé (image)' or u"Par défaut"}</dd>
    </dl>
</%def>


<%def name="format_filelist(parent_node, delete=False)">
    % if parent_node is not None:
        % for child in parent_node.children:
            % if loop.first:
                <ul>
            % endif
            % if child.type_ == 'file':
                <li>
                <% dl_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='download')) %>
                % if api.has_permission('edit.file', child):
                <% edit_url = request.route_path('/files/{id}', id=child.id) %>
                    ${child.label}
                    <a href="#!" onclick="window.openPopup('${edit_url}');" class="btn icon only unstyled" title="Modifier ce fichier" aria-label="Modifier ce fichier">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
                    </a>
                    <a href="#!" onclick="window.openPopup('${dl_url}')" class="btn icon only unstyled" title="Télécharger ce fichier" aria-label="Télécharger ce fichier">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#download"></use></svg>
                    </a>
                    % if delete:
                        <% delete_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='delete')) %>
                        <%utils:post_action_btn url="${delete_url}" icon="trash-alt" _class="btn icon only unstyled negative"
                        title="Supprimer ce fichier" aria-label="Supprimer ce fichier" onclick="return confirm('Supprimer ce fichier ?');">
                        </%utils:post_action_btn>
                    % endif
                % elif api.has_permission('view.file', child):
                    <a href="#!" onclick="window.openPopup('${dl_url}');" class="btn icon unstyled" title="Télécharger ce fichier" aria-label="Télécharger ce fichier">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#download"></use></svg>
                        ${child.label}
                    </a>
                % endif
                </li>
            % endif
            % if loop.last:
                </ul>
            % endif
        % endfor
    % endif
</%def>


<%def name="format_filetable(documents)">
    <table class="hover_table">
        <thead>
            <th scope="col" class="col_text">Description ici</th>
            <th scope="col" class="col_text">Nom du fichier</th>
            <th scope="col" class="col_date">Déposé le</th>
            <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
        </thead>
        <tbody>
            % for child in documents:
                <tr>
                    <td class="col_text">${child.description}</td>
                    <td class="col_text">${child.name}</td>
                    <td class="col_date">${api.format_date(child.updated_at)}</td>
                    <td class="col_actions">
                        % if api.has_permission('edit.file', child):
                            <% edit_url = request.route_path('/files/{id}', id=child.id) %>
                            ${table_btn(edit_url, u"Voir/Modifier", u"Voir/Modifier ce document", icon="pen", css_class="icon")}
                        % endif
                        <% dl_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='download')) %>
                        ${table_btn(dl_url, u"Télécharger", u"Télécharger ce document", icon="file-download", css_class="icon")}
                        % if api.has_permission('delete.file', child):
                            <% message = u"Ce fichier sera définitivement supprimé. Êtes-vous sûr de vouloir continuer ?" %>
                            <% del_url = request.route_path('/files/{id}', id=child.id, _query=dict(action='delete')) %>
                            ${table_btn(del_url, u"Supprimer", u"Supprimer ce document", icon="trash-alt", css_class='icon negative',
                                onclick=u"return confirm('%s')" % message)}
                        % endif
                    </td>
                </tr>
            % endfor
            % if documents == []:
                <tr><td colspan='6' class="col_text"><em>Aucun document n’est disponible</em></td></tr>
            % endif
        </tbody>
  </table>
</%def>


<%def name="company_disabled_msg()">
    &nbsp;<span class="icon"><svg class="caution"><use href="${request.static_url('endi:static/icons/endi.svg')}#danger"></use></svg>&nbsp;désactivée</span>
</%def>


<%def name="company_internal_msg()">
    <span class="icon"><svg class="neutral"><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span>&nbsp;interne
</%def>


<%def name="login_disabled_msg()">
	<span class="icon"><svg class="caution"><use href="${request.static_url('endi:static/icons/endi.svg')}#lock"></use></svg></span>&nbsp;désactivé
</%def>


<%def name="show_tags_label(tags)">
    <br /><span class="icon"><svg class="neutral"><use href="${request.static_url('endi:static/icons/endi.svg')}#tag"></use></svg>
    % for tag in tags:
        ${tag.label}
    % endfor
    </span>
</%def>


<%def name="company_list_badges(company)">
    % if not company.active:
        ${company_disabled_msg()}
    % endif
    % if company.internal:
        ${company_internal_msg()}
    % endif
</%def>


<%def name="show_project_type_and_business_types_labels(project)">
    % if project.project_type.label and project.project_type.label != "Dossier classique":
        <% business_types = [business_type.label for business_type in project.business_types] %>
        <br /><span class="icon" title="${", ".join(business_types)}"><svg class="neutral"><use href="${request.static_url('endi:static/icons/endi.svg')}#tag"></use></svg>  ${project.project_type.label}</span>
    % endif
</%def>


<%def name="post_action_btn(url, icon=None, **tag_attrs)">
    <%doc>
    :param tag_attrs: kwargs that are translated to HTML tag properties, with the following transformations :
      - _class → class
        - underscore are converted to dashes (ex: aria_role → aria-role)
    </%doc>
    <form class="btn-container" action="${url}" method="post">
        ${csrf_hidden_input()}
        <button
            % for k, v in tag_attrs.items():
            <% k = k.replace('_class', 'class').replace('_', '-') %>
            ${k}="${v}"
            % endfor
        >
            % if icon is not None:
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg>
            % endif
            ${caller.body()}
        </button>
    </form>
</%def>


<%def name="table_btn(href, label, title, icon=None, onclick=None, icotext=None, css_class='', method='get')">
    % if method == 'get':
        <a href='${href}'
    % else: # POST
        <form method="post" action="${href}" class="btn-container">
            ${csrf_hidden_input()}
            <button
    % endif
        class='btn icon only ${css_class}' href='${href}' title="${title}" aria-label="${label}"
        % if onclick:
            onclick="${onclick}"
        % endif
    >
    %if icotext:
        <span>${api.clean_html(icotext)|n}</span>
    % endif
    %if icon:
        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg>
    %endif
    % if method == 'get':
        </a>
    % else: #POST
            </button>
        </form>
    % endif
</%def>


<%def name="dropdown_item(href, label, title, icon=None, onclick=None, icotext=None, disable=False)">
    <li
    % if disable:
        class='disabled'
    % endif
    >
        <a href='${href}' title="${title}" aria-label="${title}"
            % if onclick:
                onclick="${onclick.replace('\n', '\\n')|n}"
            % endif
            >
            %if icotext:
                <span>${api.clean_html(icotext)|n}</span>
            % endif
            %if icon:
                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${icon}"></use></svg>
            %endif
            ${label}
        </a>
    </li>
</%def>


<%def name="definition_list(items)">
    <%doc>
        render a list of elements as a definition_list
        items should be an iterator of (label, values) 2-uple
    </%doc>
    <dl class="dl-horizontal">
        % for label, value in items:
            <dt>${label}</dt>
            <dd>${value}</dd>
        % endfor
    </dl>
</%def>


<%def name="csrf_hidden_input()">
    <input type="hidden" name="csrf_token" value="${get_csrf_token()}" />
</%def>
