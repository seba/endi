<%doc>
TaskLineGroup html representation
</%doc>
<%namespace file="/base/utils.mako" import="format_text" />
<table>
    <thead>
    % if group.title != '':
        <tr class='group-description'>
            <td colspan="${column_count}" class="col_text">
                <strong>${group.title}</strong>
            % if group.description != "":
                ${format_text(group.description, False)}
            % endif
            </td>
        </tr>
    % endif
        <tr>
            <th scope="col" class="col_text description">Intitulé des postes</th>
            %if display_units == 1:
                <th scope="col" class="col_number price" title="Prix Unitaire Hors Taxes">P<span class="screen-reader-text">rix</span> U<span class="screen-reader-text">nitaire</span> H<span class="screen-reader-text">ors </span>T<span class="screen-reader-text">axes</span></th>
                <th scope="col" class="col_number quantity" title="Quantité">Q<span class="screen-reader-text">uanti</span>té</th>
                <th scope="col" class="col_text unity">Unité</th>
            % elif show_progress_invoicing:
                <th scope="col" class="col_number quantity" title="Pourcentage">Avancement</th>
            % endif
            <th scope="col" class="col_number price_total">Prix HT</th>
            % if display_tvas_column:
                <th scope="col" class='col_number tva' title="Taux de TVA"><span class="screen-reader-text">Taux de </span>Tva</th>
            % endif
            % if display_ttc:
                <th scope="col" class='col_number price'>Prix TTC</th>
            % endif
        </tr>
    </thead>
    <tbody class="lines">
        % for line in group.lines:
        ${request.layout_manager.render_panel('task_pdf_task_line', context=task, line=line, display_tvas_column=display_tvas_column, first_column_colspan=first_column_colspan, column_count=column_count, show_progress_invoicing=show_progress_invoicing)}
        % endfor
% if display_subtotal:
            <tr>
                <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                    % if display_ttc:
                        Sous-total
                    % else:
                        Sous-total HT
                    % endif
                </th>
                <th class='col_number price_total'>
                    ${api.format_amount(group.total_ht(), trim=False, precision=5)}&nbsp;€
                </th>
                % if display_tvas_column:
                    <th class='col_number tva'>&nbsp;</th>
                % endif
                % if display_ttc:
                    <th class='col_number price'>
                        ${api.format_amount(group.total_ttc(), trim=False, precision=5)}&nbsp;€
                    </th>
                % endif
            </tr>
        <%doc>Ici on ne ferme pas le tableau, ce qui sera fait plus tard dans le template parent </%doc>
        </tbody>
    </table>
% endif
