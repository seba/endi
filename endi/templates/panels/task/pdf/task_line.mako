<%doc>
Renders a TaskLine object
</%doc>
<%namespace file="/base/utils.mako" import="format_text" />
<tr>
    <td class="col_text description">${format_text(line.description, False)}</td>
    % if display_units == 1:
        <td class="col_number price">${api.format_amount(line.cost, trim=False, precision=5)}&nbsp;€</td>
        <td class="col_number quantity">${api.format_quantity(line.quantity)}</td>
        <td class="col_text unity">${line.unity}</td>
    % elif show_progress_invoicing:
        <td class="col_number quantity">${progress_invoicing_percentage}&nbsp;%</td>
    % endif
    <td class="col_number price_total">${api.format_amount(line.total_ht(), trim=False, precision=5)}&nbsp;€</td>
    % if display_tvas_column:
        <td class='col_number tva'>

            % if line.tva>=0:
                ${api.format_amount(line.tva, precision=2)}&nbsp;%
            % else:
                0 %
            % endif
        </td>
    % endif
    % if display_ttc:
        <td class="col_number price">${api.format_amount(line.total(), trim=False, precision=5)}&nbsp;€</td>
    % endif
</tr>
