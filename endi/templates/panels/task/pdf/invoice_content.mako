<%doc>
    invoice panel template
</%doc>
<%inherit file="/panels/task/pdf/content.mako" />
<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
	<div class='pdf_mention_block'>
		<h4 class="title ${css}">${title}</h4>
		<p class='content'>${format_text(datas)}</p>
    </div>
</%def>

<%block name='information'>
<div class="pdf_information">
	<h3>Facture N<span class="screen-reader-text">umér</span><sup>o</sup> <strong>${task.official_number}</strong></h3>
	<strong>Libellé : </strong>${task.internal_number}<br />
	% if task.estimation is not None:
		<strong>Cette facture est associée au devis : </strong>${task.estimation.internal_number}<br />
	% endif
	<strong>Objet : </strong>${format_text(task.description)}<br />
	% if task.workplace:
		<strong>Lieu d'éxécution des travaux : </strong><br />
		${format_text(task.workplace)}<br />
	% endif
	% if config.get('coop_invoiceheader'):
		<div class="coop_header">${format_text(config['coop_invoiceheader'])}</div>
	% endif
</div>
</%block>

<%block name="notes_and_conditions">
%if task.payment_conditions:
    ${table(u"Conditions de paiement", task.payment_conditions)}
% endif
</%block>
