<%doc>
    estimation panel template
</%doc>
<%inherit file="/panels/task/pdf/content.mako" />
<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
	<div class='pdf_mention_block'>
		<h4 class="title ${css}">${title}</h4>
		<p class='content'>${format_text(datas)}</p>
    </div>
</%def>

<%block name='information'>
<div class="pdf_information">
	<h3>Devis N<span class="screen-reader-text">umér</span><sup>o</sup> <strong>${task.internal_number}</strong></h3>
	<strong>Objet : </strong>${format_text(task.description)}<br />
	% if task.workplace:
		<strong>Lieu d'éxécution des travaux : </strong><br />
		${format_text(task.workplace)}<br />
	% endif
	% if config.get('coop_estimationheader'):
		<div class="coop_header">${format_text(config['coop_estimationheader'])}</div>
	% endif
</div>
</%block>

<%block name="notes_and_conditions">
% if task.paymentDisplay != u"NONE":
    % if task.paymentDisplay == u"ALL":
        <% colspan = 3 %>
    % elif task.paymentDisplay == u"ALL_NO_DATE":
        <% colspan = 2 %>
    %else:
        <% colspan = 1 %>
    % endif
    <div class='pdf_mention_block payment_conditions'>
		<h4>Conditions de paiement</h4>
		<p>
			${task.payment_conditions}<br />
			% if task.deposit > 0 :
				Un acompte, puis paiement en ${task.get_nb_payment_lines()} fois.
			%else:
				Paiement en ${task.get_nb_payment_lines()} fois.
			%endif
		</p>
		<table>
			<tbody>
				% if task.paymentDisplay in (u"ALL", "ALL_NO_DATE"):
					## AFFICHAGE DU DETAIL DU PAIEMENT
					## L'acompte à la commande
					% if task.deposit > 0 :
						<tr>
							<td
								% if task.paymentDisplay == u"ALL":
									colspan=2
								% endif
								scope='row'
								class='col_text'
								>Acompte à la commande</td>
							<td class='col_number price'>${api.format_amount(task.deposit_amount_ttc(), precision=5)}&nbsp;€</td>
							% if multiple_tvas:
								<td class='col_number tva'>&nbsp;</td>
							% endif
							% if task.display_ttc:
								<td class="col_number price">&nbsp;</td>
							% endif
						</tr>
					% endif
					## Les paiements intermédiaires
					% for line in task.payment_lines[:-1]:
						<tr>
							% if task.paymentDisplay == u"ALL":
								<td scope='row' class='col_date'>${api.format_date(line.date)}</td>
							% endif
							<td class='col_text'>${line.description}</td>
							%if task.manualDeliverables == 1:
								<td class='col_number price'>${api.format_amount(line.amount, precision=5)}&nbsp;€</td>
							%else:
								<td class='col_number price'>${api.format_amount(task.paymentline_amount_ttc(), precision=5)}&nbsp;€</td>
							%endif
							% if multiple_tvas:
								<td class='col_number tva'>&nbsp;</td>
							% endif
							% if task.display_ttc:
								<td class="col_number price">&nbsp;</td>
							% endif
						</tr>
					% endfor
					## Le solde (qui doit être calculé séparément pour être sûr de tomber juste)
					<tr>
						% if task.paymentDisplay == u"ALL":
							<td scope='row' class='col_date'>
								${api.format_date(task.payment_lines[-1].date)}
							</td>
						% endif
						<td scope='row' class='col_text'>
							${format_text(task.payment_lines[-1].description)}
						</td>
						<td class='col_number price'>
							${api.format_amount(task.sold(), precision=5)}&nbsp;€
						</td>
						% if multiple_tvas:
							<td class='col_number tva'>&nbsp;</td>
						% endif
						% if task.display_ttc:
							<td class="col_number price">&nbsp;</td>
						% endif
					</tr>
				% endif
			</tbody>
		</table>
    </div>
%else:
    %if task.payment_conditions:
        ${table(u"Conditions de paiement", task.payment_conditions)}
    % endif
% endif
</%block>

<%block name="end_document">
<div class="pdf_sign_block">
	<div class="pdf_sign_block_width">
		<h4>Bon pour accord</h4>
		<p class='date'>Le :</p>
		<p class="signature"><em>Signature</em></p>
	</div>
</div>
</%block>
