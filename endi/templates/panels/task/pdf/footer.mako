<%namespace file="/base/utils.mako" import="format_text" />
<footer>
<div class='row pdf_footer'>
% if title:
<b>${ format_text(title) }</b>
% endif
<p>
% if more_text:
${format_text(more_text)}<br />
% endif
% if text:
${format_text(text)}
% endif
</p>
</div>
<div id='page-number' class='pdf_page_number'>
${number}
&nbsp;
&nbsp;
&nbsp;
-
&nbsp;
&nbsp;
&nbsp;
Page ${ pdf_current_page } / ${ pdf_page_count }
</div>
</footer>
