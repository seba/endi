<%doc>
    cancel invoice panel template
</%doc>
<%inherit file="/panels/task/pdf/content.mako" />
<%namespace file="/base/utils.mako" import="format_text" />

<%def name="table(title, datas, css='')">
	<div class='pdf_mention_block'>
		<h4 class="title ${css}">${title}</h4>
		<p class='content'>${format_text(datas)}</p>
    </div>
</%def>

<%block name='information'>
<div class="pdf_information">
	<h3>Avoir N<span class="screen-reader-text">umér</span><sup>o</sup> <strong>${task.official_number}</strong></h3>
	<strong>Libellé : </strong>${task.internal_number}<br />
		% if task.invoice:
			<span style='color:#999'><strong style='color:#999'>Référence facture N<span class="screen-reader-text">umér</span><sup>o</sup></strong> ${task.invoice.official_number} (${task.invoice.internal_number})</span><br /><br />
		% endif
    <strong>Objet : </strong>${format_text(task.description)}
</div>
</%block>

<%block name="notes_and_conditions">
%if task.payment_conditions:
	${table(u"Conditions de remboursement", task.payment_conditions)}
% endif
% if 'coop_reimbursement' in config:
	${table(u"Mode de remboursement", config['coop_reimbursement'])}
%endif
</%block>
