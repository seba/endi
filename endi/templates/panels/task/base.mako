<%doc>
Task content rendering panel
Only renders the content of the page
</%doc>
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_address" />

<%def name="table(title, datas, css='')">
    <div class="pdf_mention_block ${css}">
		<h4>${title}</h4>
		<p>${format_text(datas)}</p>
    </div>
</%def>

<div id='content' class='task_view'>
    <div class='pdf_header'>
        % if company.header_file:
            <img src='${api.img_url(company.header_file)}' alt='${company.name}'/>
        % else:
            <table role="presentation" class="pdf_logo">
            	<tbody>
            		<tr>
			        % if company.logo_file:
            			<td class='logo_cell'>
			                <img src='${api.img_url(company.logo_file)}' alt='Logo de ${company.name}' class='logo_img' />
            			</td>
	                % endif
            			<td>
							<h4 class="cae-details">${config.get('cae_business_name', '')}</h4>
							<div class="company-details">
								<address>
									<strong>${company.name}</strong><br />
									${format_address(company, multiline=True)}
								</address>
								<p>
									${company.email}<br />
									${company.phone}<br />
									${company.mobile}<br />
								</p>
							</div>
            			</td>
            		</tr>
            	</tbody>
            </table>
        % endif
    </div>

    <div class='pdf_address_block'>
        <div>
            ${format_text(task.address)}
			<span class="corner top left"></span>
			<span class="corner top right"></span>
			<span class="corner bottom left"></span>
			<span class="corner bottom right"></span>
        </div>
    </div>

    <div class="informationblock">
        Le ${api.format_date(task.date, False)},<br />
        <%block name='information'></%block>
        % if task.customer.tva_intracomm:
            <b>Numéro de TVA Intracommunautaire</b>&nbsp;: ${task.customer.tva_intracomm}<br />
        % endif
    </div>

    <div class='pdf_spacer'><br></div>

    <div class='row pdf_task_table'>
        % for group in groups:
            ${request.layout_manager.render_panel("task_line_group_html", task=task, group=group, display_tvas_column=multiple_tvas, first_column_colspan=first_column_colspan, column_count=column_count, show_progress_invoicing=show_progress_invoicing,is_tva_on_margin_mode=is_tva_on_margin_mode)}
        % endfor
        % if len(groups) > 1:
            </div>
            <div class='pdf_spacer'><br></div>
            <div class='pdf_task_table'>
                <table>
                    <tbody>
        % endif
        % if task.expenses_ht not in (0, None):
            <tr>
                <td class='col_text description' colspan='${first_column_colspan}'>
                    Frais forfaitaires
                </td>
                <td class="col_number price">
                    ${api.format_amount(task.expenses_ht, precision=5)}&nbsp;€
                </td>
                % if multiple_tvas:
                    <td class='col_number tva'>
                        ${api.format_amount(task.expenses_tva, precision=2)}&nbsp;%
                    </td>
                % endif
                % if task.display_ttc:
                    <td class="col_number price">&nbsp;</td>
                % endif
            </tr>
        % endif
        </tbody>
        <tfoot>
            %if hasattr(task, "discounts") and task.discounts:
                % if not is_tva_on_margin_mode:
                    <tr class="row_total">
                    <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>Total HT</th>
                    <td class='col_number price_total'>
                        ${api.format_amount(task.groups_total_ht() + task.expenses_ht, trim=False, precision=5)}&nbsp;€
                    </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class="col_number tva">&nbsp;</td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endif
                % for discount in task.discounts:
                    <tr>
                        <td colspan='${first_column_colspan}' class='col_text description'>
                            ${format_text(discount.description)}
                        </td>
                        <td class='col_number price'>
                            ${api.format_amount(discount.total_ht(), precision=5)}&nbsp;€
                        </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class='col_number tva'>
                                ${api.format_amount(discount.tva, precision=2)}&nbsp;%
                            </td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endfor
                <tr class="row_total">
                    <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                        % if is_tva_on_margin_mode:
                            Total
                        % else:
                            Total HT après remise
                        % endif
                    </th>
                    <td class='col_number price_total'>
                        ${api.format_amount(task.total_ht(), precision=5)}&nbsp;€
                    </td>
                    % if multiple_tvas and not is_tva_on_margin_mode:
                        <td class="col_number tva">&nbsp;</td>
                    % endif
                    % if task.display_ttc:
                        <td class="col_number price">&nbsp;</td>
                    % endif
                </tr>
            % else:
                % if not is_tva_on_margin_mode:
                    <tr class="row_total">
                    <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>Total HT</th>
                    <td class='col_number price_total'>
                        ${api.format_amount(task.total_ht(), precision=5)}&nbsp;€
                    </td>
                        % if multiple_tvas and not is_tva_on_margin_mode:
                            <td class="col_number tva">&nbsp;</td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endif
            % endif
            <%doc>
            Si l'on a qu'un seul taux de TVA dans le document, on affiche
            une seule ligne avec du texte pour les tvas à 0%

            Pour les documents avec plusieurs taux de TVA, on affiche le montant par taux de tva
            </%doc>
            <% tva_objects_dict = task.get_tva_objects() %>
            %for tva, tva_amount in task.get_tvas().items():
                <% tva_object = tva_objects_dict.get(tva) %>
                % if not is_tva_on_margin_mode:
                    <tr>
                        % if multiple_tvas:
                            <th scope="row" colspan='${first_column_colspan}'
                                class='col_text align_right'>
                        % elif tva > 0 :
                            <th scope="row" colspan='${first_column_colspan}'
                                class='col_text align_right'>
                        % else:
                            <th scope="row" colspan='${first_column_colspan +1}'
                                class='col_text align_right'>
                        % endif
                        % if tva_object:
                            % if tva_object.mention:
                                ${format_text(tva_object.mention)}
                            % else:
                                ${tva_object.name}
                            % endif
                        % else:
                            TVA (${api.format_amount(tva, precision=2)} %)
                        % endif
                        </th>
                        % if multiple_tvas:
                            <td class='col_number price'>
                                % if tva > 0:
                                    ${api.format_amount(tva_amount, precision=5)}&nbsp;€
                                % else:
                                    0&nbsp;€
                                % endif
                            </td>
                        % elif tva > 0:
                            <td class='col_number price'>
                                ${api.format_amount(tva_amount, precision=5)}&nbsp;€
                            </td>
                        % endif
                        % if multiple_tvas:
                            <td class="col_number tva">&nbsp;</td>
                        % endif
                        % if task.display_ttc:
                            <td class="col_number price">&nbsp;</td>
                        % endif
                    </tr>
                % endif
            % endfor
            %if task.expenses:
                <tr>
                    <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                        Frais réels
                    </th>
                    <td class='col_number price'>
                        ${api.format_amount(task.expenses_amount(), precision=5)}&nbsp;€
                    </td>
                    % if multiple_tvas:
                        <td class="col_number tva">&nbsp;</td>
                    % endif
                    % if task.display_ttc:
                        <td class="col_number price">&nbsp;</td>
                    % endif
                </tr>
            %endif

                <tr class="row_total">
                    <th scope="row" colspan='${first_column_colspan}' class='col_text align_right'>
                        % if is_tva_on_margin_mode:
                            Total
                        % else:
                            Total TTC
                        % endif
                    </th>
                    <td class='col_number price_total'>
                        ${api.format_amount(task.total(), precision=5)}&nbsp;€
                    </td>
                    % if multiple_tvas:
                        <td class="col_number tva">&nbsp;</td>
                    % endif
                    % if task.display_ttc:
                        <td class="col_number price">&nbsp;</td>
                    % endif
                </tr>
        </tfoot>
        </table>
    </div>

    <div class='pdf_spacer'><br></div>

    %if task.notes:
        ${table(u"Notes", task.notes)}
    %endif

    <%block name="notes_and_conditions">
    ## All infos beetween document lines and footer text (notes, payment conditions ...)
    </%block>

    % for mentions in (task.mandatory_mentions, task.mentions):
        % for mention in mentions:
            <div class="pdf_mention_block">
                % if mention.title:
                    <h4>${mention.title}</h4>
                    % if mention.full_text is not None:
                        <p>${format_text(api.compile_template_str(mention.full_text, mention_tmpl_context))}</p>
                    % endif
                % else:
                    % if mention.full_text is not None:
                        <p>${format_text(api.compile_template_str(mention.full_text, mention_tmpl_context))}</p>
                    % endif
                % endif
            </div>
        % endfor
    % endfor

    <%block name="end_document">
    ## Add infos at the end of the document
    </%block>

</div>
## end of content

<div id='commonfooter' class='row pdf_footer'
    ## In view_only only mode we switch footers by css, in pdf mode, we use frames (see templates/tasks/task.mako)
    % if not bulk and task.is_training():
        style="display:none"
    % endif
    >
    ## The common footer
    % if config.get('coop_pdffootertitle'):
        <b>${format_text(config.get('coop_pdffootertitle'))}</b>
    %endif
    % if config.get('coop_pdffootertext'):
        <p>${format_text(config.get('coop_pdffootertext'))}</p>
    % endif
</div>
<div id='coursefooter' class='row pdf_footer'
    ## In view_only only mode we switch footers by css, in pdf mode, we use frames (see templates/tasks/task.mako)
    % if not bulk and not task.is_training():
        style="display:none"
    % endif
    >
    ## The footer specific to courses (contains the additionnal text infos)
    % if config.get('coop_pdffootertitle'):
        <b>${format_text(config.get('coop_pdffootertitle'))}</b><br />
    %endif
    % if config.get('coop_pdffootercourse'):
        ${format_text(config.get('coop_pdffootercourse'))}<br />
    % endif
    % if config.get('coop_pdffootertext'):
        ${format_text(config.get('coop_pdffootertext'))}
    % endif
</div>

% if task.status == 'valid':
    <div id='page-number' class='pdf_page_number'>
        <%block name='footer_number'></%block>
        &nbsp;
        &nbsp;
        &nbsp;
        -
        &nbsp;
        &nbsp;
        &nbsp;
        Page <pdf:pagenumber/>/<pdf:pagecount/>
    </div>
% endif

% if bulk is UNDEFINED or not bulk:
    % if config.get('coop_cgv'):
        <pdf:nextpage />
        <div id="cgv" class='pdf_cgv'>
            ${format_text(config['coop_cgv'], False)}
        </div>
    % endif
    % if company.cgv:
        <pdf:nextpage />
        <div class='pdf_cgv'>
            ${format_text(company.cgv, False)}
        </div>
    % endif
% endif
