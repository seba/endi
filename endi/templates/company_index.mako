<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_text" />
<%namespace file="/base/utils.mako" import="format_customer" />
<%namespace file="/base/utils.mako" import="table_btn"/>

<%block name='afteractionmenu'>
<div class='layout flex dashboard'>
	% if 'welcome' in request.config:
		<div>
			<div class="alert alert-info">
				<p>
					${format_text(request.config['welcome'])}
				</p>
			</div>
		</div>
	% endif
</div>
</%block>

<%block name='content'>
<% num_elapsed = elapsed_invoices.count() %>
<div class='layout flex dashboard'>
	<div class="columns">
	% if num_elapsed:
		<div class="dash_elem">
			<h2>
				<span class='icon invalid'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#euro-slash"></use></svg></span>
				<a href="${request.route_path('company_invoices', id=company.id, _query=dict(__formid__='deform', status='notpaid'))}" title="Voir toutes les factures impayées">
					Factures impayées
					<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg>
				</a>
			</h2>
			<div>
				<p class='message neutral'>
					<span class="icon" role="presentation"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span>
					Vous avez 
					% if num_elapsed == 1:
					une facture impayée
					% else:
					des factures impayées
					% endif
					 depuis plus de 45 jours
				</p>
				<table class='hover_table'>
					<thead>
						<th scope="col" class="col_text">
							Client
						</th>
						<th scope="col" class="col_number">
							Montant
						</th>
					</thead>
					<tbody>
						% for invoice in elapsed_invoices:
						<% url = request.route_path("/invoices/{id}.html", id=invoice.id) %>
						<% onclick = "document.location='{url}'".format(url=url) %>
						<tr>
							<td onclick="${onclick}" class="col_text">
								${format_customer(invoice.customer, False)}
							</td>
							<td onclick="${onclick}"  class="col_number">
								${api.format_amount(invoice.ttc, precision=5)}&nbsp;€
							</td>
						</tr>
						% endfor
					</tbody>
				</table>
			</div>
		</div>
	% endif
	    <div id='tasklist_container'>
        ${panel('company_recent_tasks')}
    	</div>
	    <div id='event_container'>
	    ${panel('company_coming_events')}
	    </div>
    </div>
</div>
</%block>
