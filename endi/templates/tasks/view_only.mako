<%doc>
    Base template for task readonly display
</%doc>
<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_filelist" />

<%block name="headtitle">
${request.layout_manager.render_panel('task_title_panel', title=title)}
</%block>

<%block name='content'>
<div>
	% if hasattr(next, 'panel_heading'):
		<h2>${next.panel_heading()}</h2>
	% endif
	% if hasattr(next, 'before_task_tabs'):
		${next.before_task_tabs()}
	% endif
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active">
			<a href="#summary" aria-control="summary" role='tab' data-toggle='tab'>Général</a>
		</li>
		<li role="presentation">
			<a href="#documents" aria-control="documents" role='tab' data-toggle='tab'>Prévisualisation</a>
		</li>
		% if hasattr(next, 'moretabs'):
			${next.moretabs()}
		% endif
		% if api.has_permission('view.file'):
			<li role="presentation">
				<a href="#attached_files" aria-control="attached_files" role='tab' data-toggle='tab'>
					Fichiers attachés
					% if request.context.children:
						<span class="badge">${len(request.context.children)}</span>
					% endif
				</a>
			</li>
		% endif
	</ul>
	<div class='tab-content'>
		<div role='tabpanel' class="tab-pane active row" id="summary">
			% if hasattr(next, 'before_summary'):
				<div>${next.before_summary()}</div>
			% endif
			% if indicators:
				<div class="separate_bottom">
					<h3>Indicateurs</h3>
					${request.layout_manager.render_panel('sale_file_requirements', file_requirements=indicators)}
				</div>
			% endif
			<div class="layout flex two_cols separate_bottom">
				<div>
					<h3>Informations générales</h3>
					<dl class='dl-horizontal'>
						<dt>Statut</dt>
						<dd>
							<span class="icon status  ${task.global_status}"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(request.context)}"></use></svg></span>
							${api.format_status(request.context)}
						</dd>
						% if task.business_type and task.business_type.name != 'default':
						% if task.business_id is not None:
							<dt>Affaire</dt>
							<dd><a href="${request.route_path('/businesses/{id}/overview', id=task.business_id)}">${task.business_type.label} : ${task.business.name}</a></dd>
						% else:
							<dt>Affaire de type</dt>
							<dd>${task.business_type.label}</dd>
						% endif
						% endif
						<dt>Nom du document</dt>
						<dd>${request.context.name}</dd>
						<dt>Date</dt>
						<dd>${api.format_date(request.context.date)}</dd>
						<dt>Client</dt>
						<dd>
							<a href="${request.route_path('customer', id=request.context.customer.id)}">
								${request.context.customer.label}
								% if request.context.customer.code:
									&nbsp;<small>(${request.context.customer.code})</small>
								% endif
							</a>
						</dd>
						<dt>Montant HT</dt>
						<dd>${api.format_amount(request.context.ht, precision=5)}&nbsp;€</dd>
						<dt>TVA</dt>
						<dd>${api.format_amount(request.context.tva, precision=5)}&nbsp;€ </dd>
						<dt>TTC</dt>
						<dd>${api.format_amount(request.context.ttc, precision=5)}&nbsp;€</dd>
					</dl>
					% if hasattr(next, 'after_summary'):
						${next.after_summary()}
					% endif
				</div>
				<div>
					<h3>Historique</h3>
					% for status in request.context.statuses:
						<blockquote>
							${status.status_comment | n}
							<footer>
								${api.format_status_string(status.status_code)} - \
								Par ${api.format_account(status.status_person)} le ${api.format_date(status.status_date)}
							</footer>
						</blockquote>
					% endfor
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane row" id="documents">
			<div class="separate_block limited_width width60 content_padding task_view">
				${request.layout_manager.render_panel('task_html', context=task)}
			</div>
		</div>

		% if hasattr(next, 'moretabs_datas'):
			${next.moretabs_datas()}
		% endif

		<!-- attached files tab -->
		% if api.has_permission('view.file'):
			<% title = u"Liste des fichiers attachés à cette facture" %>
		   ${request.layout_manager.render_panel('task_file_tab', title=title)}
		% endif

	</div>
</div>
</%block>
