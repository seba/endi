<%inherit file="${context['main_template'].uri}" />
<%block name="content">
% if request.context.status == 'wait':
<div class='container-fluid beforeContent'>
    <div class='row'>
        <div class='col-md-12'>
            <h4>
            ${api.format_status(request.context)}
            </h4>
        </div>
    </div>
</div>
% endif
<div id='js-main-area' class='task-edit'></div>
</%block>
<%block name='footerjs'>
AppOption = {};
% for key, value in js_app_options.items():
AppOption["${key}"] = "${value}"
% endfor;
</%block>
