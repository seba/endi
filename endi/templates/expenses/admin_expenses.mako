<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="table_btn"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='content'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
        <% columns = 11 %>
        <table class="top_align_table hover_table">
            <thead>
                <tr>
                    <th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
                    <th scope="col">${sortable(u"ID", "id_")}</th>
                    <th scope="col" class="col_text">${sortable(u"Entrepreneur", "name")}</th>
                    <th scope="col" class="col_text">${sortable(u"Période", "month")}</th>
                    <th scope="col" class="col_number"><span class="screen-reader-text">Montant </span>HT</th>
                    <th scope="col" class="col_number">TVA</th>
                    <th scope="col" class="col_number">TTC</th>
                    <th scope="col" class="col_number">Kms</th>
                    <th scope="col" class="col_text">Paiements</th>
                    <th scope="col" class="col_text">Justificatifs</th>
                    <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                </tr>
            </thead>
            <tbody>
                % if records:
                    <tr class="row_recap">
                        <th scope='row' colspan='${columns - 7}' class='col_text'>Total</th>
                        <td class='col_number'>${api.format_amount(total_ht)} €</td>
                        <td class='col_number'>${api.format_amount(total_tva)} €</td>
                        <td class='col_number'>${api.format_amount(total_ttc)} €</td>
                        <td class='col_number'>${api.remove_kms_training_zeros(api.format_amount(total_km))}</td>
                        <td colspan='${columns - 7}'></td>
                    </tr>
                % endif
                % for id_, expense in records:
                    <% url = request.route_path('/expenses/{id}', id=expense.id) %>
                    <% onclick = "document.location='{url}'".format(url=url) %>
                    <tr>
                        <td class='col_status' title="${api.format_expense_status(expense)}">
                            <span class="icon status ${expense.status}">
                                <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${api.status_icon(expense)}"></use></svg>
                            </span>
                        </td>
                        <td onclick="${onclick}">${expense.id}</td>
                        <td onclick="${onclick}" class="col_text"><strong>${api.format_account(expense.user)}</strong> (${expense.company.name})</td>
                        <td onclick="${onclick}" class="col_text">${api.month_name(expense.month)} ${expense.year}</td>
                        <td class="col_number"><strong>${api.format_amount(expense.total_ht)} €</strong></td>
                        <td class="col_number">${api.format_amount(expense.total_tva)} €</td>
                        <td class="col_number">${api.format_amount(expense.total)} €</td>
                        <td class="col_number">${api.remove_kms_training_zeros(api.format_amount(expense.total_km))}</td>
                        <td onclick="${onclick}" class="col_number">${api.format_amount(expense.total, precision=2)} €</td>
                        <td onclick="${onclick}" class="col_text">
                            % for payment in expense.payments:
                                % if loop.first:
                                    <ul>
                                % endif
                                    <% url = request.route_path('expense_payment', id=payment.id) %>
                                    <li>
                                        <a href="${url}">
                                            Par ${api.format_account(payment.user)} :
                                            ${api.format_amount(payment.amount)}&nbsp;€
                                            le ${api.format_date(payment.date)}
                                            % if payment.waiver:
                                                (par abandon de créances)
                                            % else:
                                                (${api.format_paymentmode(payment.mode)})
                                            % endif
                                        </a>
                                    </li>
                                % if loop.last:
                                    </ul>
                                % endif
                            % endfor
                        </td>
                        <td class="col_text">
                            % if api.has_permission('set_justified.expensesheet', expense) and expense.status != 'valid':
                                <div
                                    class="icon_choice layout flex expense-justify"
                                    data-toggle="buttons"
                                    data-href="${request.route_path('/api/v1/expenses/{id}', id=expense.id, _query={'action': 'justified_status'})}"
                                    >
                                    <label
                                        class="btn
                                        % if not expense.justified:
                                            active
                                        % endif
                                        ">
                                        <input
                                            name="justified_${expense.id}"
                                            value="false"
                                            % if not expense.justified:
                                                checked="true"
                                            % endif
                                            autocomplete="off"
                                            type="radio"
                                            class="visuallyhidden">
                                        <span>
                                            <svg class="icon"><use href="${request.static_url('endi:static/icons/endi.svg')}#clock"></use></svg>
                                            En attente
                                        </span>
                                    </label>
                                    <label class="btn">
                                        <input
                                        name="justified_${expense.id}"
                                        value="true"
                                        % if expense.justified:
                                            checked="true"
                                        % endif
                                        autocomplete="off"
                                        type="radio"
                                        class="visuallyhidden">
                                        <span>
                                            <svg class="icon"><use href="${request.static_url('endi:static/icons/endi.svg')}#check"></use></svg>
                                            Reçus
                                        </span>
                                    </label>
                                </div>
                            % endif
                        </td>
                        <td 
                            % if request.has_permission('add_payment.expensesheet', expense):
                        	class="col_actions width_three"
                        	% else:
                        	class="col_actions width_two"
                        	% endif
                        	>
                            <ul>
                            % if request.has_permission('add_payment.expensesheet', expense):
                                <li>
                                <% onclick = "ExpenseList.payment_form(%s, '%s');" % (expense.id, api.format_amount(expense.topay(), grouping=False)) %>
                                ${table_btn('#popup-payment_form',
                                    u"Paiement",
                                    u"Saisir un paiement pour cette feuille",
                                    icon='euro-circle',
                                    onclick=onclick)}
                                </li>
                            % endif
								<li>
								<% url = request.route_path('/expenses/{id}', id=expense.id) %>
								${table_btn(url, u'Modifier', u"Voir la note de dépenses", icon="pen" )}
								</li>
								<li>
								<% url = request.route_path('/expenses/{id}.xlsx', id=expense.id) %>
								${table_btn(url, u'Excel', u"Télécharger au format Excel", icon="file-excel" )}
								</li>
                            </ul>
                        </td>
                    </tr>
                % endfor
            </tbody>
        </table>
    </div>
    ${pager(records)}
</div>
</%block>

<%block name='footerjs'>
ExpenseList.popup_selector = "#${payment_formname}";
% for i in 'year', 'month', 'status', 'owner', 'items':
    $('#${i}-select').change(function(){$(this).closest('form').submit()});
% endfor
</%block>
