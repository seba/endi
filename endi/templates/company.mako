<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_mail" />
<%namespace file="/base/utils.mako" import="format_phone" />
<%namespace file="/base/utils.mako" import="format_company" />
<%namespace file="/base/utils.mako" import="company_list_badges" />
<%namespace file="/base/utils.mako" import="login_disabled_msg" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
% for button in actions:
    ${request.layout_manager.render_panel(button.panel_name, context=button)}
% endfor
</div>
</%block>

<%block name='content'>
<div class='data_display separate_bottom'>
	<h2>
	Informations générales
	% if not company.active:
		<small><span class="icon status caution"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#lock"></use></svg></span>${company_list_badges(company)}</small>
	% endif
	</h2>
	<div class="layout flex two_cols">
		<div class="layout flex">
		    <span class='user_avatar'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#building"></use></svg></span>
		    <div>${format_company(request.context)}</div>
		</div>
		<div>
			<ul class="no_bullets">
                % for perm, route, label in ( \
                ('list.estimation', 'company_estimations', u"Voir les devis"),\
                ('list.invoice', 'company_invoices', u"Voir les factures"),\
                ('view.commercial', 'commercial_handling', u"Voir la gestion commerciale"), \
                ('view.treasury', '/companies/{id}/accounting/treasury_measure_grids', u"Voir les états de trésorerie"), \
                ('list.activity', 'company_activities', u"Voir les rendez-vous"),\
                ('list.workshop', 'company_workshops_subscribed', u"Voir les ateliers auxquels l’enseigne participe"),\
                ('list.training', 'company_workshops', u"Voir les ateliers organisés par l’enseigne"),\
                ):
                    % if request.has_permission(perm):
                    <li>
                        <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg></span>
                        <a href="${request.route_path(route, id=_context.id)}">${label}</a>
                    </li>
                    % endif
                % endfor
			</ul>
		</div>
	</div>
</div>
<div class="data_display">
	<h2>Employé(s)</h2>
	<div class='panel-body'>
		<ul>
		% for user in company.employees:
			<li class="company_employee_item">
				% if user.photo_file:
					<span class="user_avatar">
						<img src="${api.img_url(user.photo_file)}" 
							title="${api.format_account(user)}" 
							alt="Photo de ${api.format_account(user)}" 
							width="256" height="256" />
					</span>
				% else:
					<span class="user_avatar"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#user"></use></svg></span>
				% endif
				% if request.has_permission("view.user", user):
					<a href="${request.route_path('/users/{id}', id=user.id)}" title='Voir ce compte'>
				% endif
				${api.format_account(user)}
				% if request.has_permission("view.user", user):
					</a>
				% endif
				% if user.login is not None and not user.login.active:
				    <small>${login_disabled_msg()}</small>
				% endif
			</li>
		% endfor
		</ul>
		% if len(company.employees) == 0:
			Aucun entrepreneur n’est associé à cette enseigne
		% endif
	</div>
</div>
</%block>
