<%inherit file="${context['main_template'].uri}" />
<%block name='afteradminmenu'>
    <div class='alert alert-info'>
    <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#info-circle"></use></svg></span> 
    Les grilles de frais kilométriques sont configurées de manière annuelle.<br />
    Choisissez l’année que vous voulez administrer.<br />
    Note : Il est possible de dupliquer les types de frais d’une année vers l'autre.
    </div>
</%block>
<%block name='content'>
<div>
	<h2>
	Choisir une année
	</h2>
	<div class='content_vertical_padding'>
		<div class='btn-group'>
		% for year in years:
			<a
				class='btn'
				href="${request.route_path(admin_path, year=year)}"
				>
				${year}
			</a>
		 % endfor
		 </div>
	</div>
</div>
</%block>
