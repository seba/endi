# -*- coding: utf-8 -*-
import os
import sys
import subprocess
import logging

from endi.scripts.utils import (
    command,
)
from endi_base.models.base import DBSESSION


""" Fetch and load into DB the latest published anonymized demo data

OVERWRITE THE WHOLE ENDI DATABASE.

It requires the following CLI tools :

- bunzip2
- curl
- shasum
- mysql

Those tools are builtin or easy to install in OSX (w/ XCode) as well as
in GNU/Linux distros.
"""
logger = logging.getLogger(__name__)


class DumpFetcher(object):
    """
    Ensure existence and validity of reference dump (and fix it if needed).
    """
    # Hardcoded values, to be consistent accross dev envs
    DUMP_FILENAME = 'endi-anonymous-514.sql'
    COMPRESSED_DUMP_FILENAME = DUMP_FILENAME + '.bz2'
    COMPRESSED_DUMP_URL = (
        'https://upload.majerti.fr/'
        +
        COMPRESSED_DUMP_FILENAME
    )
    # SHA256 sum of the *uncompressed* dump (.sql)
    DUMP_SHA256SUM = 'c76e38b36f5aa6053548d5267638fd5592dc8a8dc9e31dc0342f1685f47c5416'

    @classmethod
    def checksum_ok(cls):
        if sys.platform.lower() == 'darwin':
            command = ['shasum', '-a', '256', cls.DUMP_FILENAME]
        else:
            command = ['sha256sum', cls.DUMP_FILENAME]
        sha256_output = subprocess.check_output(command).decode('utf-8')
        sha256sum = sha256_output.split(' ')[0].strip()
        return sha256sum == cls.DUMP_SHA256SUM

    @classmethod
    def needs_download(cls):
        return not os.path.exists(cls.DUMP_FILENAME) or not cls.checksum_ok()

    @classmethod
    def fetch_reference_dump(cls):
        if cls.needs_download():
            logger.info(
                "{} not present or outdated,".format(cls.DUMP_FILENAME)
                + " fetching from {} (compressed).".format(
                    cls.COMPRESSED_DUMP_URL,
                )
            )
            subprocess.check_call([
                'curl', cls.COMPRESSED_DUMP_URL,
                '-o', cls.COMPRESSED_DUMP_FILENAME,
            ])
            subprocess.check_call(
                ['bunzip2', '-f',  cls.COMPRESSED_DUMP_FILENAME],
            )
            if not cls.checksum_ok():
                raise Exception(
                    "Bad checksum on freshly downloaded {}".format(
                        cls.DUMP_FILENAME,
                    )
                )
        else:
            logger.info(
                "Dump {} already present, checksum OK.".format(
                    cls.DUMP_FILENAME,
                )
            )
        return cls.DUMP_FILENAME


def load_dump(dump_path):
    db = DBSESSION()
    db_params = db.connection().engine.url

    if db_params.drivername != 'mysql':
        raise Exception("Only MySQL is supported")

    # Start fresh !
    db.execute('DROP DATABASE IF EXISTS {}'.format(db_params.database))
    db.execute('CREATE DATABASE {}'.format(db_params.database))
    db.execute('USE {}'.format(db_params.database))

    # Loading the dump from python uses too much resource and crashes
    subprocess.check_call(
        'mysql -u{} -p{} -h{} -P{} {} < {}'.format(
            db_params.username,
            db_params.password,
            db_params.host,
            db_params.port or 3306,
            db_params.database,
            dump_path,
        ),
        shell=True,
    )


def load_demo_data_entry_point():
    """ Download (if required) and load reference dump into database.
    Usage:
        endi-load-demo-data <config_uri>

    Requires those tools installed : bunzip2, wget, sha256sum, mysql.
    Writes downloaded files in current dir
    """
    def callback(arguments, env):
        dump_path = DumpFetcher.fetch_reference_dump()
        logger.info("Loading {} into DB".format(dump_path))
        load_dump(dump_path)
        logger.info('OK : Demo data dump {} loaded !'.format(dump_path))

    try:
        return command(callback, load_demo_data_entry_point.__doc__)
    finally:
        pass
