# -*- coding: utf-8 -*-
import logging
import datetime
import os
import sys
import inspect
import transaction
from collections import OrderedDict
from sqlalchemy.orm import load_only
from zope.sqlalchemy import mark_changed

from endi_base.models.base import DBSESSION
from endi.models.company import Company
from endi.scripts.utils import (
    command,
    get_argument_value,
)


def remove_listeners():
    from endi.models.task.task import stop_listening


class DatabaseCleaner(object):
    """
    Class used to clean the database and remove all informations not concerning
    the given company
    """

    def __init__(self, company, logger):
        self.logger = logger
        self.company = company
        self.company_id = company.id
        self.session = DBSESSION()
        self.methods = self._load_clean_methods()

    def _load_clean_methods(self):
        methods = {}
        for method_name, method in inspect.getmembers(
            self,
            inspect.ismethod
        ):
            if method_name.startswith('_clean_'):
                methods[method_name] = method

        keys = methods.keys()
        keys.sort()
        result = OrderedDict()
        for key in keys:
            result[key] = methods[key]
        return result

    # Clean all contents
    def _clean_all_competences(self):
        from endi.models.competence import (
            CompetenceDeadline,
            CompetenceScale,
            CompetenceOption,
            CompetenceSubOption,
        )
        self.session.execute("delete from competence_grid")
        self.session.execute("delete from competence_grid_item")
        self.session.execute("delete from competence_grid_sub_item")
        self.session.execute("delete from competence_requirement")
        mark_changed(self.session)

        for item in CompetenceDeadline.query():
            self.session.delete(item)

        for item in CompetenceScale.query():
            self.session.delete(item)

        for item in CompetenceOption.query():
            self.session.delete(item)

        for item in CompetenceSubOption.query():
            self.session.delete(item)

    def _clean_all_accompagnement(self):
        """
        Remove all workshops and activities
        """
        from endi.models.activity import Event

        for item in Event.query().options(load_only('id')):
            self.session.delete(item)

    def _clean_all_userdatas(self):
        from endi.models.user.userdatas import UserDatas

        for item in UserDatas.query().options(load_only('id')):
            self.session.delete(item)

    def _clean_all_holidays(self):
        self.session.execute("delete from holiday;")
        mark_changed(self.session)

    def _clean_all_mail_history(self):
        self.session.execute("delete from mail_history;")
        mark_changed(self.session)

    def _clean_all_templates(self):
        from endi.models.files import Template

        self.session.execute("delete from template_history")
        mark_changed(self.session)

        for item in Template.query().options(load_only('id')):
            self.session.delete(item)

    # Clean tout ce qui n'est pas associé à l'enseigne
    def _clean_a_tasks(self):
        from endi.models.task import Task
        for item in Task.query().options(
            load_only('id')
        ).filter(
            Task.company_id != self.company_id
        ):
            self.session.delete(item)

        self.session.execute('delete from task_status')
        mark_changed(self.session)

    def _clean_b_third_party(self):
        """
        Remove all third parties not related to the current company
        """
        from endi.models.third_party import ThirdParty

        for item in ThirdParty.query().filter(
                ThirdParty.company_id!=self.company_id
        ):
            self.session.delete(item)

    def _clean_b_bank_remittance(self):
        # lancé après le nettoyage des task -> des paiements
        from endi.models.task.payment import Payment, BankRemittance

        for item in BankRemittance.query(
            ).options(
                load_only('id')
            ).filter(
                BankRemittance.id.notin_(
                    self.session.query(Payment.bank_remittance_id).filter(
                        Payment.bank_remittance_id != None
                    )
                )
        ):
            self.session.delete(item)

    def _clean_supplier_orders(self):
        from endi.models.supply.supplier_order import SupplierOrder
        for item in SupplierOrder.query().options(
            load_only('id')
        ).filter(
            SupplierOrder.company_id != self.company_id
        ):
            self.session.delete(item)

    def _clean_supplier_invoices(self):
        from endi.models.supply.supplier_invoice import SupplierInvoice
        for item in SupplierInvoice.query().options(
            load_only('id')
        ).filter(
            SupplierInvoice.company_id != self.company_id
        ):
            self.session.delete(item)

    def _clean_c_project(self):
        from endi.models.project import Project

        for item in Project.query().options(
                load_only('id')
        ).filter(
            Project.company_id != self.company_id
        ):
            self.session.delete(item)

    def _clean_a_sale_product(self):
        # On supprime d'abord les Work pour éviter des soucis de forignkey
        # work_item -> base_sale_product_id
        from endi.models.sale_product.work import SaleProductWork

        for item in SaleProductWork.query().options(
            load_only('id')
        ).filter(
            SaleProductWork.company_id != self.company_id
        ):
            self.session.delete(item)

        from endi.models.sale_product.base import BaseSaleProduct

        for item in BaseSaleProduct.query().options(
            load_only('id')
        ).filter(
            BaseSaleProduct.company_id != self.company_id
        ):
            self.session.delete(item)

    def _clean_a_price_study(self):
        from endi.models.price_study.price_study import PriceStudy

        for item in PriceStudy.query().options(
            load_only('id')
        ).filter(
            PriceStudy.company_id != self.company_id
        ):
            self.session.delete(item)

    def _clean_accounting(self):
        from endi.models.accounting.base import BaseAccountingMeasureGrid
        self.session.execute("delete from accounting_operation")
        self.session.execute("delete from accounting_operation_upload")
        mark_changed(self.session)
        for item in BaseAccountingMeasureGrid.query().options(
            load_only('id')
        ).filter(
            BaseAccountingMeasureGrid.company_id != self.company_id
        ):
            self.session.delete(item)

    def _clean_y_users(self):
        from endi.models.user.user import User

        self.session.execute("delete from user_connections")
        mark_changed(self.session)
        self.session.flush()

        ids = [e.id for e in Company.get(self.company_id).employees]

        for user in self.session.query(User).filter(User.id.notin_(ids)):
            self.session.delete(user)

    def _clean_z_companies(self):
        """
        remove all companies except the current
        """
        query = DBSESSION().query(Company).options(load_only('id'))
        query = query.filter(Company.id != self.company_id)

        for company in query:
            if company.header_file:
                self.session.delete(company.header_file)
            if company.logo_file:
                self.session.delete(company.logo_file)
            self.session.delete(company)

    def run_method(self, method_name):
        """
        Runs a single clean method

        :param str method_name: The name with or without the _clean_ prefix
        """
        if not method_name.startswith('_clean_'):
            method_name = u"_clean_%s" % method_name
        if method_name in self.methods:
            self.logger.debug(u"Step : {0}".format(method_name))
            self.methods[method_name]()
            transaction.commit()
            transaction.begin()

    def run(self):
        for key in self.methods:
            self.run_method(key)


def company_export_command(arguments, env):
    """
    Entry point for the company export tools
    """
    logger = logging.getLogger(__name__)
    cid = arguments.get('CID')

    if cid is None:
        raise Exception("Missing mandatory cid")
    else:
        cid = int(cid)

    company = Company.get(cid)
    if company is None:
        raise Exception(u"No company with id {}".format(cid))

    print(u"Attention, vous vous apprêtez à nettoyer la base de données et à "
          u"ne conserver que les données de l'enseigne {}, cette action "
          u"est irréversible, assrez-vous d'avoir sauvegardé : la base de "
          u"données endi, la base de données endi-payment, les fichiers "
          u"déposés.\n(y/N)".format(cid))
    choice = raw_input().lower()

    if choice not in ('y', 'yes', 'o', 'oui'):
        print("Canceled")
        sys.exit(1)
    else:
        print("Continue")
        print(" + Removing listeners")
        remove_listeners()
        print(" + Cleaning")
        cleaner = DatabaseCleaner(company, logger)
        cleaner.run()
        from endi.scripts.endi_admin import user_add_command
        user_add_command(
            {
                '--user': 'admin',
                '--pwd': 'admin',
                '--group': 'admin'
            },
            env
        )

        print(" + Done")


def company_export_entry_point():
    """Company export utilitiy tool

    Clean the databases configured in the config_uri file and the files
    directory

    Usage:
        endi-company-export <config_uri> company CID

    Arguments:
        CID  Company object id

    Options:
        -h --help             Show this screen
    """
    def callback(arguments, env):
        if arguments['company']:
            func = company_export_command
        return func(arguments, env)

    try:
        return command(callback, company_export_entry_point.__doc__)
    finally:
        pass
