# -*- coding: utf-8 -*-


def test_avatar(dbsession, config, get_csrf_request, user, login):
    from endi.utils.avatar import get_avatar
    config.testing_securitypolicy(userid="login")
    request = get_csrf_request()
    request.dbsession = dbsession
    avatar = get_avatar(request)
    assert avatar.lastname == "Lastname"
