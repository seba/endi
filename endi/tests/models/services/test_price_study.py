# -*- coding: utf-8 -*-
import pytest
from endi.models.price_study.product import PriceStudyProduct
from endi.models.price_study.price_study import PriceStudy
from endi.models.services.price_study import (
    PriceStudyProductService,
)
from endi.tests.tools import Dummy
from endi.compute.math_utils import amount


@pytest.fixture
def study(company, user, project, dbsession):
    study = PriceStudy(
        company_id=company.id, owner_id=user.id, project_id=project.id,
        name="study",
    )
    dbsession.add(study)
    dbsession.flush()
    return study

@pytest.fixture
def product(study, dbsession):
    product = PriceStudyProduct(
        study_id=study.id,
        supplier_ht=100,
        general_overhead=0.1,
        margin_rate=0.1,
    )
    dbsession.add(product)
    dbsession.flush()
    return product


class TestPriceStudyProductService:

    def test_flat_cost(self, product):
       assert amount(PriceStudyProductService.flat_cost(product), 2) == 10000
       assert PriceStudyProductService.flat_cost(Dummy(supplier_ht=None)) == 0

    def test_cost_price(self, product):
        assert amount(PriceStudyProductService.cost_price(product), 2) == 11000

    def test_intermediate_price(self, product):
        assert amount(
            PriceStudyProductService.intermediate_price(product),
            2
        ) == 12222

    def test_unit_ht(self, product):
        assert amount(
            PriceStudyProductService.unit_ht(product, 10),
            2,
        ) == 13580
        # Ref #1877
        assert amount(
            PriceStudyProductService.unit_ht(product, 10.0),
            2,
        ) == 13580
