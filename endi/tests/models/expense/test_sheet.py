# -*- coding: utf-8 -*-
from endi.models.expense.sheet import (
    ExpenseKmLine,
    ExpenseSheet,
)


def test_expensekm_duplicate_ref_7774(mk_expense_type):
    kmtype = mk_expense_type(amount=1, year=2018)
    line = ExpenseKmLine(description="", km="", expense_type=kmtype)

    sheet = ExpenseSheet(year=2019, month=1)
    assert line.duplicate(sheet) is None

    kmtype2019 = mk_expense_type(amount=1, year=2019)
    assert line.duplicate(sheet).expense_type == kmtype2019
