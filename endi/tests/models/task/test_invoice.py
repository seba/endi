# -*- coding: utf-8 -*-
import datetime
import pytest
import colander


def test_set_numbers(invoice, cancelinvoice):
    invoice.date = datetime.date(2012, 12, 1)
    invoice.set_numbers(15, 1)
    assert invoice.internal_number == "Company 2012-12 F15"
    assert invoice.name == "Facture 1"

    cancelinvoice.date = datetime.date(2012, 12, 1)
    cancelinvoice.set_numbers(15, 5)
    assert cancelinvoice.name == "Avoir 5"
    assert cancelinvoice.internal_number == "Company 2012-12 A15"


def test_set_deposit_label(invoice):
    invoice.set_numbers(5, 8)
    invoice.set_deposit_label()
    assert invoice.name == "Facture d'acompte 8"


def test_set_sold_label(invoice):
    invoice.set_numbers(5, 8)
    invoice.set_sold_label()
    assert invoice.name == "Facture de solde 8"


def test_duplicate_invoice(dbsession, full_invoice, user):
    newinvoice = full_invoice.duplicate(
        user=user,
        project=full_invoice.project,
        phase=full_invoice.phase,
        customer=full_invoice.customer,
    )
    assert len(full_invoice.default_line_group.lines) == len(
        newinvoice.default_line_group.lines
    )
    assert len(full_invoice.discounts) == len(newinvoice.discounts)
    assert newinvoice.project == full_invoice.project
    assert newinvoice.company == full_invoice.company
    assert newinvoice.status_person == user
    assert newinvoice.phase == full_invoice.phase
    assert newinvoice.mentions == full_invoice.mentions
    for key in "customer", "address", "expenses_ht", "workplace", "start_date":
        assert getattr(newinvoice, key) == getattr(full_invoice, key)


def test_duplicate_invoice_financial_year(dbsession, full_invoice, user):
    full_invoice.financial_year = 1900
    newinvoice = full_invoice.duplicate(
        user=full_invoice.owner,
        project=full_invoice.project,
        phase=full_invoice.phase,
        customer=full_invoice.customer,
    )
    assert newinvoice.financial_year == datetime.date.today().year


def test_duplicate_invoice_integration(dbsession, invoice):
    dbsession.add(invoice)
    dbsession.flush()
    newest = invoice.duplicate(
        user=invoice.owner,
        project=invoice.project,
        phase=invoice.phase,
        customer=invoice.customer,
    )
    dbsession.add(newest)
    dbsession.flush()
    assert newest.phase_id == invoice.phase_id
    assert newest.owner_id == invoice.owner_id
    assert newest.status_person_id == invoice.status_person_id
    assert newest.project_id == invoice.project_id
    assert newest.company_id == invoice.company_id


def test_duplicate_invoice_mode_ttc(invoice):
    invoice.mode = 'ttc'
    duplicate_invoice = invoice.duplicate(
        user=invoice.owner,
        project=invoice.project,
    )
    assert duplicate_invoice.mode == invoice.mode
    assert duplicate_invoice.mode == 'ttc'


def test_duplicate_invoice_mode_ht(invoice):
    duplicate_invoice = invoice.duplicate(
        user=invoice.owner,
        project=invoice.project,
    )
    assert duplicate_invoice.mode == invoice.mode
    assert duplicate_invoice.mode == 'ht'  # default value


def test_valid_invoice(
    config, invoice_base_config, dbsession, invoice, request_with_config, user
):
    request_with_config.user = user
    dbsession.add(invoice)
    dbsession.flush()
    config.testing_securitypolicy(userid='test', permissive=True)

    invoice.set_status('wait', request_with_config)
    dbsession.merge(invoice)
    dbsession.flush()
    invoice.set_status('valid', request_with_config)
    assert invoice.official_number == '1'


def test_gen_cancelinvoice(dbsession, full_invoice, user):
    cinv = full_invoice.gen_cancelinvoice(user)
    dbsession.add(cinv)
    dbsession.flush()

    assert cinv.total_ht() == -1 * full_invoice.total_ht()
    today = datetime.date.today()
    assert cinv.date == today
    assert cinv.financial_year == full_invoice.financial_year
    assert cinv.mentions == full_invoice.mentions
    assert cinv.address == full_invoice.address
    assert cinv.workplace == full_invoice.workplace
    assert cinv.project == full_invoice.project
    assert cinv.company == full_invoice.company
    assert cinv.phase == full_invoice.phase
    assert cinv.start_date == full_invoice.start_date


def test_gen_cancelinvoice_mode_ttc(dbsession, invoice, user):
    invoice.mode = 'ttc'
    cinv = invoice.gen_cancelinvoice(user)
    dbsession.add(cinv)
    dbsession.flush()
    assert cinv.mode == invoice.mode
    assert cinv.mode == 'ttc'  # to be sure


def test_gen_cancelinvoice_mode_ht(dbsession, invoice, user):
    cinv = invoice.gen_cancelinvoice(user)
    dbsession.add(cinv)
    dbsession.flush()
    assert cinv.mode == invoice.mode
    assert cinv.mode == 'ht'  # default value


def test_gen_cancelinvoice_with_payment(
    dbsession, full_invoice, tva, mode, user
):
    from endi.models.task.payment import Payment
    payment = Payment(mode=mode.label, amount=10000000, tva=tva)
    full_invoice.payments = [payment]
    cinv = full_invoice.gen_cancelinvoice(user)
    assert len(cinv.default_line_group.lines) == len(
        full_invoice.default_line_group.lines) + len(full_invoice.discounts) + 1

    # Le paiement est indiqué ttc, ici on a le HT (tva inversée)
    assert cinv.default_line_group.lines[-1].cost == 8333333
    assert cinv.default_line_group.lines[-1].tva == 2000


def test_invoice_topay(full_invoice, user):
    from endi.models.task.invoice import Payment
    values = {'amount': 2000000}
    full_invoice.payments = [Payment(**values)]

    assert full_invoice.paid() == 2000000
    assert full_invoice.topay() == full_invoice.total() - 2000000


def test_check_resulted(full_invoice, user):
    from endi.models.task.invoice import Payment
    values = {'amount': int(full_invoice.topay())}
    full_invoice.payments = [Payment(**values)]

    full_invoice.status = 'valid'
    full_invoice.paid_status = 'paid'
    full_invoice.check_resulted()
    assert full_invoice.paid_status == 'resulted'


def test_check_resulted_force(full_invoice, request_with_config, user):
    full_invoice.status = 'valid'
    full_invoice.paid_status = 'paid'
    full_invoice.check_resulted(force_resulted=True)
    assert full_invoice.paid_status == 'resulted'


def test_resulted_auto_more(full_invoice, request_with_config, user):
    # the payment is more than ttc
    from endi.models.task.invoice import Payment
    values = {'amount': int(full_invoice.topay()) + 1}
    full_invoice.payments = [Payment(**values)]

    full_invoice.status = 'valid'
    full_invoice.paid_status = 'paid'
    full_invoice.check_resulted()
    assert full_invoice.paid_status == 'resulted'


def test_historize_paid_status(full_invoice, request_with_config, user):
    from endi.models.task.invoice import Payment
    values = {'amount': int(full_invoice.topay())}
    full_invoice.payments = [Payment(**values)]

    full_invoice.status = 'valid'
    full_invoice.paid_status = 'paid'
    full_invoice.check_resulted()
    full_invoice.historize_paid_status(user)
    assert full_invoice.statuses[-1].status_code == 'resulted'
    assert full_invoice.statuses[-1].status_person_id == user.id


def test_payment_get_amount():
    from endi.models.task.invoice import Payment
    payment = Payment(amount=1895000, mode="test")
    assert payment.get_amount() == 1895000
