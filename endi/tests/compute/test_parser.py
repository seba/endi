# -*- coding: utf-8 -*-
import pytest


def test_parser():
    from endi.compute.parser import NumericStringParser
    parser = NumericStringParser()
    assert parser.eval("2 * 4 / 100.0") == 0.08
    assert int(parser.eval("100.0 / 3")) == 33
    with pytest.raises(Exception):
        parser.eval("__import__('os').remove('/tmp/titi')")

