# -*- coding: utf-8 -*-
import datetime
from endi.tests.tools import Dummy


TODAY = datetime.date.today()


def test_add_invoice(
    config, get_csrf_request_with_db, project, phase, company,
    user, customer, default_business_type,
):
    from endi.models.task.invoice import Invoice
    from endi.views.project.routes import PROJECT_ITEM_INVOICE_ROUTE
    from endi.views.invoices.invoice import InvoiceAddView
    config.add_route('/invoices/{id}', "/")
    value = {
        "name": "Facture",
        'business_type_id': default_business_type.id,
        'project_id': project.id,
        'phase_id': phase.id,
        'customer_id': customer.id,
    }

    request = get_csrf_request_with_db()
    request.context = project
    request.current_company = company.id
    request.matched_route = Dummy(name=PROJECT_ITEM_INVOICE_ROUTE)
    request.user = user
    view = InvoiceAddView(request)
    view.submit_success(value)

    # view.submit_success(value)
    invoice = Invoice.query().first()

    assert invoice.name == "Facture"
    assert invoice.phase_id == phase.id
    assert invoice.customer_id == customer.id
    assert invoice.project_id == project.id
    assert invoice.business_type_id == default_business_type.id
