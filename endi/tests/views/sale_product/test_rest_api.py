# -*- coding: utf-8 -*-


def test_product_add(get_csrf_request_with_db, company):
    request = get_csrf_request_with_db(
        post={
            'type_': 'sale_product_material',
            'label': "label"
        }
    )
    from endi.views.sale_product.rest_api import RestSaleProductView

    view = RestSaleProductView(company, request)
    result = view.post()
    json_result = result.__json__(None)

    assert json_result['label'] == 'label'
    assert json_result['type_'] == 'sale_product_material'
    assert json_result['company_id'] == company.id


def test_work_add(get_csrf_request_with_db, company):
    request = get_csrf_request_with_db(
        post={
            'type_': 'sale_product_work',
            'label': "label"
        }
    )
    from endi.views.sale_product.rest_api import RestSaleProductView
    from endi.models.sale_product.work import SaleProductWork

    view = RestSaleProductView(company, request)
    result = view.post()
    assert isinstance(result, SaleProductWork)
    json_result = result.__json__(None)

    assert json_result['label'] == 'label'
    assert json_result['type_'] == 'sale_product_work'
    assert json_result['company_id'] == company.id


# TODO :
#      Drop subclasses of BaseSaleProduct ?

def test_edit_product(
    get_csrf_request_with_db, mk_sale_product, tva, product
):
    sale_product = mk_sale_product(
        label='label',
        type_='sale_product_material',
        tva_id=tva.id,
        product_id=product.id,
        supplier_ht=100000,
        general_overhead=0.11,
        margin_rate=0.12,
    )
    sale_product.on_before_commit('add')
    assert int(sale_product.ht) == 126136

    request = get_csrf_request_with_db(
        post={
            'label': 'New label',
            'description': 'Description',
            'supplier_ht': "",
            'ht': 1,
            'tva_id': "",
        }
    )
    from endi.views.sale_product.rest_api import RestSaleProductView

    view = RestSaleProductView(sale_product, request)
    result = view.put()
    json_result = result.__json__(None)

    assert json_result['ht'] == 1
    assert sale_product.ht == 100000
    assert json_result['label'] == 'New label'
    assert json_result['description'] == 'Description'
    assert json_result['tva_id'] == None
    assert json_result['product_id'] == None


def test_edit_work(
    get_csrf_request_with_db, mk_sale_product_work, tva, product
):
    sale_product_work = mk_sale_product_work(
        label='label',
        tva_id=tva.id,
        product_id=product.id,
        general_overhead=0.11,
        margin_rate=0.12,
    )
    sale_product_work.on_before_commit('add')

    request = get_csrf_request_with_db(
        post={
            'label': 'New label',
            'description': 'Description',
            'ht': 1,
            'tva_id': "",
            'margin_rate': '',
        }
    )
    from endi.views.sale_product.rest_api import RestSaleProductView

    view = RestSaleProductView(sale_product_work, request)
    result = view.put()
    json_result = result.__json__(None)

    assert json_result['ht'] == 0
    assert json_result['label'] == 'New label'
    assert json_result['description'] == 'Description'
    assert json_result['tva_id'] == None
    assert json_result['product_id'] == None
    assert json_result['margin_rate'] == None


def test_add_work_item(
    get_csrf_request_with_db, mk_sale_product_work, tva, product
):
    sale_product_work = mk_sale_product_work(
        label='label',
        tva_id=tva.id,
        general_overhead=0.11,
        margin_rate=0.12,
    )

    request = get_csrf_request_with_db(
        post={
            'label': 'Label',
            'description': 'Description',
            'supplier_ht': 1,
            'quantity': 2,
            "type_": 'sale_product_material'
        }
    )
    request.context = sale_product_work
    from endi.views.sale_product.rest_api import RestWorkItemView

    view = RestWorkItemView(sale_product_work, request)
    result = view.post()
    assert not result.locked

    json_result = result.__json__(None)
    assert json_result['description'] == 'Description'
    # Inherited
    assert json_result['tva_id'] == tva.id
    assert json_result['product_id'] == None
    assert json_result['tva_id_editable'] == False
    assert json_result['product_id_editable'] == True
    assert json_result['margin_rate_editable'] == False
    assert json_result['general_overhead_editable'] == False

    # We've generated a base_sale_product and not locked the work_item
    assert json_result['supplier_ht_editable'] == True

    # Computing
    assert json_result['ht'] == 1.26136
    assert json_result['total_ht'] == 2.52273

    assert int(sale_product_work.ht) == 252272

    # Complete BaseSaleProduct
    assert json_result['locked'] == False
    assert result.base_sale_product is not None
    assert result.base_sale_product.label == 'Label'
    assert result.base_sale_product.supplier_ht == 100000
    assert result.base_sale_product.margin_rate == None


def test_edit_work_item(
    get_csrf_request_with_db, mk_sale_product_work,
    mk_sale_product_work_item,
    mk_sale_product,
    tva, product
):
    sale_product_work = mk_sale_product_work(
        label='label',
        tva_id=tva.id,
        general_overhead=0.11,
        margin_rate=0.12,
    )
    sale_product = mk_sale_product(
        supplier_ht=100000,
        description="Description",
    )
    work_item = mk_sale_product_work_item(
        description="Description",
        sale_product_work=sale_product_work,
        _supplier_ht=200000,
        quantity=2,
        base_sale_product=sale_product,
    )

    work_item.on_before_commit('add')
    # This value is inherited from the base_sale_product
    assert int(work_item.ht) == 126136

    request = get_csrf_request_with_db(
        post={
            'label': 'Label',
            'description': 'Description',
            'supplier_ht': '',
            'ht': 20,
            'quantity': 5,
            'locked': False
        }
    )
    request.context = work_item
    from endi.views.sale_product.rest_api import RestWorkItemView

    view = RestWorkItemView(work_item, request)
    result = view.put()
    json_result = result.__json__(None)

    # Not locked anymore
    assert json_result['ht'] == 20
    assert json_result['total_ht'] == 100

    assert sale_product_work.ht == 10000000


def test_delete_work_item(
    get_csrf_request_with_db, mk_sale_product_work,
    mk_sale_product_work_item,
    mk_sale_product,
    tva, product
):
    sale_product_work = mk_sale_product_work(
        label='label',
        tva_id=tva.id,
        general_overhead=0.11,
        margin_rate=0.12,
    )
    sale_product = mk_sale_product(
        supplier_ht=100000,
        description="Description",
    )
    work_item1 = mk_sale_product_work_item(
        description="Description",
        sale_product_work=sale_product_work,
        _supplier_ht=200000,
        quantity=2,
        base_sale_product=sale_product,
    )
    work_item2 = mk_sale_product_work_item(
        description="Description 2",
        sale_product_work=sale_product_work,
        _supplier_ht=200000,
        quantity=2,
        base_sale_product=sale_product,
        locked=False,
    )

    work_item1.on_before_commit('add')
    work_item2.on_before_commit('add')
    # This value is inherited from the base_sale_product
    assert int(work_item1.ht) == 126136
    # This value is not inherited (locked is False)
    assert int(work_item2.ht) == 252272

    assert int(sale_product_work.ht) == 756818

    request = get_csrf_request_with_db()
    from endi.views.sale_product.rest_api import RestWorkItemView

    view = RestWorkItemView(work_item1, request)
    view.delete()

    assert int(sale_product_work.ht) == 504545
