# -*- coding: utf-8 -*-
import pytest
import colander


def test_add_company_shema(user, pyramid_request):
    from endi.forms.company import CompanySchema
    schema = CompanySchema()
    pyramid_request.config = {}
    schema = schema.bind(request=pyramid_request)

    args = {
        "user_id": user.id,
        "name": "Ma petite entreprise",
        "goal": "Be Happy",
        "general_customer_account": "09890987",
        "third_party_customer_account": "00098765",
        "general_supplier_account": "0004534231",
        "third_party_supplier_account": "66661111",
    }
    result = schema.deserialize(args)
    assert result['user_id'] == user.id
    assert result['name'] == "Ma petite entreprise"
    assert result['goal'] == "Be Happy"
    assert result['general_customer_account'] == "09890987"
    assert result['third_party_customer_account'] == "00098765"
    assert result['general_supplier_account'] == "0004534231"
    assert result['third_party_supplier_account'] == "66661111"

    # mandatory fields
    wrong = args.copy()
    wrong.pop('name')
    with pytest.raises(colander.Invalid):
        schema.deserialize(wrong)

    wrong = args.copy()
    wrong['goal'] = ''
    with pytest.raises(colander.Invalid):
        schema.deserialize(wrong)
