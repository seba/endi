# -*- coding: utf-8 -*-
import logging

from endi_base.models import DBSESSION
from endi.compute.math_utils import (
    compute_tva,
    floor_to_precision,
    percentage,
)

logger = logging.getLogger(__name__)


class PriceStudyService(object):
    @classmethod
    def add_filter_with_estimation(cls, query):
        """
        Add a filter restricting the query result to those pointing to a task
        """
        from endi.models.price_study.price_study import PriceStudy
        from endi.models.task.task import Task

        ids = [a[0] for a in DBSESSION().query(Task.price_study_id).all()]
        return query.filter(PriceStudy.id.in_(ids))

    @classmethod
    def is_deletable(cls, item):
        from endi.models.task.task import Task
        return DBSESSION().query(Task.id).filter_by(
            price_study_id=item.id
        ).count() == 0

    @classmethod
    def is_editable(cls, item):
        from endi.models.task.task import Task
        return DBSESSION().query(Task.id).filter_by(
            price_study_id=item.id
        ).filter_by(status='valid').count() == 0

    @classmethod
    def can_generate(cls, item):
        from endi.models.task.task import Task
        return DBSESSION().query(Task.id).filter_by(
            price_study_id=item.id
        ).count() == 0

    @classmethod
    def amounts_by_tva(cls, price_study):
        """
        Collect HT and TVA amounts stored by tva value (in integer format)

        e.g : {tva_id: {'ht': 1000, 'tva': 200}}
        """
        if not hasattr(price_study, '_amount_cache'):
            result = {}
            for product in price_study.products:
                for tva, ht in list(product.ht_by_tva().items()):
                    result.setdefault(tva, {'ht': 0, 'tva': 0})
                    result[tva]['ht'] += ht

            for tva, amounts in list(result.items()):
                ht = amounts['ht']
                result[tva]['ht'] = floor_to_precision(ht)

                result[tva]['tva'] = floor_to_precision(
                    compute_tva(
                        ht,
                        max(tva.value, 0)  # Cas des tvas à taux négatifs
                    )
                )

            setattr(price_study, '_amount_cache', result)
        return price_study._amount_cache

    @classmethod
    def discounts_by_tva(cls, price_study):
        result = {}
        for discount in price_study.discounts:
            for tva, ht in list(discount.ht_by_tva().items()):
                result.setdefault(tva, {'ht': 0, 'tva': 0})
                result[tva]['ht'] -= ht

        for tva, amounts in list(result.items()):
            ht = amounts['ht']
            result[tva]['ht'] = floor_to_precision(ht)

            result[tva]['tva'] = floor_to_precision(
                compute_tva(
                    ht,
                    max(tva.value, 0)  # Cas des tvas à taux négatifs
                )
            )
        return result

    @classmethod
    def total_ht_before_discount(cls, price_study):
        return sum(
            amount['ht']
            for amount in list(cls.amounts_by_tva(price_study).values())
        )

    @classmethod
    def discount_ht(cls, price_study):
        return sum(
            amount['ht']
            for amount in list(cls.discounts_by_tva(price_study).values())
        )

    @classmethod
    def total_ht(cls, price_study):
        return cls.total_ht_before_discount(price_study) + \
            cls.discount_ht(price_study)

    @classmethod
    def total_tva_before_discount(cls, price_study):
        return sum(
            amount['tva']
            for amount in list(cls.amounts_by_tva(price_study).values())
        )

    @classmethod
    def discount_tva(cls, price_study):
        return sum(
            amount['tva']
            for amount in list(cls.discounts_by_tva(price_study).values())
        )

    @classmethod
    def total_tva(cls, price_study):
        return cls.total_tva_before_discount(price_study) + \
            cls.discount_tva(price_study)

    @classmethod
    def total_ttc(cls, price_study):
        return cls.total_ht(price_study) + cls.total_tva(price_study)

    @classmethod
    def sync_amounts(cls, price_study):
        if hasattr(price_study, '_amount_cache'):
            delattr(price_study, '_amount_cache')
        price_study.ht = cls.total_ht(price_study)
        DBSESSION().merge(price_study)
        return True


class BasePriceStudyProductService(object):

    @classmethod
    def get_company_id(cls, instance):
        from endi.models.price_study.price_study import PriceStudy
        return DBSESSION().query(PriceStudy.company_id).filter_by(
            id=instance.study_id
        ).scalar()


class PriceStudyProductService(BasePriceStudyProductService):

    @classmethod
    def flat_cost(cls, product):
        return product.supplier_ht or 0

    @classmethod
    def cost_price(cls, product):
        """
        Compute the cost price of the given product work suming the cost of
        its differect items

        :returns: The result in 10*5 format

        :rtype: int
        """
        overhead = product.general_overhead
        if overhead is None:
            overhead = 0

        supplier_ht = product.flat_cost()
        result = supplier_ht * (1 + overhead)
        return result

    @classmethod
    def intermediate_price(cls, product):
        """
        Compute the intermediate price of a work item

        3/    Prix intermédiaire = Prix de revient / ( 1 - ( Coefficients marge
        + aléas + risques ) )
        """
        margin_rate = product.margin_rate
        if margin_rate is None:
            margin_rate = 0

        if margin_rate != 1:
            result = product.cost_price()
            result = result / (1 - margin_rate)
        else:
            result = 0

        return result

    @classmethod
    def unit_ht(cls, product, cae_contribution=None):
        """
        Compute the ht value for the given work item
        """
        from endi.models.company import Company
        if cae_contribution is None:
            cae_contribution = Company.get_cae_contribution(
                product.get_company_id()
            )

        intermediate_price = product.intermediate_price()
        result = intermediate_price
        if intermediate_price != 0:
            if isinstance(cae_contribution, (int, float)):
                ratio = 1 - cae_contribution / 100.0
                if ratio != 0:
                    result = intermediate_price / ratio
        else:
            result = product.ht or 0

        return result

    @classmethod
    def compute_total_ht(cls, product, cae_contribution=None):
        """
        Compute total_ht value for this element
        """
        return product.unit_ht(cae_contribution) * product.quantity

    @classmethod
    def ttc(cls, product, cae_contribution=None):
        tva = product.tva
        ht = product.compute_total_ht(cae_contribution)
        return ht + compute_tva(ht, tva.value)

    @classmethod
    def ht_by_tva(cls, product):
        """
        Return the ht value stored by vta value
        """
        if product.tva:
            return {product.tva: product.total_ht}
        else:
            return {}

    @classmethod
    def sync_amounts(cls, product, cae_contribution=None):
        """
        Setup amounts for the given product and fire the information up
        """
        product.ht = product.unit_ht()
        product.total_ht = product.compute_total_ht()
        DBSESSION().merge(product)
        product.study.sync_amounts()
        return True

    @classmethod
    def _ensure_tva(cls, product):
        """
        Ensure cohesion between tva and product configuration

        Necessary because we can edit one and not the other leading to
        undesired states
        """
        # We ensure tva/product integrity
        if product.tva_id is None:
            product.product_id = None
        elif (
            product.product is not None and
            product.product.tva_id != product.tva_id
        ):
            product.product_id = None

    @classmethod
    def on_before_commit(cls, product, state, changes=None):
        """
        :param str state: 'add'/'update'
        """
        if state == 'delete':
            need_sync = False
            parent = product.study
            if product in parent.products:
                parent.products.remove(product)
            parent.sync_amounts()
        elif state == 'add':
            need_sync = True
        elif state == 'update':
            if 'tva_id' in changes:
                cls._ensure_tva(product)

            need_sync = False
            if changes is not None:
                for key in (
                    'supplier_ht', 'ht', 'margin_rate', 'general_overhead',
                    'quantity'
                ):
                    if key in changes:
                        need_sync = True
                        break
            else:
                need_sync = True

        if need_sync:
            cls.sync_amounts(product)


class PriceStudyWorkItemService(object):

    @classmethod
    def get_company_id(cls, work_item):
        from endi.models.price_study.price_study import PriceStudy
        return DBSESSION().query(PriceStudy.company_id).filter_by(
            id=work_item.price_study_work.study_id
        ).scalar()

    @classmethod
    def flat_cost(cls, work_item, unitary=False, work_level=False):
        result = work_item.supplier_ht or 0
        if unitary:
            if work_level:
                # Coût par unité d'ouvrage
                quantity = work_item.work_unit_quantity or 1
            else:
                # Coût unitaire de ce poste
                quantity = 1
        else:
            # Coût dans l'ouvrage
            quantity = work_item.total_quantity
        return quantity * result

    @classmethod
    def cost_price(cls, work_item, unitary=False):
        """
        Compute the cost price of the given price study work suming the cost of
        its different items

        :returns: The result in 10*5 format

        :rtype: int
        """
        overhead = work_item.general_overhead
        if not overhead:
            overhead = 0

        supplier_ht = work_item.flat_cost(unitary)
        result = supplier_ht * (1 + overhead)
        return result

    @classmethod
    def intermediate_price(cls, work_item, unitary=False):
        """
        Compute the intermediate price of a work item

        3/    Prix intermédiaire = Prix de revient / ( 1 - ( Coefficients marge
        + aléas + risques ) )
        """
        margin_rate = work_item.margin_rate
        if not margin_rate:
            margin_rate = 0

        if margin_rate != 1:
            result = cls.cost_price(work_item, unitary)
            result = result / (1 - margin_rate)
        else:
            result = 0

        return result

    @classmethod
    def unit_ht(cls, work_item, cae_contribution=None):
        """
        Compute the ht value for the given work item
        """
        from endi.models.company import Company
        if cae_contribution is None:
            company_id = cls.get_company_id(work_item)
            cae_contribution = Company.get_cae_contribution(company_id)

        intermediate_price = cls.intermediate_price(work_item, unitary=True)
        if intermediate_price != 0:
            result = intermediate_price
            if isinstance(cae_contribution, (int, float)):
                ratio = 1 - cae_contribution / 100.0
                if ratio != 0:
                    result = intermediate_price / ratio
        else:
            result = work_item.ht or 0
        return result

    @classmethod
    def compute_work_unit_ht(cls, work_item, cae_contribution=None):
        """
        Compute the ht value per work unit for the given work item
        """
        return cls.unit_ht(work_item, cae_contribution) * \
            work_item.work_unit_quantity

    @classmethod
    def compute_total_ht(cls, work_item, cae_contribution=None):
        """
        Compute the total ht for the given work_item
        """
        unit_ht = work_item.unit_ht(cae_contribution)
        total_quantity = work_item.total_quantity or 1
        return unit_ht * total_quantity

    @classmethod
    def compute_total_tva(cls, work_item, cae_contribution=None):
        ht = cls.compute_total_ht(work_item, cae_contribution)
        tva = work_item.tva
        if tva is not None:
            return compute_tva(ht, tva.value)
        else:
            return 0

    @classmethod
    def ttc(cls, work_item, cae_contribution=None):
        """
        Calcul du ttc indicatif pour ce work_item

        NB : les arrondis se faisant sur les totaux au niveau du devis/facture,
        cette valeur peut être imprécise
        """
        ht = cls.compute_total_ht(work_item, cae_contribution)
        tva = work_item.tva
        if tva is not None:
            return ht + compute_tva(ht, tva.value)
        else:
            return ht

    @classmethod
    def ht_by_tva(cls, work_item):
        """
        Return the ht value stored by vta value
        """
        if work_item.tva:
            return {work_item.tva: work_item.total_ht}
        else:
            return {}

    @classmethod
    def _ensure_tva(cls, work_item):
        """
        Ensure cohesion between tva and product configuration

        Necessary because we can edit one and not the other leading to
        undesired states
        """
        # We ensure tva/product integrity
        if work_item.tva_id is None:
            work_item._product_id = None
        elif (
            work_item._product is not None and
            work_item._product.tva_id != work_item.tva_id
        ):
            work_item._product_id = None

    @classmethod
    def sync_amounts(cls, work_item, work=None):
        work_item.ht = work_item.unit_ht()
        work_item.work_unit_ht = work_item.compute_work_unit_ht()
        work_item.total_ht = work_item.compute_total_ht()
        DBSESSION().merge(work_item)

        # On update le work que si ce n'est pas lui qui a fait la demande
        # initiale
        if work is None:
            work_item.price_study_work.sync_amounts()
        return True

    @classmethod
    def sync_quantities(cls, work_item, work=None):
        # Quantities are synced only if the quantity is inherited
        work_unit_quantity = work_item.work_unit_quantity
        if work_item.quantity_inherited:
            if work is None:
                work = work_item.price_study_work
            work_item.total_quantity = work_unit_quantity * work.quantity
        else:
            work_item.total_quantity = work_unit_quantity
        DBSESSION().merge(work_item)
        return True

    @classmethod
    def on_before_commit(cls, work_item, state, changes=None):
        """
        :param str state: 'add'/'update'/'delete'
        """
        if state == 'delete':
            parent = work_item.work
            if work_item in parent.items:
                parent.items.remove(work_item)
            parent.sync_amounts()
        else:
            cls.sync_quantities(work_item)
            cls.sync_amounts(work_item)
            cls._ensure_tva(work_item)


class PriceStudyWorkService(BasePriceStudyProductService):

    @classmethod
    def flat_cost(cls, work):
        """
        Compute the flat cost of a complex work

        1/    Déboursé sec = Total matériaux + Total main d'oeuvre + Total
        matériel affecté
        """
        return sum([item.flat_cost(work_level=True) for item in work.items])

    @classmethod
    def cost_price(cls, work):
        """
        Compute the cost price of the given work work suming the cost of
        its differect items

        If globally specified, uses the work's general overhead
        for the computation

        Prix de revient = Déboursé sec * ( 1 + Coefficient frais généraux )
        """
        return sum(
            [
                item.cost_price()
                for item in work.items
            ]
        )

    @classmethod
    def intermediate_price(cls, work):
        """
        Compute the intermediate price

        If globally specified, uses the work's margin rate for the
        computation
        3/    Prix intermédiaire = Prix de revient / ( 1 - ( Coefficients marge
        + aléas + risques ) )
        """
        return sum(
            [
                item.intermediate_price()
                for item in work.items
            ]
        )

    @classmethod
    def unit_ht(cls, work, cae_contribution=None):
        return sum(
            [
                item.work_unit_ht for item in work.items
            ]
        )

    @classmethod
    def compute_total_ht(cls, work, cae_contribution=None):
        return sum([
            item.total_ht for item in work.items
        ])

    @classmethod
    def ttc(cls, work, cae_contribution=None):
        return sum([item.ttc(cae_contribution) for item in work.items])

    @classmethod
    def ht_by_tva(cls, work):
        """
        Return the ht value stored by vta value
        """
        result = {}
        for work_item in work.items:
            for tva_value, ht in list(work_item.ht_by_tva().items()):
                if tva_value not in result:
                    result[tva_value] = ht
                else:
                    result[tva_value] += ht
        return result

    @classmethod
    def _ensure_tva(cls, work):
        """
        Ensure cohesion between tva and product configuration

        Necessary because we can edit one and not the other leading to
        undesired states
        """
        # We ensure tva/product integrity
        if work.tva_id is None:
            work.product_id = None
        elif (
            work.product is not None and
            work.product.tva_id != work.tva_id
        ):
            work.product_id = None

        for item in work.items:
            PriceStudyWorkItemService._ensure_tva(item)

    @classmethod
    def sync_amounts(cls, work):
        """
        Set all amounts on this work entry
        """
        work.ht = work.unit_ht()
        work.total_ht = work.compute_total_ht()
        DBSESSION().merge(work)
        work.study.sync_amounts()
        return True

    @classmethod
    def sync_quantities(cls, work):
        """
        Sync all work items quantities and update amounts
        """
        for item in work.items:
            item.sync_quantities(work)
            item.sync_amounts(work)

    @classmethod
    def on_before_commit(cls, work, state, changes=None):
        if state == 'delete':
            need_sync = False
            parent = work.study
            if work in parent.products:
                parent.products.remove(work)
            parent.sync_amounts()
        elif state == 'add':
            need_sync = True
        else:
            need_sync = False
            if changes:
                # On update les totaux uniquement si certains attributs ont été
                # modifiés
                for key in ('quantity', 'general_overhead', 'margin_rate'):
                    if key in changes:
                        need_sync = True
            else:
                need_sync = True

            if 'tva_id' in changes:
                cls._ensure_tva(work)

        if need_sync:
            cls.sync_quantities(work)
            cls.sync_amounts(work)

    @classmethod
    def find_common_value(cls, work, attrname):
        """
        Try to find a common field shared by all work_items

        :param obj work: The Work instance
        :param str attrname: The name of the attribute
        """
        result = getattr(work, attrname, None)

        if result is None:
            distinct_values = set()
            for item in work.items:
                value = getattr(work, attrname, None)
                distinct_values.add(value)

            if len(distinct_values) > 1:
                raise Exception(
                    "Work items of work {} have distinct values for {}".format(
                        work.id, attrname
                    )
                )
            result = distinct_values[0]
        return result


class PriceStudyDiscountService(object):

    @classmethod
    def ht_by_tva(cls, discount):
        result = {}
        if discount.is_percentage:
            for tva, values in list(discount.price_study.amounts_by_tva().items()):
                result.setdefault(tva, 0)
                result[tva] += percentage(values['ht'], discount.percentage)
        else:
            result[discount.tva] = discount.amount
        return result

    @classmethod
    def total_ht(cls, discount):
        return sum(ht for ht in list(cls.ht_by_tva(discount).values()))

    @classmethod
    def total_tva(cls, discount):
        value = 0
        for tva, ht in list(cls.ht_by_tva(discount).items()):
            value += compute_tva(ht, tva.value)
        return value

    @classmethod
    def total_ttc(cls, discount):
        return cls.total_ht(discount) + cls.total_tva(discount)

    @classmethod
    def on_before_commit(cls, discount, state, attributes):
        sync = True
        if state == 'update':
            keys = ['tva_id', 'amount']
            if not set(attributes.keys()).intersection(set(keys)):
                sync = False

        if sync:
            discount.price_study.sync_amounts()

    @classmethod
    def get_company_id(cls, instance):
        from endi.models.price_study.price_study import PriceStudy
        return DBSESSION().query(PriceStudy.company_id).filter_by(
            id=instance.price_study_id
        ).scalar()
