# -*- coding: utf-8 -*-
import logging

from sqlalchemy import (
    distinct,
    not_,
)

from endi_base.models.base import DBSESSION


logger = logging.getLogger(__name__)


class BusinessService(object):
    """
    Service class provding common Business related tools
    """

    @classmethod
    def invoicing_years(cls, business):
        """
        :returns list: of numbers
        """

        from endi.models.task.invoice import Invoice
        q = DBSESSION.query(
            distinct(Invoice.financial_year),
        ).filter(
            Invoice.business == business,
        )
        return [i[0] for i in q]

    @classmethod
    def get_price_studies(cls, business):
        """
        Collect PriceStudy entries associated to the given business
        """
        from endi.models.task import Task
        from endi.models.price_study.price_study import PriceStudy
        ids = [
            t[0] for t in DBSESSION().query(
                Task.price_study_id
            ).filter_by(
                business_id=business.id
            ).filter(Task.price_study_id != None)  # noqa E711
        ]
        return PriceStudy.query().filter(PriceStudy.id.in_(ids)).all()

    @classmethod
    def to_invoice(cls, business):
        """
        Compute the amount that is supposed to be invoiced regarding the
        estimation and the existing invoices

        :param obj business: The business instance
        :returns: The amount to be invoiced (in *10^5 format)
        ;rtype: int
        """
        to_invoice = sum(
            [estimation.ht for estimation in business.estimations]
        )
        invoiced = sum([invoice.ht for invoice in business.invoices])
        return max(to_invoice - invoiced, 0)

    @classmethod
    def _add_payment_deadline(cls, business, payment_line, estimation):
        """
        Add a payment deadline for the given payment line to the business
        deadlines
        """
        from endi.models.project.business import BusinessPaymentDeadline
        if payment_line not in business.payment_lines:
            business.payment_deadlines.append(
                BusinessPaymentDeadline(
                    payment_line=payment_line, estimation=estimation
                )
            )

    @classmethod
    def _add_deposit_deadline(cls, business, estimation):
        """
        Add a deposit deadline to a business
        """
        deposit = estimation.deposit
        if not deposit:
            return business
        from endi.models.project.business import BusinessPaymentDeadline
        query = BusinessPaymentDeadline.query()
        query = query.filter_by(business_id=business.id)
        query = query.filter_by(estimation_id=estimation.id)
        query = query.filter_by(deposit=True)
        if query.count() == 0:
            business.payment_deadlines.append(
                BusinessPaymentDeadline(
                    business_id=business.id,
                    estimation_id=estimation.id,
                    deposit=True,
                )
            )
            DBSESSION().merge(business)
        return business

    @classmethod
    def populate_deadlines(cls, business, estimation=None):
        """
        Populate the business deadlines with those described in the associated
        estimation(s)

        :param obj business: The Business instance
        :param obj estimation: Optionnal Estimation instance
        :returns: The Business instance
        :rtype: obj
        """
        logger.debug(
            "Populating deadlines for the business {}".format(business.id)
        )
        if estimation is not None:
            estimations = [estimation]
        else:
            estimations = business.estimations
        for estimation in estimations:
            cls._add_deposit_deadline(business, estimation)
            for payment_line in estimation.payment_lines:
                cls._add_payment_deadline(business, payment_line, estimation)

        return DBSESSION().merge(business)

    @classmethod
    def find_deadline(cls, business, deadline_id):
        """
        Find the deadline matching this id

        :param obj business: The parent Business
        :param int deadline_id: The associated deadline_id
        """
        from endi.models.project.business import BusinessPaymentDeadline
        result = BusinessPaymentDeadline.get(deadline_id)
        if result.business_id != business.id:
            result = None
        return result

    @classmethod
    def find_deadline_from_invoice(cls, business, invoice):
        """
        Find the deadline having this invoice attached to it

        :param obj business: The parent Business
        :param obj invoice: The associated Invoice
        """
        from endi.models.project.business import BusinessPaymentDeadline
        result = BusinessPaymentDeadline.query().filter_by(
            invoice_id=invoice.id
        ).filter_by(
            business_id=business.id
        ).first()
        return result

    @classmethod
    def get_deposit_deadlines(cls, business, waiting=True):
        """
        Find deadlines related to deposists

        :returns: List of deadlines
        """
        from endi.models.project.business import BusinessPaymentDeadline
        result = BusinessPaymentDeadline.query().filter_by(
            business_id=business.id
        ).filter_by(deposit=True)
        if waiting:
            result = result.filter_by(invoice_id=None)
        return result.all()

    @classmethod
    def gen_invoices(cls, business, user, payment_deadlines=None):
        """
        Generate the invoices associated to the given payment deadlines

        :param obj business: The Business in which we work
        :param obj user: The current connected user
        :param list payment_deadlines: Optionnal the deadlines for which we
        generate invoices else all deadlines
        :returns: A list of invoices
        """
        if not payment_deadlines:
            payment_deadlines = business.payment_deadlines
        elif not hasattr(payment_deadlines, '__iter__'):
            payment_deadlines = [payment_deadlines]

        invoices = []
        for deadline in payment_deadlines:
            estimation = deadline.estimation
            if deadline.deposit:
                invoice = estimation.gen_deposit_invoice(
                    user,
                )
            else:
                invoice = estimation.gen_invoice(
                    deadline.payment_line,
                    user,
                )
            invoice.initialize_business_datas(business)
            DBSESSION().add(invoice)
            DBSESSION().flush()
            deadline.invoice_id = invoice.id
            DBSESSION().merge(deadline)
            invoices.append(invoice)
        return invoices

    @classmethod
    def is_complex_project_type(cls, business):
        """
        Check if the parent's project type is of type default

        :param obj business: The current business instance this service is
        attached to
        :rtype: bool
        """
        from endi.models.project.project import Project
        from endi.models.project.types import ProjectType
        project_type_id = DBSESSION().query(
            Project.project_type_id
        ).filter_by(
            id=business.project_id
        ).scalar()

        ptype_with_business = DBSESSION().query(
            ProjectType.with_business
        ).filter_by(
            id=project_type_id
        ).scalar()
        return ptype_with_business

    @classmethod
    def add_estimation(cls, business, user):
        """
        Add a new estimation to the current business

        :param obj business: The current business instance this service is
        attached to
        :returns: A new Estimation instance
        """
        from endi.models.task.estimation import Estimation
        customer = cls.get_customer(business)
        estimation = Estimation(
            user=user,
            company=business.project.company,
            project=business.project,
            customer=customer,
            business_id=business.id,
            business_type_id=business.business_type_id,
        )
        estimation.add_default_payment_line()
        estimation.initialize_business_datas()
        if business.invoicing_mode == business.PROGRESS_MODE:
            cls.populate_progress_invoicing_status(business)
        DBSESSION().add(estimation)
        DBSESSION().flush()
        return estimation

    @classmethod
    def add_invoice(cls, business, user):
        """
        Freely add a new invoice to the current business

        :param obj business: The current business instance this service is
        attached to
        :param obj user: The User requesting the new invoice
        :returns: A new Invoice instance
        """
        from endi.models.task.invoice import Invoice
        invoice = Invoice(
            user=user,
            company=business.project.company,
            project=business.project,
            customer=cls.get_customer(business),
            business_id=business.id,
            business_type_id=business.business_type_id,
        )
        invoice.initialize_business_datas()
        DBSESSION().add(invoice)
        DBSESSION().flush()
        return invoice

    @classmethod
    def get_customer(cls, business):
        """
        Find the customer associated to this bussiness

        :param obj business: The business instance this service is attached to
        :returns: A Customer id
        :rtype: int
        """
        from endi.models.task import Task
        from endi.models.third_party.customer import Customer
        customer_id = DBSESSION().query(Task.customer_id).filter_by(
            business_id=business.id
        ).distinct().one()
        return Customer.get(customer_id)

    @classmethod
    def is_void(cls, business):
        """
        Check if a business is void

        :rtype: bool
        """
        from endi.models.task import Task
        query = DBSESSION().query(Task.id).filter_by(
            business_id=business.id
        )
        return query.count() == 0

    @classmethod
    def _get_estimations_to_invoice(cls, business):
        """
        Return estimations that should be invoiced

        :param obj business: The business instance
        """
        result = []
        for estimation in business.estimations:
            if estimation.status == 'valid' and \
                    estimation.signed_status != 'aborted':
                result.append(estimation)
        return result

    @classmethod
    def add_progress_invoicing_invoice(cls, business, user):
        """
        Build an Invoice in progress invoicing mode

        :param obj business: The current Business
        """
        if not business.estimations:
            raise Exception(
                "Erreur, cette affaire {} n'a pas de devis "
                "rattaché".format(business.id)
            )

        estimation = business.estimations[0]
        invoice = cls.add_invoice(business, user)
        for key in ('payment_conditions', 'description', 'address',
                    'workplace', 'mentions', 'notes', 'price_study_id',
                    'display_units', 'start_date'):
            setattr(invoice, key, getattr(estimation, key))

        invoice.invoicing_mode = invoice.PROGRESS_MODE
        return invoice

    @classmethod
    def populate_progress_invoicing_status(cls, business):
        """
        Populate the progress invoicing statuses based on the current business
        estimations
        Can be launched several times

        :rtype: bool
        """
        if business.invoicing_mode != business.PROGRESS_MODE:
            raise Exception(
                "Cette affaire n'utilise pas la facturation à l'avancement"
            )
        from endi.models.progress_invoicing.progress_invoicing import (
            ProgressInvoicingBaseStatus,
            ProgressInvoicingLineStatus,
            ProgressInvoicingGroupStatus,
        )

        status_ids = []

        for estimation in cls._get_estimations_to_invoice(business):
            deposit = estimation.deposit
            # The percent of each product to be invoiced (after deposit
            # invoice)
            percent_to_invoice = 100 - deposit
            for group in estimation.line_groups:
                group_status = ProgressInvoicingGroupStatus.get_or_create(
                    business,
                    group,
                    percent_to_invoice=percent_to_invoice,
                )
                status_ids.append(group_status.id)
                for line in group.lines:
                    status = ProgressInvoicingLineStatus.get_or_create(
                        business,
                        line,
                        percent_to_invoice=percent_to_invoice,
                        group_status=group_status,
                    )
                    status_ids.append(status.id)

        # On nettoye les status qui ne correspondent pas aux devis (quand un
        # devis a été marqué sans suite par exemple)
        for status in ProgressInvoicingBaseStatus.query().filter_by(
            business_id=business.id
        ).filter(
            not_(ProgressInvoicingBaseStatus.id.in_(status_ids))
        ):
            if not status.invoiced_elements:
                DBSESSION().delete(status)
            else:
                raise Exception("Ce devis a déjà donné lieu à facturation")
        DBSESSION().flush()
        return True

    @classmethod
    def clear_progress_invoicing_status(cls, business):
        """
        Clear the progress invoicing statuses attached to this business

        :rtype: bool
        """
        from endi.models.progress_invoicing.progress_invoicing import (
            ProgressInvoicingBaseStatus,
        )
        for status in ProgressInvoicingBaseStatus.query().filter_by(
            business=business
        ):
            if status.invoiced_elements:
                raise Exception("Des éléments ont déjà été facturés")
            DBSESSION().delete(status)
        DBSESSION().flush()
        return True

    @classmethod
    def on_invoice_delete(cls, business, invoice_id):
        """
        Update the business when an invoice is deleted

        :param obj business: The Business instance
        :param int invoice_id: The deleted invoice id
        """
        if business.is_void():
            DBSESSION().delete(business)
        elif business.invoicing_mode == business.PROGRESS_MODE:
            from endi.models.progress_invoicing.progress_invoicing import (
                ProgressInvoicingLine,
                ProgressInvoicingGroup,
            )
            from endi.models.task import TaskLine, TaskLineGroup
            # Retrouve les lignes et groupes générés
            # Récupère les pourcentages
            # Ajoute ces pourcentages au percent_left
            group_ids = [
                i[0]
                for i in DBSESSION().query(TaskLineGroup.id).filter_by(
                    task_id=invoice_id
                )
            ]
            line_ids = [
                i[0] for i in DBSESSION().query(TaskLine.id).filter(
                    TaskLine.group_id.in_(group_ids)
                )
            ]
            invoiced_lines = ProgressInvoicingLine.query().filter(
                ProgressInvoicingLine.task_line_id.in_(line_ids)
            )
            invoiced_groups = ProgressInvoicingGroup.query().filter(
                ProgressInvoicingGroup.task_line_group_id.in_(group_ids)
            )
            for line in invoiced_lines:
                line.status.percent_left += line.percentage
                DBSESSION().merge(line.status)
                DBSESSION().delete(line)
            for group in invoiced_groups:
                group.status.percent_left += group.percentage
                DBSESSION().merge(group.status)
                DBSESSION().delete(group)

    @classmethod
    def on_estimation_signed_status_change(cls, business):
        """
        Manage the modification of an estimation signed status
        """
        if business.invoicing_mode == business.PROGRESS_MODE:
            cls.populate_progress_invoicing_status(business)

    @classmethod
    def populate_progress_invoicing_lines(cls, business, invoice, appstruct):
        """
        generate task groups and task lines for the given business

        :param obj business: The Business instance
        :param obj invoice: The destination Invoice
        :param dict appstruct: Dict containing the percentage to invoice per
        ProgressInvoicingGroupStatus id

        {<group_status.id>: {<linen.id>: <percentage>, ...}, ...}

        :returns: The populated Invoice
        """
        original_groups = invoice.line_groups
        new_groups = []
        for group_status in business.progress_invoicing_group_statuses:
            line_config = appstruct.get(group_status.id, {})
            task_line_group = group_status.get_or_generate(
                line_config, invoice.id
            )
            new_groups.append(task_line_group)

        for task_line_group in new_groups:
            if task_line_group not in invoice.line_groups:
                invoice.line_groups.append(task_line_group)

        # Ici on nettoie notamment parce qu'une nouvelle facture a un groupe
        # par défaut
        for task_line_group in original_groups:
            if task_line_group not in new_groups:
                invoice.line_groups.remove(task_line_group)

        DBSESSION().merge(invoice)
        DBSESSION().flush()
        return invoice

    @classmethod
    def progress_invoicing_is_complete(cls, business):
        """
        Check if all the expected task amounts have been invoiced yet

        :param obj business: The current Business instance
        :rtype: bool
        """
        result = True
        for group_status in business.progress_invoicing_group_statuses:
            result = result and group_status.is_completely_invoiced()
        return result
