# -*- coding: utf-8 -*-
"""
Customer query service
"""
from endi_base.models.base import DBSESSION
from endi.models.services.third_party import ThirdPartyService
from endi.compute.math_utils import integer_to_amount


class CustomerService(ThirdPartyService):

    @classmethod
    def get_tasks(cls, instance, type_str=None):
        from endi.models.task import Task
        query = DBSESSION().query(Task)
        query = query.filter_by(customer_id=instance.id)
        if type_str is not None:
            query = query.filter(Task.type_ == type_str)
        else:
            query = query.filter(
                Task.type_.in_(('invoice', 'cancelinvoice', 'estimation'))
            )
        return query

    @classmethod
    def count_tasks(cls, instance):
        return cls.get_tasks(instance).count()

    @classmethod
    def check_project_id(cls, customer_id, project_id):
        """
        Check that the given customer is attached to the given project
        """
        from endi.models.project.project import ProjectCustomer
        return DBSESSION().query(ProjectCustomer).filter_by(
            project_id=project_id).filter_by(
                customer_id=customer_id).count() > 0

    @classmethod
    def get_project_ids(cls, customer):
        """
        Collect the ids of the projects attached to the given customer
        """
        from endi.models.project.project import ProjectCustomer
        return [p.project_id
                for p in DBSESSION().query(
                    ProjectCustomer
                ).filter_by(
                    customer_id=customer.id
                ).all()]

    @classmethod
    def get_total_expenses(cls, instance):
        from endi.models.expense.sheet import (ExpenseLine, ExpenseKmLine)
        total_expenses = 0
        query = DBSESSION().query(ExpenseLine)
        query = query.filter_by(customer_id=instance.id)
        for expense in query.all():
            if expense.sheet.status=="valid":
                total_expenses += expense.total_ht
        query = DBSESSION().query(ExpenseKmLine)
        query = query.filter_by(customer_id=instance.id)
        for expense in query.all():
            if expense.sheet.status=="valid":
                total_expenses += expense.total_ht
        return total_expenses

    @classmethod
    def get_total_income(cls, instance):
        from endi.models.task import Task
        total_income = 0
        query = DBSESSION().query(Task)
        query = query.filter_by(customer_id=instance.id)
        query = query.filter(Task.type_.in_(('invoice', 'cancelinvoice')))
        query = query.filter_by(status="valid")
        for task in query.all():
            total_income += task.ht
        return total_income

    @classmethod
    def get_general_account(cls, instance):
        result = instance.compte_cg
        if not result:
            result = instance.company.get_general_customer_account()
        return result

    @classmethod
    def get_third_party_account(cls, instance):
        result = instance.compte_tiers
        if not result:
            result = instance.company.get_third_party_customer_account()
        return result
