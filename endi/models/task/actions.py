# -*- coding: utf-8 -*-
"""
Action objects
"""
from endi.models.config import Config
from endi.models.action_manager import (
    Action,
    ActionManager,
)
from endi.models.services.invoice_sequence_number import (
    InvoiceNumberService,
)


def _set_invoice_number(request, task, **kw):
    """
    Set a official number on invoices (or cancelinvoices)

    :param obj request: The current pyramid request
    :param obj task: The current context
    """
    template = Config.get_value('invoice_number_template', None)
    assert template is not None, \
        'invoice_number_template setting should be set'

    if task.official_number is None:
        InvoiceNumberService.assign_number(
            task,
            template,
        )
    return task


def _force_file_requirement_indicators(request, task, **kw):
    """
    Force File requirement to be successfull

    :param obj request: The current pyramid request
    :param obj task: The current context
    """
    task.file_requirement_service.force_all(task)
    return task


def estimation_valid_callback(request, task, **kw):
    """
    Estimation validation callback

    :param obj request: The current pyramid request
    :param obj task: The current context
    """
    _force_file_requirement_indicators(request, task, **kw)
    return task


def invoice_valid_callback(request, task, **kw):
    """
    Invoice validation callback

    :param obj request: The current pyramid request
    :param obj task: The current context
    """
    _set_invoice_number(request, task, **kw)
    _force_file_requirement_indicators(request, task, **kw)
    return task


def get_validation_state_manager(data_type, userid_attr='status_person_id'):
    """
    Return a state machine handling the basic states

    :param str data_type: estimation/invoice/cancelinvoice/expensesheet
    :param str userid_attr: the attribute holding the user who changed status

    :returns: An action manager machine that can be used to perform state changes
    :rtype: class:`endi.models.action_manager.ActionManager`
    """
    manager = ActionManager()
    for status, prev_status, icon, label, title, css in (
        (
            'valid',
            None,
            'check-circle',
            "Valider",
            "Valider ce document (il ne pourra plus être modifié)",
            "btn-primary icon_only_mobile",
        ),
        (
            'wait',
            None,
            'clock',
            "Demander la validation",
            "Enregistrer ce document et en demander la validation",
            "btn-primary icon_only_mobile",
        ),
        (
            'invalid',
            None,
            'times-circle',
            "Invalider",
            "Invalider ce document afin que l’entrepreneur le corrige",
            "btn-primary negative icon_only_mobile",
        ),
        (
            'draft',
            'draft',
            'save',
            "Enregistrer",
            'Enregistrer en brouillon afin de modifier ce document '
            'ultérieurement',
            'icon_only_mobile',
        ),
        (
            'draft',
            'wait',
            'pen-square',
            "Brouillon",
            "Repasser ce document en brouillon",
            '',
        )
    ):
        action = Action(
            status,
            '%s.%s' % (status, data_type),
            prev_status=prev_status,
            status_attr='status',
            userid_attr=userid_attr,
            icon=icon,
            label=label,
            title=title,
            css=css,
        )
        if status == 'valid':
            if data_type in ('invoice', 'cancelinvoice'):
                action.callback = invoice_valid_callback
            elif data_type == 'estimation':
                action.callback = estimation_valid_callback
            # no callback for expenssheet nor supplier_order
        manager.add(action)
    return manager


DEFAULT_ACTION_MANAGER = {
    'estimation': get_validation_state_manager('estimation'),
    'invoice': get_validation_state_manager('invoice'),
    'cancelinvoice': get_validation_state_manager('cancelinvoice'),
}


def get_signed_status_actions():
    """
    Return actions available for setting the signed_status attribute on
    Estimation objects
    """
    manager = ActionManager()
    for status, icon, label, title, css in (
        (
            'waiting',
            'clock',
            "En attente de réponse",
            "En attente de réponse du client",
            "btn"
        ),
        (
            "sent",
            "envelope",
            "Envoyé au client",
            "A bien été envoyé au client",
            "btn",
        ),
        (
            'aborted',
            'times',
            "Sans suite",
            "Marquer sans suite",
            "btn negative",
        ),
        (
            'signed',
            'check',
            "Signé par le client",
            "Indiquer que le client a passé commande",
            "btn btn-primary",
        ),
    ):
        action = Action(
            status,
            'set_signed_status.estimation',
            status_attr='signed_status',
            icon=icon,
            label=label,
            title=title,
            css=css,
        )
        manager.add(action)
    return manager


SIGNED_ACTION_MANAGER = get_signed_status_actions()
