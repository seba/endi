# -*- coding: utf-8 -*-
"""
    Invoice model
"""
import datetime
import logging

from zope.interface import implementer
from beaker.cache import cache_region
from sqlalchemy import (
    Column,
    Integer,
    Boolean,
    String,
    ForeignKey,
    distinct,
)
from sqlalchemy.orm import (
    relationship,
    deferred,
    backref,
)
from endi_base.models.base import (
    DBSESSION,
    default_table_args,
)
from endi import forms
from endi.compute import math_utils
from endi.compute.task import (
    InvoiceCompute,
)
from endi.models.tva import Product
from endi.interfaces import (
    IMoneyTask,
    IInvoice,
)
from .task import (
    Task,
    TaskLine,
    TaskStatus,
)
from endi.models.services.task import InvoiceService, CancelInvoiceService
from .actions import DEFAULT_ACTION_MANAGER
# import Payment model for compatibility (unused here)
from endi.models.task.payment import Payment

logger = logging.getLogger(__name__)


INVOICE_STATES = (
    ('waiting', "En attente"),
    ('paid', 'Partiellement payée'),
    ('resulted', 'Soldée'),
)


def translate_invoices(invoicequery, from_point):
    """
    Translate invoice numbers to 'from_point'

    :param iter invoicequery: An iterable
    :param int from_point: from_point

    The first invoice will get from_point as official_number
    """
    for invoice in invoicequery:
        invoice.official_number = from_point
        from_point += 1
        DBSESSION().merge(invoice)

    return from_point


@implementer(IInvoice, IMoneyTask)
class Invoice(Task):
    """
        Invoice Model
    """
    __tablename__ = 'invoice'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'invoice', }
    invoice_computer = None

    id = Column(
        ForeignKey('task.id'),
        primary_key=True,
        info={
            'colanderalchemy': {'exclude': True},
        }
    )

    # Common with CancelInvoice
    financial_year = Column(
        Integer,
        info={'colanderalchemy': {'title': "Année fiscale de référence"}},
        default=0
    )
    exported = deferred(
        Column(
            Boolean(),
            info={'colanderalchemy': {'title': "A déjà été exportée ?"}},
            default=False
        ),
        group="edit"
    )

    # Specific to Invoice
    # FIXME: Use PaidStatusHolderMixin ?
    paid_status = Column(
        String(10),
        default='waiting',
        info={'colanderalchemy': {'title': 'Statut de la facture', }},
    )
    estimation_id = Column(ForeignKey('estimation.id'))
    # Le mode de facturation de l'affaire classic / progress
    invoicing_mode = deferred(
        Column(String(20), default="classic"),
        group="edit"
    )
    PROGRESS_MODE = 'progress'
    CLASSIC_MODE = 'classic'

    # Relationships
    estimation = relationship(
        "Estimation",
        primaryjoin="Invoice.estimation_id==Estimation.id",
        info={
            'colanderalchemy': forms.EXCLUDED,
            'export': {'exclude': True},
        },
    )
    validation_state_manager = DEFAULT_ACTION_MANAGER['invoice']
    _endi_service = InvoiceService

    paid_states = ('resulted',)
    not_paid_states = ('valid', 'paid', )
    valid_states = paid_states + not_paid_states

    _number_tmpl = "{s.company.name} {s.date:%Y-%m} F{s.company_index}"

    _name_tmpl = "Facture {0}"

    _deposit_name_tmpl = "Facture d'acompte {0}"

    _sold_name_tmpl = "Facture de solde {0}"

    def _get_project_index(self, project):
        """
        Return the index of the current object in the associated project
        :param obj project: A Project instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return project.get_next_invoice_index()

    def _get_company_index(self, company):
        """
        Return the index of the current object in the associated company
        :param obj company: A Company instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return company.get_next_invoice_index()

    def set_deposit_label(self):
        self.name = self._deposit_name_tmpl.format(self.project_index)

    def set_sold_label(self):
        self.name = self._sold_name_tmpl.format(self.project_index)

    def set_project(self, project):
        self.project = project

    def gen_cancelinvoice(self, user):
        """
            Return a cancel invoice with self's informations
        """
        cancelinvoice = CancelInvoice(
            user=user,
            company=self.company,
            project=self.project,
            customer=self.customer,
            phase_id=self.phase_id,
            address=self.address,
            workplace=self.workplace,
            description=self.description,
            invoice=self,
            expenses_ht=-1 * self.expenses_ht,
            financial_year=self.financial_year,
            display_units=self.display_units,
            display_ttc=self.display_ttc,
            business_type_id=self.business_type_id,
            business_id=self.business_id,
            mode=self.mode,
            start_date=self.start_date,
        )

        cancelinvoice.line_groups = []
        for group in self.line_groups:
            cancelinvoice.line_groups.append(
                group.gen_cancelinvoice_group()
            )
        order = self.get_next_row_index()

        for discount in self.discounts:
            discount_line = TaskLine(
                cost=discount.amount,
                tva=discount.tva,
                quantity=1,
                description=discount.description,
                order=order,
                unity='',
            )
            discount_line.product_id = Product.first_by_tva_value(discount.tva)
            order += 1
            cancelinvoice.default_line_group.lines.append(discount_line)

        for index, payment in enumerate(self.payments):
            paid_line = TaskLine(
                cost=math_utils.reverse_tva(
                    payment.amount,
                    payment.tva.value,
                    False,
                ),
                tva=payment.tva.value,
                quantity=1,
                description="Paiement {0}".format(index + 1),
                order=order,
                unity='NONE',
            )
            paid_line.product_id = Product.first_by_tva_value(
                payment.tva.value
            )
            order += 1
            cancelinvoice.default_line_group.lines.append(paid_line)
        cancelinvoice.mentions = self.mentions
        cancelinvoice.payment_conditions = "Réglé"
        return cancelinvoice

    def get_next_row_index(self):
        return len(self.default_line_group.lines) + 1

    def check_resulted(self, force_resulted=False):
        """
        Check if the invoice is resulted or not and set the appropriate status
        """
        logger.debug("-> There still to pay : %s" % self.topay())
        if self.topay() <= 0 or force_resulted:
            self.paid_status = 'resulted'

        elif len(self.payments) > 0 or self.cancelinvoice_amount() > 0:
            self.paid_status = 'paid'

        else:
            self.paid_status = 'waiting'
        return self

    def historize_paid_status(self, user):
        """
        Records the current paid_status in history

        :param user: the user who just changed paid status.
        """
        status_record = TaskStatus(
            status_code=self.paid_status,
            status_person_id=user.id,
            status_comment=''
        )
        self.statuses.append(status_record)
        return self

    def duplicate(self, user, **kw):
        """
        Duplicate the current invoice

        Mandatory args :

            user

                The user duplicating this estimation

            customer

            project
        """
        invoice = Invoice(
            user=user,
            company=self.company,
            **kw
        )

        invoice.mode = self.mode
        invoice.workplace = self.workplace
        invoice.price_study_id = self.price_study_id

        invoice.description = self.description
        invoice.notes = self.notes
        invoice.start_date = self.start_date

        invoice.payment_conditions = self.payment_conditions
        invoice.display_units = self.display_units
        invoice.display_ttc = self.display_ttc
        invoice.expenses_ht = self.expenses_ht
        invoice.financial_year = datetime.date.today().year

        invoice.line_groups = []
        for group in self.line_groups:
            invoice.line_groups.append(group.duplicate())

        for line in self.discounts:
            invoice.discounts.append(line.duplicate())

        invoice.mentions = self.mentions
        if self.price_study:
            invoice.price_study = self.price_study.duplicate(
                self.project_id, user.id
            )
        return invoice

    def __repr__(self):
        return "<Invoice id:{s.id}>".format(s=self)

    def __json__(self, request):
        datas = Task.__json__(self, request)

        datas.update(
            dict(
                financial_year=self.financial_year,
                exported=self.exported,
                estimation_id=self.estimation_id,
            )
        )
        return datas

    def is_tolate(self):
        """
            Return True if a payment is expected since more than
            45 days
        """
        res = False
        if self.paid_status in ('waiting', 'paid'):
            today = datetime.date.today()
            elapsed = today - self.date
            if elapsed > datetime.timedelta(days=45):
                res = True
            else:
                res = False
        return res

    @property
    def global_status(self):
        """
        hook on status and paid status to update css classes representing icons
        :return: a Sting
        """
        if self.paid_status == 'paid':
            return 'partial_unpaid'
        if self.paid_status == 'waiting' and self.status == 'valid':
            if self.date + datetime.timedelta(days=45) > datetime.date.today():
                return 'caution'
            else:
                return 'invalid'
        return self.status

    def _get_invoice_computer(self):
        """
        Return needed compute class depending on mode value
        :return: an instance of TaskCompute or TaskTtcCompute
        """
        if self.invoice_computer is None:
            self.invoice_computer = InvoiceCompute(self)
        return self.invoice_computer

    def cancelinvoice_amount(self):
        return self._get_invoice_computer().cancelinvoice_amount()

    def paid(self):
        return self._get_invoice_computer().paid()

    def topay(self):
        return self._get_invoice_computer().topay()

    def tva_paid_parts(self):
        return self._get_invoice_computer().tva_paid_parts()

    def tva_cancelinvoice_parts(self):
        return self._get_invoice_computer().tva_cancelinvoice_parts()

    def topay_by_tvas(self):
        return self._get_invoice_computer().topay_by_tvas()


@implementer(IInvoice, IMoneyTask)
class CancelInvoice(Task):
    """
        CancelInvoice model
        Could also be called negative invoice
    """
    __tablename__ = 'cancelinvoice'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'cancelinvoice'}
    id = Column(
        Integer,
        ForeignKey('task.id'),
        primary_key=True,
        info={
            'colanderalchemy': {'exclude': True},
        }
    )
    # Common with Invoice
    financial_year = Column(
        Integer,
        info={'colanderalchemy': {'title': "Année fiscale de référence"}},
        default=0
    )
    exported = deferred(
        Column(
            Boolean(),
            info={'colanderalchemy': {"title": "A déjà été exportée ?"}},
            default=False
        ),
        group="edit"
    )

    # Specific to CancelInvoice
    invoice_id = Column(
        Integer,
        ForeignKey('invoice.id'),
        info={
            'colanderalchemy': {
                'title': "Identifiant de la facture associée",
            }
        },
        default=None
    )
    invoice = relationship(
        "Invoice",
        backref=backref(
            "cancelinvoices",
            info={'colanderalchemy': forms.EXCLUDED, }
        ),
        primaryjoin="CancelInvoice.invoice_id==Invoice.id",
        info={'colanderalchemy': forms.EXCLUDED, }
    )

    validation_state_manager = DEFAULT_ACTION_MANAGER['cancelinvoice']
    valid_states = ('valid', )
    _endi_service = CancelInvoiceService

    _number_tmpl = "{s.company.name} {s.date:%Y-%m} A{s.company_index}"

    _name_tmpl = "Avoir {0}"

    def _get_project_index(self, project):
        """
        Return the index of the current object in the associated project
        :param obj project: A Project instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return project.get_next_invoice_index()

    def _get_company_index(self, company):
        """
        Return the index of the current object in the associated company
        :param obj company: A Company instance in which we will look to get the
        current doc index
        :returns: The next number
        :rtype: int
        """
        return company.get_next_invoice_index()

    def is_tolate(self):
        """
        Return False
        """
        return False

    def __repr__(self):
        return "<CancelInvoice id:{s.id}>".format(s=self)

    def __json__(self, request):
        datas = Task.__json__(self, request)

        datas.update(
            dict(
                invoice_id=self.invoice_id,
                financial_year=self.financial_year,
                exported=self.exported,
            )
        )
        return datas

    @property
    def global_status(self):
        """
        hook on status and paid status to update css classes representing icons
        :return: a Sting
        """
        return self.status


# Usefull queries
def get_invoice_years(kw=None):
    """
        Return a cached query for the years we have invoices configured

    :param kw: is here only for API compatibility
    """
    @cache_region("long_term", "taskyears")
    def taskyears():
        """
            return the distinct financial years available in the database
        """
        query = DBSESSION().query(distinct(Invoice.financial_year))
        query = query.order_by(Invoice.financial_year)
        years = [year[0] for year in query]
        current = datetime.date.today().year
        if current not in years:
            years.append(current)
        return years
    return taskyears()
