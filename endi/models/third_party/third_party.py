# -*- coding: utf-8 -*-
"""
    ThirdParty model
"""
import logging

from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    Boolean,
    ForeignKey,
)
from sqlalchemy.orm import (
    deferred,
    relationship,
)
from sqlalchemy.event import listen
from endi_base.models.base import default_table_args
from endi.models.node import Node
from endi.models.services.third_party import ThirdPartyService


log = logging.getLogger(__name__)


class ThirdParty(Node):
    """
        Metadata pour un tiers (client, fournisseur)
    """
    __tablename__ = 'third_party'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'third_party'}
    _endi_service = ThirdPartyService

    id = Column(
        Integer,
        ForeignKey('node.id'),
        primary_key=True,
        info={
            'colanderalchemy': {
                'exclude': True,
            }
        },
    )
    company_id = Column(
        "company_id",
        Integer,
        ForeignKey('company.id'),
        info={
            'export': {'exclude': True},
            'colanderalchemy': {'exclude': True},
        },
        nullable=False,
    )
    type = Column(
        'type',
        String(10),
        default='company',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )
    code = Column(
        'code',
        String(4),
        info={'colanderalchemy': {'title': "Code"}},
    )
    label = Column(
        "label",
        String(255),
        info={
            'colanderalchemy': {'exclude': True},
        },
        default='',
    )
    company_name = Column(
        "company_name",
        String(255),
        info={
            "colanderalchemy": {
                'title': 'Nom de la structure',
            },
        },
        default='',
    )
    civilite = deferred(
        Column(
            'civilite',
            String(10),
            info={
                'colanderalchemy': {
                    'title': "Civilité",
                }
            },
            default="",
        ),
        group='edit',
    )
    lastname = deferred(
        Column(
            "lastname",
            String(255),
            info={
                "colanderalchemy": {
                    'title': "Nom du contact principal",
                }
            },
            default="",
        ),
        group='edit',
    )
    firstname = deferred(
        Column(
            "firstname",
            String(255),
            info={
                'colanderalchemy': {
                    'title': "Prénom du contact principal",
                }
            },
            default="",
        ),
        group='edit',
    )
    function = deferred(
        Column(
            "function",
            String(255),
            info={
                'colanderalchemy': {
                    'title': "Fonction du contact principal",
                }
            },
            default='',
        ),
        group="edit",
    )
    registration = deferred(
        Column(
            "registration",
            String(255),
            info={
                'colanderalchemy': {
                    'title': "Numéro d'immatriculation",
                }
            },
            default='',
        ),
        group="edit",
    )
    address = deferred(
        Column(
            "address",
            String(255),
            info={
                'colanderalchemy': {
                    'title': 'Adresse',
                }
            },
            default="",
        ),
        group='edit'
    )
    zip_code = deferred(
        Column(
            "zip_code",
            String(20),
            info={
                'colanderalchemy': {
                    'title': 'Code postal',
                },
            },
            default="",
        ),
        group='edit',
    )
    city = deferred(
        Column(
            "city",
            String(255),
            info={
                'colanderalchemy': {
                    'title': 'Ville',
                }
            },
            default="",
        ),
        group='edit',
    )
    country = deferred(
        Column(
            "country",
            String(150),
            info={
                'colanderalchemy': {'title': 'Pays'},
            },
            default='France',
        ),
        group='edit',
    )
    email = deferred(
        Column(
            "email",
            String(255),
            info={
                'colanderalchemy': {
                    'title': "Adresse de messagerie",
                },
            },
            default='',
        ),
        group='edit',
    )
    mobile = deferred(
        Column(
            "mobile",
            String(20),
            info={
                'colanderalchemy': {
                    'title': "Téléphone portable",
                },
            },
            default='',
        ),
        group='edit',
    )
    phone = deferred(
        Column(
            "phone",
            String(50),
            info={
                'colanderalchemy': {
                    'title': 'Téléphone fixe',
                },
            },
            default='',
        ),
        group='edit',
    )
    fax = deferred(
        Column(
            "fax",
            String(50),
            info={
                'colanderalchemy': {
                    'title': 'Fax',
                }
            },
            default='',
        ),
        group="edit"
    )
    tva_intracomm = deferred(
        Column(
            "tva_intracomm",
            String(50),
            info={
                'colanderalchemy': {'title': "TVA intracommunautaire"},
            },
            default='',
        ),
        group='edit',
    )
    comments = deferred(
        Column(
            "comments",
            Text,
            info={
                'colanderalchemy': {
                    'title': "Commentaires",
                }
            },
        ),
        group='edit',
    )
    compte_cg = deferred(
        Column(
            String(125),
            info={
                'export': {'exclude': True},
                'colanderalchemy': {
                    'title': "Compte CG",
                },
            },
            default="",
        ),
        group="edit",
    )
    compte_tiers = deferred(
        Column(
            String(125),
            info={
                'export': {'exclude': True},
                'colanderalchemy': {
                    'title': "Compte tiers",
                }
            },
            default="",
        ),
        group="edit",
    )
    archived = Column(
        Boolean(),
        default=False,
        info={'colanderalchemy': {'exclude': True}},
    )
    company = relationship(
        "Company",
        primaryjoin="Company.id==ThirdParty.company_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    def get_company_id(self):
        """
            :returns: the id of the company this third_party belongs to
        """
        return self.company.id

    def __json__(self, request):
        """
            :returns: a dict version of the third_party object
        """
        return dict(
            id=self.id,
            created_at=self.created_at.isoformat(),
            updated_at=self.updated_at.isoformat(),
            company_id=self.company_id,
            type=self.type,
            code=self.code,
            label=self.label,
            company_name=self.company_name,
            civilite=self.civilite,
            lastname=self.lastname,
            firstname=self.firstname,
            function=self.function,
            registration=self.registration,
            address=self.address,
            zip_code=self.zip_code,
            city=self.city,
            country=self.country,
            full_address=self.full_address,
            email=self.email,
            mobile=self.mobile,
            phone=self.phone,
            fax=self.fax,
            tva_intracomm=self.tva_intracomm,
            comments=self.comments,
            compte_cg=self.compte_cg,
            compte_tiers=self.compte_tiers,
            archived=self.archived,
        )

    @property
    def full_address(self):
        """
            Return the third_party address formatted in french format
        """
        return self._endi_service.get_address(self)

    def is_deletable(self):
        """
            Return True if this third_party could be deleted
        """
        return self.archived

    def is_company(self):
        """
            Return True if this third_party is a company
        """
        return self.type == 'company'

    def _get_label(self):
        return self._endi_service.get_label(self)

    def get_name(self):
        return self._endi_service.format_name(self)

    @classmethod
    def label_query(cls):
        return cls._endi_service.label_query(cls)

    def get_general_account(self):
        return self._endi_service.get_general_account(self)

    def get_third_party_account(self):
        return self._endi_service.get_third_party_account(self)


COMPANY_FORM_GRID = (
    (('code', 4),),
    (('company_name', 12),),
    (('civilite', 6),),
    (('lastname', 6), ('firstname', 6),),
    (('function', 12),),
    (('address', 12),),
    (('zip_code', 4), ('city', 8),),
    (('country', 6),),
    (('tva_intracomm', 6), ('registration', 6),),
    (('email', 12),),
    (('mobile', 6), ('phone', 6),),
    (('fax', 6),),
    (('comments', 12),),
    (('compte_cg', 6), ('compte_tiers', 6),)
)

INDIVIDUAL_FORM_GRID = (
    (('code', 4),),
    (('civilite', 6),),
    (('lastname', 6), ('firstname', 6),),
    (('address', 12),),
    (('zip_code', 4), ('city', 8),),
    (('country', 6),),
    (('email', 12),),
    (('mobile', 6), ('phone', 6),),
    (('fax', 6),),
    (('comments', 12),),
    (('compte_cg', 6), ('compte_tiers', 6),)
)


def set_third_party_label(mapper, connection, target):
    """
    Set the label of the given third_party
    """
    target.label = target._get_label()
    target.name = target.label


listen(ThirdParty, "before_insert", set_third_party_label, propagate=True)
listen(ThirdParty, "before_update", set_third_party_label, propagate=True)
