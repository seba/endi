# -*- coding: utf-8 -*-
"""
Handle a consistent way the different statuses stored in DB

We handle several statuses (validation status, payment status, justification
status…).

This module handles two things :

- the SQLA fields to mix-in the implementors
- the optional historization of the statuses through StatusLogEntry
- and some status-related tools.

It does not handle state machine itself, see also ActionManager uses for state
machine stuff.
"""

import datetime
import logging

from sqlalchemy import (
    Column,
    String,
    Integer,
    Text,
    ForeignKey,
    or_,
)
from sqlalchemy.dialects.mysql import DATETIME
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import (
    relationship,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)

logger = logging.getLogger(__name__)

def status_date_column():
    return Column(
        DATETIME(fsp=6),
        default=datetime.date.today,
        info={
            'colanderalchemy': {
                "title": "Date du dernier changement de statut (cache)",
            },
            'export': {'exclude': True}
        }
    )


def status_user_id_column():
    return Column(
        ForeignKey('accounts.id'),
        info={
            'colanderalchemy': {
                "title": "Dernier utilisateur à avoir modifié le document (cache)",
            },
            "export": {'exclude': True},
        },
    )


def status_user_relationship(user_id_column):
    return relationship(
        "User",
        primaryjoin='User.id == StatusLogEntry.node_id, StatusLogEntry.state_manager_key == "{}")'.format(state_manager_key),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

def status_comment_column():
    return Column(
        Text,
        info={
            "colanderalchemy": {"title": "Commentaires"},
            'export': {'exclude': True}
        },
        default="",
    )


def status_history_relationship(state_manager_key):
    return relationship(
        "StatusLogEntry",
        primaryjoin='and_(Node.id == StatusLogEntry.node_id, StatusLogEntry.state_manager_key == "{}")'.format(state_manager_key),
        order_by="desc(StatusLogEntry.datetime), desc(StatusLogEntry.id)",
        cascade="all, delete-orphan",
        back_populates='node',
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )


def status_column(default=''):
    return Column(
        String(10),
        default=default,
        info={
            'colanderalchemy': {'title': "Statut"},
            'export': {'exclude': True}
        }
    )


class StatusHolderMixin(object):
    """
    Tools to implement a stored status, with all its metadata.

    This is meant to be subclassed arround a specific status (eg:
    ValidationStatusHolderMixin).

    Final class model can inherit several statusholder childs. to implement
    several statuses (ex: payment status and vaidation status).
    """

    # Maps context fields to StatusLogEntry fields
    #
    # 2-level nested dict
    # 1st-level dict :
    #  - key: state_manager_key
    #  - value: 2nd-level dict
    # 2nd-level dict is
    #  - key: field name on StatusHolderMixin child class
    #  - value : field name on StatusLogEntry
    # This map can be modified by subclassing model classes if their field
    # naming is non-standard
    LOG_ENTRY_MAPPING = {
        'validation_status': {
            'status': 'status',
            'status_comment': 'comment',
            'status_user_id': 'user_id',
            'status_date': 'datetime',
        },
        'paid_status': {
            'paid_status': 'status',
            'paid_status_comment': 'comment',
            'paid_status_user_id': 'user_id',
            'paid_status_date': 'datetime',
        }
    }

    def get_state_manager_keys(self):
        """
        Child classes contribute to this.
        """
        return []

    def historize_latest_status(self, state_manager_key):
        """
        Historize current status as a StatusLogEntry in db

        Logs a StatusLogEntry that reflects current holder object state.

        This does not handle permission / state machine stuff, it should have
        been done already.
        """
        state_manager_map = self.LOG_ENTRY_MAPPING[state_manager_key]
        params = {
            log_key: getattr(self, holder_key)
            for holder_key, log_key in list(state_manager_map.items())
        }
        event = StatusLogEntry(
            node_id=self.id,
            state_manager_key=state_manager_key,
            **params
        )
        DBSESSION().merge(event)


class StatusLogEntry(DBBASE):
    __tablename__ = 'status_log_entry'
    __table_args__ = default_table_args

    id = Column(Integer, primary_key=True)
    node_id = Column(ForeignKey('node.id'))
    node = relationship(
        "Node",
        primaryjoin="StatusLogEntry.node_id==Node.id",
    )
    state_manager_key = Column(String(40))

    status = status_column()
    comment = status_comment_column()
    datetime = status_date_column()
    user_id = status_user_id_column()

    user = relationship(
        "User",
        primaryjoin='User.id==StatusLogEntry.user_id',
#        uselist=False,
    )


class ValidationStatusHolderMixin(StatusHolderMixin):
    """Holds a state machine for validation status and caches latest state

    state_manager_key : "validation_status"

    Would be clearer to name the model fields "validation_status",
    "validation_status_comment"… But for historical reason, and to avoid
    breaking stuff, they are named simply "status", "status_comment"…
    """
    @declared_attr
    def status(cls):
        return status_column(default='draft')

    @declared_attr
    def status_comment(cls):
        return status_comment_column()

    @declared_attr
    def status_user_id(cls):
        return status_user_id_column()

    @declared_attr
    def status_user(cls):
        return relationship(
            "User",
            primaryjoin="{}.status_user_id==User.id".format(cls.__name__),
            info={
                'colanderalchemy': {'exclude': True},
            }
        )

    @declared_attr
    def status_date(cls):
        return status_date_column()

    @declared_attr
    def validation_status_history(cls):
        return status_history_relationship('validation_status')


class ValidationStatusHolderService(object):
    def __init__(self, *args, **kwargs):
        pass  # takes no parameter at the moment

    def waiting(self, *classes):
        """
        Documents waiting for validation

        :param *classes: the model classes to query (Node childs, implementing
          ValidationStatusHolderMixin)
        :returns: iterable query with documents.
        """
        from endi.models.node import Node
        query = Node.query().with_polymorphic(classes)

        status_filters = [i.status == 'wait' for i in classes]
        order_by = [c.status_date for c in classes]

        query = query.filter(or_(*status_filters))
        query = query.order_by(*order_by)
        return query


class PaidStatusHolderMixin(StatusHolderMixin):
    """
    Holds a state machine for paid status and caches latest state

    state_manager_key : "paid_status"

    Inheriting class must implement a `payment` property containing the list of
    payments.
    """
    @declared_attr
    def paid_status(cls):
        return status_column(default='waiting')

    @declared_attr
    def paid_status_comment(cls):
        return status_comment_column()

    @declared_attr
    def paid_status_user_id(cls):
        return status_user_id_column()

    @declared_attr
    def paid_status_date(cls):
        return status_date_column()

    @declared_attr
    def paid_status_history(cls):
        return status_history_relationship('paid_status')

    @property
    def payments(self):
        """
        Must be implemented and (attr or property)

        Typical implementation : one-many relationship.
        :rtype list of PaymentModelMixin implementor:
        """
        raise NotImplementedError

    def topay(self):
        """
        Must be implemented and provide a number
        """
        raise NotImplementedError

    def record_payment(self, payment, force_resulted=False):
        """
        Record a payment for the current expense
        """
        logger.debug("Recording a payment")
        logger.info("Amount : {0}".format(payment.amount))
        self.payments.append(payment)
        return self.check_resulted(
            force_resulted=force_resulted,
        )

    def check_resulted(self, force_resulted=False):
        """
        Check if the expense is resulted or not and set the appropriate status
        """
        logger.debug("-> There still to pay : %s" % self.topay())
        if self.topay() <= 0 or force_resulted:
            self.paid_status = 'resulted'
        elif len(self.payments) > 0:
            self.paid_status = 'paid'
        else:
            self.paid_status = "waiting"
        return self
