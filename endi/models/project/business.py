# -*- coding: utf-8 -*-
from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    Boolean,
    String,
)
from sqlalchemy.orm import (
    relationship,
    backref,
)
from endi_base.models.base import (
    default_table_args,
    DBBASE,
)
from endi.compute.business import BusinessCompute
from endi.models.node import Node
from endi.models.services.sale_file_requirements import (
    BusinessFileRequirementService,
)
from endi.models.services.business import BusinessService
from endi.models.services.business_status import (
    BusinessStatusService,
)


class BusinessPaymentDeadline(DBBASE):
    __tablename__ = 'business_payment_deadline'
    __table_args__ = default_table_args
    id = Column(Integer, primary_key=True)
    deposit = Column(Boolean(), default=False)
    business_id = Column(Integer, ForeignKey('business.id'))
    estimation_payment_id = Column(
        Integer, ForeignKey('estimation_payment.id', ondelete='CASCADE')
    )
    estimation_id = Column(
        Integer, ForeignKey('estimation.id', ondelete='CASCADE')
    )
    invoiced = Column(Boolean(), default=False)
    invoice_id = Column(Integer, ForeignKey('invoice.id', ondelete='SET NULL'))
    payment_line = relationship("PaymentLine")
    estimation = relationship("Estimation")
    invoice = relationship('Invoice')

    def __str__(self):
        return "<BusinessPaymentDeadline id:{}>".format(self.id)


class Business(BusinessCompute, Node):
    """
    Permet de :

        * Collecter les fichiers

        * Regrouper devis/factures/avoirs

        * Calculer le CA d'une affaire

        * Générer les factures

        * Récupérer le HT à dépenser

        * Des choses plus complexes en fonction du type de business

    Business.estimations
    Business.invoices
    Business.invoices[0].cancelinvoices
    """
    __tablename__ = "business"
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': "business"}
    file_requirement_service = BusinessFileRequirementService
    status_service = BusinessStatusService
    _endi_service = BusinessService

    id = Column(
        Integer,
        ForeignKey('node.id'),
        primary_key=True,
        info={'colanderalchemy': {'exclude': True}},
    )
    closed = Column(
        Boolean(),
        default=False,
        info={
            'colanderalchemy': {
                'title': "Cette affaire est-elle terminée ?"
            }
        },
    )
    status = Column(
        String(8),
        default="danger",
        info={
            'colanderalchemy': {"title": "Statut de cette affaire"}
        }
    )

    business_type_id = Column(
        ForeignKey('business_type.id'),
        info={'colanderalchemy': {'title': "Type d'affaires"}}
    )
    project_id = Column(
        ForeignKey('project.id'),
        info={'colanderalchemy': {'exclude': True}},
        nullable=False,
    )

    # Le mode de facturation de l'affaire classic / progress
    invoicing_mode = Column(String(20), default="classic")
    PROGRESS_MODE = 'progress'
    CLASSIC_MODE = 'classic'

    # Relations
    business_type = relationship(
        "BusinessType",
    )
    project = relationship(
        "Project",
        primaryjoin="Project.id == Business.project_id",
    )
    tasks = relationship(
        "Task",
        primaryjoin="Task.business_id==Business.id",
        back_populates="business"
    )
    estimations = relationship(
        "Task",
        back_populates="business",
        primaryjoin="and_(Task.business_id==Business.id, "
        "Task.type_=='estimation')",
    )
    invoices = relationship(
        "Task",
        back_populates="business",
        primaryjoin="and_(Task.business_id==Business.id, "
        "Task.type_.in_(('invoice', 'cancelinvoice')))"
    )
    invoices_only = relationship(
        "Invoice",
        primaryjoin="Task.business_id==Business.id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }

    )

    payment_deadlines = relationship(
        "BusinessPaymentDeadline",
        primaryjoin="BusinessPaymentDeadline.business_id==Business.id",
        cascade="all, delete-orphan",
    )
    indicators = relationship(
        "CustomBusinessIndicator",
        primaryjoin="CustomBusinessIndicator.business_id==Business.id",
    )
    progress_invoicing_statuses = relationship(
        "ProgressInvoicingBaseStatus", back_populates="business"
    )
    progress_invoicing_group_statuses = relationship(
        "ProgressInvoicingBaseStatus",
        back_populates="business",
        primaryjoin="and_("
        "ProgressInvoicingBaseStatus.business_id==Business.id, "
        "ProgressInvoicingBaseStatus.type_ == "
        "'progress_invoicing_group_status')"
    )

    supplier_invoice_lines = relationship("SupplierInvoiceLine")
    base_expense_lines = relationship('BaseExpenseLine')

    @property
    def payment_lines(self):
        """
        Collect payment lines that are referenced by a deadline
        """
        return [deadline.payment_line
                for deadline in self.payment_deadlines
                if deadline.payment_line is not None]

    @property
    def invoiced(self):
        indicator = self.status_service.get_or_create_invoice_indicator(self)
        return indicator.status == indicator.SUCCESS_STATUS

    def get_company_id(self):
        return self.project.company_id

    def get_customer(self):
        return self._endi_service.get_customer(self)

    def amount_to_invoice(self):
        """
        Compute the amount to invoice in this business
        :returns: The amount in the *10^5 precision
        :rtype: int
        """
        return self._endi_service.to_invoice(self)

    def populate_indicators(self):
        """
        Populate custom business indicators

        :returns: The business we manage
        """
        return self.status_service.populate_indicators(self)

    def populate_deadlines(self, estimation=None):
        """
        Populate the current business with its associated payment deadlines
        regarding the estimations belonging to this business

        :param obj estimation: An optionnal Estimation instance
        :returns: This instance
        """
        return self._endi_service.populate_deadlines(
            self, estimation
        )

    def find_deadline(self, deadline_id):
        """
        Find the deadline matching this id

        :param int deadline_id: The deadline id
        :returns: A Payment instance
        """
        return self._endi_service.find_deadline(self, deadline_id)

    def gen_invoices(self, user, payment_deadlines=None):
        """
        Generate invoices for the given list of payment_deadlines (or all if
        not provided)

        :param obj business: The Business in which we work
        :param obj user: The current connected user
        :param list payment_deadlines: Optionnal the deadlines for which we
        generate invoices else all deadlines
        :returns: A list of invoices
        """
        return self._endi_service.gen_invoices(
            self, user, payment_deadlines
        )

    def find_deadline_from_invoice(self, invoice):
        """
        Find the deadline associated to the current business and the given
        invoice

        :param obj invoice: The Invoice we're working on
        :returns: A BusinessPaymentDeadline instance
        """
        return self._endi_service.find_deadline_from_invoice(
            self, invoice
        )

    def get_deposit_deadlines(self, waiting=True):
        """
        Collect deposit related deadlines

        :param bool waiting: Only deadlines not yet invoiced ?

        :returns: A list of deadlines
        """
        return self._endi_service.get_deposit_deadlines(self, waiting)

    def is_visible(self):
        """
        Check if this business should be shown to the end user (if it's parent
        project is not of default type)

        :rtype: bool
        """
        return self._endi_service.is_complex_project_type(self)

    def add_estimation(self, user):
        """
        Generate a new estimation attached to the current business

        :param obj user: The user generating the estimation
        :rtype: class endi.models.task.estimation.Estimation
        """
        return self._endi_service.add_estimation(self, user)

    def add_invoice(self, user):
        """
        Generate a new invoice attached to the current business

        :param obj user: The user generating the invoice
        :rtype: class endi.models.task.invoice.Invoice
        """
        return self._endi_service.add_invoice(self, user)

    def is_void(self):
        """
        Check if the current business is Void
        :rtype: bool
        """
        return self._endi_service.is_void(self)

    def get_price_studies(self):
        return self._endi_service.get_price_studies(self)

    def invoicing_years(self):
        """
        List the financial years of related invoices
        :rtype: list
        """
        return self._endi_service.invoicing_years(self)

    # Progress Invoicing related methods
    def progress_invoicing_is_complete(self):
        """
        Check if this business has been invoiced totally

        :returns: True if it's completely invoiced
        :rtype: bool
        """
        return self._endi_service.progress_invoicing_is_complete(self)

    def set_progress_invoicing_mode(self):
        """
        Change invoicing_mode to progress and populate
        """
        self.invoicing_mode = self.PROGRESS_MODE
        self._endi_service.populate_progress_invoicing_status(self)

    def unset_progress_invoicing_mode(self):
        """
        Change invoicing_mode to classic and clear progress_invoicing related
        elements
        """
        self.invoicing_mode = self.CLASSIC_MODE
        self._endi_service.clear_progress_invoicing_status(self)

    def add_progress_invoicing_invoice(self, user):
        """
        Generate a new invoice attached to the current business

        :param obj user: The user generating the invoice
        :rtype: class endi.models.task.invoice.Invoice
        """
        return self._endi_service.add_progress_invoicing_invoice(self, user)

    def populate_progress_invoicing_lines(self, invoice, appstruct):
        """
        Populate the given invoice with lines and groups generated through the
        current business progress invoicing datas and the given appstruct

        :param obj invoice: The Invoice to populate
        :param dict appstruct: Dict containing the percentage to invoice per
        ProgressInvoicingGroupStatus.id

        e.g:

            {<group_status.id>: {<linen.id>: <percentage>, ...}, ...}

        :returns: The populated Invoice
        """
        self._endi_service.populate_progress_invoicing_lines(
            self, invoice, appstruct
        )

    def on_invoice_delete(self, invoice_id):
        """
        Callback launched when a draft invoice is deleted

        :param int invoice_id: The deleted invoice
        """
        self._endi_service.on_invoice_delete(self, invoice_id)

    def on_estimation_signed_status_change(self):
        """
        Callback launched when estimation signed_status changes
        """
        self._endi_service.on_estimation_signed_status_change(self)
        self._endi_service.on_estimation_signed_status_change(self)
