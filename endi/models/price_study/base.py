# -*- coding: utf-8 -*-
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    BigInteger,
    Boolean,
    Text,
    Numeric
)
from sqlalchemy.orm import (
    relationship,
)

from endi_base.models.base import (
    DBBASE,
    default_table_args,
)
from endi_base.models.mixins import TimeStampedMixin
from endi.compute.math_utils import integer_to_amount
from endi.models.services.price_study import BasePriceStudyProductService


class BasePriceStudyProduct(DBBASE, TimeStampedMixin):
    """
    Base class for PriceStudyProducts and PriceStudyWorks
    """
    __tablename__ = "base_price_study_product"
    __table_args__ = default_table_args
    __mapper_args__ = {
        'polymorphic_on': 'type_',
        'polymorphic_identity': __tablename__,
    }
    id = Column(
        Integer,
        primary_key=True,
    )
    study_id = Column(
        ForeignKey('price_study.id', ondelete='CASCADE'), nullable=False
    )
    type_ = Column('type_', String(30), nullable=False)
    general_overhead = Column(Numeric(6, 5, asdecimal=False))
    margin_rate = Column(Numeric(6, 5, asdecimal=False))

    # ce produit est-il à jour avec le catalogue produit ?
    uptodate = Column(Boolean(), default=True)

    # Coût unitaire
    ht = Column(BigInteger(), default=0)

    description = Column(Text())
    product_id = Column(Integer, ForeignKey('product.id'))
    tva_id = Column(Integer, ForeignKey('tva.id'))
    unity = Column(
        String(100),
        info={
            'colanderalchemy': {'title': "Unité"}
        },
    )
    quantity = Column(
        Numeric(15, 5, asdecimal=False),
        info={
            'colanderalchemy': {"title": "Quantité"}
        },
        default=1
    )
    total_ht = Column(BigInteger(), default=0)

    order = Column(Integer, default=0)

    # Relationships
    study = relationship('PriceStudy', back_populates='products')
    tva = relationship("Tva")
    product = relationship("Product")

    TYPE_LABELS = {
        'price_study_work': 'Produit composé (ouvrage)',
        'price_study_product': 'Produit simple',
    }
    _endi_service = BasePriceStudyProductService

    def __json__(self, request):
        return dict(
            id=self.id,
            study_id=self.study_id,
            type_=self.type_,
            uptodate=self.uptodate,
            margin_rate=self.margin_rate,
            general_overhead=self.general_overhead,
            ht=integer_to_amount(self.ht, 5, 0),
            description=self.description,
            product_id=self.product_id,
            tva_id=self.tva_id,
            unity=self.unity,
            quantity=self.quantity,
            total_ht=integer_to_amount(self.total_ht, 5),
            order=self.order,
        )

    @classmethod
    def from_sale_product(cls, sale_product):
        instance = cls(quantity=1)
        for field in (
            'ht', 'description', 'product_id', 'tva_id', 'unity',
            'general_overhead', 'margin_rate',
        ):
            setattr(instance, field, getattr(sale_product, field, None))
        return instance

    def sync_from_sale_product(self, sale_product, excludes=()):
        for field in (
            'ht', 'description', 'product_id', 'tva_id', 'unity',
            'general_overhead', 'margin_rate',
        ):
            if field not in excludes:
                catalog_value = getattr(sale_product, field, None)
                if catalog_value:
                    setattr(self, field, catalog_value)

    def duplicate(self):
        instance = self.__class__()
        for field in (
            'general_overhead', 'margin_rate',
            'study_id', 'uptodate', 'ht', 'description',
            'product_id', 'tva_id', 'unity', 'quantity',
            'total_ht'
        ):
            setattr(instance, field, getattr(self, field, None))
        return instance

    def get_company_id(self):
        return self._endi_service.get_company_id(self)

    # Computing tools
    def flat_cost(self):
        return self._endi_service.flat_cost(self)

    def cost_price(self):
        return self._endi_service.cost_price(self)

    def intermediate_price(self):
        return self._endi_service.intermediate_price(self)

    def unit_ht(self, cae_contribution=None):
        return self._endi_service.unit_ht(self, cae_contribution)

    def compute_total_ht(self, cae_contribution=None):
        return self._endi_service.compute_total_ht(self, cae_contribution)

    def ht_by_tva(self):
        return self._endi_service.ht_by_tva(self)

    def ttc(self, cae_contribution=None):
        return self._endi_service.ttc(self, cae_contribution)

    def sync_amounts(self):
        return self._endi_service.sync_amounts(self)

    def on_before_commit(self, state, attributes=None):
        return self._endi_service.on_before_commit(self, state, attributes)
