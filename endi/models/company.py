# -*- coding: utf-8 -*-
"""
    Company model
"""
import logging
import datetime

from sqlalchemy import (
    Table,
    Column,
    Integer,
    Float,
    Numeric,
    String,
    Text,
    ForeignKey,
    Date,
    Boolean,
)
from sqlalchemy.orm import (
    relationship,
    deferred,
    backref,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)
from endi_base.models.mixins import (
    PersistentACLMixin,
)

from endi.compute import math_utils
from endi.models.options import (
    ConfigurableOption,
    get_id_foreignkey_col,
)
from endi.models.tools import (
    get_excluded_colanderalchemy,
)
from endi.models.services.company import CompanyService
from endi.models.user.user import COMPANY_EMPLOYEE

log = logging.getLogger(__name__)


COMPANY_ACTIVITY = Table(
    'company_activity_rel',
    DBBASE.metadata,
    Column("company_id", Integer, ForeignKey('company.id')),
    Column("activity_id", Integer, ForeignKey('company_activity.id')),
    mysql_charset=default_table_args['mysql_charset'],
    mysql_engine=default_table_args['mysql_engine'],
)


class CompanyActivity(ConfigurableOption):
    """
    Company activities
    """
    __colanderalchemy_config__ = {
        'title': "Domaine d'activité",
        'validation_msg': "Les domaines d'activité ont bien été configurées",
    }
    id = get_id_foreignkey_col('configurable_option.id')


class Company(DBBASE, PersistentACLMixin):
    """
        Company model
        Store all company specific stuff (headers, logos, RIB, ...)
    """
    __tablename__ = 'company'
    __table_args__ = default_table_args
    id = Column("id", Integer, primary_key=True)
    name = Column("name", String(150), nullable=False)
    internal = Column(Boolean(), default=False, nullable=False)

    goal = deferred(
        Column(
            "object",
            String(255),
            default="",
        ),
        group='edit'
    )
    email = deferred(
        Column(
            "email",
            String(255)
        ),
        group='edit',
    )
    phone = deferred(
        Column(
            "phone",
            String(20),
            default=""
        ),
        group='edit'
    )
    mobile = deferred(
        Column(
            "mobile",
            String(20)
        ),
        group='edit'
    )
    address = deferred(
        Column(
            "address",
            String(255),
            info={
                'colanderalchemy': {
                    'title': 'Adresse',
                }
            },
            default="",
        ),
        group='edit'
    )
    zip_code = deferred(
        Column(
            "zip_code",
            String(20),
            info={
                'colanderalchemy': {
                    'title': 'Code postal',
                },
            },
            default="",
        ),
        group='edit',
    )
    city = deferred(
        Column(
            "city",
            String(255),
            info={
                'colanderalchemy': {
                    'title': 'Ville',
                }
            },
            default="",
        ),
        group='edit',
    )
    country = deferred(
        Column(
            "country",
            String(150),
            info={
                'colanderalchemy': {
                    'title': 'Pays'
                },
            },
            default='France',
        ),
        group='edit',
    )
    comments = deferred(
        Column(
            "comments",
            Text
        ),
        group='edit'
    )

    created_at = deferred(
        Column(
            Date(),
            default=datetime.date.today,
            nullable=False,
        ),
    )
    updated_at = deferred(
        Column(
            Date(),
            default=datetime.date.today,
            onupdate=datetime.date.today,
            nullable=False,
        )
    )
    active = deferred(Column(Boolean(), default=True))
    RIB = deferred(
        Column(
            "RIB",
            String(255)
        ),
        group='edit'
    )
    IBAN = deferred(
        Column(
            "IBAN",
            String(255)
        ),
        group='edit'
    )

    code_compta = deferred(
        Column(
            String(30),
            default=""
        ),
        group="edit",
    )

    general_customer_account = deferred(
        Column(
            String(255),
            default=""
        ),
        group="edit",
    )

    third_party_customer_account = deferred(
        Column(
            String(255),
            default=""
        ),
        group="edit",
    )

    general_supplier_account = deferred(
        Column(
            String(255),
            default=""
        ),
        group="edit",
    )

    third_party_supplier_account = deferred(
        Column(
            String(255),
            default=""
        ),
        group="edit",
    )

    bank_account = deferred(
        Column(
            String(255),
            default=""
        ),
        group="edit",
    )

    custom_insurance_rate = deferred(
        Column(Float),
        group='edit'
    )

    contribution = deferred(
        Column(Float),
        group='edit'
    )

    header_id = Column(
        ForeignKey('file.id'),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True}
        },
    )
    logo_id = Column(
        ForeignKey('file.id'),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True}
        },
    )
    cgv = deferred(
        Column(Text, default=''),
        group='edit',
    )

    # sequences related, used for counters initialization on migration from
    # another system (eg: WinScop). Contain the latest index already assigned.
    month_company_sequence_init_value = deferred(
        Column(Integer),
    )

    month_company_sequence_init_date = deferred(
        Column(Date),
    )

    general_overhead = deferred(
        Column(Numeric(6, 5, asdecimal=False), default=0),
        group='edit'
    )
    margin_rate = deferred(
        Column(Numeric(6, 5, asdecimal=False), default=0),
        group='edit'
    )

    # Relationships
    header_file = relationship(
        "File",
        primaryjoin="File.id==Company.header_id",
        # backref utilisé pour le calcul des acls
        backref=backref(
            "company_header_backref",
            uselist=False,
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True}
            },
        ),
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True}
        },
    )

    logo_file = relationship(
        "File",
        primaryjoin="File.id==Company.logo_id",
        # backref utilisé pour le calcul des acls
        backref=backref(
            "company_logo_backref",
            uselist=False,
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True}
            },
        ),
        uselist=False,
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True}
        },
    )

    activities = relationship(
        "CompanyActivity",
        secondary=COMPANY_ACTIVITY,
        backref=backref(
            'companies',
            info={
                'colanderalchemy': {'exclude': True},
                'export': {'exclude': True},
            },
        ),
        info={
            'colanderalchemy': {
                'title': 'Activités',
            },
            'export': {'exclude': True},
        },
    )

    customers = relationship(
        "Customer",
        order_by="Customer.code",
        back_populates="company",
        info={
            'colanderalchemy': {'exclude': True},
            "export": {'exclude': True}
        }
    )

    suppliers = relationship(
        "Supplier",
        order_by="Supplier.code",
        back_populates="company",
        info={
            'colanderalchemy': {'exclude': True},
            "export": {'exclude': True}
        }
    )

    projects = relationship(
        "Project",
        order_by="Project.id",
        back_populates="company",
        info={
            'colanderalchemy': {'exclude': True},
            "export": {'exclude': True}
        },
    )
    tasks = relationship(
        "Task",
        primaryjoin="Task.company_id==Company.id",
        order_by='Task.date',
        back_populates="company",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        },
    )
    employees = relationship(
        "User",
        secondary=COMPANY_EMPLOYEE,
        back_populates="companies",
        info={
            'colanderalchemy': get_excluded_colanderalchemy('Employés'),
            'export': {'exclude': True}
        },
    )
    sale_products = relationship(
        "BaseSaleProduct",
        order_by="BaseSaleProduct.label",
        back_populates="company",
        info={
            'export': {'exclude': True},
        }
    )
    expense = relationship(
        "ExpenseSheet",
        order_by="ExpenseSheet.month",
        cascade="all, delete-orphan",
        back_populates="company",
        info={
            'export': {'exclude': True},
        }
    )

    _endi_service = CompanyService

    def get_company_id(self):
        """
            Return the current company id
            Allows company id access through request's context
        """
        return self.id

    @property
    def header(self):
        return self.header_file

    @header.setter
    def header(self, appstruct):
        if self.header_file is not None and appstruct.get('delete'):
            DBSESSION().delete(self.header_file)
        else:
            if self.header_file is None:
                from endi.models.files import File
                self.header_file = File()

            appstruct['filename'] = appstruct['name'] = 'header.png'
            appstruct['mimetype'] = 'image/png'

            data = appstruct.pop('data', None)
            for key, value in list(appstruct.items()):
                setattr(self.header_file, key, value)

            if data is not None:
                self.header_file.data = data

            self.header_file.description = 'Header'

    @property
    def logo(self):
        return self.logo_file

    @logo.setter
    def logo(self, appstruct):
        if self.logo_file is not None and appstruct.get('delete'):
            DBSESSION().delete(self.logo_file)
        else:
            if self.logo_file is None:
                from endi.models.files import File
                self.logo_file = File()

            self.logo_file.name = appstruct.get('name', 'logo.png')
            for key, value in list(appstruct.items()):
                setattr(self.logo_file, key, value)
            self.logo_file.description = 'Logo'

    @classmethod
    def query(cls, keys=None, active=True):
        """
            Return a query
        """
        if keys:
            query = DBSESSION().query(*keys)
        else:
            query = super(Company, cls).query()
        if active:
            query = query.filter(cls.active == True)  # noqa: E712
        return query.order_by(cls.name)

    def __json__(self, request):
        """
            return a dict representation
        """
        customers = [customer.__json__(request) for customer in self.customers]
        suppliers = [supplier.__json__(request) for supplier in self.suppliers]
        projects = [project.__json__(request) for project in self.projects]
        return dict(id=self.id,
                    name=self.name,
                    goal=self.goal,
                    email=self.email,
                    phone=self.phone,
                    mobile=self.mobile,
                    comments=self.comments,
                    RIB=self.RIB,
                    IBAN=self.IBAN,
                    customers=customers,
                    suppliers=suppliers,
                    projects=projects)

    def disable(self):
        """
        Disable the current company
        """
        self.active = False

    def enable(self):
        """
        enable the current company
        """
        self.active = True

    def get_tasks(self):
        """
        Get all tasks for this company, as a list
        """
        return self._endi_service.get_tasks(self)

    def get_recent_tasks(self, page_nb, nb_per_page):
        """
        :param int nb_per_page: how many to return
        :param int page_nb: pagination index

        .. todo:: this is naive, use sqlalchemy pagination

        :return: pagination for wanted tasks, total nb of tasks
        """
        count = self.get_tasks().count()
        offset = page_nb * nb_per_page
        items = self._endi_service.get_tasks(
            self, offset=offset, limit=nb_per_page
        )
        return items, count

    def get_estimations(self, valid=False):
        """
        Return the estimations of the current company
        """
        return self._endi_service.get_estimations(self, valid)

    def get_invoices(self, valid=False):
        """
        Return the invoices of the current company
        """
        return self._endi_service.get_invoices(self, valid)

    def get_cancelinvoices(self, valid=False):
        """
        Return the cancelinvoices of the current company
        """
        return self._endi_service.get_cancelinvoices(self, valid)

    def has_invoices(self):
        """
            return True if this company owns invoices
        """
        return self.get_invoices(self, valid=True).count() > 0 or \
            self.get_cancelinvoices(self, valid=True).count() > 0

    def get_real_customers(self, year):
        """
        Return the real customers (with invoices)
        """
        return self._endi_service.get_customers(self, year)

    def get_late_invoices(self):
        """
        Return invoices waiting for more than 45 days
        """
        return self._endi_service.get_late_invoices(self)

    def get_customer_codes_and_names(self):
        """
        Return current company's customer codes and names
        """
        return self._endi_service.get_customer_codes_and_names(self)

    def get_supplier_codes_and_names(self):
        """
        Return current company's supplier codes and names
        """
        return self._endi_service.get_supplier_codes_and_names(self)

    def get_project_codes_and_names(self):
        """
        Return current company's project codes and names
        """
        return self._endi_service.get_project_codes_and_names(self)

    def get_next_estimation_index(self):
        """
        Return the next estimation index
        """
        return self._endi_service.get_next_estimation_index(self)

    def get_next_invoice_index(self):
        """
        Return the next invoice index
        """
        return self._endi_service.get_next_invoice_index(self)

    def get_next_cancelinvoice_index(self):
        """
        Return the next cancelinvoice index
        """
        return self._endi_service.get_next_cancelinvoice_index(self)

    def get_turnover(self, year):
        """
        Retrieve the annual turnover for the current company

        :param int year: The current year
        """
        ca = self._endi_service.get_turnover(self, year)
        return math_utils.integer_to_amount(ca, precision=5)

    def get_business_nested_options(self):
        return self._endi_service.get_business_nested_options(self)

    @classmethod
    def label_query(cls):
        return cls._endi_service.label_query(cls)

    @classmethod
    def query_for_select(cls, active_only=False):
        return cls._endi_service.query_for_select(cls, active_only)

    @classmethod
    def get_id_by_analytical_account(cls, analytical_account):
        return cls._endi_service.get_id_by_analytical_account(
            cls,
            analytical_account
        )

    @classmethod
    def query_for_select_with_trainer(cls):
        return cls._endi_service.query_for_select_with_trainer(cls)

    def has_trainer(self):
        return self.has_group_member(group_name='trainer')

    def get_employee_ids(self):
        return self._endi_service.get_employee_ids(self)

    def has_group_member(self, group_name):
        return self._endi_service.has_group_member(self, group_name)

    def employs(self, uid):
        """
        :param uiud int: User id
        """
        return self._endi_service.employs(self, uid)

    @classmethod
    def get_cae_contribution(cls, company_id):
        """
        :returns: The cae contribution percentage
        """
        return cls._endi_service.get_cae_contribution(company_id)

    def sync_general_overhead(self, old_value, new_value):
        return self._endi_service.sync_general_overhead(
            self, old_value, new_value
        )

    def sync_margin_rate(self, old_value, new_value):
        return self._endi_service.sync_margin_rate(
            self, old_value, new_value
        )

    def get_general_customer_account(self):
        return self._endi_service.get_general_customer_account(self)

    def get_third_party_customer_account(self):
        return self._endi_service.get_third_party_customer_account(self)

    def get_general_supplier_account(self):
        return self._endi_service.get_general_supplier_account(self)

    def get_third_party_supplier_account(self):
        return self._endi_service.get_third_party_supplier_account(self)
