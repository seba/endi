# -*- coding: utf-8 -*-
import deform
import deform_extensions
import colander
from sqlalchemy import (
    Boolean,
    Column,
    Float,
    ForeignKey,
    Integer,
    String,
    UniqueConstraint,
)
from sqlalchemy.orm import (
    backref,
    relationship,
)

from endi import forms
from endi.forms.training.bpf import (
    deferred_financial_year_validator,
    deferred_invoice_widget,
    deferred_income_source_select,
    deferred_nsf_training_speciality_id_widget,
    deferred_training_goal_select,
    deferred_trainee_type_select,
    deferred_income_source_validator,
    deferred_training_goal_validator,
    deferred_trainee_type_validator,
)
from endi.models.options import (
    ConfigurableOption,
    get_id_foreignkey_col,
)
from endi.models.task.invoice import (
    Invoice,
    get_invoice_years,
)
from endi.utils.datetimes import get_current_year
from endi_base.models.base import (
    default_table_args,
    DBBASE,
)
from endi_base.models.mixins import (
    TimeStampedMixin,
)


INCOME_SOURCE_GRID = (
    (
        ('invoice_id', 6),
        ('income_category_id', 6),
    ),
)
TRAINEE_TYPE_GRID = (
    (
        ('trainee_type_id', 6),
        ('headcount', 3),
        ('total_hours', 3),
    ),
)
BPF_DATA_GRID = [
    (
        ('financial_year', 6),
        ('cerfa_version', 6),
    ),
    (
        ('headcount', 6),
        ('total_hours', 6),
    ),
    (
        ('trainee_types', 12),
    ),
    (
        ('has_subcontract', 6),
        ('has_subcontract_amount', 2),
        ('has_subcontract_headcount', 2),
        ('has_subcontract_hours', 2),
    ),
    (
        ('is_subcontract', 6),
    ),
    (
        ('income_sources', 12),
    ),
    (
        ('training_goal_id', 12),
    ),
    (
        ('training_speciality_id', 12),
    ),
]


class TraineeCount(DBBASE):
    __tablename__ = 'business_bpf_data_trainee_count'
    __table_args__ = default_table_args
    __colanderalchemy_config__ = {
        "widget": deform_extensions.GridMappingWidget(
            named_grid=TRAINEE_TYPE_GRID,
        ),
        "title": "type de stagiaire",
    }

    id = Column(
        Integer,
        primary_key=True,
        info={
            'colanderalchemy': {
                'widget': deform.widget.HiddenWidget(),
                'missing': colander.drop,
            },
        }
    )
    business_bpf_data_id = Column(
        Integer,
        ForeignKey('business_bpf_data.id', ondelete='cascade'),
        nullable=False,
        info={
            'colanderalchemy': {'exclude': True}
        },
    )
    trainee_type_id = Column(
        Integer,
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Type de stagiaire",
                'widget': deferred_trainee_type_select,
                'validator': deferred_trainee_type_validator,
            },
        }
    )
    headcount = Column(
        Integer,
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Nb. stagiaires",
                'validator': colander.Range(min=0)
            },
        },
    )
    total_hours = Column(
        Float(),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Nb. Heures total",
                'validator': colander.Range(min=0),
                'description': (
                    "Si tous les stagiaires de ce type ont le même volume "
                    + "horaire : nb. heures x nb. stagiaires de ce type."
                ),
            },
        },
    )


class IncomeSource(DBBASE):
    __tablename__ = 'business_bpf_data_income_source'
    __table_args__ = default_table_args
    __colanderalchemy_config__ = {
        'widget': deform_extensions.GridMappingWidget(
            named_grid=INCOME_SOURCE_GRID,
        ),
        'title': "financement",
    }
    id = Column(
        Integer,
        primary_key=True,
        info={
            'colanderalchemy': {
                'widget': deform.widget.HiddenWidget(),
                'missing': colander.drop,
            },
        }
    )
    business_bpf_data_id = Column(
        Integer,
        ForeignKey('business_bpf_data.id', ondelete='cascade'),
        nullable=False,
        info={
            'colanderalchemy': {'exclude': True}
        },
    )
    invoice_id = Column(
        Integer,
        ForeignKey('invoice.id', ondelete='CASCADE'),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Facture",
                'widget': deferred_invoice_widget,
            },
        }
    )
    invoice = relationship(
        "Invoice",
        primaryjoin="IncomeSource.invoice_id==Invoice.id",
        info={
            'colanderalchemy': {'exclude': True},
        }
    )
    income_category_id = Column(
        Integer,
        nullable=False,
        info={
            'colanderalchemy': {
                'title': 'Catégorie',
                'widget': deferred_income_source_select,
                'validator': deferred_income_source_validator,
            },
        }
    )

class BusinessBPFData(TimeStampedMixin, DBBASE):
    """
    Meant to be subclassed (multi-table inheritance) and never
    used alone.
    """
    __tablename__ = 'business_bpf_data'
    __table_args__ = (
        UniqueConstraint('business_id', 'financial_year'),
        default_table_args,
    )
    __colanderalchemy_config__ = {
        "widget": deform_extensions.GridFormWidget(named_grid=BPF_DATA_GRID),
    }

    id = Column(
        Integer,
        primary_key=True,
        info={
            'colanderalchemy': {
                'widget': deform.widget.HiddenWidget(),
                'missing': colander.drop,
            },
        },
    )

    business_id = Column(
        ForeignKey('business.id', ondelete="cascade"),
        nullable=False,
    )
    business = relationship(
        "Business",
        primaryjoin="BusinessBPFData.business_id==Business.id",
        # on utilise passive_deletes car on a définit le ondelete cascade au
        # niveau DB
        backref=backref('bpf_datas', uselist=True, passive_deletes=True),
    )
    financial_year = Column(
        Integer,
        nullable=False,
        default=get_current_year,
        info={'colanderalchemy': {
            'title': "Année fiscale de référence",
            'widget': forms.get_year_select_deferred(
                query_func=get_invoice_years,
            ),
            'validator': deferred_financial_year_validator,
        }},
    )
    cerfa_version = Column(
        String(10),
        nullable=False,
        info={
            'colanderalchemy': {
                'widget': deform.widget.TextInputWidget(readonly=True),
                'missing': colander.drop,
                'title': 'Version du formulaire CERFA',
            },
        }
    )
    total_hours = Column(
        Float(),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Nb. Heures total suivies par l'ensemble des stagiaires",
                'description': "Si tous les stagiaires ont le même volume horaire : nb. heures x nb. stagiaires",
                'validator': colander.Range(min=0)
            },
        },
    )
    headcount = Column(
        Integer(),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Nb. de stagiaires",
                'validator': colander.Range(min=0),
            },
        },
    )
    has_subcontract = Column(
        String(5),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Cette formation est-elle sous-traitée à un autre OF ?",
                'widget': deform.widget.SelectWidget(
                    values=(
                        ('no', 'Non'),
                        ('full', 'Totalement'),
                        ('part', 'Partiellement'),
                    ),
                    inline=True,
                ),
                'description': "Correspond à l'achat ou non de formation à un tiers",
            },
        },
    )
    has_subcontract_hours = Column(
        Float(),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Nb. heures sous-traitées",
                'validator': colander.Range(min=0),
            },
        },
    )
    has_subcontract_headcount = Column(
        Integer(),
        nullable=False,
        default=0,
        info={
            'colanderalchemy': {
                'title': "Nb. stagiaires concernés",
                'validator': colander.Range(min=0),
            },
        },
    )
    has_subcontract_amount = Column(
        Float(),
        nullable=False,
        default=0,
        info={
            'colanderalchemy': {
                'title': "Montant HT",
                'description': "Dépensé en sous-traitance",
                'validator': colander.Range(min=0),
            },
        },
    )

    is_subcontract = Column(
        Boolean(),
        nullable=False,
        default=0,
        info={
            'colanderalchemy': {
                'widget': deform.widget.SelectWidget(
                    values=(
                        ('false', 'Oui'),
                        ('true', "Non (portée par un autre Organisme "
                         "de formation qui m'achète de la formation "
                         "en sous-traitance)"),
                    ),
                ),
                'title': "Cette formation est-elle portée en direct "
                "par la CAE ?",
            },
        }
    )
    training_speciality_id = Column(
        Integer(),
        ForeignKey('nsf_training_speciality_option.id'),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Spécialité de formation",
                'widget': deferred_nsf_training_speciality_id_widget,
            },
        }
    )
    training_goal_id = Column(
        Integer(),
        nullable=False,
        info={
            'colanderalchemy': {
                'title': "Objectif principal de formation",
                'widget': deferred_training_goal_select,
                'validator': deferred_training_goal_validator,
            },
        }
    )
    training_speciality = relationship(
        'NSFTrainingSpecialityOption',
        primaryjoin='BusinessBPFData.training_speciality_id==NSFTrainingSpecialityOption.id',
    )
    trainee_types = relationship(
        "TraineeCount",
        primaryjoin="BusinessBPFData.id==TraineeCount.business_bpf_data_id",
        cascade='all, delete-orphan',
        info={
            'colanderalchemy': {
                'title': "Typologie des stagiaires",
                "widget": deform.widget.SequenceWidget(
                    add_subitem_text_template="Renseigner un type de stagiaire supplémentaire",
                    min_len=1,
                ),
            }
        }
    )

    income_sources = relationship(
        "IncomeSource",
        primaryjoin="BusinessBPFData.id==IncomeSource.business_bpf_data_id",
        cascade='all, delete-orphan',
        info={
            'colanderalchemy': {
                'title': "Financement",
                'widget': deform.widget.SequenceWidget(
                    add_subitem_text_template="Renseigner un financement supplémentaire",
                    min_len=1,
                )
            }
        }
    )


class NSFTrainingSpecialityOption(ConfigurableOption):
    """
    Nomenclature des spécialités de formation

    https://public.opendatasoft.com/explore/dataset/codes-nsf/
    https://www.data.gouv.fr/fr/datasets/582c8978c751df788ec0bb7e/
    """
    __tablename__ = 'nsf_training_speciality_option'
    __mapper_args__ = {
        'polymorphic_identity': 'nsf_training_speciality_option'
    }

    id = get_id_foreignkey_col('configurable_option.id')
