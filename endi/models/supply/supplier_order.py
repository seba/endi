# -*- coding: utf-8 -*-
import datetime

from beaker.cache import cache_region
from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    distinct,
    ForeignKey,
    Integer,
    func,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)
from endi_base.models.mixins import DuplicableMixin
from endi.models.node import Node
from endi.models.status import ValidationStatusHolderMixin
from endi.models.task.actions import get_validation_state_manager
from endi.compute.supplier_order import (
    SupplierOrderCompute,
    SupplierOrderLineCompute,
)
from endi.models.supply.mixins import LineModelMixin
from endi.models.services.supplier_order import SupplierOrderService


class SupplierOrderLine(LineModelMixin, DBBASE, SupplierOrderLineCompute):
    __tablename__ = 'supplier_order_line'
    __table_args__ = default_table_args

    id = Column(
        Integer,
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )
    supplier_order_id = Column(
        Integer,
        ForeignKey("supplier_order.id", ondelete="cascade"),
        nullable=False,
        info={'colanderalchemy': {'exclude': True}}
    )

    supplier_order = relationship(
        "SupplierOrder",
        primaryjoin="SupplierOrder.id==SupplierOrderLine.supplier_order_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    def __json__(self, request):
        ret = super(SupplierOrderLine, self).__json__(request)
        ret.update(dict(
            supplier_order_id=self.supplier_order_id,
        ))
        return ret


class SupplierOrder(
        DuplicableMixin,
        ValidationStatusHolderMixin,
        SupplierOrderCompute,
        Node,
):
    __tablename__ = 'supplier_order'
    __table_args__ = default_table_args
    __mapper_args__ = {'polymorphic_identity': 'supplier_order'}
    __duplicable_fields__ = [
        'company_id',
        'supplier_id',
        'cae_percentage',
    ]

    _endi_service = SupplierOrderService

    id = Column(
        ForeignKey('node.id'),
        primary_key=True,
        info={"colanderalchemy": {'exclude': True}},
    )

    company_id = Column(
        Integer,
        ForeignKey('company.id'),
        info={
            'export': {'exclude': True},
            'colanderalchemy': {'exclude': True},
        },
        nullable=False,
    )

    company = relationship(
        "Company",
        primaryjoin="Company.id==SupplierOrder.company_id",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    validation_state_manager = get_validation_state_manager(
        'supplier_order',
        userid_attr='status_user_id',
    )

    supplier_id = Column(
        Integer,
        ForeignKey("supplier.id"),
        info={
            'export': {'exclude': True},
        },
    )

    supplier = relationship(
        "Supplier",
        primaryjoin="Supplier.id==SupplierOrder.supplier_id",
        back_populates="orders",
        info={
            'colanderalchemy': {'exclude': True},
            'export': {'exclude': True},
        }
    )

    supplier_invoice_id = Column(
        Integer,
        ForeignKey('supplier_invoice.id'),
        info={
            'export': {'exclude': True},
        },
        nullable=True,
    )

    supplier_invoice = relationship(
        "SupplierInvoice",
        primaryjoin='SupplierInvoice.id==SupplierOrder.supplier_invoice_id',
        backref='supplier_orders',
        info={
            'export': {'exclude': True},
        }
    )

    lines = relationship(
        "SupplierOrderLine",
        cascade="all, delete-orphan",
        order_by="SupplierOrderLine.id",
        info={
            "colanderalchemy": {
                "title": "Entrées",
                "description": "Vous pouvez soit lister le détail de votre "
                + "commande soit vous contenter d'un total global.",
            }
        }
    )

    cae_percentage = Column(
        Integer,
        default=0,
        info={
            'colanderalchemy': {
                'title': 'pourcentage décaissé par la CAE'
            },
        },
    )

    @classmethod
    def query_for_select(cls, valid_only=False, company_id=None, invoiced=None):
        return cls._endi_service.query_for_select(
            cls,
            valid_only,
            company_id,
            invoiced,
        )

    @classmethod
    def filter_by_year(cls, query, year):
        return cls._endi_service.filter_by_year(cls, query, year)

    # FIXME: factorize ?
    def check_validation_status_allowed(self, status, request, **kw):
        return self.validation_state_manager.check_allowed(
            status,
            self,
            request,
        )

    # FIXME: factorize ?
    def set_validation_status(self, status, request, **kw):
        return self.validation_state_manager.process(
            status,
            self,
            request,
            **kw
        )

    def get_company_id(self):
        # for company detection in menu display
        return self.company_id

    def get_company(self):
        # for dashboard
        return self.company

    def import_lines_from_order(self, supplier_order):
        """
        Copies all lines from a SupplierOrder
        """
        return self._endi_service.import_lines(
            dest_line_factory=SupplierOrderLine,
            src_obj=supplier_order,
            dest_obj=self,
        )

    def __json__(self, request):
        return dict(
            id=self.id,
            name=self.name,
            created_at=self.created_at.isoformat(),
            updated_at=self.updated_at.isoformat(),

            company_id=self.company_id,
            # user_id=self.user_id,
            # paid_status=self.paid_status,
            # justified=self.justified,
            status=self.status,
            status_user_id=self.status_user_id,
            status_date=self.status_date.isoformat(),
            cae_percentage=self.cae_percentage,

            lines=[line.__json__(request) for line in self.lines],
            attachments=[
                f.__json__(request)for f in self.children if f.type_ == 'file'
            ]
        )


# Usefull queries
def get_supplier_orders_years(kw=None):
    """
        Return a cached query for the years we have invoices configured

    :param kw: is here only for API compatibility
    """
    @cache_region("long_term", "supplier_orders_years")
    def years():
        """
            return the distinct financial years available in the database
        """
        query = DBSESSION().query(
            distinct(func.year(SupplierOrder.created_at))
        )
        query = query.order_by(SupplierOrder.created_at)
        years = [year[0] for year in query]
        current = datetime.date.today().year
        if current not in years:
            years.append(current)
        return years
    return years()
