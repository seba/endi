/*
    Script to exec on default layout pages startup (not OPA)
*/

$( function() {

    // Popups initialisation
    $('.endi-utils-widgets-popup').each(function(){
        setPopUp($(this).attr("id"), $(this).attr("data-title"));
    });

    // Initialisation of company select menu (if needed)
    var company_select_tag = $('#company-select-menu');
    if (!_.isUndefined(company_select_tag)){
        $('#company-select-menu').select2();
        $('#company-select-menu').change(
            function() { window.location = $(this).val(); }
        );
    }

    // Handle size of the main menu (mini/maxi)
    if(getCookie('endi__menu_mini') == "true") {
        resize("menu", document.getElementById('menu_size_btn'));
    }

    // Keep user menu open if needed
    $('#user_menu:has(.current_item)').show();

    // Hack to prevent deform sequence widgets to display title twice
    $('.deform-seq').prev('label').hide();

    // Hack to isolate deform form-group elements having multiple inputs
    $('.item-start, .item-start_date').parent().addClass('range');

    // Hack to open the dropdown that contain the current menu
    $('.current_item').parent().siblings('button').click();

    // End of page loading
    removeClass(document.body, 'preload');
    
});
