# -*- coding: utf-8 -*-
from endi.compute.base_line import BaseLineCompute
from endi.models.expense.types import ExpenseType


class SupplierInvoiceCompute(object):
    """
    Used for computation :
    - suppliers_orders (liste d'orders)
    -
    """
    @property
    def orders_total(self):
        return sum(order.total for order in self.supplier_orders)

    @property
    def orders_cae_total(self):
        return sum(order.cae_total for order in self.supplier_orders)

    @property
    def orders_worker_total(self):
        return sum(order.worker_total for order in self.supplier_orders)

    @property
    def orders_total_ht(self):
        return sum(order.total_ht for order in self.supplier_orders)

    @property
    def orders_total_tva(self):
        return sum(order.total_tva for order in self.supplier_orders)

    @property
    def total(self):
        return sum([line.total for line in self.lines])

    @property
    def total_ht(self):
        return sum([line.total_ht for line in self.lines])

    @property
    def total_tva(self):
        return sum([line.total_tva for line in self.lines])

    @property
    def cae_total(self):
        return sum(order.cae_total for order in self.supplier_orders)

    def topay(self):
        # Handle the case where sum(order.total) > invoice.total
        return max(
            self.cae_total - self.paid(),
            0
        )

    def paid(self):
        return sum([payment.get_amount() for payment in self.payments])

    def get_lines_by_type(self):
        """
        Return supplier invoice lines grouped by treasury code
        """
        ret_dict = {}
        for line in self.lines:
            line.type_object = ExpenseType.query().filter_by(id=line.type_id).first()
            ret_dict.setdefault(line.type_object.code, []).append(line)
        return list(ret_dict.values())


class SupplierInvoiceLineCompute(BaseLineCompute):
    pass
