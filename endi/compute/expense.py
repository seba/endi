# -*- coding: utf-8 -*-
"""
Expense computing tool
"""
from endi.compute import math_utils


class ExpenseCompute(object):
    lines = ()
    kmlines = ()
    payments = ()

    def _lines_by_category(self, lines, category=None):
        """
        Return the lines matching the category if provided
        """
        for line in lines:
            if category in ('1', '2') and line.category != category:
                continue
            else:
                yield line

    def get_lines_by_type(self, category="0"):
        """
        Return expense lines grouped by treasury code
        """
        ret_dict = {}
        for line in self._lines_by_category(self.lines, category):
            ret_dict.setdefault(line.expense_type.code, []).append(line)

        for line in self._lines_by_category(self.kmlines, category):
            ret_dict.setdefault(line.expense_type.code, []).append(line)

        return list(ret_dict.values())

    @property
    def total(self):
        return sum([line.total for line in self.lines]) + \
            sum([line.total for line in self.kmlines])

    def paid(self):
        return sum([payment.get_amount() for payment in self.payments])

    def topay(self):
        return self.total - self.paid()

    @property
    def total_tva(self):
        return sum([line.total_tva for line in self.lines]) + \
            sum([line.total_tva for line in self.kmlines])

    @property
    def total_ht(self):
        return sum([line.total_ht for line in self.lines]) + \
            sum([line.total_ht for line in self.kmlines])

    @property
    def total_km(self):
        return sum([line.km for line in self.kmlines])

    def get_total(self, category=None):
        line_total = sum(
            [
                line.total
                for line in self._lines_by_category(self.lines, category)
            ]
        )
        kmlines_total = sum(
            [
                line.total
                for line in self._lines_by_category(self.kmlines, category)
            ]
        )
        return line_total + kmlines_total


class ExpenseLineCompute(object):
    """
    Expense lines related computation tools
    """
    expense_type = None

    def _compute_value(self, val):
        result = 0
        if self.expense_type is not None:
            if self.expense_type.type == 'expensetel':
                percentage = self.expense_type.percentage
                val = val * percentage / 100.0
            result = math_utils.floor(val)
        return result

    @property
    def total(self):
        return self.total_ht + self.total_tva

    @property
    def total_ht(self):
        return self._compute_value(self.ht)

    @property
    def total_tva(self):
        return self._compute_value(self.tva)


class ExpenseKmLineCompute(object):
    expense_type = None

    @property
    def total(self):
        indemnity = self.expense_type.amount
        return math_utils.floor(indemnity * self.km)

    @property
    def ht(self):
        # Deprecated function kept for compatibility
        return self.total

    @property
    def total_ht(self):
        return self.total

    @property
    def total_tva(self):
        return 0
