# -*- coding: utf-8 -*-
import colander
import datetime

from endi import forms


@colander.deferred
def deferred_january_first(node, kw):
    """
    Deferly returns the first january value
    """
    today = datetime.date.today()
    return datetime.date(today.year, 1, 1)


class YearPeriodSchema(colander.MappingSchema):
    """
    A form used to select a period
    """
    start_date = colander.SchemaNode(
        colander.Date(),
        title="Début",
        description=None,
        missing=colander.drop,
        default=deferred_january_first
    )
    end_date = colander.SchemaNode(
        colander.Date(),
        title="Fin",
        description=None,
        missing=colander.drop,
        default=forms.deferred_today
    )

    def validator(self, form, value):
        """
            Validate the period
        """
        if value['start_date'] > value['end_date']:
            exc = colander.Invalid(
                form,
                "La date de début doit précéder la date de fin"
            )
            exc['start_date'] = "Doit précéder la date de fin"
            raise exc
