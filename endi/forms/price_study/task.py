# -*- coding: utf-8 -*-
import colander
import deform

from endi.models.third_party.customer import Customer
from endi.models.project import Project
from endi.forms.third_party.customer import customer_choice_node_factory


@colander.deferred
def deferred_default_name(node, kw):
    """
    Return a default name for the new document
    """
    return kw['request'].context.name


@colander.deferred
def deferred_default_customer(node, kw):
    """
    Return the default customer regarding the current price study's project
    """
    request = kw['request']
    res = 0
    customers = request.context.project.customers
    if len(customers) == 1:
        res = customers[0].id
    return res


def _get_project_customers(project_id):
    """
    Return project customers

    :param int project_id: The current project id
    :returns: A list of Customer instances
    :rtype: list
    """
    query = Customer.label_query()
    customers = query.filter(
        Customer.projects.any(Project.id == project_id)
    ).order_by(Customer.label)
    return customers


def _get_customers_options(request):
    """
    Retrieve customers that should be presented to the end user

    Context is PriceStudy
    """
    price_study = request.context
    return _get_project_customers(price_study.project_id)


def get_business_types_from_request(request):
    """
    Collect available business types allowed for the current user/context

    :param obj request: The current Pyramid request
    """
    context = request.context
    if hasattr(context, "project"):
        project = context.project
    else:
        return []

    result = []
    if project.project_type.default_business_type:
        result.append(project.project_type.default_business_type)

    for business_type in project.business_types:
        if business_type != project.project_type.default_business_type:
            if business_type.allowed(request):
                result.append(business_type)
    return result


@colander.deferred
def business_type_id_validator(node, kw):
    allowed_ids = [
        i.id
        for i in get_business_types_from_request(kw['request'])
    ]
    return colander.OneOf(allowed_ids)


@colander.deferred
def deferred_business_type_description(node, kw):
    request = kw['request']
    business_types = get_business_types_from_request(request)
    if len(business_types) == 1:
        return ""
    else:
        return "Type d'affaire",


@colander.deferred
def deferred_business_type_widget(node, kw):
    """
    Collect the widget to display for business type selection

    :param node: The node we affect the widget to
    :param dict kw: The colander schema binding dict
    :returns: A SelectWidget or an hidden one
    """
    request = kw['request']
    business_types = get_business_types_from_request(request)
    if len(business_types) == 1:
        return deform.widget.HiddenWidget()
    else:
        return deform.widget.SelectWidget(
            values=[
                (business_type.id, business_type.label)
                for business_type in business_types
            ]
        )


@colander.deferred
def deferred_business_type_default(node, kw):
    """
    Collect the default value to present to the end user
    """
    request = kw['request']
    project = request.context
    context = request.context
    if isinstance(context, Project):
        project = context
    elif hasattr(context, "project"):
        project = context.project
    else:
        raise KeyError(
            "No project could be found starting from current : %s" % (
                context,
            )
        )

    if project.project_type.default_business_type:
        return project.project_type.default_business_type.id
    else:
        return get_business_types_from_request(request)[0].id


class GenerateTaskSchema(colander.Schema):
    name = colander.SchemaNode(
        colander.String(),
        title="Nom du document",
        description="Ce nom n'apparaît pas dans le document final",
        validator=colander.Length(max=255),
        default=deferred_default_name,
        missing="",
    )
    customer_id = customer_choice_node_factory(
        default=deferred_default_customer,
        query_func=_get_customers_options,
    )
    business_type_id = colander.SchemaNode(
        colander.Integer(),
        title="Type d'affaire",
        widget=deferred_business_type_widget,
        default=deferred_business_type_default,
    )
