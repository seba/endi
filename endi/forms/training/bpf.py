# -*- coding: utf-8 -*-
import colander
from colanderalchemy import SQLAlchemySchemaNode
import deform

from endi_base.models.base import DBSESSION
from endi.models.services.bpf import BPFService
from endi.models.task.invoice import (
    get_invoice_years,
    Invoice
)

@colander.deferred
def deferred_nsf_training_speciality_id_widget(node, kw):
    from endi.models.training.bpf import NSFTrainingSpecialityOption
    query = NSFTrainingSpecialityOption.query()
    query = query.order_by(NSFTrainingSpecialityOption.label)
    values = [(i.id, i.label) for i in query]
    values.insert(0, ('', "- Sélectionner une spécialité -"))
    return deform.widget.Select2Widget(values=values)

def get_year_from_request(request):
    return int(request.matchdict['year'])

def _get_cerfa_spec(request):
    assert request is not None
    year = get_year_from_request(request)
    return BPFService.get_spec_from_year(year)


def _build_select_values(choices_tree):
    """
    Build values suitable for SelectWidget, from a nested list structure

    Example choices_tree (with 1 optgroup and 2 uncategorized items):
        [
            (0, 'soap', []),
            (None, "Vegetables", [
                (1, "carots"),
                (2, "celery"),
            ]),
           (3, 'toilet paper'),
        ]
    """
    values = []
    for source in choices_tree:
        index, source_label, subsources = source

        if len(subsources) == 0:
            values.append([index, source_label])
        else:
            optgroup_values = []
            for subsource_index, subsource_label in subsources:
                optgroup_values.append([subsource_index, subsource_label])
            optgroup = deform.widget.OptGroup(
                source_label,
                *optgroup_values
            )
            values.append(optgroup)
    return values


@colander.deferred
def deferred_income_source_select(node, kw):
    spec = _get_cerfa_spec(kw.get('request'))
    values = _build_select_values(spec.INCOME_SOURCES)
    values.insert(0, ('', "- Sélectionner une source de revenu -"))
    return deform.widget.SelectWidget(values=values)


@colander.deferred
def deferred_income_source_validator(node, kw):
    spec = _get_cerfa_spec(kw.get('request'))
    return colander.OneOf(spec.INCOME_SOURCES_IDS)


@colander.deferred
def deferred_training_goal_select(node, kw):
    spec = _get_cerfa_spec(kw.get('request'))
    values = _build_select_values(spec.TRAINING_GOALS)
    values.insert(0, ('', "- Sélectionner un objectif de formation -"))
    return deform.widget.SelectWidget(values=values)


@colander.deferred
def deferred_training_goal_validator(node, kw):
    spec = _get_cerfa_spec(kw.get('request'))
    return colander.OneOf(spec.TRAINING_GOALS_IDS)


@colander.deferred
def deferred_trainee_type_select(node, kw):
    spec = _get_cerfa_spec(kw.get('request'))
    values = _build_select_values(spec.TRAINEE_TYPES)
    values.insert(0, ('', "- Sélectionner un type de stagiaire -"))
    return deform.widget.SelectWidget(values=values)


@colander.deferred
def deferred_trainee_type_validator(node, kw):
    spec = _get_cerfa_spec(kw.get('request'))
    return colander.OneOf(spec.TRAINEE_TYPES_IDS)


@colander.deferred
def deferred_financial_year_validator(node, kw):
    """
    Validate the BPF year

    Validates:
    - existing financial year
    - the year is not already "filled" with in another object
    """
    request = kw.get('request')
    assert request is not None

    business = request.context
    current_bpf_year = int(request.matchdict['year'])

    years_w_bpf_data = [
        bpf.financial_year
        for bpf in business.bpf_datas
    ]
    invoicing_years = get_invoice_years()

    def validator(node, value):
        if value not in invoicing_years:
            raise colander.Invalid(
                node,
                'Pas une année fiscale valide',
                value,
            )
        if value in years_w_bpf_data and value != current_bpf_year:
            raise colander.Invalid(
                node,
                'Il y a déjà des données BPF pour cette année',
                value,
            )
    return validator


@colander.deferred
def deferred_invoice_widget(node, kw):
    """
    Return a select for invoice selection
    """
    assert kw['request'] is not None
    query = DBSESSION().query(Invoice.id, Invoice.name)
    query = query.filter_by(business_id=kw['request'].context.id)
    choices = []
    for invoice in query:
        choices.append((invoice.id, invoice.name))
    return deform.widget.SelectWidget(values=choices)


def get_business_bpf_edit_schema():
    from endi.models.training.bpf import BusinessBPFData
    schema = SQLAlchemySchemaNode(
        BusinessBPFData,
        excludes=(
            'id',
            'business_id',
            'business',
            'training_speciality',
        ),
    )
    return schema
