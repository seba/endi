# -*- coding: utf-8 -*-
"""
Schema used for trainings manipulation
"""
import colander
import deform

from endi.forms.lists import BaseListsSchema
from endi.forms import year_filter_node

from endi.forms.company import company_filter_node_factory
from endi.forms.third_party.customer import customer_filter_node_factory

from endi.models.task.invoice import get_invoice_years


def get_training_list_schema(is_global):
    """
    Build the Training list filter schema
    """
    schema = BaseListsSchema().clone()
    schema['search'].title = "Numéro de facture"
    schema.add_before(
        "items_per_page",
        year_filter_node(
            query_func=get_invoice_years,
            title='Année de facturation',
            name='invoicing_year',
        ),
    )
    schema.add_before(
        "items_per_page",
        colander.SchemaNode(
            colander.String(),
            title='BPF renseigné',
            name="bpf_filled",
            widget=deform.widget.SelectWidget(
                values=(
                    ('', 'Peu importe'),
                    ('yes', 'Renseigné'),
                    ('no', 'Non renseigné ou partiellement renseigné'),
                )
            ),
            missing=colander.drop,
        ),
    )

    if is_global:
        schema.add_before(
            "items_per_page",
            company_filter_node_factory(name='company_id')
        )
    schema.add_before(
        "items_per_page",
        customer_filter_node_factory(name='customer_id', is_global=is_global)
    )
    schema.add_before(
        "items_per_page",
        colander.SchemaNode(
            colander.Boolean(),
            name="include_closed",
            title="",
            label="Inclure les affaires clôturées",
            missing=False
        )
    )
    return schema
