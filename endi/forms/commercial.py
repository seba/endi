# -*- coding: utf-8 -*-
"""
    Form schemas for commercial handling
"""
import datetime
import colander
from deform import widget

from endi.models.task import invoice
from endi import forms
from .custom_types import AmountType


def get_years(kw):
    years = invoice.get_invoice_years()
    next_year = datetime.date.today().year + 1
    if next_year not in years:
        years.append(next_year)
    return years


class CommercialFormSchema(colander.MappingSchema):
    year = forms.year_select_node(query_func=get_years)


class CommercialSetFormSchema(colander.MappingSchema):
    month = colander.SchemaNode(
        colander.Integer(),
        widget=widget.HiddenWidget(),
        title='',
        validator=colander.Range(1, 12),
    )
    value = colander.SchemaNode(
        AmountType(5),
        title="CA prévisionnel"
    )
    comment = forms.textarea_node(
        title="Commentaire",
        missing=""
    )
