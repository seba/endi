# -*- coding: utf-8 -*-
"""
    form schemas for invoices related views
"""
import functools
import colander
import deform
import deform_extensions

from endi_base.models.base import DBSESSION
from endi.models.task import (
    invoice,
    Estimation,
    Task,
)
from endi.models.tva import Product
from endi.models.task.invoice import (
    Invoice,
    CancelInvoice,
    get_invoice_years,
    INVOICE_STATES,
)
from endi import forms
from endi.forms.company import company_filter_node_factory
from endi.forms.third_party.customer import customer_filter_node_factory
from endi.forms.custom_types import (
    AmountType,
)
from endi.forms.widgets import FixedLenSequenceWidget
from endi.forms.widgets import CleanMappingWidget
from endi.forms.tasks.lists import (
    PeriodSchema,
    AmountRangeSchema,
)
from endi.forms.tasks.task import get_add_edit_task_schema


PAID_STATUS_OPTIONS = (
    ("all", "Tous", ),
    ("paid", "Les factures payées", ),
    ("notpaid", "Seulement les impayés", )
)

STATUS_OPTIONS = (
    ('all', "Tous", ),
    ('draft', "Brouillon"),
    ('wait', "En attente de validation"),
    ('invalid', "Invalide"),
    ('valid', "Valide"),
)


TYPE_OPTIONS = (
    ("both", "Tous", ),
    ('invoice', "Seulement les factures", ),
    ('cancelinvoice', "Seulement les avoirs",),
)


def get_product_choices():
    """
        Return data structure for product code select widget options
    """
    return [(p.id, "{0} ({1} - {2})".format(
        p.name, p.compte_cg, p.tva.name),)
            for p in Product.query()]


@colander.deferred
def deferred_product_validator(node, kw):
    options = [option[0] for option in get_product_choices()]
    return colander.OneOf(options)


@colander.deferred
def deferred_product_widget(node, kw):
    """
        return a widget for product selection
    """
    products = get_product_choices()
    wid = deform.widget.SelectWidget(values=products)
    return wid


def product_match_tva_validator(form, line_value):
    product_id = line_value.get('product_id')
    product = Product.get(product_id)
    if product.tva.value != line_value['tva']:
        exc = colander.Invalid(
            form,
            "Le code produit doit correspondre à la TVA associée",
        )
        raise exc


@colander.deferred
def deferred_financial_year_widget(node, kw):
    request = kw['request']
    if request.has_permission('manage', request.context):
        return deform.widget.TextInputWidget(mask='9999')
    else:
        return deform.widget.HiddenWidget()


FINANCIAL_YEAR = colander.SchemaNode(
    colander.Integer(),
    name="financial_year",
    title="Année comptable de référence",
    widget=deferred_financial_year_widget,
    default=forms.deferred_default_year,
)


class FinancialYearSchema(colander.MappingSchema):
    """
        colander Schema for financial year setting
    """
    financial_year = FINANCIAL_YEAR


class ProductTaskLine(colander.MappingSchema):
    """
        A single estimation line
    """
    id = colander.SchemaNode(
        colander.Integer(),
        widget=deform.widget.HiddenWidget(),
        missing="",
        css_class="span0"
    )
    description = colander.SchemaNode(
        colander.String(),
        widget=deform.widget.TextInputWidget(readonly=True),
        missing='',
        css_class='col-md-3',
    )
    tva = colander.SchemaNode(
        AmountType(),
        widget=deform_extensions.DisabledInput(),
        css_class='col-md-1',
        title='TVA',
    )
    product_id = colander.SchemaNode(
        colander.Integer(),
        widget=deferred_product_widget,
        validator=deferred_product_validator,
        missing="",
        css_class="col-md-2",
        title="Code produit",
    )


class ProductTaskLines(colander.SequenceSchema):
    taskline = ProductTaskLine(
        missing="",
        title="",
        validator=product_match_tva_validator,
        widget=CleanMappingWidget(),
    )


class SetProductsSchema(colander.MappingSchema):
    """
        Form schema used to configure Products
    """
    lines = ProductTaskLines(
        widget=FixedLenSequenceWidget(),
        missing="",
        title=''
    )


# INVOICE LIST RELATED SCHEMAS
def get_list_schema(is_global=False, excludes=()):
    """
    Return a schema for invoice listing

    is_global

        If False, customer select is only related to the current context
    """
    schema = forms.lists.BaseListsSchema().clone()

    if 'paid_status' not in excludes:
        schema.insert(0, forms.status_filter_node(
            PAID_STATUS_OPTIONS,
            name='paid_status',
            title='Statut de paiement',
        ))

    if 'status' not in excludes:
        schema.insert(0, forms.status_filter_node(STATUS_OPTIONS))

    schema.insert(
        0,
        colander.SchemaNode(
            colander.String(),
            name='doctype',
            title='Types de factures',
            widget=deform.widget.SelectWidget(values=TYPE_OPTIONS),
            validator=colander.OneOf([s[0] for s in TYPE_OPTIONS]),
            missing='both',
            default='both',
        )
    )

    if 'customer' not in excludes:
        schema.insert(0, customer_filter_node_factory(
            is_global=is_global,
            name='customer_id',
            title='Client',
            with_invoice=True,
        ))

    if 'company_id' not in excludes:
        schema.insert(
            0,
            company_filter_node_factory(name='company_id', title='Enseigne')
        )

    schema.insert(
        0,
        PeriodSchema(
            name='period',
            title="",
            validator=colander.Function(
                forms.range_validator,
                msg="La date de début doit précéder la date de début"
            ),
            widget=CleanMappingWidget(),
            missing=colander.drop,
        )
    )
    schema.insert(
        0,
        AmountRangeSchema(
            name='ttc',
            title="",
            validator=colander.Function(
                forms.range_validator,
                msg="Le montant de départ doit être inférieur ou égale \
à celui de la fin"
            ),
            widget=CleanMappingWidget(),
            missing=colander.drop,
        )
    )

    if 'year' not in excludes:
        def get_year_options(kw):
            values = invoice.get_invoice_years(kw)
            return values

        node = forms.year_filter_node(
            name='year',
            query_func=get_year_options,
            title="Année fiscale",
            default=forms.deferred_default_year,
        )

        schema.insert(0, node)

    schema['search'].title = "Numéro de facture"

    return schema


def _existing_invoice_official_number_validator(value):
    query = Task.query().filter_by(official_number=value)
    return DBSESSION.query(query.exists()).scalar()


existing_invoice_official_number_validator = colander.Function(
    _existing_invoice_official_number_validator,
    msg="Aucune facture n'existe avec ce n° de facture"
)


class InvoicesRangeSchema(colander.MappingSchema):
    """
        Form schema for an invoice number selection (year + number)
    """
    financial_year = forms.year_filter_node(
        title="Année comptable",
        query_func=get_invoice_years,
        widget_options={'default_val': ('', 'Sélectionner une année')},
        missing=colander.required,
    )
    start = colander.SchemaNode(
        colander.String(),
        title='Depuis la facture numéro',
        description="Numéro de facture à partir duquel exporter",
        validator=existing_invoice_official_number_validator,
    )
    end = colander.SchemaNode(
        colander.String(),
        title="Jusqu'à la facture numéro",
        description="Numéro de facture jusqu'auquel exporter "
        "(dernier document si vide)",
        missing=colander.null,
        validator=existing_invoice_official_number_validator,
    )

    def validator(self, form, value):
        """
        Validate the number range
        """
        year = value['financial_year']
        start_num = value['start']
        end_num = value['end']

        if end_num != colander.null:
            start_time = Task.find_task_status_date(start_num, year)
            end_time = Task.find_task_status_date(end_num, year)

            if start_time > end_time:
                exc = colander.Invalid(
                    form,
                    "Le numéro de début doit être plus petit ou égal à celui "
                    "de fin"
                )
                exc['start'] = "Doit être inférieur au numéro de fin"
                raise exc


pdfexportSchema = InvoicesRangeSchema(
    title="Exporter un ensemble de factures dans un fichier pdf",
)


@colander.deferred
def deferred_estimation_widget(node, kw):
    """
    Return a select for estimation selection
    """
    query = Estimation.query()
    query = query.filter_by(project_id=kw['request'].context.project_id)
    choices = [(e.id, e.name) for e in query]
    choices.insert(0, ('', 'Aucun devis'))
    return deform.widget.SelectWidget(values=choices)


class EstimationAttachSchema(colander.Schema):
    estimation_id = colander.SchemaNode(
        colander.Integer(),
        widget=deferred_estimation_widget,
        missing=colander.drop,
        title="Devis à rattacher à cette facture",
    )


def _customize_invoice_schema(schema):
    """
    Add form schema customization to the given Invoice edition schema

    :param obj schema: The schema to edit
    """
    customize = functools.partial(forms.customize_field, schema)
    customize(
        "paid_status",
        widget=deform.widget.SelectWidget(values=INVOICE_STATES),
        validator=colander.OneOf(list(dict(INVOICE_STATES).keys()))
    )
    customize(
        'financial_year',
        widget=deform.widget.TextInputWidget(mask='9999')
    )
    customize('estimation_id', missing=colander.drop)
    return schema


def _customize_cancelinvoice_schema(schema):
    """
    Add form schema customization to the given Invoice edition schema

    :param obj schema: The schema to edit
    """
    customize = functools.partial(forms.customize_field, schema)
    customize('invoice_id', missing=colander.required)
    customize(
        'financial_year',
        widget=deform.widget.TextInputWidget(mask='9999')
    )
    return schema


def get_add_edit_invoice_schema(isadmin=False, includes=None, **kw):
    """
    Return add edit schema for Invoice edition

    :param bool isadmin: Are we asking for an admin schema ?
    :param tuple includes: Field that should be included in the schema
    :rtype: `colanderalchemy.SQLAlchemySchemaNode`
    """
    schema = get_add_edit_task_schema(
        Invoice, isadmin=isadmin, includes=includes, **kw
    )
    schema = _customize_invoice_schema(schema)
    return schema


def get_add_edit_cancelinvoice_schema(isadmin=False, includes=None, **kw):
    """
    Return add edit schema for CancelInvoice edition

    :param bool isadmin: Are we asking for an admin schema ?
    :param tuple includes: Field that should be included in the schema
    :rtype: `colanderalchemy.SQLAlchemySchemaNode`
    """
    schema = get_add_edit_task_schema(
        CancelInvoice, isadmin=isadmin, includes=includes, **kw
    )
    schema = _customize_cancelinvoice_schema(schema)
    return schema


def validate_invoice(invoice_object, request):
    """
    Globally validate an invoice_object

    :param obj invoice_object: An instance of Invoice
    :param obj request: The pyramid request
    :raises: colander.Invalid

    try:
        validate_invoice(est, self.request)
    except colander.Invalid as err:
        error_messages = err.messages
    """
    schema = get_add_edit_invoice_schema()
    schema = schema.bind(request=request)
    appstruct = invoice_object.__json__(request)
    cstruct = schema.deserialize(appstruct)
    return cstruct


def validate_cancelinvoice(cancelinvoice_object, request):
    """
    Globally validate an cancelinvoice_object

    :param obj cancelinvoice_object: An instance of CancelInvoice
    :param obj request: The pyramid request
    :raises: colander.Invalid

    try:
        validate_cancelinvoice(est, self.request)
    except colander.Invalid as err:
        error_messages = err.messages
    """
    schema = get_add_edit_cancelinvoice_schema()
    schema = schema.bind(request=request)
    appstruct = cancelinvoice_object.__json__(request)
    cstruct = schema.deserialize(appstruct)
    return cstruct
