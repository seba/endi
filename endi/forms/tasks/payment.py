# -*- coding: utf-8 -*-
"""
    form schemas for invoices related views
"""
import functools
import colander
import deform

from colanderalchemy import SQLAlchemySchemaNode

from endi.models.tva import Tva
from endi.models.payments import (
    Bank,
    BankAccount,
    PaymentMode,
)
from endi.models.task.invoice import Invoice
from endi.models.task.payment import Payment
from endi.utils.strings import format_amount
from endi import forms
from endi.forms.custom_types import AmountType
from endi.forms.payments import (
    get_amount_topay,
    deferred_amount_default,
    deferred_payment_mode_widget,
    deferred_payment_mode_validator,
    deferred_bank_account_widget,
    deferred_bank_account_validator,
    deferred_customer_bank_widget,
)


AMOUNT_PRECISION = 5
# 5€ => 500000 in db format
PAYMENT_EPSILON = 5 * 10 ** AMOUNT_PRECISION
PAYMENT_SUM_EPSILON = 0.1 * 10 ** AMOUNT_PRECISION

PAYMENT_GRID = (
    (("date", 6), ),
    (("mode", 6), ("amount", 6),),
    (("bank_remittance_id", 6), ("bank_id", 6),),
    (("check_number", 6),),
    (("customer_bank_id", 6), ("issuer", 6),),
    (("tva_id", 6),),
    (("resulted", 12),),
)
MULTIPLE_PAYMENT_GRID = (
    (("date", 6),),
    (("mode", 6), ("payment_amount", 6),),
    (("bank_remittance_id", 6), ("bank_id", 6),),
    (("check_number", 6),),
    (("customer_bank_id", 6), ("issuer", 6),),
    (("tvas", 12),),
    (("resulted", 12),),
)


def get_invoice_from_context(request):
    if isinstance(request.context, Invoice):
        return request.context
    else:
        return request.context.invoice


@colander.deferred
def deferred_bank_remittance_id_default(node, kw):
    """
    Default value for the bank remittance id
    """
    from endi.models.services.user import UserPrefsService
    id = UserPrefsService.get(kw['request'], 'last_bank_remittance_id')
    if id is None:
        return ""
    else:
        return id


@colander.deferred
def deferred_issuer_default(node, kw):
    """
    Default value for payment's issuer
    """
    invoice = get_invoice_from_context(kw['request'])
    return invoice.customer.label


@colander.deferred
def deferred_amount_by_tva_validation(node, kw):
    invoice = get_invoice_from_context(kw['request'])
    tva_parts = invoice.tva_ttc_parts()

    def validate_amount_by_tva(values):
        tva_id = values.get('tva_id')
        tva = Tva.get(tva_id)
        if tva is None:
            return u"Tva inconnue"
        amount = values.get('amount')
        # Fix #433 : encaissement et tva multiples
        # Add a tolerance for 5 € of difference
        if amount > tva_parts[tva.value] + PAYMENT_EPSILON:
            return u"Le montant de l'encaissement doit être inférieur à la \
part de cette Tva dans la facture"
        return True

    return colander.Function(validate_amount_by_tva)


@colander.deferred
def deferred_payment_amount_validation(node, kw):
    """
    Validate that the remittance amount is equal to the sum of the tva parts
    """
    def validate_sum_of_tvapayments(values):
        """
        Validate the sum of the tva payments is equal to the payment amount
        """
        tva_sum = sum([tvap['amount'] for tvap in values['tvas']])
        payment_amount = values['payment_amount']
        diff = abs(tva_sum - payment_amount)
        if diff >= PAYMENT_SUM_EPSILON:
            return u"Le montant du paiement doit correspondre à la somme \
des encaissements correspondants"
        return True

    return colander.Function(validate_sum_of_tvapayments)


@colander.deferred
def deferred_total_validator(node, kw):
    """
    Validate the amount to keep the sum under the total
    """
    topay = get_amount_topay(kw)
    max_msg = u"Le montant ne doit pas dépasser %s (total ttc - somme \
des paiements + montant d'un éventuel avoir)" % (
        format_amount(topay, precision=AMOUNT_PRECISION, grouping=False)
    )
    min_msg = u"Le montant doit être positif"

    # We insert a large epsilon to allow larger payments to be registered
    max_value = topay + PAYMENT_EPSILON
    return colander.Range(
        min=0, max=max_value, min_err=min_msg, max_err=max_msg,
    )


@colander.deferred
def deferred_tva_id_validator(node, kw):
    values = []
    invoice = get_invoice_from_context(kw['request'])
    for tva_value in invoice.topay_by_tvas().keys():
        values.append(Tva.by_value(tva_value))

    def validator(node, value):
        if value not in [v.id for v in values]:
            raise colander.Invalid(
                node,
                u"Ce taux de tva n'est pas utilisé dans la facture",
            )

    return validator


class PaymentSchema(colander.MappingSchema):
    """
        colander schema for payment recording
    """
    come_from = forms.come_from_node()
    date = forms.today_node()
    amount = colander.SchemaNode(
        AmountType(5),
        title=u"Montant de l'encaissement",
        validator=deferred_total_validator,
        default=deferred_amount_default,
    )
    mode = colander.SchemaNode(
        colander.String(),
        title=u"Mode de paiement",
        widget=deferred_payment_mode_widget,
        validator=deferred_payment_mode_validator,
    )
    issuer = colander.SchemaNode(
        colander.String(),
        title=u"Émetteur du paiement",
        default=deferred_issuer_default,
    )
    customer_bank_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Banque de l'émetteur du paiement",
        widget=deferred_customer_bank_widget,
        missing=colander.drop
    )
    check_number = colander.SchemaNode(
        colander.String(),
        title=u"Numéro de chèque",
        missing=colander.drop
    )
    tva_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Tva liée à cet encaissement",
        widget=forms.get_deferred_select(
            Tva, mandatory=True, keys=('id', 'name')
        ),
        validator=deferred_tva_id_validator
    )
    bank_remittance_id = colander.SchemaNode(
        colander.String(),
        title=u"Numéro de remise en banque",
        description=u"Permet d'associer cet encaissement à une "
        u"remise en banque (laisser vide si pas de remise)",
        default=deferred_bank_remittance_id_default,
        missing=colander.drop
    )
    bank_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Compte bancaire",
        widget=deferred_bank_account_widget,
        validator=deferred_bank_account_validator,
        default=forms.get_deferred_default(BankAccount),
        description=u"Configurables dans Configuration - Module Ventes - "
        u"Configuration comptable des encaissements"
    )
    resulted = colander.SchemaNode(
        colander.Boolean(),
        title=None,
        label=u"Soldée",
        description=u"Indique que la facture est soldée (ne recevra plus "
        u"de paiement), si le montant indiqué correspond au montant "
        u"de la facture celle-ci est soldée automatiquement",
        default=False,
        missing=False,
    )


class TvaPayment(colander.MappingSchema):
    amount = colander.SchemaNode(
        AmountType(5),
        title=u"Montant de l'encaissement",
    )
    tva_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Tva liée à cet encaissement",
        widget=forms.get_deferred_select(
            Tva, mandatory=True, keys=('id', 'name')
        ),
        validator=deferred_tva_id_validator
    )


class TvaPaymentSequence(colander.SequenceSchema):
    tvas = TvaPayment(title=u'', validator=deferred_amount_by_tva_validation)


class MultiplePaymentSchema(colander.MappingSchema):
    """
        colander schema for payment recording
    """
    come_from = forms.come_from_node()
    date = forms.today_node()
    payment_amount = colander.SchemaNode(
        AmountType(5),
        title=u"Montant du paiement",
        description=u"Permet de contrôler que la somme des encaissements "
        u" par TVA corresponde bien au montant du paiement",
        validator=deferred_total_validator,
        default=deferred_amount_default,
    )
    mode = colander.SchemaNode(
        colander.String(),
        title=u"Mode de paiement",
        widget=deferred_payment_mode_widget,
        validator=deferred_payment_mode_validator,
    )
    issuer = colander.SchemaNode(
        colander.String(),
        title=u"Émetteur du paiement",
        default=deferred_issuer_default,
    )
    customer_bank_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Banque de l'émetteur du paiement",
        widget=deferred_customer_bank_widget,
        missing=colander.drop
    )
    check_number = colander.SchemaNode(
        colander.String(),
        title=u"Numéro de chèque",
        missing=colander.drop
    )
    tvas = TvaPaymentSequence(title=u'Encaissements par taux de TVA')
    bank_remittance_id = colander.SchemaNode(
        colander.String(),
        title=u"Numéro de remise en banque",
        description=u"Permet d'associer cet encaissement à une "
        u"remise en banque (laisser vide si pas de remise)",
        default=deferred_bank_remittance_id_default,
        missing=colander.drop
    )
    bank_id = colander.SchemaNode(
        colander.Integer(),
        title=u"Compte bancaire",
        widget=deferred_bank_account_widget,
        validator=deferred_bank_account_validator,
        default=forms.get_deferred_default(BankAccount),
        description=u"Configurables dans Configuration - Module Ventes - "
        u"Configuration comptable des encaissements"
    )
    resulted = colander.SchemaNode(
        colander.Boolean(),
        title=u"",
        label=u"Soldé",
        description=u"Indique que le document est soldé (ne recevra plus "
        u"de paiement), si le montant indiqué correspond au montant de la "
        u"facture celle-ci est soldée automatiquement",
        default=False,
        missing=False,
    )


def get_payment_schema(request, with_new_remittance_confirm=None):
    """
    Returns the schema for payment registration
    """
    invoice = get_invoice_from_context(request)
    tva_module = request.config.get('receipts_active_tva_module')
    num_tvas = len(invoice.get_tvas().keys())
    if num_tvas == 1 or isinstance(request.context, Payment):
        schema = PaymentSchema().clone()
        if not tva_module or tva_module == '0':
            schema['tva_id'].widget = deform.widget.HiddenWidget()
    else:
        schema = MultiplePaymentSchema(
            validator=deferred_payment_amount_validation
        ).clone()
        schema['tvas'].widget = deform.widget.SequenceWidget(
            min_len=1,
            max_len=num_tvas,
            orderable=False,
        )
    if with_new_remittance_confirm:
        schema.add_before(
            'bank_id',
            colander.SchemaNode(
                colander.Boolean(),
                name='new_remittance_confirm',
                title=u"",
                label=u"Confirmer la création de cette remise en banque",
                default=False,
                missing=False,
                widget=deform.widget.HiddenWidget()
            )
        )
    return schema


def _customize_payment_schema(schema):
    """
    Add form schema customization to the given payment edition schema

    :param obj schema: The schema to edit
    """
    customize = functools.partial(forms.customize_field, schema)
    customize(
        "mode",
        validator=forms.get_deferred_select_validator(
            PaymentMode, id_key='label'
        ),
        missing=colander.required
    )
    customize("amount", typ=AmountType(5), missing=colander.required)
    customize("bank_remittance_id", missing=colander.drop)
    customize("date", missing=colander.required)
    customize("task_id", missing=colander.required)
    customize(
        "bank_id",
        validator=forms.get_deferred_select_validator(BankAccount),
        missing=colander.required,
    )
    customize(
        "tva_id",
        validator=forms.get_deferred_select_validator(Tva),
        missing=colander.drop,
    )
    customize("user_id", missing=colander.required)
    customize(
        "customer_bank_id",
        validator=forms.get_deferred_select_validator(Bank),
        missing=colander.drop
    )
    customize("check_number", missing=colander.drop)
    return schema


def get_add_edit_payment_schema(includes=None):
    """
    Return add edit schema for Payment edition

    :param tuple includes: Field that should be included in the schema
    :rtype: `colanderalchemy.SQLAlchemySchemaNode`
    """
    schema = SQLAlchemySchemaNode(Payment, includes=includes)
    schema = _customize_payment_schema(schema)
    return schema
