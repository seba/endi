# -*- coding: utf-8 -*-
"""
    Main file for our pyramid application
"""
import locale
locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")
locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")

from sqlalchemy import engine_from_config
from pyramid.config import Configurator
from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.path import DottedNameResolver
from pyramid_beaker import set_cache_regions_from_settings
from endi.utils.session import get_session_factory
from endi.utils.filedepot import (
    configure_filedepot,
)
from endi.resources import lib_endi as fanstatic_endi_library


ENDI_MODULES = (
    "endi.views.accompagnement.activity",
    "endi.views.accounting",
    "endi.views.auth",
    "endi.views.business",
    "endi.views.commercial",
    "endi.views.company",
    "endi.views.competence",
    "endi.views.csv_import",
    "endi.views.customer",
    "endi.views.estimations",
    "endi.views.expenses",
    "endi.views.files",
    "endi.views.holiday",
    "endi.views.index",
    "endi.views.indicators",
    "endi.views.invoices",
    "endi.views.job",
    "endi.views.manage",
    "endi.views.payment",
    "endi.views.sale_product",
    "endi.views.project",
    "endi.views.price_study",
    "endi.views.export.routes",
    "endi.views.export.bpf",
    "endi.views.export.invoice",
    "endi.views.export.expense",
    "endi.views.export.payment",
    "endi.views.export.expense_payment",
    "endi.views.export.supplier_invoice",
    "endi.views.export.supplier_payment",
    "endi.views.progress_invoicing",
    "endi.views.static",
    "endi.views.statistics",
    "endi.views.supply",
    "endi.views.supplier",
    "endi.views.tests",
    'endi.views.training',
    "endi.views.treasury_files",
    "endi.views.user",
    "endi.views.userdatas",
    "endi.views.workshops.workshop"
)

ENDI_LAYOUTS_MODULES = (
    "endi.default_layouts",
    "endi.views.user.layout",
)

ENDI_PANELS_MODULES = (
    "endi.panels.activity",
    "endi.panels.company_index",
    'endi.panels.files',
    "endi.panels.form",
    'endi.panels.indicators',
    "endi.panels.menu",
    "endi.panels.navigation",
    "endi.panels.project",
    'endi.panels.sidebar',
    "endi.panels.supply",
    'endi.panels.tabs',
    "endi.panels.task",
    "endi.panels.widgets",
    "endi.panels.workshop",
)

ENDI_EVENT_MODULES = (
    "endi.events.model_events",
    "endi.events.status_changed",
    "endi.events.files",
    "endi.events.indicators",
    "endi.events.business",
)
ENDI_REQUEST_SUBSCRIBERS = (
    "endi.subscribers.new_request",
    "endi.subscribers.before_render",
)

ENDI_SERVICE_FACTORIES = (
    (
        "services.treasury_invoice_producer",
        "endi.compute.sage.InvoiceExport",
        "endi.interfaces.ITreasuryInvoiceProducer",
    ),
    (
        "services.treasury_invoice_writer",
        "endi.export.sage.SageInvoiceCsvWriter",
        "endi.interfaces.ITreasuryInvoiceWriter",
    ),
    (
        "services.treasury_expense_producer",
        "endi.compute.sage.ExpenseExport",
        "endi.interfaces.ITreasuryExpenseProducer",
    ),
    (
        "services.treasury_expense_writer",
        "endi.export.sage.SageExpenseCsvWriter",
        "endi.interfaces.ITreasuryExpenseWriter",
    ),
    (
        "services.treasury_payment_producer",
        "endi.compute.sage.PaymentExport",
        "endi.interfaces.ITreasuryPaymentProducer",
    ),
    (
        "services.treasury_payment_writer",
        "endi.export.sage.SagePaymentCsvWriter",
        "endi.interfaces.ITreasuryPaymentWriter",
    ),
    (
        "services.task_pdf_rendering_service",
        "endi.views.task.pdf_rendering_service.TaskPdfFromHtmlService",
        "endi.interfaces.ITaskPdfRenderingService",
    ),
    (
        "services.task_pdf_storage_service",
        "endi.views.task.pdf_storage_service.PdfFileDepotStorageService",
        "endi.interfaces.ITaskPdfStorageService",
    ),
    (
        "services.payment_record_service",
        "endi_payment.public.PaymentService",
        "endi.interfaces.IPaymentRecordService",
    ),
    (
        "services.waiting_documents_service",
        "endi.models.status.ValidationStatusHolderService",
        "endi.interfaces.IValidationStatusHolderService",
    ),
)
ENDI_SERVICES = (
)


def resolve(dotted_path):
    """
    Return the module or the python variable matching the dotted_path
    """
    return DottedNameResolver().resolve(dotted_path)


def get_groups(login, request):
    """
        return the current user's groups
    """
    import logging
    logger = logging.getLogger(__name__)
    user = request.user
    if user is None:
        logger.debug("User is None")
        principals = None

    elif getattr(request, 'principals', []):
        principals = request.principals

    else:
        logger.debug(" + Building principals")
        principals = []
        for group in user.login.groups:
            principals.append('group:{0}'.format(group))

        for company in user.companies:
            if company.active:
                principals.append('company:{}'.format(company.id))

        request.principals = principals
        logger.debug(" -> Principals Built : caching")

    return principals


def prepare_config(**settings):
    """
    Prepare the configuration object to setup the main application elements
    """
    session_factory = get_session_factory(settings)
    set_cache_regions_from_settings(settings)
    auth_policy = SessionAuthenticationPolicy(callback=get_groups)
    acl_policy = ACLAuthorizationPolicy()

    config = Configurator(
        settings=settings,
        authentication_policy=auth_policy,
        authorization_policy=acl_policy,
        session_factory=session_factory,
    )
    return config


def hack_endi_static_path(settings):
    if "endi.fanstatic_path" in settings:
        path_name = settings.get("endi.fanstatic_path")
        print(("Hacking fanstatic's source path with %s" % path_name))
        fanstatic_endi_library.path = path_name


def setup_bdd(settings):
    """
    Configure the database:

        - Intialize tables
        - populate database with default values

    :param obj settings: The ConfigParser object
    :returns: The dbsession
    :rtype: obj
    """
    from endi_base.models.initialize import initialize_sql
    from endi.models import adjust_for_engine
    engine = engine_from_config(settings, 'sqlalchemy.')
    adjust_for_engine(engine)
    dbsession = initialize_sql(engine)
    return dbsession


def setup_services(config, settings):
    """
    Setup the services (pyramid_services) used in enDI
    """
    for service_name, default, interface_path in ENDI_SERVICES:
        module_path = settings.get("endi." + service_name, default)
        interface = resolve(interface_path)
        module = resolve(module_path)
        config.register_service(module(), interface)

    for service_name, default, interface_path in ENDI_SERVICE_FACTORIES:
        module_path = settings.get("endi." + service_name, default)
        interface = resolve(interface_path)
        module = resolve(module_path)
        config.register_service_factory(module, interface)


def add_static_views(config, settings):
    """
        Add the static views used in enDI
    """
    statics = settings.get('endi.statics', 'static')
    config.add_static_view(
        statics,
        "endi:static",
        cache_max_age=3600,
    )

    # Static path for generated files (exports / pdfs ...)
    tmp_static = settings.get('endi.static_tmp', 'endi:tmp')
    config.add_static_view('cooked', tmp_static)

    # Allow to specify a custom fanstatic root path
    hack_endi_static_path(settings)


def include_custom_modules(config, settings):
    """
    Include custom modules using the endi.includes mechanism
    """
    for module in settings.get('endi.includes', '').split():
        if module.strip():
            config.include(module)


def base_configure(config, dbsession, from_tests=False, **settings):
    """
    All plugin and others configuration stuff
    """
    from endi.utils.security import (
        RootFactory,
        TraversalDbAccess,
        set_models_acl,
    )
    from endi.models.config import get_config
    from endi.utils.avatar import (
        get_avatar,
    )
    set_models_acl()
    TraversalDbAccess.dbsession = dbsession

    # Application main configuration
    config.set_root_factory(RootFactory)
    config.set_default_permission('view')

    # Adding some usefull properties to the request object
    config.add_request_method(
        lambda _: dbsession(), 'dbsession', property=True, reify=True
    )
    config.add_request_method(
        get_avatar, 'user', property=True, reify=True
    )
    config.add_request_method(
        lambda _: get_config(), 'config', property=True, reify=True
    )

    from endi.utils.predicates import (
        SettingHasValuePredicate,
        ApiKeyAuthenticationPredicate,
    )
    # Allows to restrict view acces only if a setting is set
    config.add_view_predicate(
        'if_setting_has_value', SettingHasValuePredicate
    )
    # Allows to authentify a view through hmac api key auth
    config.add_view_predicate(
        'api_key_authentication', ApiKeyAuthenticationPredicate
    )

    from endi.utils.rest import add_rest_service
    config.add_directive('add_rest_service', add_rest_service)

    from endi.utils.menu import add_menu_item_directive
    add_menu_item_directive(config)

    add_static_views(config, settings)

    for module in ENDI_LAYOUTS_MODULES:
        config.include(module)

    for module in ENDI_REQUEST_SUBSCRIBERS:
        config.include(module)


    if from_tests:
        # add_tree_view_directive attache des classes les unes aux autres et
        # provoquent des problèmes ingérables dans les tests
        # Il devrait en fait utiliser le registry pour attacher parents et
        # enfants
        def add_tree_view_directive(config, *args, **kwargs):
            if 'parent' in kwargs:
                kwargs.pop('parent')
            if 'route_name' not in kwargs:
                # Use the route_name set on the view by default
                kwargs['route_name'] = args[0].route_name
            config.add_view(*args, **kwargs)
    else:
        from endi.views import add_tree_view_directive
    config.add_directive('add_tree_view', add_tree_view_directive)

    for module in ENDI_MODULES:
        config.include(module)

    for module in ENDI_PANELS_MODULES:
        config.include(module)

    for module in ENDI_EVENT_MODULES:
        config.include(module)

    # On register le module views.admin car il contient des outils spécifiques
    # pour les vues administrateurs (Ajout autonomatisé d'une arborescence,
    # ajout de la directive config.add_admin_view
    # Il s'occupe également d'intégrer toutes les vues, layouts... spécifiques à
    # l'administration
    config.include("endi.views.admin")

    setup_services(config, settings)

    from endi.utils.renderer import (
        customize_renderers,
    )
    customize_renderers(config)

    config.commit()
    config.begin()

    include_custom_modules(config, settings)
    return config


def version():
    """
    Return enDI's version number (as defined in setup.py)
    """
    import pkg_resources
    version = pkg_resources.require(__name__)[0].version
    return version


def main(global_config, **settings):
    """
    Main entry function

    :returns: a Pyramid WSGI application.
    """
    config = prepare_config(**settings)

    import logging
    logger = logging.getLogger(__name__)

    logger.debug("Setting up the bdd")
    dbsession = setup_bdd(settings)

    logger.debug("Loading views…")
    config = base_configure(config, dbsession, **settings)
    config.include('endi.utils.sqlalchemy_fix')

    logger.debug("Configuring file depot")
    configure_filedepot(settings)

    config.configure_celery(global_config['__file__'])

    return config.make_wsgi_app()
