# -*- coding: utf-8 -*-
"""
    Panels for the top main menus

    A common user has his company menu with customers, projects ...
    A manager or an admin has an admin menu and eventually a usermenu if he's
    consulting a company's account

    each user has his own menu with preferences, logout, holidays declaration
"""
import logging
from sqlalchemy import or_
from webhelpers2.html import tags
from webhelpers2.html import HTML
from endi.models.company import Company
from endi.models.services.user import UserPrefsService
from endi.utils.menu import HtmlAppMenuItem
from endi.views.export.routes import BPF_EXPORT_ODS_URL
from endi.views.sale_product.routes import CATALOG_ROUTE

logger = logging.getLogger(__name__)


def get_current_company(request, submenu=False, is_user_company=True):
    """Extract the current company from the request

    - If already retrieved -> request.current_company
    - If company-context request → request.context
    - If non-admin and single-company → that company
    - If non-admin and multi-company and not a company-context request:
       → latest used company

    :param obj request: the pyramid request
    :param bool submenu: Do we ask this for the submenu ?
    """
    # Try to get the current company object from cache
    company = getattr(request, "current_company", None)
    if company is None:
        # Pas un manager et une seule enseigne
        if len(request.user.active_companies) == 1 and not submenu:
            company = request.user.active_companies[0]
        # The current context provide a get_company_id utility that allows to
        # retrieve the concerned company
        elif hasattr(request, "context"):
            if isinstance(request.context, Company):
                company = request.context
            else:
                cid = None
                if hasattr(request.context, 'company_id'):
                    cid = request.context.company_id
                elif hasattr(request.context, "get_company_id"):
                    cid = request.context.get_company_id()
                if cid is not None:  # case Workshops interne CAE
                    company = Company.get(cid)
        # Usefull for non-admin having several companies:
        # display the menu even if current context is not company related
        if not submenu:
            if company is not None:
                UserPrefsService.set(request, 'last_used_company', company.id)
            else:
                cid = UserPrefsService.get(request, 'last_used_company')
                if cid is not None:  # Prevent empty last_used_company
                    company = Company.get(cid)
            # fallback on a default one : first company
            if company is None and len(request.user.active_companies):
                cid = request.user.active_companies[0].id
                company = Company.get(cid)
        # Place the current company in cache (only for the request's lifecycle)
        request.current_company = company
    return company


def get_companies(request, company=None):
    """
    Retrieve the companies the current user has access to

    :param obj request: The current pyramid request
    :param obj company: The current company
    :returns: The list of companies
    :rtype: list
    """
    companies = []
    if request.has_permission('manage'):
        if company is not None:
            companies = Company.label_query().filter(
                or_(
                    Company.active == True,  # noqa: E712
                    Company.id == company.id
                )
            ).all()
        else:
            companies = Company.label_query().filter(
                Company.active == True
            ).all()
    else:
        companies = request.user.active_companies
    return companies


def get_company_menu(
    request, company, css=None, submenu=True, is_user_company=True
):
    """
    Build the Company related menu
    """
    menu_builder = request.registry.company_menu
    menu = menu_builder.build(
        request,
        context=company,
        user_id=request.user.id,
        company_id=company.id,
        submenu=submenu,
        is_user_company=is_user_company,
        company=company
    )
    menu['css'] = css

    # # Accueil
    # href = request.route_path('company_index', id=company.id)
    # menu.add_item("Accueil", icon="", href=href)
    # # Tiers
    # dd = AppMenuDropDown(label="Tiers")
    #dd.add_item(
    #    "Clients",
    #    icon='',
    #    href=request.route_path("company_customers", id=company.id),
    #    routes_prefixes=[
    #        'customer',
    #    ],
    #)
    #dd.add_item(
    #    "Fournisseurs",
    #    icon='',
    #    href=request.route_path("company_suppliers", id=company.id),
    #    routes_prefixes=[
    #        'supplier',
    #    ],
    #)
    #menu.add(dd)
    # Catalogue produits
    # menu.add_item(
    #     "Catalogue produits",
    #     icon="",
    #     href=request.route_path(CATALOG_ROUTE, id=company.id),
    #     routes_prefixes=[
    #         'sale_categories',
    #         'sale_category',
    #         'sale_products_group',
    #         'sale_product_groups',
    #         'sale_training_group',
    #         'sale_training_groups',
    #     ],
    # )
    # Vente
    # dd = AppMenuDropDown(label="Vente")
    # from endi.views.project.routes import COMPANY_PROJECTS_ROUTE
    # dd.add_item(
    #     "Dossiers",
    #     icon="",
    #     href=request.route_path(COMPANY_PROJECTS_ROUTE, id=company.id),
    #     routes_prefixes=[
    #         '/projects/{id}',
    #     ],
    # )
 #   dd.add_item(
 #       "Devis",
 #       icon="",
 #       href=request.route_path("company_estimations", id=company.id),
 #       routes_prefixes=[
 #          '/estimations/{id}',
 #       ],
 #   )
 #   dd.add_item(
 #       "Factures",
 #       icon="",
 #       href=request.route_path("company_invoices", id=company.id),
 #       routes_prefixes=[
 #           '/invoices/{id}',
 #       ],
 #   )
    # menu.add(dd)
    # Achat
    # dd = AppMenuDropDown(label="Achat")
    # dd.add_item(
    #     "Notes de dépenses",
    #     icon="",
    #     href=request.route_path("company_expenses", id=company.id),
    #     routes_prefixes=[
    #         '/expenses/{id}',
    #     ],
    # )
    # menu.add(dd)

    #dd.add_item(
    #    'Commandes fournisseurs',
    #    icon='',
    #    href=request.route_path(
    #        "/company/{id}/suppliers_orders",
    #        id=company.id,
    #    ),
    #    routes_prefixes=[
    #        '/suppliers_orders/{id}',
    #    ],
    #)

    #dd.add_item(
    #    'Factures fournisseurs',
    #    icon='',
    #    href=request.route_path(
    #        "/company/{id}/suppliers_invoices",
    #        id=company.id,
    #    ),
    #    routes_prefixes=[
    #        "/suppliers_invoices/{id}",
    #    ],
    #)

    # Etats de gestion
    #dd = AppMenuDropDown(label="Etats de gestion")
    #href = request.route_path(
    #    "/companies/{id}/accounting/treasury_measure_grids",
    #    id=company.id
    #)
    #dd.add_item("État de trésorerie", icon="", href=href)
    #href = request.route_path(
    #    "/companies/{id}/accounting/income_statement_measure_grids",
    #    id=company.id
    #)
    #dd.add_item("Compte de résultat", icon="", href=href)
    #href = request.route_path("commercial_handling", id=company.id)
    #dd.add_item("Gestion commerciale", icon="", href=href)
    #menu.add(dd)

    # Documents
    # dd = AppMenuDropDown(label="Documents")
    # href = request.route_path("treasury", id=company.id)
    # dd.add_item("Trésorerie", icon="", href=href)
    # href = request.route_path("incomestatement", id=company.id)
    # dd.add_item("Compte de résultat", icon="", href=href)
    # href = request.route_path("salarysheet", id=company.id)
    # dd.add_item("Bulletin de salaire", icon="", href=href)
    # if request.user.has_userdatas():
    #     href = request.route_path(
    #         '/users/{id}/userdatas/mydocuments',
    #         id=request.user.id
    #     )
    #     dd.add_item("Mes documents", icon="", href=href)
    # menu.add(dd)

    # Accompagnement
    #dd = AppMenuDropDown(label="Accompagnement")
    #href = request.route_path("company_activities", id=company.id)
    #dd.add_item("Mes rendez-vous", icon="", href=href)
    #if request.has_permission('manage'):
    #    href = request.route_path(
    #        "company_workshops_subscribed",
    #        id=company.id
    #    )
    #    dd.add_item("Ateliers", icon="", href=href)
    #if is_user_company:
    #    href = request.route_path(
    #        "user_workshop_subscriptions",
    #        id=request.user.id
    #    )
    #    dd.add_item("Inscriptions aux ateliers", icon="", href=href)
    #    href = request.route_path('user_competences', id=request.user.id)
    #    dd.add_item("Compétences", icon="", href=href)
    #menu.add(dd)

    # Outils métier
   # dd = AppMenuDropDown(label="Outils métier")
   # if request.has_permission("list.training", company):
   #     from endi.views.training.routes import TRAINING_DASHBOARD_URL
   #     href = request.route_path(TRAINING_DASHBOARD_URL, id=company.id)
   #     dd.add_item("Formation", icon="", href=href)
   #     href = request.route_path("company_workshops", id=company.id)
   #     dd.add_item("Organisation d'ateliers", icon="", href=href)

    # Mon enseigne
    # href = request.route_path("company", id=company.id)
    # if request.has_permission('manage'):
    #     menu.add_item("Fiche de l'enseigne", icon="", href=href)
    # else:
    #     menu.add_item("Mon enseigne", icon="", href=href)

    # Annuaire
    #if not request.has_permission('manage'):
    #    href = request.route_path("/users")
    #    menu.add_item("Annuaire", icon="", href=href)

    ## Aide
    #if not request.has_permission('manage'):
    #    dd = AppMenuDropDown(label="Aide")
    #    dd.add(HtmlAppMenuItem(html=ENDI_SITE_LINK))
    #    dd.add(HtmlAppMenuItem(html=ENDI_DOC_LINK))
    #    menu.add(dd)
    return menu


def get_admin_menus(request):
    """
        Build the admin menu
    """
    menu_builder = request.registry.admin_menu
    menu = menu_builder.build(request, user_id=request.user.id)
    return menu


    # menu = AppMenu()
    # Gestion commerciale
    # dd = AppMenuDropDown(label="Gestion commerciale")
    # dd.add_item(
    #     "Devis",
    #     icon="",
    #     href=request.route_path("estimations"),
    #     #routes_prefixes=['/estimations/{id}',]
    # )
    # href = request.route_path("invoices")
    # dd.add_item("Factures clients", icon="", href=href)
    # dd.add_item(
    #     'Notes de dépenses',
    #     icon="",
    #     href=request.route_path('expenses'),
    #     routes_prefixes=['/expenses/{id}',]
    # )
    # dd.add_item(
    #     'Commandes fournisseur',
    #     icon="",
    #     href=request.route_path('/suppliers_orders'),
    #     routes_prefixes=['/suppliers_orders/{id}',]
    # )
    # dd.add_item(
    #     'Factures fournisseur',
    #     icon="",
    #     href=request.route_path('/suppliers_invoices'),
    #     routes_prefixes=['/suppliers_invoices/{id}',]
    # )
    # menu.add(dd)
    # Comptabilité
    #if request.has_permission("admin_treasury"):
        # dd = AppMenuDropDown(label="Comptabilité")

        # href = request.route_path("/export/treasury/invoices")
        # dd.add_item("Export des factures", icon="", href=href)

        # href = request.route_path("/export/treasury/payments")
        # dd.add_item("Export des encaissements", icon="", href=href)

        # href = request.route_path("/export/treasury/expenses")
        # dd.add_item("Export des notes de dépenses", icon="", href=href)

        # href = request.route_path("/export/treasury/expense_payments")
        # dd.add_item("Export des paiements de dépense", icon="", href=href)

        # href = request.route_path("/export/treasury/supplier_invoices")
        # dd.add_item("Export des factures fournisseurs", icon="", href=href)

        # href = request.route_path("/export/treasury/supplier_payments")
        # dd.add_item("Export des paiements de facture fournisseur", \
        #     icon="", href=href)

        # href = request.route_path("admin_treasury_all")
        # dd.add_item("Bulletins de salaire", icon="", href=href)

        # href = request.route_path("/accounting/operation_uploads")
        # dd.add_item("Remontée comptable", icon="", href=href)

        #dd.add_item(
        #    "Remises en banque",
        #    href=request.route_path("/accounting/bank_remittances"),
        #    routes_prefixes=['/accounting/bank_remittances/{id}',]
        #)
        #menu.add(dd)
    # Accompagnement
    # dd = AppMenuDropDown(label="Accompagnement")
    # href = request.route_path('activities')
    # dd.add_item("Rendez-vous", icon="", href=href)
    # href = request.route_path('cae_workshops')
    # dd.add_item("Ateliers", icon="", href=href)
    # href = request.route_path("user_workshop_subscriptions", id=request.user.id)
    # dd.add_item("Mes inscriptions", icon="", href=href)
    # href = request.route_path('competences')
    # dd.add_item("Compétences", icon="", href=href)
    # menu.add(dd)
    # Gestion sociale
    # dd = AppMenuDropDown(label="Gestion sociale")
    # href = request.route_path('/userdatas')
    # dd.add_item("Consulter", icon="", href=href)
    # href = request.route_path('statistics')
    # dd.add_item("Statistiques", icon="", href=href)
    # href = request.route_path("holidays")
    # dd.add_item("Congés des entrepreneurs", icon="", href=href)
    # menu.add(dd)
    # Formations
    # if request.has_permission('admin.training'):
    #     dd = AppMenuDropDown(label="Formations")
    #     href = request.route_path('/trainings')
    #     dd.add_item("Formations", icon="", href=href)
    #     href = request.route_path('workshops')
    #     dd.add_item("Ateliers", icon="", href=href)
    #     href = request.route_path("/trainers")
    #     dd.add_item("Annuaire des formateurs", icon="", href=href)
    #     href = request.route_path(BPF_EXPORT_ODS_URL)
    #     dd.add_item("Export BPF", icon="", href=href)
    #     menu.add(dd)
    # Annuaires
    # dd = AppMenuDropDown(label="Annuaires")
    # href = request.route_path("/users")
    # dd.add_item("Utilisateurs", icon="", href=href)
    # href = request.route_path("companies")
    # dd.add_item("Enseignes", icon="", href=href)
    # menu.add(dd)
    # # Aide
    # dd = AppMenuDropDown(label="Aide")
    # dd.add(HtmlAppMenuItem(html=ENDI_SITE_LINK))
    # dd.add(HtmlAppMenuItem(html=ENDI_DOC_LINK))
    # menu.add(dd)
    # return menu


def company_choice(request, companies, current_company=None):
    """
        Add the company choose menu
    """
    options = tags.Options()
    options.add_option("Sélectionner une enseigne...", '/')
    for company in companies:
        if request.context.__name__ == 'company':
            url = request.current_route_path(id=company.id)
        else:
            url = request.route_path("company", id=company.id)
        name = company.name
        if not company.active:
            name += " (désactivée)"
        options.add_option(name, url)
    if current_company is not None:
        if request.context.__name__ == 'company':
            default = request.current_route_path(id=current_company.id)
        else:
            default = request.route_path("company", id=current_company.id)
    else:
        default = request.current_route_path()
    html_attrs = {
        'class': 'company-search',
        'id': "company-select-menu",
    }
    html_code = HTML.li(
        tags.select("companies", default, options, **html_attrs)
    )
    return HtmlAppMenuItem(html=html_code).build()


def get_usermenu(request):
    """
        Return the user menu (My account, holidays ...)
    """
    menu_builder = request.registry.user_menu
    return menu_builder.build(request, user_id=request.user.id)


def menu_panel(context, request):
    """
    Top menu panel

    Build the top menu dict representation

    :rtype: dict
    """
    # If we've no user in the current request, we don't return anything
    if not getattr(request, 'user'):
        return {}

    menu = None
    if request.has_permission('manage'):
        menu = get_admin_menus(request)
    else:
        current_company = get_current_company(request)
        if current_company:
            menu = get_company_menu(request, current_company, submenu=False)
            companies = get_companies(request, current_company)
            # If there is more than 1 company accessible for the current user,
            # we provide a usefull dropdown menu
            if len(companies) > 1:
                menu['items'].insert(
                    0, company_choice(request, companies, current_company)
                )

    usermenu = get_usermenu(request)

    return {
        'menu': menu,
        'usermenu': usermenu,
    }


def submenu_panel(context, request):
    """
    Submenu panel, build a dict representation of the submenu, if one is
    expected

    :rtype: dict
    """
    # If we've no user in the current request, we don't return anything
    if not getattr(request, 'user'):
        return {}

    # There are no submenus for non admins
    if not request.has_permission('manage'):
        return {}

    current_company = get_current_company(request, submenu=True)
    if not current_company:
        submenu = {
            "items": [
                company_choice(request, get_companies(request))
            ]
        }
        return {"submenu": submenu}

    is_user_company = current_company.employs(request.user.id)
    submenu = get_company_menu(
        request,
        current_company,
        css="nav-pills",
        is_user_company=is_user_company,
    )
    if submenu:
        companies = get_companies(request, current_company)
        # If there is more than 1 company accessible for the current user,
        # we provide a usefull dropdown menu
        if len(companies) > 1:
            submenu['items'].insert(
                0,
                company_choice(request, companies, current_company)
            )
    return {"submenu": submenu}


def includeme(config):
    """
        Pyramid's inclusion mechanism
    """
    config.add_panel(
        menu_panel,
        'menu',
        renderer='/panels/menu.mako',
    )
    config.add_panel(
        submenu_panel,
        'submenu',
        renderer='/panels/menu.mako',
    )
