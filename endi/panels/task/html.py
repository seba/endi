def html_wrapper_panel(context, request):
    return dict(task=context)


def includeme(config):
    """
    Pyramid's inclusion mechanism
    """
    config.add_panel(
        html_wrapper_panel,
        "task_html",
        renderer="panels/task/task_html.mako"
    )
