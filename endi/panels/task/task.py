# -*- coding: utf-8 -*-
"""

    Panels used for task rendering

"""
import logging
from sqla_inspect.py3o import SqlaContext
from endi.models.company import Company
from endi.models.task import Invoice

from endi.utils.strings import major_status


logger = logging.getLogger(__name__)


CompanySerializer = SqlaContext(Company)


def task_panel(context, request, task=None, bulk=False):
    """
        Task panel
    """
    logger.debug("IN THE TASK PANEL")
    if task is None:
        task = context

    tvas = task.get_tvas()
    # Si on a plusieurs TVA dans le document, cela affecte l'affichage
    multiple_tvas = len([val for val in tvas if val]) > 1

    is_tva_on_margin_mode = task.business_type.tva_on_margin

    tmpl_context = CompanySerializer.compile_obj(task.project.company)

    # Calcul des nombres de
    if task.display_units:
        column_count = 5
        first_column_colspan = 4
    else:
        column_count = 2
        first_column_colspan = 1

    if multiple_tvas:
        column_count += 1

    if task.display_ttc:
        column_count += 1

    if isinstance(task, Invoice) and \
            task.invoicing_mode == task.PROGRESS_MODE:
        show_progress_invoicing = True
        column_count += 1
        first_column_colspan += 1
    else:
        show_progress_invoicing = False

    return dict(
        task=task,
        groups=task.get_groups(),
        project=task.project,
        company=task.project.company,
        multiple_tvas=multiple_tvas,
        tvas=tvas,
        config=request.config,
        bulk=bulk,
        mention_tmpl_context=tmpl_context,
        first_column_colspan=first_column_colspan,
        column_count=column_count,
        show_progress_invoicing=show_progress_invoicing,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def task_line_group_panel(
    context,
    request,
    task,
    group,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
    is_tva_on_margin_mode,
):
    """
    A panel representing a TaskLineGroup
    """
    display_subtotal = False
    if len(task.get_groups()) > 1:
        display_subtotal = True

    return dict(
        task=task,
        group=group,
        display_subtotal=display_subtotal,
        display_units=task.display_units,
        display_tvas_column=display_tvas_column,
        display_ttc=task.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


def task_line_panel(
    context,
    request,
    task,
    line,
    display_tvas_column,
    column_count,
    first_column_colspan,
    show_progress_invoicing,
    is_tva_on_margin_mode,
):
    """
    A panel representing a single TaskLine

    :param obj task: The current task to be rendered
    :param obj line: A taskline
    """
    percentage = 0
    if show_progress_invoicing:
        from endi.models.progress_invoicing import ProgressInvoicingLine
        percentage = ProgressInvoicingLine.find_percentage(line.id)
        if percentage is None:
            percentage = 0

    return dict(
        task=task,
        line=line,
        display_units=task.display_units,
        display_tvas_column=display_tvas_column,
        display_ttc=task.display_ttc,
        column_count=column_count,
        first_column_colspan=first_column_colspan,
        show_progress_invoicing=show_progress_invoicing,
        progress_invoicing_percentage=percentage,
        is_tva_on_margin_mode=is_tva_on_margin_mode,
    )


STATUS_LABELS = {
    'draft': "Brouillon",
    "wait": "En attente de validation",
    "invalid": {
        "estimation": "Invalidé",
        "invoice": "Invalidée",
        "cancelinvoice": "Invalidé",
        "expensesheet": "Invalidée",
        "supplier_order": "Invalidée",
    },
    "valid": {
        "estimation": "En cours",
        "invoice": "En attente de paiement",
        "cancelinvoice": "Soldé",
        "expensesheet": "Validée",
        "supplier_order": "Validée",
    },
    "aborted": "Sans suite",
    'sent': "Envoyé",
    "signed": "Signé par le client",
    "geninv": "Factures générées",
    "paid": "Payée partiellement",
    "resulted": "Soldée",
    "justified": "Justificatifs reçus",
}


def task_title_panel(context, request, title):
    """
    Panel returning a label for the given context's status
    """
    # FIXME: factorize properly into render_api and common panels : this is
    # used for other stuff than tasks.
    # See render_api.STATUS_CSS_CLASS, among others

    status = major_status(context)
    print(("The major status is : %s" % status))
    status_label = STATUS_LABELS.get(status)
    if isinstance(status_label, dict):
        status_label = status_label[context.type_]

    css = 'status status-%s' % context.status
    if hasattr(context, 'paid_status'):
        css += ' paid-status-%s' % context.paid_status
        if hasattr(context, 'is_tolate'):
            css += ' tolate-%s' % context.is_tolate()
        elif hasattr(context, 'justified'):
            css += ' justified-%s' % context.justified
    elif hasattr(context, 'signed_status'):
        css += ' signed-status-%s geninv-%s' % (
            context.signed_status,
            context.geninv,
        )
    else:  # cancelinvoice
        if status == 'valid':
            css += ' paid-status-resulted'

    return dict(
        title=title,
        item=context,
        css=css,
        status_label=status_label
    )


def includeme(config):
    """
        Pyramid's inclusion mechanism
    """
    config.add_panel(
        task_line_group_panel,
        'task_line_group_html',
        renderer="panels/task/task_line_group.mako"
    )

    config.add_panel(
        task_line_panel,
        'task_line_html',
        renderer="panels/task/task_line.mako"
    )

    config.add_panel(
        task_title_panel,
        "task_title_panel",
        renderer="endi:templates/panels/task/title.mako",
    )
