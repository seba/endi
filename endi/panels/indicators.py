# -*- coding: utf-8 -*-
from endi.models.task import Task
from endi.models.project.business import Business
from endi.views.indicators import INDICATOR_ROUTE
from endi.panels.files import stream_actions


def _get_status_icon(indicator):
    """
    Translate the inidicator status in an icon name
    """
    result = indicator.status
    if result == 'warning':
        result = 'clock'
    return result


def sale_file_requirements_panel(context, request, file_requirements):
    """
    Show the file requirements indicators for the given context
    """
    if isinstance(context, Task):
        file_add_route = "/%ss/{id}/addfile" % (context.type_,)
    elif isinstance(context, Business):
        file_add_route = "/businesses/{id}/addfile"

    file_add_url = request.route_path(
        file_add_route,
        id=context.id,
    )
    return dict(
        indicators=file_requirements,
        stream_actions=stream_actions,
        file_add_url=file_add_url,
    )


def sale_file_requirement_panel(context, request, file_add_url):
    return dict(
        indicator=context,
        status_icon=_get_status_icon(context),
        force_route=INDICATOR_ROUTE,
        get_csrf_token=request.session.get_csrf_token,
        file_add_url=file_add_url,
    )


def custom_indicator_panel(context, request, indicator):
    """
    Panel displaying an indicator in a generic format
    """
    force_url = request.route_path(
        INDICATOR_ROUTE,
        id=indicator.id,
        _query={'action': 'force'},
    )
    return dict(
        indicator=indicator,
        force_url=force_url,
        status_icon=_get_status_icon(indicator),
    )


def includeme(config):
    TEMPLATE_PATH = "endi:templates/panels/indicators/{}"
    config.add_panel(
        sale_file_requirements_panel,
        "sale_file_requirements",
        renderer=TEMPLATE_PATH.format("sale_file_requirements.mako"),
    )
    config.add_panel(
        sale_file_requirement_panel,
        "sale_file_requirement",
        renderer=TEMPLATE_PATH.format("sale_file_requirement.mako"),
    )
    config.add_panel(
        custom_indicator_panel,
        "custom_indicator",
        renderer=TEMPLATE_PATH.format("custom_indicator.mako")
    )
