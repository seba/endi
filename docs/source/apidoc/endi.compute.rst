endi.compute package
=========================

Submodules
----------

endi.compute.math_utils module
-----------------------------------

.. automodule:: endi.compute.math_utils
    :members:
    :undoc-members:
    :show-inheritance:

endi.compute.sage module
-----------------------------

.. automodule:: endi.compute.sage
    :members:
    :undoc-members:
    :show-inheritance:

endi.compute.task module
-----------------------------

.. automodule:: endi.compute.task
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: endi.compute
    :members:
    :undoc-members:
    :show-inheritance:
