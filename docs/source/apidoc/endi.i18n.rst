endi.i18n package
======================

Submodules
----------

endi.i18n.translater module
--------------------------------

.. automodule:: endi.i18n.translater
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: endi.i18n
    :members:
    :undoc-members:
    :show-inheritance:
