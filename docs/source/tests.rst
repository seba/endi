Le principe
-----------------

Les tests sont lancés depuis la base du répertoire tests par ``py.test``.

Ce script récolte tous les fichiers qui commencent par test_ et lance toutes les fonctions et méthodes commençant par test_ trouvées dans ces fichiers.


Lancement manuel des tests
-----------------

Pour lancer les tests manuellement sur les machines de dev : 

``cd endi/tests/``

``py.test -xv`` (x : s'arrête à la première erreur, v : affiche les print)
     
/!\\ Requiert la configuration d'un fichier ``test.ini`` à la racine du repo ainsi que l'installation des librairies de test (``pip install .[test]``)


Contexte de lancement des tests
-----------------

Pour le contexte de lancement on utilise des "pytest.fixtures".

Pour chaque test les fixtures sont recherchées  dans le fichier de test puis dans le fichier conftest.py du répertoire courant puis dans conftest.py du répertoire parent.

Les fonctions/méthodes de tests sont appelées avec le résultat des fixtures en question.
Une fixture peut être initialisée à chaque test où elle est appelé ou à chaque fichier test_ ou globalement (lorsqu'on lance tous les tests, la fixture en charge d'initialiser une bdd mysql est chargée une seule fois par exemple).


Mise en place des fixtures
-----------------

Les fixtures pytest permettent de fournir des variables facilement
intégrables dans les tests

Si dans un fichier chargé pour les tests (idéalement un fichier contest.py) on a :

.. code-block:: python

    import pytest

    @pytest.fixture
    def model(dbsession):
        item = Model(param1=param1, param2=param2)
        item = dbsession.add(item)
        dbsession.flush()
        return item

On peut alors utiliser cette fixture dans tous les tests en l'intégrant comme paramètre dans la signature de la fonctione

.. code-block:: python

    def test_fonctionnalite(model):
        assert model.method() == resultat_attendu

La fixture nommée model la plus proche en terme d'arborescence du fichier de test est utilisée.
