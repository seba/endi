'use strict';

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const merge = require('webpack-merge');

const APP_DIR = path.resolve(__dirname, 'src');
const BUILD_DIR = path.resolve(__dirname, "..", "endi/static/js/build/");
var PROD = (process.env.NODE_ENV === 'production');

const config = {
  entry: {
      base_setup: path.join(APP_DIR, 'base_setup.js'),
      task: path.join(APP_DIR, 'task', 'task.js'),
      expense: path.join(APP_DIR, 'expense', 'expense.js'),
      sale_product: path.join(APP_DIR, 'sale_product', 'sale_product.js'),
      supplier_order: path.join(APP_DIR, 'supplier_order', 'supplier_order.js'),
      price_study: path.join(APP_DIR, 'price_study', 'price_study.js'),
      supplier_invoice: path.join(APP_DIR, 'supplier_invoice', 'supplier_invoice.js'),
      vendor: [
          'jquery', 'backbone', 'underscore', 'backbone.marionette',
          'tinymce', 'bootstrap', 'backbone-validation',
          'math'
        ]
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.mustache$/,
        loader: "handlebars-loader"
      }
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: PROD ? '[name].min.js': '[name].js'
  },
  plugins: [].concat(
    [
      new CopyWebpackPlugin([
        {
          from: path.join(__dirname, './node_modules/tinymce-i18n/langs/fr_FR.js'),
          to: 'tinymce-assets/langs',
        },
        {
          from: path.join(__dirname, './node_modules/tinymce/skins/lightgray'),
          to: 'tinymce-assets/skins/lightgray',
        },

      ]),
      // Pre-Provide datas used by other libraries
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery',
        _: 'underscore'
      }),
    ], PROD ? [
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
    ] : [], [
      new webpack.optimize.CommonsChunkPlugin(
        "vendor",
        PROD ? "vendor.min.js" : "vendor.bundle.js",
        Infinity
      )
    ]
  ),
  resolve: {
    root: path.join(__dirname, './src')
  },
  resolveLoader: {
    root: path.join(__dirname, './node_modules')
  },
  watchOptions: {
      ignored: './node_modules'
  }
};

module.exports = config;
