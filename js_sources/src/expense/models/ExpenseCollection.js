import Bb from 'backbone';
import ExpenseModel from "./ExpenseModel.js";
import Radio from 'backbone.radio';
import { round } from '../../math.js';

const ExpenseCollection = Bb.Collection.extend({
    /*
     *  Collection of expense lines
     */
    model: ExpenseModel,
    initialize(){
        this.on('destroyed', this.channelCall);
        this.on('saved', this.channelCall);
    },
    channelCall: function(model){
        var channel = Radio.channel('facade');
        channel.trigger('changed:line');
    },
    url(){
        return AppOption['context_url'] + "/lines";
    },
    comparator: function( a, b ){
      /*
       * Sort the collection and place special lines at the end
       */
      var res = 0;
      if ( b.isTelType() ){
        res = -1;
      }else if( a.isTelType() ){
        res = 1;
      }else{
        var acat = a.get('category');
        var bcat = b.get('category');
        if ( acat < bcat ){
          res = -1;
        }else if ( acat > bcat ){
          res = 1;
        }
        if (res === 0){
          var adate = a.get('altdate');
          var bdate = a.get('altdate');
          if (adate < bdate){
            res = -1;
          } else if ( acat > bcat ){
            res = 1;
          }
        }
      }
      return res;
    },
    total_ht: function(category){
      /*
       * Return the total value
       */
      var result = 0;
      this.each(function(model){
        if (category != undefined){
          if (model.get('category') != category){
            return;
          }
        }
        result += round(model.getHT());
      });
      return result;
    },
    total_tva: function(category){
      /*
       * Return the total value
       */
      var result = 0;
      this.each(function(model){
        if (category != undefined){
          if (model.get('category') != category){
            return;
          }
        }
        result += round(model.getTva());
      });
      return result;
    },
    total: function(category){
      /*
       * Return the total value
       */
      var result = 0;
      this.each(function(model){
        if (category != undefined){
          if (model.get('category') != category){
            return;
          }
        }
        result += round(model.total());
      });
      return result;
    },
});
export default ExpenseCollection;
