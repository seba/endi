import Mn from 'backbone.marionette';
import FacadeModelApiMixin from '../../base/components/FacadeModelApiMixin.js';
import Radio from 'backbone.radio';
import { ajax_call } from '../../tools.js';

import TotalModel from '../models/TotalModel.js';
import ExpenseCollection from '../models/ExpenseCollection.js';
import ExpenseKmCollection from '../models/ExpenseKmCollection.js';
import BookMarkCollection from '../models/BookMarkCollection.js';

const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    radioEvents: {
        "changed:line": "computeLineTotal",
        "changed:kmline": "computeKmLineTotal",
    },
    radioRequests: {
        'get:collection': 'getCollectionRequest',
        'get:model': 'getModelRequest',
        'get:totalmodel': 'getTotalModelRequest',
        'get:bookmarks': 'getBookMarks',
    },
    setup(options){
        console.log("Facade.setup");
        console.table(options);
        this.url = options['context_url'];
        this.config = Radio.channel('config');
    },
    start(){
        console.log("Starting the facade");
        let deferred = ajax_call(this.url);
        return deferred.then(this.setupModels.bind(this));
    },
    setupModels(context_datas){
        this.datas = context_datas;
        this.models = {};
        this.collections = {};
        this.totalmodel = new TotalModel();

        var lines = context_datas['lines'];
        this.collections['lines'] = new ExpenseCollection(lines);

        var kmlines = context_datas['kmlines'];
        this.collections['kmlines'] = new ExpenseKmCollection(kmlines);

        const bookmarks = this.config.request('get:options', 'bookmarks');
        this.bookmarks = new BookMarkCollection(bookmarks);

        this.computeLineTotal();
        this.computeKmLineTotal();
    },
    getBookMarks(){
        return this.bookmarks;
    },
    computeLineTotal(){
        /*
         * compute the line totals for the given category
         */
        const categories = ['1', '2'];

        var collection = this.collections['lines'];
        var datas = {}
        _.each(categories, function(category){
            datas['ht_' + category] = collection.total_ht(category);
            datas['tva_' + category] = collection.total_tva(category);
            datas['ttc_' + category] = collection.total(category);
        });
        datas['ht'] = collection.total_ht();
        datas['tva'] = collection.total_tva();
        datas['ttc'] = collection.total();
        this.totalmodel.set(datas);
        var channel = this.getChannel();
        _.each(categories, function(category){
            channel.trigger('change:lines_' + category);
        });
    },
    computeKmLineTotal(){
        /*
         * Compute the kmline totals for the given category
         */
        const categories = ['1', '2'];
        var collection = this.collections['kmlines'];
        var datas = {}
        _.each(categories, function(category){
            datas['km_' + category] = collection.total_km(category);
            datas['km_ttc_' + category] = collection.total(category);
        });
        datas['km_tva'] = collection.total_tva();
        datas['km_ht'] = collection.total_ht();
        datas['km'] = collection.total_km();
        datas['km_ttc'] = collection.total();
        this.totalmodel.set(datas);

        var channel = this.getChannel();
        _.each(categories, function(category){
            channel.trigger('change:kmlines_' + category);
        });
    },
    getTotalModelRequest(){
        return this.totalmodel;
    },
});
const Facade = new FacadeClass();
export default Facade;
