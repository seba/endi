import $ from 'jquery';
import _ from 'underscore';

import {applicationStartup} from '../backbone-tools.js';

import App from './components/App.js';
import Facade from './components/Facade.js';
import AppAction from './components/AppAction.js';
import ActionView from "../common/views/ActionView.js";
import ExpenseTypeService from "../common/components/ExpenseTypeService.js";


AppAction.on('start', function(app, actions) {
    var view = new ActionView({actions: actions});
    AppAction.showView(view);
});
$(function(){
    applicationStartup(AppOption, App, Facade, AppAction, ExpenseTypeService);
});
