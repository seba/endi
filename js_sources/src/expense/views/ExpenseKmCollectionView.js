import Mn from 'backbone.marionette';
import ExpenseKmView from './ExpenseKmView.js';
import ExpenseEmptyView from './ExpenseEmptyView.js';

const ExpenseKmCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'kmline:edit',
        'delete': 'kmline:delete',
        'duplicate': 'kmline:duplicate',
    },
    childView: ExpenseKmView,
    emptyView: ExpenseEmptyView,
    emptyViewOptions(){
        return {
            colspan: 6,
            edit: this.getOption('edit')
        };
    },
    childViewOptions(){
        return {edit: this.getOption('edit')};
    },
    viewFilter(view, index, children) {
        if (view.model.get('category') == this.getOption('category').value){
            return true;
        } else {
            return false;
        }
    }
});
export default ExpenseKmCollectionView;
