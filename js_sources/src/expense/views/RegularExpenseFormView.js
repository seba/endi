import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import BaseExpenseFormView from './BaseExpenseFormView.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import InputWidget from '../../widgets/InputWidget.js';


const RegularExpenseFormView = BaseExpenseFormView.extend({
    childViewEvents: {
        'finish': 'onChildChange',
    },
    initialize(){
        BaseExpenseFormView.prototype.initialize.apply(this);

        var channel = Radio.channel('config');
        this.categories = channel.request(
            'get:options',
            'categories'
        );
        this.businesses = channel.request(
            'get:options',
            'businesses'
        );
    },
    getTypeOptions() {
        var channel = Radio.channel('config');
        return channel.request(
            'get:typeOptions',
            'regular'
        );
    },

    getCategoryWidget() {
        return new SelectWidget({
            value: this.model.get('category'),
            field_name: 'category',
            options: this.categories,
            id_key: 'value',
            title: "Catégorie",
        });
    },

    onRender(){
        BaseExpenseFormView.prototype.onRender.apply(this);
        let view;
        if(this.model.get('category')!=1) {
            view = new SelectBusinessWidget({
                title: 'Affaire concernée',
                options: this.businesses,
                customer_value: this.model.get('customer_id'),
                project_value: this.model.get('project_id'),
                business_value: this.model.get('business_id'),
            });
            this.showChildView('business_link', view);
        }

        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Description',
            field_name: 'description'
        });
        this.showChildView('description', view);
    },
    updateBusinessLinkWidget() {
        if(this.model.get('category')!=1) {
            this.getRegion('business_link').empty();
        }else{
            var view = new SelectBusinessWidget({
                title: 'Affaire concernée',
                options: this.businesses,
                customer_value: this.model.get('customer_id'),
                project_value: this.model.get('project_id'),
                business_value: this.model.get('business_id'),
            });
            this.showChildView('business_link', view);
        }
    },
    onChildChange(attribute, value) {
        if(attribute=="category"){
            this.updateBusinessLinkWidget();
        }
        RegularExpenseFormView.__super__.onChildChange.apply(this, [attribute, value]);
    },
});
export default RegularExpenseFormView;
