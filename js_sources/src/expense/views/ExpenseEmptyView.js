import Mn from 'backbone.marionette';

const ExpenseEmptyView = Mn.View.extend({
    template: require('./templates/ExpenseEmptyView.mustache'),
    templateContext(){
        var colspan = this.getOption('colspan');
        if (this.getOption('edit')){
            colspan += 1;
        }
        return {
            colspan: colspan
        };
    }
});
export default ExpenseEmptyView;
