import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Select2Widget from '../../widgets/Select2Widget.js';
import Radio from 'backbone.radio';


const BaseExpenseFormView = Mn.View.extend({
    behaviors: [FormBehavior],
    template: require('./templates/ExpenseFormView.mustache'),
	regions: {
        'category': '.category',
		'date': '.date',
		'type_id': '.type_id',
		'description': '.description',
		'ht': '.ht',
		'tva': '.tva',
		'business_link': '.business_link',
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
    },
    childViewEvents: {
        'finish': 'onChildChange',
    },
    initialize(){
        // Common initialization.
        var channel = Radio.channel('config');
        this.type_options = this.getTypeOptions();
        console.log('this.type_options', this.getTypeOptions());
        console.log(this.type_options);
        this.today = channel.request(
            'get:options',
            'today',
        );
    },
    onRender(){
        var view;

        view = this.getCategoryWidget();
        this.showChildView('category', view);

        view = new DatePickerWidget({
            date: this.model.get('date'),
            title: "Date",
            field_name: "date",
            default_value: this.today,
            required: true,
        });
        this.showChildView("date", view);
        console.log('Options are : ', this.type_options);
        view = new Select2Widget({
            value: this.model.get('type_id'),
            title: 'Type de frais',
            field_name: 'type_id',
            options: this.type_options,
            id_key: 'id',
            required: true,
        });
        this.showChildView('type_id', view);

        // Syncs model to allow proppper rendering of the form based on wether
        // we have TVA or not.
        this.triggerMethod('data:modified', 'type_id', view.getCurrentValues()[0])

        let htParams = {
            value: this.model.get('ht'),
            title: 'Montant HT',
            field_name: 'ht',
            addon: "€",
            required: true,
        }

        let tvaParams = {
            value: this.model.get('tva'),
            title: 'Montant TVA',
            field_name: 'tva',
            addon: "€",
            required: true,
        }

        if (! this.model.hasDeductibleTva()) {
            tvaParams.title = ''
            tvaParams.type = 'hidden';
            tvaParams.value = 0;
            htParams.title = 'Montant TTC'

        }

        view = new InputWidget(htParams);
        this.showChildView('ht', view);
        view = new InputWidget(tvaParams);
        this.showChildView('tva', view);
    },
    onChildChange(field_name, value) {
        this.triggerMethod('data:modified', field_name, value);

        if (field_name == 'type_id') {
            this.onRender();
        }
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            add: this.getOption('add'),
        };
    }
});
export default BaseExpenseFormView;
