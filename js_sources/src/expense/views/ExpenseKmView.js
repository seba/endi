import { formatAmount } from '../../math.js';

import BaseExpenseView from "./BaseExpenseView.js";

const ExpenseKmView = BaseExpenseView.extend({
    template: require('./templates/ExpenseKmView.mustache'),
    ui: {
        edit: 'button.edit',
        delete: 'button.delete',
        duplicate: 'button.duplicate',
    },
    triggers: {
        'click @ui.edit': 'edit',
        'click @ui.delete': 'delete',
        'click @ui.duplicate': 'duplicate',
    },
    templateContext(){
        var total = this.model.total();
        var typelabel = this.model.getTypeLabel();
        return {
            edit: this.getOption('edit'),
            customer: this.model.get('customer_label'),
            is_achat: this.isAchat(),
            typelabel:typelabel,
            total:formatAmount(total),
        };
    }
});
export default ExpenseKmView;
