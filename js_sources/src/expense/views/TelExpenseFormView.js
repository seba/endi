import Mn from 'backbone.marionette';
import BaseExpenseFormView from './BaseExpenseFormView.js';
import InputWidget from '../../widgets/InputWidget.js';
import Radio from 'backbone.radio';

const TelExpenseFormView = BaseExpenseFormView.extend({
    getTypeOptions() {
        var channel = Radio.channel('config');
        return channel.request(
            'get:typeOptions',
            'tel'
        );
    },
    getCategoryWidget() {
        return new InputWidget({
            value: "1",
            field_name: 'category',
            type: 'hidden',
        });
    },
});
export default TelExpenseFormView;
