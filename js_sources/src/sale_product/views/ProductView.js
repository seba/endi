/*
 * File Name : ProductView.js
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import ButtonCollection from '../../base/models/ButtonCollection.js';
import ButtonModel from '../../base/models/ButtonModel.js';
import ActionButtonsWidget from '../../widgets/ActionButtonsWidget.js';

const template = require('./templates/ProductView.mustache');

const ProductView = Mn.View.extend({
    tagName: 'tr',
    className: "white_tr",
    template: template,
    regions: {
        'actions': 'td.col_actions',
    },
    ui: {
        'edit': '.edit',
        'delete': '.delete',
        'duplicate': '.duplicate'
    },
    childViewEvents: {
        'action:clicked': 'onActionClicked'
    },
    getViewButtonModel(showLabel){
        return {
            label: "Voir / Modifier",
            action: "edit",
            showLabel: showLabel,
            icon: "pen"
        };
    },
    getDropDownCollection(){
        let collection = new ButtonCollection();
        let buttons = [
            this.getViewButtonModel(true),
            {
                label: "Dupliquer",
                action: "duplicate",
                icon: "copy",
            }
        ]
        if (! this.model.get('locked')){
            buttons.push({
                label: "Supprimer",
                action: "delete",
                icon: "trash-alt",
                css: 'negative'
            });
        }
        collection.add(buttons);
        return collection;
    },
    onRender(){
        let view = new ActionButtonsWidget({
            dropdownLabel: 'Actions',
            icon: "dots",
            showLabel: false,
            collection: this.getDropDownCollection(),
            primary: new ButtonModel(this.getViewButtonModel(false))
        });
        this.showChildView('actions', view);
    },
    onActionClicked(actionName){
        this.triggerMethod("model:" + actionName, this);
    },
    templateContext(){
        let icons = false;
        if (this.model.is_complex){
            icons = this.model.getIcons();
        }
        return {
            category_label: this.model.category_label(),
            tva_label: this.model.tva_label(),
            ht_label: this.model.ht_label(),
            supplier_ht_label: this.model.supplier_ht_label(),
            supplier_label: this.model.supplier_label(),
            current_stock: this.model.get("current_stock"),
            icons: icons,
        };
    }
});
export default ProductView;
