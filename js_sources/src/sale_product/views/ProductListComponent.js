/*
 * File Name : ProductListComponent.js
 *
 */
import Bb from 'backbone';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import ButtonModel from '../../base/models/ButtonModel.js';
import ButtonWidget from '../../widgets/ButtonWidget.js';
import MessageView from '../../base/views/MessageView.js';

import CategoriesComponent from './CategoriesComponent.js';
import ProductTable  from './ProductTable.js';
import ProductFilterForm from './ProductFilterForm.js';

const template = require('./templates/ProductListComponent.mustache');

const ProductListComponent = Mn.View.extend({
    /*
     * A list view : filter + list + category component
     *
     * takes 2 parameters:
     *
     *  collection
     *  filter_model
     */
    template: template,
    regions: {
        filters: '.search_filters',
        messageContainer: {el: ".message-container", replaceElement: true},
        table: ".table_container",
        categoryContainer: '.category-container',
    },
    ui: {
        add_button: 'button[value=add]',
        navigation_buttons: 'button.navigation',
    },
    events: {
        'click @ui.add_button': 'onAddButtonClicked',
        'click @ui.navigation_buttons': 'onNavigate',
    },
    // Listen to child view events
    childViewEvents: {
        'list:filter': 'onListFilter',
        'list:navigate': 'onListNavigate',
        'filter:submit': 'onListFilter',
        'model:edit': 'onModelEdit',
        'model:delete': 'onModelDelete',
        'model:duplicate': 'onModelDuplicate',
    },
    initialize(){
        this.facade = Radio.channel('facade');
        this.filter_model = this.facade.request('get:model', 'ui_list_filter')
        this.app = Radio.channel('app');
    },
    showFilters(){
        this.filterView = new ProductFilterForm({model: this.filter_model});
        this.showChildView('filters', this.filterView);
    },
    loadTable(){
        var serverCall = this.facade.request(
            'get:collection:page', 'products', this.filter_model.get('currentPage')
        );
        serverCall.done(this.showTable.bind(this));
    },
    showTable(){
        this.filter_model.set('currentPage', this.collection.state['currentPage'])
        let view = new ProductTable({collection: this.collection});
        this.showChildView('table', view);
    },
    showCategories(){
        let collection = this.facade.request(
            'get:collection',
            'categories'
        );
        let view = new CategoriesComponent({collection: collection});
        this.showChildView('categoryContainer', view);
    },
    showMessageView(){
       var model = new Bb.Model();
       var view = new MessageView({model: model});
       this.showChildView('messageContainer', view);
    },
    onRender(){
        this.showMessageView();
        this.showFilters();
        this.showCategories();
        this.loadTable();
    },
    /* List related events */
    onNavigate(event){
        /*
         * The event target has an event-type (getPreviousPage, getNextPage
         * ...) attached that is used for the facade request
         * */
        let event_type = $(event.target).data('event-type');
        this.triggerMethod('list:navigate', event_type);
    },
    onListFilter(childView, filters){
        var serverCall = this.facade.request(
            'get:collection:filter', 'products', filters
        );
        serverCall.done(this.showTable.bind(this));
    },
    onListNavigate(event_type){
        /*
         * Launched when navigating in the list's pages
         */
        var serverCall = this.facade.request(
            'get:collection:' + event_type, 'products'
        );
        serverCall.done(this.showTable.bind(this));
    },
    /* Add product action */
    onAddButtonClicked(){
        this.app.trigger('navigate', 'addproduct');
    },
    onModelEdit(childView){
        const modelId = childView.model.get('id');
        this.app.trigger('navigate', 'products/' + modelId);
    },
    onModelDelete(childView){
        this.app.trigger('product:delete', childView);
    },
    onModelDuplicate(childView){
        this.app.trigger('product:duplicate', childView);
    }
});
export default ProductListComponent;
