/*
 * Module name : ProductTable
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ProductView  from './ProductView.js';
import ProductEmptyView  from './ProductEmptyView.js';

const template = require('./templates/ProductTable.mustache');

const ProductCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: ProductView,
    emptyView: ProductEmptyView,
    collectionEvents: {
        'sync': 'render'
    },
    // Bubble up child view events
    childViewTriggers: {
        'model:edit': 'model:edit',
        'model:delete': 'model:delete',
        'model:duplicate': 'model:duplicate',
    },
});

const ProductTable = Mn.View.extend({
    template: template,
    regions: {
        body: {el: 'tbody', replaceElement: true}
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {
        'model:edit': 'model:edit',
        'model:delete': 'model:delete',
        'model:duplicate': 'model:duplicate',
    },
    initialize(){
        this.facade = Radio.channel('facade');
    },
    onRender(){
        this.showChildView(
            'body',
            new ProductCollectionView({collection: this.collection})
        );
    },
    templateContext(){
        return {
            totalRecords: this.collection.state.totalRecords,
            currentPage: this.collection.state.currentPage + 1,
            totalPages: this.collection.state.totalPages,
            hasPreviousPage: this.collection.hasPreviousPage(),
            hasNextPage: this.collection.hasNextPage(),
        };
    }
});
export default ProductTable
