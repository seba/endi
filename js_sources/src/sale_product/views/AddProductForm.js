/*
 * Module name : AddProductForm
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';

const template = require('./templates/AddProductForm.mustache');

const AddProductForm = Mn.View.extend({
    template: template,
    className: 'main_content',
    behaviors: [FormBehavior],
    regions: {
        label: '.field-label',
        type_: '.field-type_',
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onSuccessSync(){
        const app = Radio.channel('app');
        app.trigger('navigate', 'products/' + this.model.get('id'));
    },
    onCancelForm(){
        let app = Radio.channel('app');
        app.trigger('navigate', 'index');
    },
    onRender(){
        this.showChildView(
            'label',
            new InputWidget(
                {
                    field_name: 'label',
                    label: "Nom interne",
                    description: "Nom du produit dans le catalogue",
                    required: true
                }
            )
        );
        this.showChildView(
            "type_",
            new SelectWidget(
                {
                    field_name: "type_",
                    label: "Type de produit",
                    value: this.model.get('type_'),
                    options: this.config.request(
                        'get:options',
                        'product_types'
                    ),
                    label_key: 'label',
                    id_key: 'value',
                    required: true
                }
            )
        );

    },
});
export default AddProductForm
