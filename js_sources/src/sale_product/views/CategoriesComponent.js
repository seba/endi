/*
 * Module name : CategoriesComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import CategoryModel from "../models/CategoryModel.js";
import CategoryForm from "./CategoryForm.js";

const template = require('./templates/CategoriesComponent.mustache');

const CategoryView = Mn.View.extend({
    tagName: 'tr',
    template: require('./templates/CategoryView.mustache'),
    ui: {
        edit: "button.edit",
        delete: "button.delete"
    },
    events: {
        'click @ui.edit': "onEdit",
        'click @ui.delete': "onDelete"
    },
    modelEvents: {'change': 'render'},
    onEdit(){
        this.triggerMethod('category:edit', this.model, this);
    },
    onDelete(){
        this.triggerMethod('category:delete', this.model, this);
    }
});

const NoCategoryView = Mn.View.extend({
    tagName: 'td',
    template: require('./templates/CategoryEmptyView.mustache'),
})

const CategoryCollectionView = Mn.CollectionView.extend({
    template: require('./templates/CategoryCollectionView.mustache'),
    childView: CategoryView,
    childViewContainer: 'tbody',
    childViewTriggers: {
        'category:edit': 'category:edit',
        'category:delete': 'category:delete'
    },
    emptyView: NoCategoryView,
    ui: {
        add_button: "button.add",
    },
    events: {
        'click @ui.add_button': "onAddClicked"
    },
    onAddClicked(){
        this.triggerMethod('category:add');
    }
});


const CategoriesComponent = Mn.View.extend({
    template: template,
    regions: {
        list: '.category-list',
        modal: '.modal-container'
    },
    // Listen to child view events
    childViewEvents: {
        'category:add': "onAdd",
        'category:edit': "onEdit",
        'category:delete': "onDelete",
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onRender(){
        this.showChildView(
            'list',
            new CategoryCollectionView({collection: this.collection})
        );
    },
    onDeleteSuccess: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('success', this, "Vos données ont bien été supprimées");
    },
    onDeleteError: function(){
        let messagebus = Radio.channel('message');

        messagebus.trigger('error', this,
                           "Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
    },
    showForm(model, edit){
        let view = new CategoryForm(
            {model: model, destCollection: this.collection, edit: edit}
        );
        this.showChildView('modal', view);
    },
    onAdd(){
        let model = new CategoryModel();
        this.showForm(model, false);
    },
    onEdit(model, childView){
        this.showForm(model, true);
    },
    onDelete(model, childView){
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette catégorie ?");
        if (result){
            model.destroy(
                {
                    success: this.onDeleteSuccess,
                    error: this.onDeleteError,
                    wait: true
                }
            );
        }
    }
});
export default CategoriesComponent
