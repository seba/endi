/*
 * File Name :  CategoryCollection
 */
import Bb from 'backbone';
import CategoryModel from './CategoryModel.js';

const CategoryCollection = Bb.Collection.extend({
    model: CategoryModel
});
export default CategoryCollection;
