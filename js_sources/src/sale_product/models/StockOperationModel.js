/*
 * File Name : StockOperationModel.js
 *
 */
import BaseModel from "../../base/models/BaseModel.js";
import Radio from 'backbone.radio';
import DuplicableMixin from '../../base/models/DuplicableMixin.js';
import { dateToIso } from '../../date.js';


const StockOperationModel = BaseModel.extend(DuplicableMixin).extend({
    props: [
        'id',
        'date',
        'description',
        'stock_variation',
        'base_sale_product_id',
    ],
    defaults: {
        date: dateToIso(new Date()),
        stock_variation: 0,
    },
    validation: {
        date: {
            required: true, 
            pattern:/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
            msg: "Veuillez saisir une date valide"
        },
        stock_variation: {
            required: true,
            pattern:/^[\+\-]?[0-9]+$/,
            msg: "Veuillez saisir la quantité de variation du stock",
        },
    },
    initialize: function(){
        StockOperationModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
    },
});
export default StockOperationModel;
