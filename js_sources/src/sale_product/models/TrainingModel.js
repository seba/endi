/*
 * File Name :  TrainingModel
 */
import Bb from 'backbone';
import Radio from 'backbone.radio';
import ProductWorkModel from './ProductWorkModel.js';

const TrainingModel = ProductWorkModel.extend({
    /* props spécifique à ce modèle */
    props: [
        "goals",
        "prerequisites",
        "for_who",
        "duration",
        "content",
        "teaching_method",
        "logistics_means",
        "more_stuff",
        "evaluation",
        "place",
        "modality_one",
        "modality_two",
        "types",
        "date",
        "price",
        "free_1",
        "free_2",
        "free_3",
    ],
    validation: {
    },
    defaults:{
        types: []
    },
    initialize(){
        TrainingModel.__super__.initialize.apply(this, arguments);
        this.on('change:types', this.ensureTypesIsList, this);
    },
    ensureTypesIsList(){
        let types = this.get('types');
        if (!_.isArray(types)){
            this.attributes['types'] = [types];
        }
    },
    getIcons(){
        return "chalkboard-teacher";
    }
});
/*
 * On complète les 'props' du ProductWorkModel avec celle du TrainingModel
 */
TrainingModel.prototype.props = TrainingModel.prototype.props.concat(ProductWorkModel.prototype.props);
_.extend(TrainingModel.prototype.validation, ProductWorkModel.prototype.validation);
export default TrainingModel;
