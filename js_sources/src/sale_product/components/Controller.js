import Radio from 'backbone.radio';
import Mn from 'backbone.marionette';
import BaseProductModel from '../models/BaseProductModel.js';

const Controller = Mn.Object.extend({
    initialize(options){
        this.facade = Radio.channel('facade');
        console.log("Controller initialize");
        this.rootView = options['rootView'];
    },
    index(){
        console.log("Controller.index");
        this.rootView.index();
    },
    showModal(view){
        this.rootView.showModal(view);
    },
    productDuplicate(childView){
        console.log("Controller.productDuplicate");
        let request = childView.model.duplicate();
        var this_ = this;
        request.done(
            function(model){
                const dest_route = "/products/" + model.get('id');
                window.location.hash = dest_route;
                this_.rootView.showEditProductForm(model);
            }
        );
    },
    // Product related views
    addProduct(){
        const collection = this.facade.request('get:collection', 'products');
        let model = new BaseProductModel();
        this.rootView.showAddProductForm(model, collection);
    },
    editProduct(modelId){
        let collection = this.facade.request('get:collection', 'products');
        let request = collection.fetchSingle(modelId);
        request.then(this.rootView.showEditProductForm.bind(this.rootView));
    },
    // Common model views
    _onModelDeleteSuccess: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('success', this, "Vos données ont bien été supprimées");
        this.rootView.index();
    },
    _onModelDeleteError: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('error', this,
                           "Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
    },
    productDelete(childView){
        console.log("Controller.modelDelete");
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");
        if (result){
            childView.model.destroy(
                {
                    success: this._onModelDeleteSuccess.bind(this),
                    error: this._onModelDeleteError.bind(this),
                    wait: true
                }
            );
        }
    },
});
export default Controller;
