import AppRouter from 'marionette.approuter';

const Router = AppRouter.extend({
    appRoutes: {
        'index': 'index',
        '': 'index',
        'addproduct': 'addProduct',
        'products/:id': 'editProduct',
    }
});
export default Router;
