import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import { ajax_call } from '../../tools.js';

const ConfigBusClass = Mn.Object.extend({
    channelName: 'config',
    radioRequests: {
        'get:options': 'getFormOptions',
        'has:form_section': 'hasFormSection',
        'get:form_section': 'getFormSection',
        'get:form_actions': 'getFormActions',
    },
    setFormConfig(form_config){
        console.log("ConfigBus.start : form_config loaded");
        console.log(form_config);
        this.form_config = form_config;
    },
    getFormOptions(option_name){
        /*
         * Return a clone of the form options for option_name
         *
         * :param str option_name: The name of the option
         * :returns: A list of dict with options (for building selects)
         */
        let options = this.form_config['options'][option_name];
        if (_.isArray(options)) {
            return _.map(options, _.clone);
        } else {
            return _.clone(options);
        }
    },
    hasFormSection(section_name){
         /*
          *
          * :param str section_name: The name of the section
          * :rtype: bool
          */
        return _.has(this.form_config['sections'], section_name);
    },
    getFormSection(section_name){
        /*
         *
         * Return the form section description
         * :param str section_name: The name of the section
         * :returns: The section definition
         * :rtype: Object
         */
        return this.form_config['sections'][section_name];
    },
    getFormActions(){
        /*
         * Return available form action config
         */
        return this.form_config['actions'];
    },
    setup(url){
        /*
         * Set the configuration url
         */
        this._url = url;
    },
    start(){
        /*
         * Load the page configuration at the given url
         */
        console.log("ConfigBus.start : Loading Form configuration");
        let request = ajax_call(this._url);
        request = request.then(this.setFormConfig.bind(this));
        return request;
    }
});
const ConfigBus = new ConfigBusClass();
export default ConfigBus;
