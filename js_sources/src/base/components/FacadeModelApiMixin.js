/*
 * File Name :  FacadeModelApiMixin
 */

const FacadeModelApiMixin = {
    channelName: 'facade',
    setModelUrl(label, url){
        this.models[label].url = url;
    },
    setCollectionUrl(label, url){
        this.collections[label].url = url;
    },
    loadModel(label){
        let model = this.models[label];
        let request = model.fetch();
        return request.then(function(){return model;});
    },
    loadCollection(label, params){
        /*
         * Load a collection and returns the collection itself
        */
        let options = {}
        if (params){
            options['data'] = params;
            options['processData'] = true;
        }
        if (!label in this.collections){
            console.error("Unknown collection : %s", label);
        }
        let collection = this.collections[label];
        let request = collection.fetch(options);
        return request.then(function(){return collection});
    },
    getCollectionRequest(label){
        return this.collections[label];
    },
    getModelRequest(label){
        return this.models[label];
    },
    saveModel(modelName){
        console.log("FacadeModelApiMixin.saveModel");
        console.log(modelName);
        return this.models[modelName].save(
            null,
            {wait: true, sync: true, patch: true}
        );
    }
};
export default FacadeModelApiMixin;
