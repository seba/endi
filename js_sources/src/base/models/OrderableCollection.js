import _ from 'underscore';
import Bb from 'backbone';

const OrderableCollection = Bb.Collection.extend({
    comparator: 'order',
    initialize: function(options) {
        this.on('change:reorder', this.updateModelOrder);
        this.updateModelOrder(false);
    },
    updateModelOrder: function(sync){
        /*
         * Update the model's order
         *
         * :param bool sync: Should we synchronize the change ?
         */
        if (_.isUndefined(sync)){
            sync = true;
        }
        this.each(function(model, index) {
            model.set('order', index);
            if (sync){
                model.save(
                    {'order': index},
                    {patch: true},
                );
            }
        });

    },
    getOrder(comp_func){
        /*
        :param func comp_func: The function used to compare two items returning one oh them
        */
        if (this.models.length == 0){
            return 0
        }
        return this.models.reduce(comp_func).get('order');
    },
    getMinOrder: function(){
        const comp_func = (prev, curr) => prev.get('order') < curr.get('order') ? prev : curr;
        return this.getOrder(comp_func);
    },
    getMaxOrder: function(){
        const comp_func = (prev, curr) => prev.get('order') < curr.get('order') ? curr : prev;
        return this.getOrder(comp_func);
    },
    moveUp: function(model) { // I see move up as the -1
        var index = this.indexOf(model);
        if (index > 0) {
            this.models.splice(index - 1, 0, this.models.splice(index, 1)[0]);
            this.trigger('change:reorder');
        }
    },
    moveDown: function(model) {
        // I see move up as the -1
        var index = this.indexOf(model);
        if (index < this.models.length) {
            this.models.splice(index + 1, 0, this.models.splice(index, 1)[0]);
            this.trigger('change:reorder');
        }
    },
});
export default OrderableCollection;
