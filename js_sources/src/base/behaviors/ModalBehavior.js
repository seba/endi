import Mn from 'backbone.marionette';
import { closeModal } from '../../tools.js';


const ModalBehavior = Mn.Behavior.extend({
    /** Modal behaviour for views occuring within a modal
     *
     * Views adopting this behavior MUST define an `id` attribute.
     *
     * fires following events

        modal:beforeClose

        destroy:modal

     */
    modalClasses: 'modal_view modal_form appear',
    ui: {
        close: '.close',
        modal: '>:first-child'
    },
    events: {
        'click @ui.close': 'onClose',
    },
    onAttach(){
        console.log("ModalBehavior : Showing the modal");
        this.ui.modal.addClass(this.modalClasses);
        this.ui.modal.css('display', 'flex');
    },
    onClose(){
        this.view.trigger("modal:canceled");
        console.log("Trigger modal:beforeClose from ModalBehavior");
        this.view.triggerMethod('modal:beforeClose');
        console.log("Trigger modal:close from ModalBehavior");
        this.view.triggerMethod('modal:close');
    },
    onModalClose() {
        console.log("ModalBehavior.onModalClose");
        closeModal(this.ui.modal);
        this.triggerFinish();
    },
    triggerFinish() {
        console.log("Trigger destroy:modal from ModalBehavior");
        this.view.triggerMethod('destroy:modal');
        this.view.destroy();
    }
});
export default ModalBehavior;
