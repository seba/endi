import $ from 'jquery';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ActionCollection from '../models/ActionCollection.js';
import ActionListView from './ActionListView.js';
import { formatAmount } from '../../math.js';

var template = require("./templates/ActionView.mustache");

const ActionView = Mn.View.extend({
    regions: {
        container: ".child-container",
    },
    ui: {
        buttons: 'button',
    },
    events: {
        'click @ui.buttons': 'onButtonClick'
    },
    modelEvents: {
        'change': 'render'
    },
    template: template,
    templateContext: function(){
        let config = Radio.channel('config');
        let compute_mode = config.request('get:options', 'compute_mode');
        let is_ttc_mode = (compute_mode == 'ttc');
        return {
            is_ttc_mode: is_ttc_mode,
            buttons: this.getOption('actions')['status'],
            ttc: formatAmount(this.model.get('ttc'), true),
            ht: formatAmount(this.model.get('ht', true)),
            ht_before: formatAmount(this.model.get('ht_before_discounts'), true),
            ttc_before: formatAmount(this.model.get('ttc_before_discounts'), true),
            tvas: this.model.tva_labels(),
        }
    },
    onButtonClick: function(event){
        let target = $(event.target);
        let status = target.data('status');
        let title = target.data('title');
        let label = target.data('label');
        let url = target.data('url');
        this.triggerMethod('status:change', status, title, label, url);
    },
    onRender: function(){
        const action_collection = new ActionCollection(
            this.getOption('actions')['others']
        );
        this.showChildView(
            'container',
            new ActionListView({collection: action_collection})
        );
    }
});
export default ActionView;
