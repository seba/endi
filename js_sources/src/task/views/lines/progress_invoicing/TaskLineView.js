import _ from 'underscore';
import Mn from 'backbone.marionette';
import { formatAmount, strToFloat } from '../../../../math.js';
import Radio from 'backbone.radio';

import PercentDisplayWidget from './PercentDisplayWidget.js';

const template = require('./templates/TaskLineView.mustache');

const TaskLineView = Mn.View.extend({
    tagName: 'tr',
    className: 'row taskline',
    template: template,
    regions:{
        slider_container: "td.col_percentage_graphic",
    },
    childViewEvents: {
        'slider:clicked': 'onSliderClicked'
    },
    initialize(){
        var channel = Radio.channel('config');
        this.tva_options = channel.request('get:options', 'tvas');
        this.product_options = channel.request('get:options', 'products');
    },
    getTvaLabel(){
        let res = "";
        let current_value = this.model.get('tva');
        _.each(this.tva_options, function(tva){
            if (tva.value == current_value){
                res = tva.name;
            }
        });
        return res
    },
    getProductLabel(){
        let res = "";
        let current_value = this.model.get('product_id');
        _.each(this.product_options, function(product){
            if (product.id == current_value){
                res = product.label;
            }
        });
        return res
    },
    onRender(){
        const view = new PercentDisplayWidget({
            'left': this.model.get('current_percent_left'),
            'current': this.model.get('current_percent'),
            'done': 100 - this.model.get('percent_left'),
        });
        this.showChildView('slider_container', view);
    },
    templateContext(){
        return {
            ht_to_invoice_label: this.model.total_ht_to_invoice_label(),
            tva_to_invoice_label: this.model.tva_to_invoice_label(),
            ttc_to_invoice_label: this.model.total_ttc_to_invoice_label(),
            percent_editable: this.model.isEditable(),
        };

    },
    onSliderClicked(){
        console.log("Slider clicked");
        this.trigger('edit', this);
    },
});
export default TaskLineView;
