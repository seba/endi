import $ from 'jquery';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ActionCollection from '../../common/models/ActionCollection.js';
import ActionListView from '../../common/views/ActionListView.js';

var template = require("../../common/views/templates/ActionView.mustache");

const PriceStudyActionView = Mn.View.extend({
    regions: {
        container: "#more-actions",
    },
    ui: {
        statusButtons: '.btn-status',
    },
    events: {
        'click @ui.statusButtons': 'onStatusButtonClick'
    },
    modelEvents: {
        'change': 'render'
    },
    template: template,
    onStatusButtonClick: function(event){
        let target = $(event.target);
        let status = target.data('status');
        let title = target.data('title');
        let label = target.data('label');
        let url = target.data('url');
    },
    onRender: function(){
        const action_collection = new ActionCollection(
            this.getOption('actions')['price_study']
        );
        this.showChildView(
            'container',
            new ActionListView({collection: action_collection})
        );
    }
});
export default PriceStudyActionView;
