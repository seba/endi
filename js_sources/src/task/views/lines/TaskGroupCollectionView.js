import Mn from 'backbone.marionette';
import TaskGroupView from './TaskGroupView.js';

const TaskGroupCollectionView = Mn.CollectionView.extend({
    tagName: 'div',
    childView: TaskGroupView,
    collectionEvents: {
        'change:reorder': 'render',
        'sync': 'render'
    },
    childViewOptions(model){
        // Forward the edit option to the children
        return {edit: this.getOption('edit')}
    },
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'group:edit',
        'delete': 'group:delete',
        'catalog:insert': 'catalog:insert'
    },
    childViewEvents:{
        'order:up': "onChildViewOrderUp",
        'order:down': "onChildViewOrderDown",
    },
    onChildViewOrderUp: function(childView){
        console.log("Moving group up");
        this.collection.moveUp(childView.model);
    },
    onChildViewOrderDown: function(childView){
        console.log("Moving group down");
        this.collection.moveDown(childView.model);
    },
});

export default TaskGroupCollectionView;
