import Mn from 'backbone.marionette';
import { formatAmount } from '../../../math.js';

const PaymentDepositView = Mn.View.extend({
    tagName: 'tr',
    className: 'row taskline',
    template: require('./templates/PaymentDepositView.mustache'),
    modelEvents: {
        'change:amount': 'render'
    },
    templateContext(){
        return {
            show_date: this.getOption('show_date'),
            amount_label: formatAmount(this.model.get('amount'))
        }
    }
});
export default PaymentDepositView;
