import _ from 'underscore';
import Mn from 'backbone.marionette';
import AnchorWidget from '../../widgets/AnchorWidget.js';
import ToggleWidget from '../../widgets/ToggleWidget.js';
import POSTButtonWidget from '../../widgets/POSTButtonWidget.js';


const ActionListView = Mn.CollectionView.extend({
    childTemplates: {
        'anchor': AnchorWidget,
        'toggle': ToggleWidget,
        'POSTButton': POSTButtonWidget,
    },
    tagName: 'div',
    childView: function(item){
        const widget = this.childTemplates[item.get('widget')];
        if (_.isUndefined(widget)){
            console.log("Error : invalid widget type %s", item.get('widget'));
        }
        return widget;
    }
});
export default ActionListView;
