import _ from 'underscore';
import BaseModel from "../../../base/models/BaseModel.js";
import TaskLineCollection from './TaskLineCollection.js';
import { formatAmount } from '../../../math.js';

const TaskGroupModel = BaseModel.extend({
    props: [
        'id',
        'title',
        'description',
        'total_ht_to_invoice',
        'tva_to_invoice',
        'total_ttc_to_invoice',
        'percent_left', // Avant cette facture
        'current_percent', // Couramment configuré
        'current_percent_left',  // Restant après cette facture
        'line_statuses',
        'total_ht',
        'tva_amount',
        'total_ttc',
        "has_deposit",
    ],
    validation:{
        current_percent: function(value){
            if (value > this.get('percent_left')){
                return "Le pourcentage dépasse ce qu'il reste à facturer"
            }
        }
    },
    initialize: function(){
        this.populate();
        this.on('saved', this.updateLines.bind(this));
        this.listenTo(this.lines, 'current_percent_changed', this.updateCurrentPercent);
        this.on('change:current_percent', this.updateLineCurrentPercent);
    },
    populate: function(){
        this.lines = new TaskLineCollection(this.get('line_statuses'));
    },
    updateLines(){
        this.lines.set(this.get('line_statuses'), {});
        this.lines.trigger('updated', this, {});
    },
    updateLineCurrentPercent(){
        /*
         * The current_percent value has been set
         */
        const current_percent = this.get('current_percent');
        if (!_.isNull(current_percent)){
            this.lines.updateCurrentPercent(current_percent);
        }
    },
    updateCurrentPercent(){
        /*
         * Update the current_percent value of the Group when one of the lines
         * has been set individually
         */
        if (this.isEditable()){
            let values = new Set();
            this.lines.each(
                function(model){values.add(model.get('current_percent'))}
            );
            if (values.length > 1){
                this.set('current_percent', null);
            }
        }
        this.save({wait: true});
    },
    ht: function(){
        return this.lines.ht();
    },
    tvaParts: function(){
        return this.lines.tvaParts();
    },
    ttc: function(){
        return this.lines.ttc();
    },
    toJSON(){
        return {'lines': this.lines.toJSON()};
    },
    isEditable(){
        const value = this.get('percent_left');
        return ! _.isNull(value) && !_.isUndefined(value) && value > 0;
    },
    hasCurrentPercent(){
        const value = this.get('current_percent');
        return ! _.isNull(value) && !_.isUndefined(value) && this.isUnified();
    },
    isUnified(){
        // Renvoie False si l'ouvrage a été splitté dans sa configuration
        const value = this.get('percent_left');
        return ! _.isNull(value) && !_.isUndefined(value);
    },
    total_ht_to_invoice_label(){
        return formatAmount(this.get('total_ht_to_invoice', false, false));
    },
    tva_to_invoice_label(){
        return formatAmount(this.get('tva_to_invoice', false, false));
    },
    total_ttc_to_invoice_label(){
        return formatAmount(this.get('total_ttc_to_invoice', false, false));
    },
    total_ht_label(){
        return formatAmount(this.get('total_ht', false, false));
    },
    tva_label(){
        return formatAmount(this.get('tva_amount', false, false));
    },
    total_ttc_label(){
        return formatAmount(this.get('total_ttc', false, false));
    }
});
export default TaskGroupModel;
