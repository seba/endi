import _ from 'underscore';
import Bb from 'backbone';
import Radio from 'backbone.radio';
import { formatAmount, strToFloat, getTvaPart, getPercent } from '../../../math.js';
import BaseModel from "../../../base/models/BaseModel.js";

const TaskLineModel = BaseModel.extend({
    props: [
        'id',
        'description',
        'tva',
        'product_id',
        'task_id',
        // Globallement dans l'affaire
        'total_ht_to_invoice',
        'tva_to_invoice',
        'total_ttc_to_invoice',
        'percent_left', // Avant cette facture
        'current_percent', // Couramment configuré
        'current_percent_left',  // Restant après cette facture
        // Totaux pour cette entrée
        'total_ht',
        'tva_amount',
        'total_ttc',
        "has_deposit",
    ],
    ht: function(){
        return parseFloat(this.get('total_ht'));
    },
    tva_value: function(){
        var tva = this.get('tva');
        if (tva < 0){
            tva = 0;
        }
        return tva;
    },
    tva: function(){
        return parseFloat(this.get('tva_amount'));
    },
    ttc: function(){
        return this.ht() + this.tva();
    },
    toJSON(){
        return {
            'id': this.get('id'),
            'current_percent': this.get('current_percent')
        }
    },
    isEditable(){
        const percent_left = this.get('percent_left');
        return percent_left > 0;
    },
    total_ht_to_invoice_label(){
        return formatAmount(this.get('total_ht_to_invoice', false, false));
    },
    tva_to_invoice_label(){
        return formatAmount(this.get('tva_to_invoice', false, false));
    },
    total_ttc_to_invoice_label(){
        return formatAmount(this.get('total_ttc_to_invoice', false, false));
    },
    total_ht_label(){
        return formatAmount(this.get('total_ht', false, false));
    },
    tva_label(){
        return formatAmount(this.get('tva_amount', false, false));
    },
    total_ttc_label(){
        return formatAmount(this.get('total_ttc', false, false));
    }
});

export default TaskLineModel;
