import Bb from 'backbone';
import Radio from 'backbone.radio';
import {
    strToFloat,
    getTvaPart,
    htFromTtc,
    formatAmount,
} from '../../math.js';
import BaseModel from '../../base/models/BaseModel.js';

const DiscountModel = BaseModel.extend({
    props: [
        'id',
        'amount',
        'tva',
        'ht',
        'description',
        'mode',
    ],
    defaults: function(){
        const result = {};
        result['mode'] = Radio.channel('config').request('get:options', 'compute_mode');
        return result;
    },
    validation: {
        description: {
            required: true,
            msg: "Remise : Veuillez saisir un objet",
        },
        amount: {
            required: true,
            pattern: "amount",
            msg: "Remise : Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        tva: {
            required: true,
            pattern: "number",
            msg: "Remise : Veuillez sélectionner une TVA"
        },
    },
    ht: function(){
        if (this.get('mode') === 'ht') {
            return -1 * strToFloat(this.get('amount'));
        } else {
            return htFromTtc(this.ttc(), this.tva_value());
        }
    },
    tva_value: function(){
        var tva = this.get('tva');
        if (tva < 0){
            tva = 0;
        }
        return tva;
    },
    tva: function(){
        return getTvaPart(this.ht(), this.tva_value());
    },
    ttc: function(){
        if (this.get('mode') === 'ht') {
            return this.ht() + this.tva();
        } else {
            return strToFloat(this.get('cost')) * strToFloat(this.get('quantity'));
        }
    },
    amount_label(){
        return formatAmount(this.get('amount'), false, false);
    }
});
export default DiscountModel;
