import _ from 'underscore';
import Bb from 'backbone';
import Radio from 'backbone.radio';
import {
    strToFloat,
    getTvaPart,
    htFromTtc,
} from '../../math.js';
import BaseModel from "../../base/models/BaseModel.js";

const TaskLineModel = BaseModel.extend({
    props: [
        'id',
        'order',
        'description',
        'cost',
        'quantity',
        'unity',
        'tva',
        'product_id',
        'task_id',
        'mode',
    ],
    validation: {
        description: {
            required: true,
            msg: "Veuillez saisir un objet",
        },
        cost: {
            required: true,
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        quantity: {
            required: true,
            pattern: "amount",
            msg: "Veuillez saisir une quantité, dans la limite de 5 chiffres après la virgule",
        },
        tva: {
            required: true,
            pattern: "number",
            msg: "Veuillez sélectionner une TVA"
        },
    },
    defaults(){
        let config = Radio.channel('config');
        return config.request('get:options', 'defaults');
    },
    ht: function(){
        if (this.get('mode') === 'ht') {
            return strToFloat(this.get('cost')) * strToFloat(this.get('quantity'));
        } else {
            return htFromTtc(this.ttc(), this.tva_value());
        }
    },
    tva_value: function(){
        var tva = this.get('tva');
        if (tva < 0){
            tva = 0;
        }
        return tva;
    },
    tva: function(){
        var val = getTvaPart(this.ht(), this.tva_value());
        return val;
    },
    ttc: function(){
        if (this.get('mode') === 'ht') {
            return this.ht() + this.tva();
        } else {
            return strToFloat(this.get('cost')) * strToFloat(this.get('quantity'));
        }
    },
    loadProduct: function(product_model){

        this.set('description', product_model.get('description'));
        this.set('cost', product_model.get('ht'));
        this.set('quantity', 1);
        this.set('unity', product_model.get('unity'));

        if (product_model.has('tva_id')){
            const config = Radio.channel('config');
            const tva_options = config.request('get:options', 'tvas');

            let tva_id = product_model.get('tva_id');
            this.set('tva_id', tva_id);

            const tva = this.findLabelFromId('tva_id', 'value', tva_options);
            this.set('tva', tva);
            console.log("Setting vta %s", tva);
        }

        this.set('product_id', product_model.get('product_id'));
        this.trigger('set:product');
    }
});

export default TaskLineModel;
