import Bb from 'backbone';
const DiscountPercentModel = Bb.Model.extend({
    validation: {
        description: {
            required: true,
            msg: "Veuillez saisir un objet",
        },
        percentage: {
            required: true,
            range: [1, 99],
            msg: "Veuillez saisir un pourcentage",
        },
        tva: {
            required: true,
            pattern: "number",
            msg: "Veuillez sélectionner une TVA"
        },
    },

});
export default DiscountPercentModel;
