import _ from 'underscore';
import TaskLineCollection from './TaskLineCollection.js';
import BaseModel from "../../base/models/BaseModel.js";


const TaskGroupModel = BaseModel.extend({
    props: [
        'id',
        'order',
        'title',
        'description',
        'lines',
        'task_id',
    ],
    validation:{
        lines: function(value){
            if (value.length === 0){
                return "Veuillez saisir au moins un produit";
            }
        }
    },
    initialize: function(){
        this.populate();
        this.on('change:id', this.populate.bind(this));
        this.listenTo(this.lines, 'saved', this.updateLines);
        this.listenTo(this.lines, 'destroyed', this.updateLines);
    },
    populate: function(){
        if (this.get('id')){
            this.lines = new TaskLineCollection(this.get('lines'));
            this.lines.url = this.url() + '/task_lines';
        }
    },
    updateLines(){
        this.set('lines', this.lines.toJSON());
    },
    ht: function(){
        return this.lines.ht();
    },
    tvaParts: function(){
        return this.lines.tvaParts();
    },
    ttc: function(){
        return this.lines.ttc();
    }
});
export default TaskGroupModel;
