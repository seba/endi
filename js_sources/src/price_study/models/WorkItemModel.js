/*
 * File Name :  WorkItemModel
 */
import BaseModel from '../../base/models/BaseModel.js';
import Radio from 'backbone.radio';
import { strToFloat, getTvaPart, round } from '../../math.js';
import { formatAmount } from '../../math.js';

const WorkItemModel = BaseModel.extend({
    props: [
        "id",
        "type_",
        "label",
        "description",
        "ht",
        "supplier_ht",
        "work_unit_quantity",
        "total_quantity",
        'quantity_inherited',
        "unity",
        "work_unit_ht",
        "total_ht",
        "tva_id",
        "tva_id_editable",
        "product_id",
        "product_id_editable",
        "general_overhead",
        "general_overhead_editable",
        "margin_rate",
        "margin_rate_editable",
        'base_sale_product_id',
        "sync_catalog",
        'uptodate'
    ],
    inherited_props: ['general_overhead', 'margin_rate', 'tva_id', 'product_id'],
    defaults() {
        let config = Radio.channel('config');
        let defaults = config.request('get:options', 'defaults');
        return {
            work_unit_quantity: 1,
            quantity_inherited: true,
            general_overhead_editable: true,
            margin_rate_editable: true,
        }
    },
    validation: {
        label: {required: true, msg: "Veuillez saisir un nom"},
        description: {required: true, msg: "Veuillez saisir une description"},
        tva_id: {required: true, msg: "Veuillez choisir le taux de Tva à appliquer"},
        product_id: {required: true, msg: "Veuillez choisir le type de produit"},
        work_unit_quantity: {
            required: function(value, attr, computedState){
                if (! this.get('locked')){
                    return true;
                }
            }
        }
    },
    initialize: function(){
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products'
        );
    },
    supplier_ht_label(){
        return formatAmount(this.get('supplier_ht'), false, false);
    },
    ht_label(){
        return formatAmount(this.get('ht'), false, false);
    },
    tva_label(){
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    product_label(){
        return this.findLabelFromId('product_id', 'label', this.product_options);
    },
    total_ht_label(){
        return formatAmount(this.get('total_ht'), false, false);
    },
    work_unit_ht_label(){
      return formatAmount(this.get('work_unit_ht'), false, false);
    },
    loadFromCatalog(model){
        let datas = model.toJSON();
        datas['base_sale_product_id'] = datas['id'];
        datas['locked'] = false;
        delete(datas['id']);
        delete(datas['type_']);
        console.log("ProductModel.loadFromCatalog");
        this.set(datas);
    },
    ht(){
        /* Return the ht value of this entry */
        return strToFloat(this.get('total_ht'));
    },
    tva(){
        /*
         * Compute tva value and return a dict {tva_id: tva_amount}
         */
        let tva_value = this.findLabelFromId('tva_id', 'value', this.tva_options);
        return getTvaPart(this.ht(), tva_value);
    },
    ttc(){
        /* Return the ttc value of this entry */
        return this.ht() + this.tva();
    },
    isFromParent(attribute){
        /*
        * Check if the given attribute's value comes from the parent ProductWork
        */
        let result;
        if (! this.inherited_props.includes(attribute)){
            result = false;
        } else if (! this.get(attribute + '_editable')){
            result = true;
        } else {
            result = false;
        }
        return result;
    },
    supplier_ht_mode(){
        if (_.isNull(this.get('supplier_ht')) || ! this.has('supplier_ht') || this.get('supplier_ht') == 0){
            return false;
        }else{
            return true;
        }
    },
});
export default WorkItemModel;
