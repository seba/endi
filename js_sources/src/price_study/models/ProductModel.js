/*
 * File Name : ProductModel.js
 *
 */
import BaseModel from "../../base/models/BaseModel.js";
import Radio from 'backbone.radio';
import { getTvaPart, formatAmount, strToFloat, round } from '../../math.js';
import DuplicableMixin from '../../base/models/DuplicableMixin.js';


const ProductModel = BaseModel.extend(DuplicableMixin).extend({
    props: [
        'id',
        'type_',
        'label',
        'description',
        'supplier_ht',
        'general_overhead',
        'margin_rate',
        "quantity",
        'ht',
        'total_ht',
        'unity',
        'tva_id',
        'product_id',
        "base_sale_product_id",
        "order",
        'uptodate',
    ],
    validation: {
        'description': {required: true, msg: "Veuillez saisir une description"},
         ht: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        supplier_ht: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule",
        },
		general_overhead: {
            required: false,
            pattern: 'amount',
            msg: "Le coefficient de frais généraux doit être un nombre, dans la limite de 5 chiffres après la virgule"
        },
        margin_rate: {
            required: false,
            pattern: 'amount',
            msg: "Le coefficient de marge doit être un nombre, dans la limite de 5 chiffres après la virgule"
        },
        tva_id: {required: true, msg:'Requis'},
        product_id: {required: true, msg:'Requis'},
    },
    defaults(){
        console.log("In Product model defaults");
        let config = Radio.channel('config');
        let form_defaults = config.request('get:options', 'defaults');
        return {
            type_: 'price_study_product',
            tva_id: form_defaults['tva_id'],
            margin_rate: form_defaults['margin_rate'],
            general_overhead: form_defaults['general_overhead'],
            quantity: 1,
        };
    },
    initialize: function(){
        ProductModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.tva_options = this.config.request('get:options', 'tvas');
    },
    product_label(){
        const product_options = this.config.request('get:options', 'products');
        return this.findLabelFromId('product_id', 'label', product_options);
    },
    tva_label(){
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    ht_label(){
        return formatAmount(this.get('ht'), false, false);
    },
    total_ht_label(){
        return formatAmount(this.get('total_ht'), false, false);
    },
    supplier_ht_label(){
        return formatAmount(this.get('supplier_ht'), false, false);
    },
    supplier_ht_mode(){
        if (_.isNull(this.get('supplier_ht')) || ! this.has('supplier_ht') || this.get('supplier_ht') == 0){
            return false;
        }else{
            return true;
        }
    },
    loadFromCatalog(model){
        let datas = model.toJSON();
        datas['base_sale_product_id'] = datas['id'];
        datas['sale_product_work_id'] = datas['id'];
        delete(datas['id']);
        delete(datas['type_']);
        this.set(datas);
    },
    validateModel(){
        return this.validate();
    },
    supplier_ht_mode(){
        if (_.isNull(this.get('supplier_ht')) || ! this.has('supplier_ht') || this.get('supplier_ht') == 0){
            return false;
        }else{
            return true;
        }
    },
});
export default ProductModel;
