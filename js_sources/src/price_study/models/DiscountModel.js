import Bb from 'backbone';
import Radio from 'backbone.radio';
import { formatAmount} from '../../math.js';
import BaseModel from '../../base/models/BaseModel.js';

const DiscountModel = BaseModel.extend({
    props: [
        'id',
        'type_',
        'amount',
        'tva_id',
        'description',
        'total_ht',
        'total_tva',
        'total_ttc',
        'order',
        'percentage',
    ],
    validation: {
        description: {
            required: true,
            msg: "Remise : Veuillez saisir un objet",
        },
        value: {
            required: function(value, attr, computedState){
                if (this.get('type_') == 'amount'){
                    return true;
                }
            },
            pattern: "amount",
            msg: "Remise : Veuillez saisir un montant"
        },
        percentage: {
            required: function(value, attr, computedState){
                if (this.get('type_') == 'percentage'){
                    return true;
                }
            },
            range: [1, 99],
            msg: "Veuillez saisir un pourcentage",
        },
        tva_id: {
            required: true,
            pattern: "number",
            msg: "Remise : Veuillez sélectionner une TVA"
        },
    },
    initialize: function(){
        BaseModel.__super__.initialize.apply(this, arguments);
        this.config = Radio.channel('config');
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
    },
    tva_label(){
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    total_ht_label(){
        return formatAmount(this.get('total_ht'), false, false);
    },
    is_percentage(){
        return this.get('type_') == 'percentage';
    }
});
export default DiscountModel;
