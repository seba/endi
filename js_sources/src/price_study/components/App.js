import Bb from 'backbone';
import Mn from 'backbone.marionette';
import Router from './Router.js';
import { hideLoader } from '../../tools.js';
import RootComponent from '../views/RootComponent.js';
import Controller from './Controller.js';


const AppClass = Mn.Application.extend({
    channelName: 'app',
    radioEvents: {
        "navigate": 'onNavigate',
        "show:modal": "onShowModal",
        "product:delete": "onModelDelete",
        "discount:delete": "onModelDelete",
        "product:duplicate": "onProductDuplicate",
    },
    region: '#js-main-area',
    onBeforeStart(app, options){
        console.log("AppClass.onBeforeStart");
        this.rootView = new RootComponent();
        this.controller = new Controller({rootView: this.rootView});
        this.router = new Router({controller: this.controller});
        console.log("AppClass.onBeforeStart finished");
    },
    onStart(app, options){
      this.showView(this.rootView);
      console.log("Starting the history");
      hideLoader();
      Bb.history.start();
    },
    onNavigate(route_name, parameters){
        console.log("App.onNavigate");
        let dest_route = route_name;
        if (!_.isUndefined(parameters)){
            dest_route += "/" + parameters;
        }
        window.location.hash = dest_route;
        Bb.history.loadUrl(dest_route);
    },
    onShowModal(view){
        this.controller.showModal(view);
    },
    onModelDelete(view){
        this.controller.modelDelete(view);
    },
    onProductDuplicate(view){
        this.controller.productDuplicate(view);
    }
});
const App = new AppClass();
export default App;
