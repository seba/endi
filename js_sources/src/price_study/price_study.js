/* global AppOption; */
import $ from 'jquery';
import _ from 'underscore';
import Mn from 'backbone.marionette';

import { applicationStartup } from '../backbone-tools.js';
import App from './components/App.js';
import Facade from './components/Facade.js';



$(function(){
    applicationStartup(AppOption, App, Facade);
});
