/*
 * Module name : PriceStudyComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import PriceStudyView from "./PriceStudyView.js";
import PriceStudyResume from "./PriceStudyResume.js";
import ProductComponent from "./ProductComponent.js";
import DiscountComponent from "./discount/DiscountComponent.js";
import ActionView from '../../common/views/ActionView.js';
import TaskListComponent from './TaskListComponent.js';
import ErrorView from '../../base/views/ErrorView.js';

const template = require('./templates/PriceStudyComponent.mustache');


const PriceStudyComponent = Mn.View.extend({
    template: template,
    regions: {
        more_actions: '.more-actions',
        tasks: '.tasks',
        common: '.common',
        products: '.products',
        discounts: '.discounts',
        resume: '.resume',
        errors: '.errors',
    },
    ui:{
        anchors: 'a'
    },
    events: {
        'click @ui.anchors': 'onAnchorClicked'
    },
    initialize(options){
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.tasks = this.config.request('get:options', 'tasks');
    },
    onRender(){

        if (this.tasks.length > 0){
            this.showChildView(
                'tasks',
                new TaskListComponent({tasks: this.tasks}),
            );
        }
        this.showChildView(
            'common',
            new PriceStudyView({model: this.model})
        );
        const collection = this.facade.request("get:collection", 'products');
        this.showChildView(
            'products',
            new ProductComponent({collection: collection})
        );
        const actions = this.config.request('get:form_actions');
        let view = new ActionView({actions: actions});
        this.showChildView('more_actions', view);
        view = new PriceStudyResume({model: this.model});
        this.showChildView('resume', view);
        this.showDiscounts();
    },
    showDiscounts(){
        const collection = this.facade.request("get:collection", 'discounts');
        this.showChildView(
            'discounts',
            new DiscountComponent({collection: collection})
        );
    },
    templateContext(){
        let editable = this.config.request('get:options', 'editable');
        return {
            editable:editable,
            has_tasks: this.tasks.length > 0
        };
    },
    formOk(){
        var result = true;
        var errors = this.facade.request('is:valid');
        if (!_.isEmpty(errors)){
            this.showChildView(
                'errors',
                new ErrorView({errors:errors})
            );
            result = false;
        } else {
            this.detachChildView('errors');
        }
        return result;
    },
    onAnchorClicked(event){
        console.log("onAnchorClicked");
        const button = $(event.target);
        console.log(button);
        if (button.hasClass('genestimation') || button.hasClass('geninvoice')){
            return this.formOk();
        }
        return true;
    }
});
export default PriceStudyComponent
