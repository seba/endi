/*
 * Module name : ProductEmptyView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

const template = require('./templates/ProductEmptyView.mustache');

const ProductEmptyView = Mn.View.extend({
    template: template,
});
export default ProductEmptyView
