/*
 * Module name : WorkItemView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

const template = require('./templates/WorkItemView.mustache');

const WorkItemView = Mn.View.extend({
    tagName: "tr",
    template: template,
    modelEvents: {'sync': 'render'},
    templateContext(){
        console.log("WorkItemView Calling the templating context")
        return {
            tva_label: this.model.tva_label(),
            ht_label: this.model.ht_label(),
            supplier_ht_label: this.model.supplier_ht_label(),
            supplier_ht_mode: this.model.supplier_ht_mode(),
            product_label: this.model.product_label(),
            work_unit_ht_label: this.model.work_unit_ht_label(),
            total_ht_label: this.model.total_ht_label(),
        };
    },
});
export default WorkItemView
