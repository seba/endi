/*
 * Module name : ProductView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import Validation from 'backbone-validation';
import ButtonModel from '../../base/models/ButtonModel.js';
import ButtonCollection from '../../base/models/ButtonCollection.js';
import ActionButtonsWidget from '../../widgets/ActionButtonsWidget.js';

const template = require('./templates/ProductView.mustache');

const ProductView = Mn.View.extend({
    tagName: 'div',
    className: 'product separate_block quotation_item',
    template: template,
    regions: {
        actions: "td.col_actions"
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {
        'action:clicked': 'onActionClicked'
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');

        this.facade = Radio.channel('facade');

        this.listenTo(this.facade, 'bind:validation', this.bindValidation);
        this.listenTo(this.facade, 'unbind:validation', this.unbindValidation);
        this.listenTo(this.model, 'validated:invalid', this.showErrors);
        this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
    },
    showErrors(model, errors){
        this.$el.addClass('error');
    },
    hideErrors(model){
        this.$el.removeClass('error');
    },
    bindValidation(){
        Validation.bind(this);
    },
    unbindValidation(){
        Validation.unbind(this);
    },
    showActions(){
        let min_order = this.model.collection.getMinOrder();
        let max_order = this.model.collection.getMaxOrder();
        let order = this.model.get('order');


        const collection = new ButtonCollection();
        let buttons = [
            {
                label: "Dupliquer",
                action: "duplicate",
                icon: "copy",
            },
            {
                label: "Supprimer",
                action: "delete",
                icon: "trash-alt",
                css: 'negative',
            }
        ];
        if (order != min_order){
            buttons.push({
                label: "Remonter",
                action: "up",
                icon: "arrow-up"

            });
        }
        if (order != max_order){
            buttons.push({
                label: "Descendre",
                action: "down",
                icon: "arrow-down"

            });
        }
        const primary = new ButtonModel({
            label: "Modifier / Voir le détail",
            action: "edit",
            icon: "pen",
            showLabel: false
        });


        collection.add(buttons);

        const view = new ActionButtonsWidget({
            collection: collection,
            dropdownLabel: 'Actions',
            icon: "dots",
            showLabel: false,
            primary: primary
        });
        this.showChildView('actions', view);
    },
    onRender(){
        const editable = this.config.request('get:options', "editable");
        if (editable){
            this.showActions();
        }
    },
    templateContext(){
        return {
            supplier_ht_mode: this.model.supplier_ht_mode(),
            supplier_ht_label: this.model.supplier_ht_label(),
            ht_label: this.model.ht_label(),
            total_ht_label: this.model.total_ht_label(),
            tva_label: this.model.tva_label(),
            product_label: this.model.product_label(),
        };
    },
    onActionClicked(action_name){
        console.log("onActionClicked %s", action_name)
        this.triggerMethod('model:' + action_name, this);
    }
});
export default ProductView
