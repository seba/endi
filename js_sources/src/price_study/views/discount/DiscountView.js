/*
 * Module name : DiscountView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ButtonModel from "../../../base/models/ButtonModel.js";
import ButtonWidget from "../../../widgets/ButtonWidget.js"

const template = require('./templates/DiscountView.mustache');

const DiscountView = Mn.View.extend({
    template: template,
    tagName: 'tr',
    regions: {
        editButtonContainer: {el: '.col_actions .edit', replaceElement: true},
        delButtonContainer: {el: '.col_actions .delete', replaceElement: true},
    },
    // Listen to child view events
    childViewEvents: {
        'action:clicked': 'onActionClicked'
    },
    modelEvents: {
        'sync': 'render'
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onRender(){
        let editModel = new ButtonModel({
            ariaLabel: 'Modifier cet élément',
            icon:'pen',
            showLabel: false,
            action: 'edit'
        });
        let deleteModel = new ButtonModel({
            ariaLabel: 'Supprimer cet élément',
            icon: 'trash-alt',
            showLabel: false,
            action: 'delete'
        });
        this.showChildView(
            'editButtonContainer',
            new ButtonWidget({model: editModel})
        );
        this.showChildView(
            'delButtonContainer',
            new ButtonWidget({model: deleteModel})
        );
    },
    templateContext(){
        return {
            tva_label: this.model.tva_label(),
            is_percentage: this.model.is_percentage(),
            total_ht_label: this.model.total_ht_label()
        };
    },
    onActionClicked(action){
        if (action == 'edit'){
            this.triggerMethod('edit', this);
        } else if (action == 'delete'){
            this.triggerMethod('delete', this);
        }
    }
});
export default DiscountView
