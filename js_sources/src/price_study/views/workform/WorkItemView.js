/*
 * Module name : WorkItemView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import ButtonModel from '../../../base/models/ButtonModel.js';
import FormBehavior from '../../../base/behaviors/FormBehavior.js';
import InputWidget from '../../../widgets/InputWidget.js';

import ButtonWidget from '../../../widgets/ButtonWidget.js';
import TextAreaWidget from '../../../widgets/TextAreaWidget.js';
import SelectWidget from '../../../widgets/SelectWidget.js';

const template = require('./templates/WorkItemView.mustache');

const WorkItemView = Mn.View.extend({
    template: template,
    tagName: 'tr',
    regions: {
        editButtonContainer: {el: '.col_actions .edit', replaceElement: true},
        delButtonContainer: {el: '.col_actions .delete', replaceElement: true},
    },
    childViewEvents: {
       'action:clicked': 'onActionClicked'
    },
    modelEvents: {
        'sync': 'render'
    },
    initialize(){
        this.config = Radio.channel('config');
        this.unity_options = this.config.request(
            'get:options',
            'unities'
        );
    },
    onRender(){
        let editModel = new ButtonModel({
            ariaLabel: 'Modifier cet élément',
            icon:'pen',
            showLabel: false,
            action: 'edit'
        });
        let deleteModel = new ButtonModel({
            ariaLabel: 'Supprimer cet élément',
            icon: 'trash-alt',
            showLabel: false,
            action: 'delete'
        });
        this.showChildView(
            'editButtonContainer',
            new ButtonWidget({model: editModel})
        );
        this.showChildView(
            'delButtonContainer',
            new ButtonWidget({model: deleteModel})
        );
    },
    templateContext(){
        console.log("WorkItemView Calling the templating context")
        return {
            tva_label: this.model.tva_label(),
            ht_label: this.model.ht_label(),
            supplier_ht_label: this.model.supplier_ht_label(),
            supplier_ht_mode: this.model.supplier_ht_mode(),
            product_label: this.model.product_label(),
            work_unit_ht_label: this.model.work_unit_ht_label(),
            total_ht_label: this.model.total_ht_label(),
        };
    },
    onActionClicked(action){
        if (action == 'edit'){
            this.triggerMethod('model:edit', this.model, this);
        } else if (action == 'delete'){
            this.triggerMethod('model:delete', this.model, this);
        }
    }
});
export default WorkItemView
