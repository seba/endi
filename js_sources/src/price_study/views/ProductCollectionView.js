/*
 * Module name : ProductCollectionView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ProductView from './ProductView.js';
import ProductEmptyView from './ProductEmptyView.js';
import WorkView from './WorkView.js';

const ProductCollectionView = Mn.CollectionView.extend({
    emptyView: ProductEmptyView,
    collectionEvents: {
        'change:reorder': 'render',
        sync: 'render'
    },
    childView: function(model){
       if (model.get('type_') == 'price_study_work'){
           return WorkView;
       } else {
           return ProductView;
       }
    },
    // Bubble up child view events
    childViewTriggers: {
        'model:edit': 'product:edit',
        'model:delete': 'product:delete',
        'model:duplicate': 'product:duplicate',
        'model:down': 'product:down',
        'model:up': 'product:up',
    },
});
export default ProductCollectionView
