/*
 * Module name : RootComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import DiscountForm from './discount/DiscountForm.js';
import ProductForm from './ProductForm.js';
import PriceStudyComponent from "./PriceStudyComponent.js";
import AddWorkForm from './workform/AddWorkForm.js';
import WorkForm from './workform/WorkForm.js';

const template = require('./templates/RootComponent.mustache');

const RootComponent = Mn.View.extend({
    className:'main_content',
    template: template,
    regions: {
        main: '.main',
        modalContainer: '.modal-container',
    },
    childViewEvents: {
        'product:delete': "onModelDelete",
        'product:duplicate': 'onProductDuplicate'
    },
    initialize(){
        this.facade = Radio.channel('facade');
        this.initialized = false;
    },
    index(){
        this.initialized = true;
        let model = this.facade.request('get:model', 'price_study');
        const view = new PriceStudyComponent({model: model});
        this.showChildView('main', view);
    },
    showModal(view){
        this.showChildView('modalContainer', view);
    },
    _showProductForm(model, collection, edit){
        this.index();
        let view = new ProductForm(
            {model: model, destCollection: collection, edit: edit}
        );
        this.showModal(view);
    },
    showAddProductForm(model, collection){
        this._showProductForm(model, collection, false);
    },
    showEditProductForm(model){
        this._showProductForm(model, model.collection, true);
    },
    _showDiscountForm(model, collection, edit){
        this.index();
        let view = new DiscountForm(
            {model: model, destCollection: collection, edit: edit}
        );
        this.showModal(view);
    },
    showAddDiscountForm(model, collection){
        this._showDiscountForm(model, collection, false);
    },
    showEditDiscountForm(model){
        this._showDiscountForm(model, model.collection, true);
    },
    showAddWorkForm(model, collection){
        this.index();
        let view = new AddWorkForm(
            {model: model, destCollection: collection}
        );
        this.showModal(view);
    },
    showEditWorkForm(model){
        const view = new WorkForm({model: model});
        this.showChildView('main', view);
    },
});
export default RootComponent
