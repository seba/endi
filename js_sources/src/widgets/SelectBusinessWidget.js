import _ from 'underscore';
import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from "../tools.js";

var template = require('./templates/SelectBusinessWidget.mustache');

function fill_project_options(widget_options, project_field, customer_id) {
    var project_options = [];
    for (var i = 0; i<widget_options.length; i++){
        if(widget_options[i].id==customer_id) {
            project_options = widget_options[i].projects;
            break;
        }
    }
    project_field.append($('<option>', {
        value: "",
        text: "- Choisir un dossier -",
        selected: "selected"
    }));
    for (var j = 0; j<project_options.length; j++){
        project_field.append($('<option>', {
            value: project_options[j].id,
            text: project_options[j].label
        }));
    }
}

function fill_business_options(widget_options, business_field, customer_id, project_id) {
    var business_options = [];
    for (var i = 0; i<widget_options.length; i++){
        if(widget_options[i].id==customer_id) {
            var project_options = widget_options[i].projects;
            for (var j = 0; j<project_options.length; j++){
                if(project_options[j].id==project_id) {
                    business_options = project_options[j].businesses;
                    break;
                }
            }
        }
    }
    business_field.append($('<option>', {
        value: "",
        text: "- Choisir une affaire -",
        selected: "selected"
    }));
    for (var k = 0; k<business_options.length; k++){
        business_field.append($('<option>', {
            value: business_options[k].id,
            text: business_options[k].label
        }));
    }
}


const SelectBusinessWidget = BaseFormWidget.extend({
    tagName: 'div',
    className: 'form-group',
    template: template,
    ui: {
        select_customer: 'select[id=expense_customer]',
        select_project: 'select[id=expense_project]',
        select_business: 'select[id=expense_business]',
    },
    events: {
        'change @ui.select_customer': "onCustomerChange",
        'change @ui.select_project': "onProjectChange",
        'change @ui.select_business': "onBusinessChange",
    },
    onCustomerChange: function(event){
        var customer_id = this.getUI('select_customer').val();
        var project_field = this.getUI('select_project');
        var business_field = this.getUI('select_business');
        project_field.empty();
        project_field.hide();
        business_field.empty();
        business_field.hide();
        if(customer_id!="") {
            var widget_options = this.getOption('options');
            fill_project_options(widget_options, project_field, customer_id);
            project_field.show();
            this.triggerMethod(
                "finish",
                "customer_id",
                customer_id
            );
        }
    },
    onProjectChange: function(event){
        var customer_id = this.getUI('select_customer').val();
        var project_id = this.getUI('select_project').val();
        var business_field = this.getUI('select_business');
        business_field.empty();
        business_field.hide();
        if(customer_id!="") {
            var widget_options = this.getOption('options');
            fill_business_options(widget_options, business_field, customer_id, project_id);
            business_field.show();
            this.triggerMethod(
                "finish",
                "project_id",
                project_id
            );
        }
    },
    onBusinessChange: function(event){
        var business_id = this.getUI('select_business').val();
        this.triggerMethod(
            "finish",
            "business_id",
            business_id
        );
    },
    onAttach: function(){
        var widget_options = this.getOption('options');
        var customer_field = this.getUI('select_customer');
        var project_field = this.getUI('select_project');
        var business_field = this.getUI('select_business');
        var current_customer = this.getOption('customer_value');
        var current_project = this.getOption('project_value');
        var current_business = this.getOption('business_value');
        project_field.hide();
        business_field.hide();
        if(current_customer!=null) {
            customer_field.val(current_customer);
            fill_project_options(widget_options, project_field, current_customer);
            project_field.show();
            if(current_project!=null) {
                project_field.val(current_project);
                fill_business_options(widget_options, business_field, current_customer, current_project);
                if(current_business!=null) business_field.val(current_business);
                business_field.show();
            }
        }
    },
    templateContext: function(){
        let ctx = this.getCommonContext();
        var options = this.getOption('options');

        let more_ctx = {
            options:options,
            id_key: "id",
        }
        return Object.assign(ctx, more_ctx);
    },
});
export default SelectBusinessWidget;
