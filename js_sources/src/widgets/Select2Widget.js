import SelectWidget from './SelectWidget.js'


const Select2Widget = SelectWidget.extend({
    /*
     * A select2 widget
     *
     * Share the API of SelectWidget
     */
    onAttach: function() {
        this.getUI('select').select2();
    },

    getCurrentValues: function(){
        return this.getUI('select').val();
    },

});
export default Select2Widget;
