import Bb from 'backbone';
import ActionModel from './ActionModel.js';


const ActionCollection = Bb.Collection.extend({
    model: ActionModel
});
export default ActionCollection;
