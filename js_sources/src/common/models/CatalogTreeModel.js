/*
 * File Name :  CatalogTreeModel
 */
import Bb from 'backbone';
import {formatAmount} from '../../math.js';


const CatalogTreeModel = Bb.Model.extend({
    matchPattern(search){
        // i for non-case-sensitive
        const regexp = new RegExp(search, 'i');
        if (
            (this.get('label').search(regexp) !== -1) ||
            (this.get('description').search(regexp) !== -1) ||
            (this.get('category_label').search(regexp) !== -1)
            ) {
            return true;
        }
        return false;
    },
    ht_label(){
        return formatAmount(this.get('ht'), false, false);
    },
    supplier_ht_label(){
        return formatAmount(this.get('supplier_ht'), false, false);
    }
});
export default CatalogTreeModel;
