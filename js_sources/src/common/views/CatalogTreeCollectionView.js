/*
 * Module name : CatalogTreeCollectionView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import CatalogTreeItemView from './CatalogTreeItemView.js';


const template = require('./templates/CatalogTreeCollectionView.mustache');

const CatalogTreeCollectionView = Mn.CollectionView.extend({
    template: template,
    childView: CatalogTreeItemView,
    childViewContainer: 'tbody',
});
export default CatalogTreeCollectionView
