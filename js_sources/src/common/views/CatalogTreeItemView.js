/*
 * Module name : CatalogTreeItemView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import RadioWidget from '../../widgets/RadioWidget.js';
const template = require('./templates/CatalogTreeItemView.mustache');

const CatalogTreeItemView = Mn.View.extend({
    tagName: 'tr',
    template: template,
    regions: {radio: 'td.radio'},
    // Listen to child view events
    childViewEvents: {
        'finish': "onRadioChange"
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
        this.model.set('selected', false);
    },
    onRender(){
        this.showChildView(
            'radio',
            new RadioWidget({
                field_name: "check",
                ariaLabel: "Sélectionner ce produit",
                value: this.model.get('id')
            })
        );
    },
    onRadioChange(field_name, value){
        this.model.collection.setSelected(this.model);
    },
    templateContext(){
        return {
            ht_label: this.model.ht_label(),
        };
    }
});
export default CatalogTreeItemView
