import Mn from 'backbone.marionette';
import ModalBehavior from '../../base/behaviors/ModalBehavior.js';
import { serializeForm, ajax_call, showLoader, hideLoader } from '../../tools.js';
import Radio from 'backbone.radio';
import { parseDate, dateToIso } from '../../date.js';

var template = require("./templates/StatusView.mustache");

/** The modal to log a message about a new status
 */
const StatusView = Mn.View.extend({
    id: 'status-msg-modal',
    template: template,
    ui: {
        'textarea': 'textarea',
        btn_cancel: '.cancel',
        submit: 'button[type=submit]',
        form: 'form'
    },
    behaviors: {
        modal: {
            behaviorClass: ModalBehavior
        }
    },
    events: {
        'click @ui.btn_cancel': 'destroy',
        'click @ui.submit': 'onSubmit'
    },
    submitCallback: function(result){
    },
    onSubmit: function(event){
        event.preventDefault();
        let datas = serializeForm(this.getUI('form'));
        datas['submit'] = this.getOption('status');
        const url = this.getOption('url');
        showLoader();
        this.serverRequest = ajax_call(url, datas, "POST");
        this.serverRequest.then(
            this.submitCallback.bind(this)
        );
    },
    templateContext: function(){
        let result = {
            title: this.getOption('title'),
            label: this.getOption('label'),
            status: this.getOption('status'),
            url: this.getOption('url'),
        }
        return result;
    }
});
export default StatusView;
