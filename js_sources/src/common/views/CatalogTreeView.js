/*
 * Module name : CatalogTreeView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import InputWidget from '../../widgets/InputWidget.js';
import CatalogTreeCollectionView from './CatalogTreeCollectionView.js';

const template = require('./templates/CatalogTreeView.mustache');

const CatalogTreeView = Mn.View.extend({
    template: template,
    regions: {
        filter: '.field-filter',
        treeContainer: '.tree-container',
    },
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {
        "change": "onFilterKeyUp"
    },
    // Bubble up child view events
    childViewTriggers: {
    },
    initialize(){
        this.config = Radio.channel('config');
    },
    onRender(){
        this.showChildView(
            'filter',
            new InputWidget({
                field_name: 'filter',
                placeholder: 'Filtrer',
                ariaLabel: 'Filtrer par nom/référence',
            })
        );
        this.showChildView(
            'treeContainer',
            new CatalogTreeCollectionView({collection: this.collection})
        );
    },
    onFilterKeyUp(fieldName, value){
        value = value.toLowerCase().trim();
        if (value.length > 1){
            const filter = function(view, index, children){
                if (view.model.matchPattern(value) || view.selected){
                    return true;
                }
            }
            this.getChildView('treeContainer').setFilter(filter);
        } else {
            this.getChildView('treeContainer').removeFilter();
        }
    }
});
export default CatalogTreeView
