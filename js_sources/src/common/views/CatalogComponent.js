/*
 * Module name : CatalogComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import LoadingWidget from '../../widgets/LoadingWidget.js';
import {getOpt} from "../../tools.js";
import CatalogTreeView from './CatalogTreeView.js';

const template = require('./templates/CatalogComponent.mustache');

const CatalogComponent = Mn.View.extend({
    /*
    Class displaying the sale_product catalog allowing item selection

    Expects the following options :

        collection_name

            The name of the collection in the associated Facade

        params

            The parameters used when querying the server to get the catalog items


    Emits the following events :

        'catalog:insert' => (model)

            Emitted when the Insert button is clicked
            Pass the current model :  instance of `class:CatalogTreeModel`


    */
    template: template,
    regions: {
        main: '.main',
    },
    ui: {
        insert_btn: 'button[value=insert]',
        load_btn: 'button[value=load]',
        cancel_btn: 'button.reset',
        form: 'form'
    },
    // Listen to the current's view events
    events: {
        'click @ui.insert_btn': "onInsertClicked",
    },
    triggers: {
        'click @ui.cancel_btn': 'cancel:click'
    },
    initialize(){
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
        this.app = Radio.channel('app');
    },
    showTree(tree){
        this.collection = tree;
        this.showChildView(
            'main',
            new CatalogTreeView({collection: tree})
        );
        this.collection.on(
            'change:selected',
            this.onItemSelect,
            this
        );
    },
    loadCatalogTree(){
        let serverRequest = this.facade.request(
            'load:collection',
            this.getOption('collection_name'),
            getOpt(this, 'params', {})
        );
        let this_ = this;
        serverRequest.done(this.showTree.bind(this));
    },
    onRender(){
        this.myid = _.uniqueId();
        console.log(this.myid);
        this.showChildView('main', new LoadingWidget());
        this.loadCatalogTree();
    },
    onItemSelect: function(){
        if (this.collection){
            let model = this.collection.getSelected();
            let btn = this.getUI('insert_btn');
            btn.attr('disabled', false);
            btn.attr('aria-disabled', false);
        }
    },
    onDomRemove(){
        this.collection.off('change:selected');
    },
    onInsertClicked(){
        let models = this.collection.getSelected();
        this.triggerMethod('catalog:insert', models);
    }
});
export default CatalogComponent;