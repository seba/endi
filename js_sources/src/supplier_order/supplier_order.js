/* global AppOption; */
import $ from 'jquery';
import Mn from 'backbone.marionette';
import App from './components/App.js';
import Facade from './components/Facade.js';
import AppAction from './components/AppAction.js';
import ExpenseTypeService from "../common/components/ExpenseTypeService.js";
import { applicationStartup } from '../backbone-tools.js';
import ActionView from "../common/views/ActionView.js";


AppAction.on('start', function(app, actions) {
    var view = new ActionView({actions: actions});
    AppAction.showView(view);
});


$(function(){
    applicationStartup(AppOption, App, Facade, AppAction, ExpenseTypeService);
});
