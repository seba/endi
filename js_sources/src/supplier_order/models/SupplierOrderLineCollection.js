import SupplierOrderLineModel from "./SupplierOrderLineModel.js";
import BaseLineCollection from '../../base/models/BaseLineCollection.js';

const SupplierOrderLineCollection = BaseLineCollection.extend({
    model: SupplierOrderLineModel,
});
export default SupplierOrderLineCollection;
