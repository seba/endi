import Mn from 'backbone.marionette';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
// import DatePickerWidget from '../../widgets/DatePickerWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import Radio from 'backbone.radio';


const SupplierOrderLineFormView = Mn.View.extend({
    id: 'supplierorderline-form',
    behaviors: [FormBehavior],
    template: require('./templates/SupplierOrderLineFormView.mustache'),
	regions: {
		// 'date': '.date',
		'type_id': '.type_id',
		'description': '.description',
		'ht': '.ht',
		'tva': '.tva',
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
    },
    initialize(){
        var channel = Radio.channel('config');
        this.type_options = this.getTypeOptions();
        // this.today = channel.request(
        //     'get:options',
        //     'today',
        // );
    },

    getTypeOptions() {
        var channel = Radio.channel('config');
        return channel.request(
            'get:typeOptions',
            'regular'
        );
    },

    onRender(){
        var view;
        
        // view = new DatePickerWidget({
        //     date: this.model.get('date'),
        //     title: "Date",
        //     field_name: "date",
        //     default_value: this.today,
        // });
        // this.showChildView("date", view);

        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Description',
            field_name: 'description'
        });
        this.showChildView('description', view);

        view = new InputWidget({
            value: this.model.get('ht'),
            title: 'Montant HT',
            field_name: 'ht',
            addon: "€",
        });
        this.showChildView('ht', view);

        view = new InputWidget({
            value: this.model.get('tva'),
            title: 'Montant TVA',
            field_name: 'tva',
            addon: "€",
        });
        this.showChildView('tva', view);


        view = new SelectWidget({
            value: this.model.get('type_id'),
            title: 'Type de frais',
            field_name: 'type_id',
            options: this.type_options,
            id_key: 'id',
        });

        this.showChildView('type_id', view);
    },
    templateContext: function(){
        return {
            title: this.getOption('title'),
            add: this.getOption('add'),
        };
    }
});
export default SupplierOrderLineFormView;
