import Mn from 'backbone.marionette';
import Bb from 'backbone';
import Radio from 'backbone.radio';

import StatusView from '../../common/views/StatusView.js';
import SupplierOrderLineModel from '../models/SupplierOrderLineModel.js';
import SupplierOrderLineTableView from './SupplierOrderLineTableView.js';
import SupplierOrderLineFormPopupView from './SupplierOrderLineFormPopupView.js';
import SupplierOrderFormView from './SupplierOrderFormView.js';
import SupplierOrderLineDuplicateFormView from './SupplierOrderLineDuplicateFormView.js';
import TotalView from './TotalView.js';
import MessageView from '../../base/views/MessageView.js';
import LoginView from '../../base/views/LoginView.js';
import {displayServerSuccess, displayServerError} from '../../backbone-tools.js';

const MainView = Mn.View.extend({
    className: 'container-fluid page-content',
    template: require('./templates/MainView.mustache'),
    regions: {
        modalRegion: '.modalRegion',
        supplierOrderForm: '.supplier-order',
        linesRegion: '.lines-region',
        totals: '.totals',
        messages: {
            el: '.messages-container',
            replaceElement: true,
        }
    },
    childViewEvents: {
        'line:add': 'onLineAdd',
        'line:edit': 'onLineEdit',
        'line:delete': 'onLineDelete',
        'order:modified': 'onDataModified',
        'line:duplicate': 'onLineDuplicate',
        'status:change': 'onStatusChange',
    },
    onDataModified() {
        let totals = this.facade.request('get:totalmodel');
        let order = this.facade.request('get:model');

        let ttc = totals.get('ttc');
        let ttc_cae = ttc * order.get('cae_percentage') / 100;
        let ttc_worker = ttc - ttc_cae;
        totals.set('ttc_cae', ttc_cae);
        totals.set('ttc_worker', ttc_worker);
    },
    initialize(){
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.edit = this.config.request('get:form_section', 'general')['edit'];
        this.listenTo(this.facade, 'status:change', this.onStatusChange);
    },
    showSupplierOrderForm(){
        var model = this.facade.request('get:model');
        var view = new SupplierOrderFormView({
            model:model,
            edit: this.edit,
        });

        this.showChildView('supplierOrderForm', view);
    },
    onLineAdd(childView){
        var model = new SupplierOrderLineModel({});
        this.showLineForm(model, true, "Ajouter une ligne");
    },
    onLineEdit(childView){
        this.showLineForm(childView.model, false, "Modifier une ligne");
    },
    showLineForm(model, add, title){
        var view = new SupplierOrderLineFormPopupView({
            title: title,
            add:add,
            model:model,
            destCollection: this.facade.request('get:collection', 'lines'),
        });
        this.showChildView('modalRegion', view);
    },
    showDuplicateForm(model){
        var view = new SupplierOrderLineDuplicateFormView({model: model});
        this.showChildView('modalRegion', view);
    },
    onLineDuplicate(childView){
        this.showDuplicateForm(childView.model);
    },
    onDeleteSuccess: function(){
        displayServerSuccess("Vos données ont bien été supprimées");
    },
    onDeleteError: function(){
        displayServerError("Une erreur a été rencontrée lors de la " +
                            "suppression de cet élément");
    },
    onLineDelete: function(childView){
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette ligne ?");
        if (result){
            childView.model.destroy(
                {
                    success: this.onDeleteSuccess,
                    error: this.onDeleteError
                }
            );
        }
     },
    showLinesRegion(){
        var collection = this.facade.request(
            'get:collection',
            'lines'
        );
        var view = new SupplierOrderLineTableView(
            {
                collection: collection,
                edit: this.edit,
            }
        );
        this.showChildView('linesRegion', view);
    },
    showMessages(){
        var model = new Bb.Model();
        var view = new MessageView({model: model});
        this.showChildView('messages', view);
    },
    showTotals(){
        let model = this.facade.request('get:totalmodel');
        var view = new TotalView({model: model});
        this.showChildView('totals', view);
    },
    showLogin: function(){
        var view = new LoginView({});
        this.showChildView('modalRegion', view);
    },
    onRender(){
        this.showSupplierOrderForm();
        this.showLinesRegion();
        this.showTotals();
        this.showMessages();
    },
    _showStatusModal(status, title, label, url) {
        var view = new StatusView({
            status: status,
            title: title,
            label: label,
            url: url
        });
        this.showChildView('modalRegion', view);
    },
    onStatusChange(status, title, label, url){
        if (this.edit) {
            // Prior to any status change, we want to save and make sure it went OK
            var model = this.facade.request('get:model');
            var that = this;
            model.save(
                null,
                {
                    patch: true,
                    success: function() {
                        that._showStatusModal(status, title, label, url);
                    },
                    error: function() {
                        displayServerError("Erreur pendant la sauvegarde");
                    }

                }
            );
        } else {
            this._showStatusModal(status, title, label, url);
        }
    },
});
export default MainView;
