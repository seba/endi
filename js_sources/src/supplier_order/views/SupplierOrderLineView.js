import Mn from 'backbone.marionette';
import { formatAmount } from '../../math.js';

require("jquery-ui/ui/effects/effect-highlight");

const SupplierOrderLineView = Mn.View.extend({
    tagName: 'tr',
    ui: {
        edit: 'button.edit',
        delete: 'button.delete',
        duplicate: 'button.duplicate',
    },
    triggers: {
        'click @ui.edit': 'edit',
        'click @ui.delete': 'delete',
        'click @ui.duplicate': 'duplicate',
    },
    modelEvents: {
        'change': 'render'
    },
    template: require('./templates/SupplierOrderLineView.mustache'),
    templateContext(){
        var total = this.model.total();
        return {
            edit: this.getOption('edit'),
            total: formatAmount(total),
            ht_label: formatAmount(this.model.get('ht')),
            tva_label: formatAmount(this.model.get('tva')),
        };
    }
});
export default SupplierOrderLineView;
