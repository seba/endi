import SupplierInvoiceLineModel from "./SupplierInvoiceLineModel.js";
import BaseLineCollection from '../../base/models/BaseLineCollection.js';

const SupplierInvoiceLineCollection = BaseLineCollection.extend({
    model: SupplierInvoiceLineModel,
});
export default SupplierInvoiceLineCollection;
