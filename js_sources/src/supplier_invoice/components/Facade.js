import Mn from 'backbone.marionette';
import { ajax_call } from '../../tools.js';
import { getPercent } from '../../math.js';
import TotalModel from '../models/TotalModel.js';
import SupplierInvoiceModel from '../models/SupplierInvoiceModel.js';
import SupplierInvoiceLineCollection from '../models/SupplierInvoiceLineCollection.js';


const FacadeClass = Mn.Object.extend({
    // FIXME: could take advantage of FacadeModelApiMixin
    channelName: 'facade',
    radioEvents: {
        "changed:line": "computeLineTotal",
        "changed:totals": "computeFundingTotals",
    },
    radioRequests: {
        'get:collection': 'getCollectionRequest',
        'get:totalmodel': 'getTotalModelRequest',
        'get:model': 'getModelRequest',
    },
    start(){
        console.log("Starting the facade");
        let deferred = ajax_call(this.url);
        return deferred.then(this.setupModels.bind(this));
    },
    setup(options){
        console.log("Facade.setup");
        console.table(options);
        this.url = options['context_url'];
    },
    setupModels(context_datas){
        this.datas = context_datas;
        this.models = {};
        this.collections = {};
        this.totalmodel = new TotalModel();

        var lines = context_datas['lines'];
        this.collections['lines'] = new SupplierInvoiceLineCollection(lines);

        this.supplierInvoice = new SupplierInvoiceModel(context_datas);
        this.supplierInvoice.url = AppOption['context_url'];
        this.listenTo(this.supplierInvoice, 'saved', this.reloadLines);

        this.computeLineTotal();
    },
    reloadLines(savedData){
        let savedAttrs = Object.keys(savedData);
        if (savedAttrs.includes('supplier_orders')) {
            let this_ = this;
            this.supplierInvoice.fetch().then(function(context_data) {
                let lines = context_data['lines'];
                this_.collections['lines'].set(lines);
            });
        }
    },
    computeLineTotal(){
        var collection = this.collections['lines'];

        var datas = {};
        datas['ht'] = collection.total_ht();
        datas['tva'] = collection.total_tva();
        datas['ttc'] = collection.total();
        var channel = this.getChannel();
        this.totalmodel.set(datas);
        channel.trigger('change:lines');

        // Refresh totals model
        this.computeFundingTotals();
    },

    computeFundingTotals() {
        var invoice = this.supplierInvoice;
        var datas = {};
        var ttc = this.totalmodel.get('ttc');
        // For now, this is unique to invoice
        var caePercentage = invoice.get('cae_percentage');

        datas['ttc_cae'] = getPercent(ttc, caePercentage);
        datas['ttc_worker'] = ttc - datas['ttc_cae'];
        this.totalmodel.set(datas);
    },

    getCollectionRequest(label){
        return this.collections[label];
    },
    getTotalModelRequest(){
        return this.totalmodel;
    },
    getModelRequest() {
        return this.supplierInvoice;
    },
});
const Facade = new FacadeClass();
export default Facade;
