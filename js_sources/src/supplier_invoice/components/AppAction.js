import Mn from 'backbone.marionette';

const AppActionClass = Mn.Application.extend({
  region: '#js_actions'
});
const AppAction = new AppActionClass();
export default AppAction;
