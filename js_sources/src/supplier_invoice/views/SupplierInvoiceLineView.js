import Mn from 'backbone.marionette';
import { formatAmount } from '../../math.js';
import BusinessLinkView from "../../common/views/BusinessLinkView.js";

require("jquery-ui/ui/effects/effect-highlight");

const SupplierInvoiceLineView = Mn.View.extend({
    tagName: 'tr',
    regions: {
        businessLink: {
            el: '.business-link',
        }
    },
    ui: {
        edit: 'button.edit',
        delete: 'button.delete',
        duplicate: 'button.duplicate',
    },
    triggers: {
        'click @ui.edit': 'edit',
        'click @ui.delete': 'delete',
        'click @ui.duplicate': 'duplicate',
    },
    modelEvents: {
        'change': 'render'
    },
    template: require('./templates/SupplierInvoiceLineView.mustache'),
    templateContext(){
        var total = this.model.total();
        return {
            typelabel: this.model.getType().get('label'),
            edit: this.getOption('edit'),
            total: formatAmount(total),
            ht_label: formatAmount(this.model.get('ht')),
            tva_label: formatAmount(this.model.get('tva')),
        };
    },
    onRender(){
        var view = new BusinessLinkView({
            customer_label: this.model.get('customer_label'),
            project_label: this.model.get('project_label'),
            business_label: this.model.get('business_label'),
        });
        this.showChildView('businessLink', view);
    },


});
export default SupplierInvoiceLineView;
