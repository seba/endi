import Mn from 'backbone.marionette';
import SupplierInvoiceLineView from './SupplierInvoiceLineView.js';
import SupplierInvoiceLineEmptyView from './SupplierInvoiceLineEmptyView.js';

const SupplierInvoiceLineCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'line:edit',
        'delete': 'line:delete',
        'bookmark': 'bookmark:add',
        'duplicate': 'line:duplicate',
    },
    childView: SupplierInvoiceLineView,
    emptyView: SupplierInvoiceLineEmptyView,
    emptyViewOptions(){
        return {
            colspan: 6,
            edit: this.getOption('edit'),
        };
    },
    childViewOptions(){
        return {edit: this.getOption('edit')};
    },
});
export default SupplierInvoiceLineCollectionView;
